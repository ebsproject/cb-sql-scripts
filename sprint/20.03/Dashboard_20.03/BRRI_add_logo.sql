/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-03-09
 *
 * Add organization logo in BRRI instance
 */

insert into platform.config (abbrev, name, config_value, usage, creator_id)
values (
	'ORGANIZATION_LOGO',
	'Logo image and url of Organization where instance is deployed',
	'
	{
		"logo_url": "https://b4r-assets.s3-ap-southeast-1.amazonaws.com/clients/BRRI/logo.png",
		"organization_site_url": "http://www.brri.gov.bd/",
		"description": "Bangladesh Rice Research Institute"
	}
	',
	'application',
	1
);