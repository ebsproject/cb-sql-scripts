/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-03-18
 *
 * Add Administration - Users in navigation
 */

UPDATE platform.space
SET
	menu_data = '{
		"left_menu_items": [
			{
				"name": "experiment-creation",
				"label": "Experiments",
				"appAbbrev": "EXPERIMENT_CREATION"
			},
			{
				"name": "operational-studies",
				"label": "Operational studies",
				"appAbbrev": "OPERATIONAL_STUDIES"
			},
			{
				"name": "tasks",
				"label": "Tasks",
				"appAbbrev": "TASKS"
			},
			{
				"name": "data-collection-qc",
				"items": [
					{
						"name": "data-collection-qc-quality-control",
						"label": "Quality control",
						"appAbbrev": "QUALITY_CONTROL"
					}
				],
				"label": "Data collection & QC"
			},
			{
				"name": "seeds",
				"items": [
					{
						"name": "seeds-find-seeds",
						"label": "Find seeds",
						"appAbbrev": "FIND_SEEDS"
					},
					{
						"name": "seeds-harvest-manager",
						"label": "Harvest manager",
						"appAbbrev": "HARVEST_MANAGER"
					}
				],
				"label": "Seeds"
			}
		],
		"main_menu_items": [
			{
				"icon": "folder_special",
				"name": "data-management",
				"items": [
					{
						"name": "data-management_seasons",
						"label": "Seasons",
						"appAbbrev": "MANAGE_SEASONS"
					},
					{
						"name": "data-management_products",
						"label": "Products",
						"appAbbrev": "PRODUCT_CATALOG"
					}
				],
				"label": "Data management"
			},
			{
				"icon": "settings",
				"name": "administration",
				"items": [
					{
						"name": "administration_users",
						"label": "Users",
						"appAbbrev": "USERS"
					}
				],
				"label": "Administration"
			}
		]
	}'
WHERE
	abbrev = 'ADMIN';

-- add users in applications
update platform.application set is_void = false where abbrev = 'USERS';