-- FUNCTION: operational.get_harv_lbl_nurseries_values(integer, integer, integer)

-- DROP FUNCTION operational.get_harv_lbl_nurseries_values(integer, integer, integer);

CREATE OR REPLACE FUNCTION operational.get_harv_lbl_nurseries_values(
	studyid integer,
	from_page integer,
	to_page integer,
	OUT program character varying,
	OUT plot_code character varying,
	OUT entcode character varying,
	OUT designation character varying,
	OUT study character varying,
	OUT harvest_method text,
	OUT qrcode text,
	OUT page_number bigint)
    RETURNS SETOF record 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE

  plot_rec varchar;
  result varchar; 
  start_query varchar;
  end_query varchar;
BEGIN
  /**
    * Get the plot values of the given study id for the harvest label for nursery
    * @param study id
    * @param from_plot
    * @param to_plot
    * @return query plot values 
    * @author Maria Relliza A. Pasang <m.pasang@irri.org>
    * @date 2019-06-16 2:00pm
  **/

  if(from_page is not NULL)
  then
    start_query = 'select * from ( ';
    end_query = ' ) as t where t.page_number between ' || from_page || ' and ' || to_page;
  else
    start_query = '';
    end_query = '';
  end if;

  plot_rec = start_query ||  
	'select
		pr.abbrev as program,
		p.code as plot_code,
		e.entcode as entcode,
		(case
			when value is not null and selection is not null and product.product_type = ''' || 'fixed_line' || '''
			then designation
			when value is not null and selection is not null and selection = ''' || 'B' || '''
		 	then designation || ''' || '-' || ''' || trim(selection) 
			else
				(case 
					when ((select count(product_id) from operational.seed_storage where 
							source_plot_id = p.id and source_study_id = s.id and program_id = pr.id) = 1)
					then
						(select designation from master.product where id =
							(select product_id from operational.seed_storage where source_plot_id is not null 
							and source_plot_id = p.id and source_study_id = s.id and program_id = pr.id)
						)
					else (select designation from master.product where id =
							(select product_id from operational.seed_storage where source_plot_id is not null 
							and source_plot_id = p.id and source_study_id = s.id and program_id = pr.id 
							and selection_number = selection::integer))
				end)
		end) as designation,
		s.study as study,
		(case
			when lower(trim(pm.value)) ilike ''' || '%plant%' || ''' and selection is not null and selection <> ''' || 'B' || '''
			then ''' || 'plant ' || ''' || selection
		 	when lower(trim(pm.value)) = ''' || 'panicle selection' || ''' and selection is not null
		 	then ''' || 'panicle ' ||''' || selection
		 	else ''' || '' || ''' 
		 end) as harvest_method,
		(case
			when value is not null and selection is not null and product.product_type = ''' || 'fixed_line' || '''
			then p.key::text || ''' || '-' ||''' || trim(selection) 
			when value is not null and selection is not null and product.product_type = ''' || 'progeny' || '''
		 	then p.key::text || ''' || '#' ||''' || trim(selection) 
		 	else p.key::text
		end) as qr_code,
		ROW_NUMBER () OVER (ORDER BY p.plotno) as page_number
	from operational.study s,
	   operational.entry e,
	   master.product product,
	   operational.plot p
			left join operational.plot_metadata pm 
				on p.id = pm.plot_id
				and pm.variable_id = (select id from master.variable where abbrev = ''' || 'HV_METH_DISC' || ''')
				and pm.is_void = false,
	   master.program pr
			cross join unnest(
				case 
					when lower(trim(pm.value)) = ''' || 'bulk' || ''' or lower(trim(pm.value)) = ''' || 'bulk fixedline' || '''
					then array[''' || 'B' || ''']

					when lower(trim(pm.value)) = ''' || 'single plant selection' || '''
					then array(select generate_series(1,(select cast(value as integer) from operational.plot_data 
						where variable_id = (select id from master.variable where abbrev = ''' || 'NO_OF_PLANTS' || ''')
							and value is not null and plot_id = p.id and study_id = s.id and is_void = false limit 1))::text)

					when lower(trim(pm.value)) = ''' || 'plant-specific' || '''
					then string_to_array((select value from operational.plot_data 
						where variable_id = (select id from master.variable where abbrev = ''' || 'SPECIFIC_PLANT' || ''')
							and value is not null and plot_id = p.id and study_id = s.id and is_void = false limit 1), ''' || ',' || ''')

					when lower(trim(pm.value)) = ''' || 'single plant selection and bulk' || ''' and (select value from operational.plot_data 
						where variable_id = (select id from master.variable where abbrev = ''' || 'NO_OF_PLANTS' || ''') 
							and plot_id = p.id and study_id = s.id and is_void = false limit 1) is not null
					then array(select generate_series(1,(select cast(value as integer) from operational.plot_data 
						where variable_id = (select id from master.variable where abbrev = ''' || 'NO_OF_PLANTS' || ''')
							and plot_id = p.id and study_id = s.id and is_void = false limit 1))::text) || array[ ''' || 'B' || ''']

					when lower(trim(pm.value)) = ''' || 'plant-specific and bulk' || '''
					then string_to_array(
						(select case when value is not null then value || ''' || ',B' || ''' else ''' || '' || ''' end from operational.plot_data 
						where variable_id = (select id from master.variable where abbrev =  ''' || 'SPECIFIC_PLANT' || ''')
							and plot_id = p.id and study_id = s.id and is_void = false limit 1), ''' || ',' || ''')
				
					when lower(trim(pm.value)) = ''' || 'panicle selection' || '''
					then array(select generate_series(1,(select cast(value as integer) from operational.plot_data 
						where variable_id = (select id from master.variable where abbrev = ''' || 'PANNO_SEL' || ''')
							and value is not null and plot_id = p.id and study_id = s.id and is_void = false limit 1))::text)
				end) as selection
	where 
	   s.id = '''|| studyid || '''
	   and p.study_id = s.id
	   and pm.study_id = s.id
	   and p.entry_id = e.id    
	   and pr.id = s.program_id
	   and s.is_void = false
	   and e.is_void = false
	   and p.is_void = false
	   and product.id= e.product_id
	   and pr.is_void = false
	order by p.plotno, SUBSTRING(selection FROM  ''' || '([0-9]+)' || ''')::int ASC, selection'
       || end_query;
  
       return query execute plot_rec;
END;$BODY$;

ALTER FUNCTION operational.get_harv_lbl_nurseries_values(integer, integer, integer)
    OWNER TO testb4radmin;

GRANT EXECUTE ON FUNCTION operational.get_harv_lbl_nurseries_values(integer, integer, integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION operational.get_harv_lbl_nurseries_values(integer, integer, integer) TO dev_readwrite;

GRANT EXECUTE ON FUNCTION operational.get_harv_lbl_nurseries_values(integer, integer, integer) TO testb4radmin;

COMMENT ON FUNCTION operational.get_harv_lbl_nurseries_values(integer, integer, integer)
    IS 'Function for retrieving the harvest label - nursery';
