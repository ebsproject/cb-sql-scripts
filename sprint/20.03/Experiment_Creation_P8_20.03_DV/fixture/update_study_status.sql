do $$
declare
	exp_names varchar[];
	exp_name text;
	stdy_id integer;
begin
		
--created; entry list created; plots created; field layout finalized
	
	exp_names = ARRAY['Parent List 09-03','Parent List 19-02','Parent List 1'];

	foreach exp_name in ARRAY exp_names
	loop
	
		--Get study ids from study
		select
			id
		from 
			operational.study
		where
			experiment_id
		in
			(
				select 
					id 
				from 
					operational.experiment
				where 
					experiment_name = exp_name
			)
		into 
			stdy_id;
			
		--Update study_status
		update
			operational.study
		set
			study_status = 'created; committed study',
			is_draft = false
		where
			id = stdy_id;
		
		raise notice 'UPDATED STUDY_STATUS %', stdy_id;
		
	end loop;

end;

$$

--select id, study_status, is_draft, is_void from operational.study where id in (3144,3145,3146);
		