/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Gene Romuga <g.romuga@irri.org>
 * @date 2020-03-10
 *
 * Commit generated loc reps
 */

do $$

declare
	exp_id integer;
	stdy_id integer;
	ent_id integer;
	plt_id integer;
	temp_entry_record record;
	temp_plot_record record;
	exp_names varchar[];
	exp_name text;
	
begin
	
	exp_names = ARRAY['Parent List 09-03','Parent List 19-02','Parent List 1'];

	foreach exp_name in ARRAY exp_names
	loop
		
		raise notice 'GETTING INFO %', exp_name;
		
		--Get experiment
		select id from operational.experiment where experiment_name = exp_name and is_void = false into exp_id;
		raise notice '!EXPERIMENT_ID %', exp_id;

		--Get study in the experiment
		select id from operational.study os where os.experiment_id = exp_id and is_void = false into stdy_id;
		raise notice '!STUDY_ID %', stdy_id;

		--Get temp entries
		for temp_entry_record in 
			select * from operational.temp_entry where study_id = stdy_id and is_void = false order by entno
		loop

			--Insert temp_entry to entry
			insert into 
				operational.entry (key, 
								   study_id, 
								   entno, 
								   entcode, 
								   product_id, 
								   product_gid, 
								   product_name, 
								   creator_id, 
								   notes,
								   seed_storage_log_id, 
								   product_gid_id, 
								   entry_type,
								   times_rep)
			values (
				temp_entry_record.key,
				temp_entry_record.study_id,
				temp_entry_record.entno,
				temp_entry_record.entcode,
				temp_entry_record.product_id,
				temp_entry_record.product_gid,
				temp_entry_record.product_name,
				'1',
				'Added by g.romuga 2020-03-10',
				temp_entry_record.seed_storage_log_id,
				temp_entry_record.product_gid_id,
				temp_entry_record.entry_type,
				temp_entry_record.times_rep
			) returning id into ent_id;

			raise notice '+ADDED ENTRY %', ent_id;

			for temp_plot_record in (
				select * from operational.temp_plot where study_id = stdy_id and entry_id = temp_entry_record.id and is_void = false	
			)
			loop

				insert into 
					operational.plot (key,
									  study_id,
									  entry_id,
									  rep, 
									  code,
									  plotno,
									  creator_id,
									  notes,
									  design_x,
									  design_y,
									  map_x,
									  map_y,
									  field_x,
									  field_y,
									  block_no,
									  locrep_block_id)
				values (
					temp_plot_record.key,
					temp_plot_record.study_id,
					ent_id, --new entry id
					temp_plot_record.rep,
					temp_plot_record.code,
					temp_plot_record.plotno,
					'1',
					'Added by g.romuga 2020-03-10',
					temp_plot_record.design_x,
					temp_plot_record.design_y,
					temp_plot_record.map_x,
					temp_plot_record.map_y,
					temp_plot_record.field_x,
					temp_plot_record.field_y,
					temp_plot_record.block_no,
					temp_plot_record.locrep_block_id
				) returning id into plt_id;

			raise notice '...+ADDED PLOT %', plt_id;

			end loop;

		end loop;
		
	end loop;
	

end;

$$;

--select * from operational.entry where creation_timestamp between '2020-03-10 00:00:00' and  '2020-03-11 00:00:00' order by id desc;
--select * from operational.plot where creation_timestamp between '2020-03-10 00:00:00' and  '2020-03-11 00:00:00' order by id desc;
