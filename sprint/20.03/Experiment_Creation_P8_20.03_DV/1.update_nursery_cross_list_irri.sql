update platform.config set config_value = '{
	"Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
	"Values": [{
			"fixed": true,
			"default": false,
			"required": "required",
			"field_label": "Parent Role",
			"order_number": 1,
			"variable_abbrev": "PARENT_TYPE",
			"field_description": "Parent Role"
		},
		{
			"disabled": false,
			"field_label": "Remarks",
			"order_number": 2,
			"variable_abbrev": "REMARKS",
			"field_description": "Entry Remarks"
		}
	]
}' where abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL';
