
alter table platform.list_member add column is_active boolean DEFAULT true;

update
	platform.list_member
set
	is_active = FALSE
where
	is_void = true
;