/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Gene Romuga <g.romuga@irri.org>
 * @date 2020-03-29
 *
 * ACL: Populate studies and program team members in fixture db for testing
 */
 
do $$
declare
	record_study record;
	record_team_member record;
	record_study_metadata record;
	record_item_service_team record;
	var_team_id int;
	var_program_id int;
	var_item_service_team_program_id int;
	var_member_id int;
	var_study_id int;
	var_creator_id int;
	count_team_member int;
	var_var_id_process_path int;
	var_process_path_val int;
	var_role_id_data_owner int;
	var_role_id_data_producer int;
	child_json text;
	child_json_item text;
	combined_json text;
	combined_json_item text;
	new_combined_json text;
	access_data_json jsonb;
	val_text text;
	val_text_item text;
begin

	--get role id of data owner
	select id from master.role where abbrev = 'DATA_OWNER' into var_role_id_data_owner;
	raise notice '!DATA_OWNER ID %', var_role_id_data_owner; 
	
	--get role id of data producer
	select id from master.role where abbrev = 'DATA_PRODUCER' into var_role_id_data_producer;
	raise notice '!DATA_PRODUCER ID %', var_role_id_data_producer; 
	
	--get variable id of PROCESS_PATH_ID
	select id from master.variable where abbrev = 'PROCESS_PATH_ID' into var_var_id_process_path;
	raise notice '!PROCESS_PATH_ID ID %', var_var_id_process_path; 

	for record_study in (select * from operational.study) --where id in (3571))
	loop
	
		--get study id
		var_study_id = record_study.id;
		raise notice 'STUDY_ID %', var_study_id; 

		--get program id of a study
		var_program_id = record_study.program_id;
		raise notice '!PROGRAM_ID %', var_program_id; 

		--get team id
		select team_id INTO var_team_id from master.program_team where program_id in (var_program_id) AND is_void = false;
		raise notice '!TEAM_ID %', var_team_id;
		
		--get creator id
		var_creator_id = record_study.creator_id;
		raise notice 'CREATOR_ID %', var_creator_id;
		
		select count(1) from master.team_member where team_id in (var_team_id) into count_team_member;
		if count_team_member > 0 then
			--add default creator id of the study
			child_json = '"'||'1'||'": {"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_owner||'},';
			val_text = concat(child_json);
		else
			child_json = '"'||'1'||'": {"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_owner||'}';
			val_text = concat(child_json);
		end if;
		
		--get team members
		for record_team_member in (select * from master.team_member where team_id in (var_team_id) and member_id not in (var_creator_id) order by member_id)
		loop
			var_member_id = record_team_member.member_id;
			raise notice '!MEMBER ID %', var_member_id;
			
			child_json = '"'||var_member_id||'": {"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_producer||'}';
			val_text = concat(val_text, child_json, ',');
		end loop;

		combined_json = '"user":{'||val_text||'}';
		select replace(combined_json, ',}', '}') into combined_json;
		
		access_data_json = '{'||combined_json||', "program":{"'||var_program_id||'":{"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_producer||'}}}';
		raise notice '+REGULAR STUDY UPDATE %', access_data_json;
			
		for record_study_metadata in (select * from operational.study where id = var_study_id AND is_void = false)
		loop
			--get process path value
			var_process_path_val = record_study_metadata.process_path_id;
			raise notice '!PROCESS_PATH ID %', var_process_path_val;

			for record_item_service_team in (select * from master.item_service_team where item_id = var_process_path_val)
			loop
				var_item_service_team_program_id = record_item_service_team.program_id;
				raise notice '!ITEM_SERVICE_TEAM_PROGRAM_ID %', var_item_service_team_program_id;

				if var_item_service_team_program_id != var_program_id then
					raise notice '!ITEM_SERVICE_TEAM_PROGRAM_ID % PROGRAM_ID % STUDY_ID %', var_item_service_team_program_id,var_program_id, var_study_id;

					child_json_item = '"'||var_program_id||'": {"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_producer||'},'
									  '"'||var_item_service_team_program_id||'": {"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_owner||'}';

					val_text_item = concat(val_text_item, child_json_item);
				end if;

				--combine programs
				combined_json_item = '"program":{'||val_text_item||'}';
				--raise notice 'TEST %',combined_json;
				
				--original
				--access_data_json = '{'||combined_json||','||combined_json_item||'}';
				
				--new
				--replace id 1 by actual creator id
				select replace(combined_json,':{"1"', ':{"'||var_creator_id||'"') into new_combined_json;
				access_data_json = '{'||new_combined_json||','||combined_json_item||'}';
				
				raise notice '+SERVICE REQUEST UPDATE %', access_data_json;

				--reset strings
				val_text_item = '';
				val_text = '';
				
			end loop;
			 	
		end loop;
		
		--construct update
		UPDATE 
			operational.study 
		SET 
			access_data = access_data_json
		WHERE 
			id = record_study.id;
	end loop;

end $$;