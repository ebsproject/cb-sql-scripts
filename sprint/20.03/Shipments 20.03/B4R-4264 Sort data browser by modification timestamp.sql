﻿DROP TRIGGER shipment_event_log_tgr ON seed_warehouse.shipment;

with t as (
select 
    id,
    creation_timestamp as ct
from
seed_warehouse.shipment 
where
    modification_timestamp is null
)
update seed_warehouse.shipment st
set 
    modification_timestamp=t.ct
from 
    t
where
    t.id=st.id;
 
CREATE TRIGGER shipment_event_log_tgr
  BEFORE INSERT OR UPDATE
  ON seed_warehouse.shipment
  FOR EACH ROW
  EXECUTE PROCEDURE z_admin.log_record_event();
 