-- Function: seed_warehouse.validate_seedlot(integer)

-- DROP FUNCTION seed_warehouse.validate_seedlot(integer);

CREATE OR REPLACE FUNCTION seed_warehouse.validate_seedlot(shipment_id integer)
  RETURNS character varying AS
$BODY$DECLARE
	r_var RECORD;

BEGIN

     FOR r_var IN EXECUTE format('SELECT * FROM temporary_data.shipment_il_%s', shipment_id)
LOOP	
    execute '
    UPDATE temporary_data.shipment_il_'||shipment_id||' t
    set 
        gid_status=''invalid''
    from
    (
        select name_type,gid 
        from
            z_admin.get_all_product_names(in_product :='''||r_var.designation||''')
        where name_type in(''NEW_NAME'',''VOID_NAME'')
    )a
    where t.gid=a.gid and t.gid_status is not null';
    raise notice '
    UPDATE temporary_data.shipment_il_% t
    set 
        gid_status=''invalid''
    from
    (
        select name_type,gid 
        from
            z_admin.get_all_product_names(in_product :=''%'')
        where name_type in(''NEW_NAME'',''VOID_NAME'')
    )a
    where t.gid=a.gid and t.gid_status is not null',shipment_id,r_var.designation;

END LOOP;
    
    
  RETURN 'success';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
