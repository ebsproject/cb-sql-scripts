-- DO NOT RUN IN FIXTURE
-- B4RSUPPORT-2185: Find Seeds > Database Exception encountered

-- get program_id
select * from master.program where abbrev = 'RICE'; -- program_id: 160

-- get team_id of program
select * from master.program_team where program_id = 160; -- team_id: 50

-- get user_id
select * from master.user where username = 'a.flores'; -- user_id: 4

-- get role_id
select * from master.role where abbrev = 'ADMIN'; -- role_id: 1

-- insert user as a member of a team with assigned role
insert into
	master.team_member (
		team_id,
		member_id,
		role_id
	)
values (
		50,
		4,
		1
	)
;



-- l.gallardo

-- get user_id
select * from master.user where username = 'l.gallardo'; -- user_id: 17

-- get role_id
select * from master.role where abbrev = 'ETM'; -- role_id: 16

-- insert user as a member of a team with assigned role
insert into
	master.team_member (
		team_id,
		member_id,
		role_id
	)
values (
		50, -- team_id: RICE
		17, -- user_id
		16 -- role_id
	)
;




-- m.karkkainen

-- get user_id
select * from master.user where username = 'm.karkkainen'; -- user_id: 20

-- get role_id
select * from master.role where abbrev = 'ETM'; -- role_id: 16

-- insert user as a member of a team with assigned role
insert into
	master.team_member (
		team_id,
		member_id,
		role_id
	)
values (
		50, -- team_id: RICE
		20, -- user_id
		16 -- role_id
	)
;
