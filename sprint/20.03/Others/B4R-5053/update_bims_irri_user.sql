-- B4R-5053 Change user type of 'bims.irri' user to admin

update
    master.user mu
set
    user_type = 1
where
    mu.username = 'bims.irri'
    and user_type <> 1
;
