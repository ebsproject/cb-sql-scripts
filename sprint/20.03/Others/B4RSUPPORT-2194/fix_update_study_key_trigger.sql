-- FUNCTION: operational.update_study_key_records()

-- DROP FUNCTION operational.update_study_key_records();

CREATE OR REPLACE FUNCTION operational.update_study_key_records()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
declare
    study_key varchar;
	offset_number integer;
begin
    -- this event function must be ran after the update event
    if (TG_WHEN <> 'AFTER' or TG_OP <> 'UPDATE') then
        raise exception 'operational.update_study_key_records() may only run as an AFTER trigger on an UPDATE event';
    end if;
    
    if (
        new.program_id <> old.program_id
        or new.place_id <> old.place_id
        or new.phase_id <> old.phase_id
        or new.year <> old.year
        or new.season_id <> old.season_id
        or new.number <> old.number
    ) then
        -- set as new study_key
        study_key = new.key;
		
		-- for studies exceeding the normal study key length
		offset_number = length(study_key) - 19;

        -- update entry_key values of entries
        update
            operational.entry oe
        set
            key = (study_key || (oe.entno + 10000)::varchar)::numeric
        where
            oe.study_id = new.id
            and oe.is_void = false
        ;

        -- update plot_key values of plot records
        update
            operational.plot op
        set
            key = (study_key || (oe.entno + 10000)::varchar || (op.rep + 10)::varchar)::numeric
		from
			operational.entry oe
        where
            op.study_id = new.id
			and op.entry_id = oe.id
			and oe.is_void = false
            and op.is_void = false
        ;

        -- update entry_key values of entries
        update
            operational.temp_entry oe
        set
            key = (study_key || (oe.entno + 10000)::varchar)::numeric
        where
            oe.study_id = new.id
            and oe.is_void = false
        ;

        -- update plot_key values of plot records
        update
            operational.temp_plot op
        set
            key = (study_key || (oe.entno + 10000)::varchar || (op.rep + 10)::varchar)::numeric
        from
			operational.temp_entry oe
        where
            op.study_id = new.id
			and op.entry_id = oe.id
			and oe.is_void = false
            and op.is_void = false
        ;
    end if;
    
    -- return updated record
    return old;
end; $BODY$;

