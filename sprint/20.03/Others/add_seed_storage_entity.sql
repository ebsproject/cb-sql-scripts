insert into
	dictionary.entity (
		abbrev,
		name,
		creator_id,
		notes,
		table_id
	)
	select
		'SEED_STORAGE',
		'seed_storage',
		6,
		'CM-5210 populate table_id - a.flores 2018-08-23',
		(
			select
				t.id
			from
				dictionary.table t
				join dictionary.schema s
					on t.schema_id = s.id
			where
				s.abbrev = 'OPERATIONAL'
				and t.abbrev = 'SEED_STORAGE'
				and s.is_void = false
				and t.is_void = false
		)
;
