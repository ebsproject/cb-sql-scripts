-- B4R-5156: Unvoid IRRI experiment templates in release.b4rdev.org

update master.item set is_void = false where abbrev ilike '%IRRI%' and type = 40 and process_type = 'experiment_creation_data_process' and is_void = true;

update master.item set is_void = false where id in (
	select child_id from master.item_relation where root_id in (select id from master.item where type = 40 and is_void = false and process_type = 'experiment_creation_data_process')
) and is_void = true;
