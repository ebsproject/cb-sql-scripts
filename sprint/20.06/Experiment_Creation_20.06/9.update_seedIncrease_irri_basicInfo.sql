--"EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL"
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for seed increase data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
      "secondary_target_column":"itemDbId",
      "target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "crops",
			"api_resource_sort": "sort=cropCode",
		  "variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   		"target_value":"programCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "programs",
			"api_resource_sort": "sort=programCode",
			"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},	
    {
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},	
    {
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
      "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
			"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
	
		{
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "BRE",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"BRE",
				"SEM"
			],
			"target_column": "stageDbId",
      "target_value":"stageCode",
		  "api_resource_method": "POST",
		  "api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
			"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
		{
			"disabled": false,
			"required": "required",		
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
      "target_value":"seasonCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "seasons",
			"api_resource_sort": "sort=seasonCode",
		  "variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
      "disabled": false,
      "required": "required",
      "variable_abbrev": "PLANTING_SEASON"
    },
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "persons",
			"api_resource_sort": "sort=personName",
		  "variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
    {
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
      "target_value": "projectCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "projects",
			"api_resource_sort": "sort=projectCode",
		  "variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
    {
			"disabled": false,
		  "variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL';
