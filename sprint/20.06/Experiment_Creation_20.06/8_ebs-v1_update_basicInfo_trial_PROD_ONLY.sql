--KINDLY RUN THIS IN PROD [PROD ONLY]---
--EXPT_TRIAL_BASIC_INFO_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required experiment level metadata variables for trial data process",
    "Values": [
        {
            "default": false,
            "disabled": true,
            "required": "required",
            "field_label": "Experiment Template",
            "order_number": 1,
            "variable_abbrev": "EXPERIMENT_TEMPLATE",
            "field_description": "Experiment Template"
        },
        {
            "default": "Trial",
            "disabled": true,
            "required": "required",
            "field_label": "Experiment Type",
            "order_number": 2,
            "variable_abbrev": "EXPERIMENT_TYPE",
            "field_description": "Experiment Type"
        },
        {
            "default": "RICE",
            "disabled": true,
            "required": "required",
            "field_label": "Crop",
            "order_number": 3,
            "variable_abbrev": "CROP",
            "field_description": "Program Crop"
        },
        {
            "disabled": true,
            "required": "required",
            "field_label": "Program",
            "order_number": 4,
            "variable_abbrev": "PROGRAM",
            "field_description": "Program"
        },
        {
            "disabled": false,
            "field_label": "Project",
            "order_number": 5,
            "allow_new_val": true,
            "variable_abbrev": "PROJECT",
            "field_description": "Project where the experiment is under"
        },
        {
            "disabled": true,
            "required": "required",
            "field_label": "Experiment Code",
            "order_number": 6,
            "variable_abbrev": "EXPERIMENT_CODE",
            "field_description": "Auto-generated code for the experiment"
        },
        {
            "disabled": false,
            "required": "required",
            "field_label": "Experiment Name",
            "order_number": 7,
            "variable_abbrev": "EXPERIMENT_NAME",
            "field_description": "User inputted name for the experiment"
        },
        {
            "disabled": false,
            "required": "required",
            "field_label": "Experiment sub-type",
            "order_number": 8,
            "allowed_values": [
                "IYT",
                "OYT",
                "PYT",
                "AYT",
                "MET0",
                "MET1",
                "MET2",
                "RYT",
                "AGR"
            ],
            "variable_abbrev": "PHASE",
            "field_description": "Experiment Sub-type/Phase of the experiment"
        },
        {
            "disabled": false,
            "field_label": "Experiment Sub Sub-type",
            "order_number": 9,
            "allowed_values": [
                "First Year",
                "Second Year",
                "Re Test",
                "Germplasm Viability"
            ],
            "variable_abbrev": "EXPERIMENT_SUB_SUBTYPE",
            "field_description": "Experiment Sub Sub-type/Objective of the experiment"
        },
        {
            "disabled": false,
            "required": "required",
            "field_label": "Evaluation Year",
            "order_number": 10,
            "variable_abbrev": "YEAR",
            "field_description": "Evaluation Year"
        },
        {
            "disabled": false,
            "required": "required",
            "field_label": "Evaluation Season",
            "order_number": 11,
            "variable_abbrev": "SEASON",
            "field_description": "Evaluation Season"
        },
        {
            "disabled": false,
            "required": "required",
            "field_label": "Experiment Steward",
            "order_number": 12,
            "variable_abbrev": "EXPERIMENT_STEWARD",
            "field_description": "Steward of the experiment"
        },
        {
            "disabled": false,
            "field_label": "Remarks",
            "order_number": 13,
            "variable_abbrev": "REMARKS",
            "field_description": "Experiment Comment"
        },
        {
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Has Experiment group?",
            "order_number": 14,
            "variable_abbrev": "HAS_EXPERIMENT_GROUP",
            "field_description": "Experiment group/s exist/s in the experiment"
        }
    ]
}
'  where abbrev='EXPT_TRIAL_BASIC_INFO_ACT_VAL';