
--update the display
update master.item set (name, description, display_name) = ('Specify Site', 'Specify Site', 'Site') where abbrev in (
'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT',
'EXPT_NURSERY_CB_PLACE_ACT',
'EXPT_DEFAULT_PLACE_ACT',
'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT',
'EXPT_TRIAL_PLACE_ACT',
'EXPT_NURSERY_PARENT_LIST_PLACE_ACT',
'EXPT_NURSERY_CROSS_LIST_PLACE_ACT',
'EXPT_CROSS_PRE_PLANNING_PLACE_ACT',
'EXPT_SEED_INCREASE_PLACE_ACT',
'EXPT_CROSS_POST_PLANNING_PLACE_ACT',
'EXPT_TRIAL_IRRI_PLACE_ACT',
'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT',
'EXPT_SEED_INCREASE_IRRI_PLACE_ACT');

--update the status
update platform.module set (name, description,action_id,required_status) = ('Specify Occurrences','Specify Occurrences','specify-occurrences','occurrences created') where abbrev in ('EXPT_NURSERY_CB_PLACE_ACT_MOD',
'EXPT_DEFAULT_PLACE_ACT_MOD',
'EXPT_TRIAL_PLACE_ACT_MOD',
'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_MOD',
'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_MOD',
'EXPT_CROSS_PRE_PLANNING_PLACE_ACT_MOD',
'EXPT_SEED_INCREASE_PLACE_ACT_MOD',
'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_MOD',
'EXPT_CROSS_POST_PLANNING_PLACE_ACT_MOD',
'EXPT_TRIAL_IRRI_PLACE_ACT_MOD',
'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_MOD',
'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD',
'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_MOD');

