--unvoid IRRI templates
update master.item set is_void = false where abbrev in ('EXPT_SEED_INCREASE_IRRI_DATA_PROCESS','EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS') and type = 40 and process_type = 'experiment_creation_data_process' and is_void = true;

update master.item set is_void = false where id in (
	select child_id from master.item_relation where root_id in (select id from master.item where type = 40 and is_void = false and process_type = 'experiment_creation_data_process')
) and is_void = true;

---Update Seed Increase for IRRI template's children
update master.item set is_void = false where abbrev ilike '%EXPT_SEED_INCREASE_IRRI%';

---Update Selection and Advancement for IRRI template's children
update master.item set is_void = false where abbrev ilike '%EXPT_SELECTION_ADVANCEMENT_IRRI%';

---Add the seed increase for IRRI template's children to item relation table
insert into master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select 
  (select id from master.item where abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as root_id,
  (select id from master.item where abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as parent_id,
   id as child_id,
   case 
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT' then 1
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT' then 2
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT' then 3
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT' then 4
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_PLACE_ACT' then 5
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT' then 6
     else 7 end as order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
from 
   master.item
where 
   abbrev in ('EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT','EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT','EXPT_SEED_INCREASE_IRRI_DESIGN_ACT','EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_PLACE_ACT','EXPT_SEED_INCREASE_IRRI_REVIEW_ACT');



--UPDATE THE BASIC INFO OF SEED INCREASE FOR IRRI TEMPLATE
--"EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL"
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for seed increase data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
      "secondary_target_column":"itemDbId",
      "target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "crops",
			"api_resource_sort": "sort=cropCode",
		  "variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   		"target_value":"programCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "programs",
			"api_resource_sort": "sort=programCode",
			"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},	
    {
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},	
    {
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
      "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
			"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "BRE",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"BRE",
				"SEM"
			],
			"target_column": "stageDbId",
      "target_value":"stageCode",
		  "api_resource_method": "POST",
		  "api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
			"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
		{
			"disabled": false,
			"required": "required",		
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
      "target_value":"seasonCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "seasons",
			"api_resource_sort": "sort=seasonCode",
		  "variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
      "disabled": false,
      "required": "required",
      "variable_abbrev": "PLANTING_SEASON"
    },
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "persons",
			"api_resource_sort": "sort=personName",
		  "variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
    {
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
      "target_value": "projectCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "projects",
			"api_resource_sort": "sort=projectCode",
		  "variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
    {
			"disabled": false,
		  "variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL';


--Add selection and advancement for IRRI template's children to item relation table
insert into master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select 
  (select id from master.item where abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS') as root_id,
  (select id from master.item where abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS') as parent_id,
   id as child_id,
   case 
     when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT' then 1
     when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT' then 2
     when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT' then 3
     when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT' then 4
     when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT' then 5
     when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_REVIEW_ACT' then 6
     else 7 end as order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
from 
   master.item
where 
   abbrev in ('EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_REVIEW_ACT');

--Update the platform.modules for Seed Increase IRRI
update platform.module set is_void = false where abbrev ilike '%EXPT_SEED_INCREASE_IRRI%';

--unvoid item module record of the steps
update platform.item_module set is_void = false where item_id in (1459,1460,1461,1466,1470,1471) and module_id in (258,259,260,265,269,270);

--unvoid the item module for planting protocols
update platform.item_module set is_void = false where  item_id in (1467, 1468,1469) and module_id in (266,267,268);

--insert  in master.item_relation the children of protocols
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT' then 1
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT'then 2
    when abbrev = 'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT'then 3
		else 4 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT');
;

--insert  in master.item_relation the children of planting arrangement
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT' then 1
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT'then 2
      when abbrev = 'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT'then 3
      when abbrev = 'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT'then 4
		else 5 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT','EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT','EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT','EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT');
;

--unvoid the planting arrangement children in the platform.item_module 
update platform.item_module set is_void = false where item_id in (1462,1463,1464,1465) and module_id in (261,262,263,264);


--SELECTION AND ADVANCEMENT

--add the planting arrangement children to item relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ADD_BLOCKS_ACT' then 1
		when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ASSIGN_ENTRIES_ACT'then 2
      when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_MANAGE_BLOCKS_ACT'then 3
      when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_OVERVIEW_ACT'then 4
		else 5 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SELECTION_ADVANCEMENT_IRRI_ADD_BLOCKS_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_ASSIGN_ENTRIES_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_MANAGE_BLOCKS_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_OVERVIEW_ACT');
;

--Add the children of the protocols activity
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PLANTING_PROTOCOLS_ACT' then 1
		when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_TRAITS_PROTOCOLS_ACT'then 2
        when abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT'then 3
		else 3 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SELECTION_ADVANCEMENT_IRRI_PLANTING_PROTOCOLS_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_TRAITS_PROTOCOLS_ACT','EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT');
;


--Update the platform.modules for Selection and Advancement IRRI
update platform.module set is_void = false where abbrev ilike '%EXPT_SELECTION_ADVANCEMENT_IRRI%';


--unvoid the selection and advancement steps in the platform item module
update platform.item_module set is_void = false where item_id in (1484,1485,1473,1474,1475,1480) and module_id in (282,283,271,272,273,278);

--unvoid the planting arrangement children in the item module table
update platform.item_module set is_void = false where item_id in (1476,1477,1478,1479) and module_id in (274,275,276,277);

--unvoid protocol's children in the item module table
update platform.item_module set is_void = false where item_id in (1481,1482,1483) and module_id in (279,280,281);
