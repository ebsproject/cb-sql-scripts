/*
"EXPT_NURSERY_CB_PLACE_ACT_VAL"
"EXPT_NURSERY_CROSS_LIST_PLACE_ACT_VAL"
"EXPT_TRIAL_PLACE_ACT_VAL"
"EXPT_NURSERY_PARENT_LIST_PLACE_ACT_VAL"
"EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_VAL"
"EXPT_CROSS_PRE_PLANNING_PLACE_ACT_VAL"
"EXPT_SEED_INCREASE_PLACE_ACT_VAL"
"EXPT_CROSS_POST_PLANNING_PLACE_ACT_VAL"
"EXPT_TRIAL_IRRI_PLACE_ACT_VAL"
"EXPT_SEED_INCREASE_IRRI_PLACE_ACT_VAL"
"EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_VAL"
"EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_VAL"
*/

--EXPT_NURSERY_CB_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default place metadata variables for nursery crossing block data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_NURSERY_CB_PLACE_ACT_VAL';

--EXPT_NURSERY_CROSS_LIST_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for nursery parent list data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_VAL';


--EXPT_TRIAL_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for trial data process",
   "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_TRIAL_PLACE_ACT_VAL';


--EXPT_NURSERY_PARENT_LIST_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for nursery parent list data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_VAL';


--EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for Selection and Advancement data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_VAL';



--EXPT_CROSS_PRE_PLANNING_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for cross pre-planning data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_CROSS_PRE_PLANNING_PLACE_ACT_VAL';



--EXPT_SEED_INCREASE_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for seed increase data process",
   "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_SEED_INCREASE_PLACE_ACT_VAL';


--EXPT_CROSS_POST_PLANNING_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for cross post-planning data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_CROSS_POST_PLANNING_PLACE_ACT_VAL';


update platform.config set config_value = '
{
	"Name": "Required and default entry level metadata variables for IRRI Trial data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_TRIAL_IRRI_PLACE_ACT_VAL';


--EXPT_SEED_INCREASE_IRRI_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for seed increase data process",
  "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_VAL';



--EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for IRRI Selection and Advancement data process",
   "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME",
	  
      "order_number": "1"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_VAL';


--EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_VAL
update platform.config set config_value = '
{
  "Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
   "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "disabled": true,
      "default": "IRRIHQ",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
}
' where abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_VAL';