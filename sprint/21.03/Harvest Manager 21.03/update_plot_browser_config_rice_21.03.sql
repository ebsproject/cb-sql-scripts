/**
 *  Delete plot browser rice configuration record if it already exists
 */
DELETE FROM platform.config WHERE abbrev='HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE';

/** 
 *  Insert record for plot browser rice configuration
 *  "methods" refers to the master.scale_value table, "value" column 
 */
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE',
        'Harvest Manager Plot Browser Config for Rice',
$$			
		
{
    "name": "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE",
    "values": [
        {
            "germplasm_states": {
                "fixed": {
                    "methods": [
                        "Bulk"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "methods": [
                        "Bulk",
                        "Panicle Selection",
                        "Single Plant Selection",
                        "Single Plant Selection and Bulk",
                        "plant-specific",
                        "Plant-Specific and Bulk",
                        "Single Seed Descent"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                }
            }
        }
    ]
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-53 update config - j.bantay 2021-02-04');
