DELETE FROM platform.config WHERE abbrev='HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT',
        'Harvest Manager Plot Browser Config for Wheat',
$$			
		
{
    "name": "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT",
    "values": [
        {
            "germplasm_states": {
                "fixed": {
                    "methods": [
                        "Bulk"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "methods": [
                        "Bulk",
                        "Selected bulk",
                        "Single Plant Selection",
                        "Individual spike"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                }
            }
        }
    ]
}
$$,
        1,
        'harvest_manager',
        1,
        'update config - j.bantay 2021-03-08');
