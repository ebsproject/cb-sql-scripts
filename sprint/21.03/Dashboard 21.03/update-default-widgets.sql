-- update default dashboard widgets
update 
	platform.config
set
	config_value = '{"col_1": [{"widget_id": 1}, {"widget_id": 4}], "col_2": [{"widget_id": 9}]}'
where
	abbrev = 'DEFAULT_DASHBOARD_WIDGETS';