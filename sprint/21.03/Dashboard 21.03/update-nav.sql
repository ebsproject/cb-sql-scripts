-- update default space
update 
	platform.space
set 
	menu_data = '{
	"left_menu_items": [{
		"name": "experiment-creation",
		"label": "Experiment creation",
		"appAbbrev": "EXPERIMENT_CREATION"
	}, {
		"name": "experiment-manager",
		"label": "Experiment manager",
		"appAbbrev": "OCCURRENCES"
	}, {
		"name": "data-collection-qc-quality-control",
		"label": "Data collection",
		"appAbbrev": "QUALITY_CONTROL"
	}, {
		"name": "seeds-harvest-manager",
		"label": "Harvest manager",
		"appAbbrev": "HARVEST_MANAGER"
	}, {
		"name": "searchs-germplasm",
		"label": "Germplasm",
		"appAbbrev": "GERMPLASM_CATALOG"
	}, {
		"name": "search-seeds",
		"label": "Seeds",
		"appAbbrev": "FIND_SEEDS"
	}, {
		"name": "search-traits",
		"label": "Traits",
		"appAbbrev": "TRAITS"
	}],
	"main_menu_items": [{
		"icon": "help_outline",
		"name": "help",
		"items": [{
			"url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
			"icon": "headset_mic",
			"name": "help_support-portal",
			"label": "Support portal",
			"tooltip": "Go to EBS Service Desk portal"
		}, {
			"url": "https://riceinfo.atlassian.net/wiki/spaces/ABOUT/pages/326172737/Breeding4Results+B4R",
			"icon": "info_outline",
			"name": "help_wiki",
			"label": "Wiki",
			"tooltip": "Go to B4R Wiki site"
		}, {
			"url": "https://uat-b4rapi.b4rdev.org",
			"icon": "code",
			"name": "help_api",
			"label": "API",
			"tooltip": "Go to B4R API Documentation"
		}],
		"label": "Help"
	}]
}'
where
	abbrev = 'DEFAULT';

-- update admin space
update 
	platform.space
set 
	menu_data = '
	{
		"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiment creation",
			"appAbbrev": "EXPERIMENT_CREATION"
		}, {
			"name": "experiment-manager",
			"label": "Experiment manager",
			"appAbbrev": "OCCURRENCES"
		}, {
			"name": "data-collection-qc-quality-control",
			"label": "Data collection",
			"appAbbrev": "QUALITY_CONTROL"
		}, {
			"name": "seeds-harvest-manager",
			"label": "Harvest manager",
			"appAbbrev": "HARVEST_MANAGER"
		}, {
			"name": "searchs-germplasm",
			"label": "Germplasm",
			"appAbbrev": "GERMPLASM_CATALOG"
		}, {
			"name": "search-seeds",
			"label": "Seeds",
			"appAbbrev": "FIND_SEEDS"
		}, {
			"name": "search-traits",
			"label": "Traits",
			"appAbbrev": "TRAITS"
		}],
		"main_menu_items": [{
			"icon": "help_outline",
			"name": "help",
			"items": [{
				"url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
				"icon": "headset_mic",
				"name": "help_support-portal",
				"label": "Support portal",
				"tooltip": "Go to EBS Service Desk portal"
			}, {
				"url": "https://riceinfo.atlassian.net/wiki/spaces/ABOUT/pages/326172737/Breeding4Results+B4R",
				"icon": "info_outline",
				"name": "help_wiki",
				"label": "Wiki",
				"tooltip": "Go to B4R Wiki site"
			}, {
				"url": "https://uat-b4rapi.b4rdev.org",
				"icon": "code",
				"name": "help_api",
				"label": "API",
				"tooltip": "Go to B4R API Documentation"
			}],
			"label": "Help"
		}, {
			"icon": "folder_special",
			"name": "data-management",
			"items": [{
				"name": "data-management_seasons",
				"label": "Seasons",
				"appAbbrev": "MANAGE_SEASONS"
			}],
			"label": "Data management"
		}, {
			"icon": "settings",
			"name": "administration",
			"items": [{
				"name": "administration_users",
				"label": "Persons",
				"appAbbrev": "PERSONS"
			}],
			"label": "Administration"
		}]
	}'
where
	abbrev = 'ADMIN';