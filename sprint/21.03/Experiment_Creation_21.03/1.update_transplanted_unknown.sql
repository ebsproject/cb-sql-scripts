/*
CORB-693: Remove the required attributes for transplanted unknown plot type
*/
update platform.config set config_value = '{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": false,
			"disabled": false,
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"default": false,
			"disabled": false,
			"variable_abbrev": "HILLS_PER_ROW_CONT"
		},
		{
			"unit": "cm",
			"default": false,
			"disabled": false,
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "cm",
			"default": false,
			"disabled": false,
			"variable_abbrev": "DIST_BET_HILLS"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"variable_abbrev": "PLOT_LN_1"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"variable_abbrev": "PLOT_WIDTH_RICE"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"variable_abbrev": "PLOT_AREA_1"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
		{
			"default": false,
			"disabled": false,
			"variable_abbrev": "PLANTING_INSTRUCTIONS"
		}
	]
}' where abbrev = 'PLOT_TYPE_TRANSPLANTED_UNKNOWN';