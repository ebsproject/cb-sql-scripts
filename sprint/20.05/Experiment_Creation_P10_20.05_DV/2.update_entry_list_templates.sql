/*
"EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL"
"EXPT_NURSERY_CROSS_LIST_ENTRY_LIST_ACT_VAL"
"EXPT_TRIAL_ENTRY_LIST_ACT_VAL"
"EXPT_NURSERY_PARENT_LIST_ENTRY_LIST_ACT_VAL"
"EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT_VAL"
"EXPT_CROSS_PRE_PLANNING_ENTRY_LIST_ACT_VAL"
"EXPT_SEED_INCREASE_ENTRY_LIST_ACT_VAL"
"EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL"
"EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_VAL"
"EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_VAL"
"EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_VAL"
"EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL"
*/

--EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default place metadata variables for nursery crossing block data process",
    "Values": [{
            "fixed": true,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "ENTRY_CLASS",
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
        	"api_resource_endpoint": "entries"
            
        }
    ]
}
' where abbrev = 'EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL';

--EXPT_NURSERY_CROSS_LIST_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for nursery parent list data process",
    "Values": [{
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
        	"api_resource_endpoint": "entries"
        }
    ]
}
' where abbrev = 'EXPT_NURSERY_CROSS_LIST_ENTRY_LIST_ACT_VAL';


--EXPT_TRIAL_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for trial data process",
    "Values": [{
            "default": "entry",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_TYPE",
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "ENTRY_CLASS",
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "api_resource_endpoint": "entries"
        }
    ]
}
' where abbrev = 'EXPT_TRIAL_ENTRY_LIST_ACT_VAL';


--EXPT_NURSERY_PARENT_LIST_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for nursery parent list data process",
    "Values": [{
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
        	"api_resource_endpoint": "entries"
        }
    ]
}
' where abbrev = 'EXPT_NURSERY_PARENT_LIST_ENTRY_LIST_ACT_VAL';


--EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for Selection and Advancement data process",
    "Values": [
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
        	"api_resource_endpoint": "entries"
        }
    ]
}
' where abbrev = 'EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT_VAL';



--EXPT_CROSS_PRE_PLANNING_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for cross pre-planning data process",
    "Values": [
        {
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "required": "required",
            "variable_abbrev": "HYBRID_ROLE",
            "api_resource_endpoint": "entry-data"    
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "api_resource_endpoint": "entries"
        }
    ]
}
' where abbrev = 'EXPT_CROSS_PRE_PLANNING_ENTRY_LIST_ACT_VAL';



--EXPT_SEED_INCREASE_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for seed increase data process",
    "Values": [{
        "disabled": false,
        "variable_abbrev": "DESCRIPTION",
        "api_resource_endpoint": "entries"
    }]
}
' where abbrev = 'EXPT_SEED_INCREASE_ENTRY_LIST_ACT_VAL';


--EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for cross post-planning data process",
    "Values": [{
        "disabled": false,
        "variable_abbrev": "DESCRIPTION",
        "api_resource_endpoint": "entries"
    }]
}
' where abbrev = 'EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL';


update platform.config set config_value = '
{
	"Name": "Required and default entry level metadata variables for IRRI Trial data process",
	"Values": [
		{
			"default": "entry",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "ENTRY_TYPE",
			"api_resource_endpoint": "entries"
		},
		{
            "disabled": false,
            "variable_abbrev": "ENTRY_CLASS",
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "api_resource_endpoint": "entries"
        }
	]
}
' where abbrev = 'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_VAL';


--EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for seed increase data process",
    "Values": [{
        "disabled": false,
        "variable_abbrev": "DESCRIPTION",
        "api_resource_endpoint": "entries"
    }]
}
' where abbrev = 'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_VAL';



--EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for IRRI Selection and Advancement data process",
    "Values": [{
        "disabled": false,
        "variable_abbrev": "DESCRIPTION",
        "api_resource_endpoint": "entries"
    }]
}
' where abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_VAL';


--EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
    "Values": [{
            "fixed": true,
            "default": false,
            "required": "required",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "api_resource_endpoint": "entries"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "api_resource_endpoint": "entries"
        }
    ]
}
' where abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL';