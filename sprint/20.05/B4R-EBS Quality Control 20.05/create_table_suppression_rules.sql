-- data_terminal.suppression_rule definition

-- Drop table

-- DROP TABLE data_terminal.suppression_rule;

CREATE TABLE data_terminal.suppression_rule (
	id serial NOT NULL,
  transaction_id integer NOT NULL,
	target_variable_id integer NOT NULL,
	factor_variable_id integer NOT NULL,
	conjunction varchar NOT NULL,
	"operator" varchar NOT NULL,
	value varchar NULL,
	status varchar NOT NULL,
	entity character varying, -- refers to the table where the entity_id is the identifier
  entity_id integer, -- Identifier of the entity (e.g plot_id) 
  creator_id integer NOT NULL, -- Id of the user who created the record
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record is created.
  modifier_id integer, -- Id of the user who last modified the transaction record.
  modification_timestamp timestamp without time zone, -- Timestamp when the last modification made.
  is_void boolean, -- Indicates whether the record deleted or not.
  remarks character varying, -- Additional details about the transaction.
  notes text,	
	CONSTRAINT suppression_rule_id_pkey PRIMARY KEY (id),
CONSTRAINT suppression_rule_transaction_id_fkey FOREIGN KEY (transaction_id)
      REFERENCES data_terminal.transaction (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the transaction_id column, which refers to the id column of data_terminal.transaction table
  CONSTRAINT suppression_rule_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of master.user table
  CONSTRAINT suppression_rule_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)WITH (
  OIDS=FALSE
);
COMMENT ON TABLE data_terminal.suppression_rule
  IS 'Suppression rules applied in the transaction dataset.';

CREATE INDEX suppression_rule_transaction_id_idx
  ON data_terminal.suppression_rule
  USING btree
  (transaction_id);