--create new application for germplasm
insert into platform.application (abbrev, label, action_label, icon, creator_id)
values ('TRAITS', 'Traits', 'Browse Traits', 'border_color', 1);

--map new application with action (url)
insert into platform.application_action (application_id, module, controller,creator_id)
select
	app.id,
	'traits',
	'default',
	1
from
	platform.application app
where
	app.abbrev = 'TRAITS';

-- unvoid locations in applications
update platform.application set is_void = false, notes = '' where abbrev = 'LOCATIONS';

--update navigation
UPDATE
	platform.space
SET
	menu_data = '{
    "left_menu_items": [
        {
            "name": "experiment-creation",
            "items": [
                {
                    "name": "experiment-creation-experiments",
                    "label": "Experiments",
                    "appAbbrev": "EXPERIMENT_CREATION"
                },
                {
                    "name": "experiment-creation-occurrences",
                    "label": "Occurrences",
                    "appAbbrev": "OCCURRENCES"
                },
                {
                    "name": "experiment-creation-locations",
                    "label": "Locations",
                    "appAbbrev": "LOCATIONS"
                }
            ],
            "label": "Experiments"
        },
        {
            "name": "data-collection-qc",
            "items": [
                {
                    "name": "data-collection-qc-quality-control",
                    "label": "Quality control",
                    "appAbbrev": "QUALITY_CONTROL"
                }
            ],
            "label": "Data collection & QC"
        },
        {
            "name": "seeds",
            "items": [
                {
                    "name": "seeds-find-seeds",
                    "label": "Find seeds",
                    "appAbbrev": "FIND_SEEDS"
                },
                {
                    "name": "seeds-harvest-manager",
                    "label": "Harvest manager",
                    "appAbbrev": "HARVEST_MANAGER"
                }
            ],
            "label": "Seeds"
        }
    ],
    "main_menu_items": [
        {
            "icon": "folder_special",
            "name": "data-management",
            "items": [
                {
                    "name": "data-management_seasons",
                    "label": "Seasons",
                    "appAbbrev": "MANAGE_SEASONS"
                },
                {
                    "name": "data-management_germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                },
                {
                    "name": "data-management_traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }
            ],
            "label": "Data management"
        },
        {
            "icon": "settings",
            "name": "administration",
            "items": [
                {
                    "name": "administration_users",
                    "label": "Persons",
                    "appAbbrev": "PERSONS"
                }
            ],
            "label": "Administration"
        }
    ]
}'
WHERE abbrev = 'ADMIN';


--update navigation
UPDATE
    platform.space
SET
    menu_data = '{
    "left_menu_items": [
        {
            "name": "experiment-creation",
            "items": [
                {
                    "name": "experiment-creation-experiments",
                    "label": "Experiments",
                    "appAbbrev": "EXPERIMENT_CREATION"
                },
                {
                    "name": "experiment-creation-occurrences",
                    "label": "Occurrences",
                    "appAbbrev": "OCCURRENCES"
                },
                {
                    "name": "experiment-creation-locations",
                    "label": "Locations",
                    "appAbbrev": "LOCATIONS"
                }
            ],
            "label": "Experiments"
        },
        {
            "name": "data-collection-qc",
            "items": [
                {
                    "name": "data-collection-qc-quality-control",
                    "label": "Quality control",
                    "appAbbrev": "QUALITY_CONTROL"
                }
            ],
            "label": "Data collection & QC"
        },
        {
            "name": "seeds",
            "items": [
                {
                    "name": "seeds-find-seeds",
                    "label": "Find seeds",
                    "appAbbrev": "FIND_SEEDS"
                },
                {
                    "name": "seeds-harvest-manager",
                    "label": "Harvest manager",
                    "appAbbrev": "HARVEST_MANAGER"
                }
            ],
            "label": "Seeds"
        }
    ]
}'
WHERE abbrev = 'DEFAULT';