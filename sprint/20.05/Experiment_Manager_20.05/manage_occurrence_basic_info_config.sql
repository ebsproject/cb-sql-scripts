-- add occurrence level configuration
INSERT INTO
	platform.config (abbrev, name, config_value, rank, usage)
VALUES (
	'EXPT_TRIAL_DATA_PROCESS_OCCURRENCE_FIELDS',
	'Occurrence fields configuration for Trial Experiment Template',
	'
	{
		"basic": {
			"label": "Basic info",
			"fields": [
				{
					"variable_abbrev": "OCCURRENCE_NAME",
					"variable_type": "identification",
					"required": "required"
				},
				{
					"variable_abbrev": "DESCRIPTION",
					"variable_type": "identification"
				},
				{
					"variable_abbrev": "CONTCT_PERSON_CONT"
				},
				{
					"variable_abbrev": "ECOSYSTEM"
				}
			]
		}
	}
	',
	1,
	'experiment_manager_occurrence'
);

INSERT INTO
	platform.config (abbrev, name, config_value, rank, usage)
VALUES (
	'DEFAULT_OCCURRENCE_FIELDS',
	'Occurrence fields configuration for Default experiment template',
	'
	{
		"basic": {
			"label": "Basic info",
			"fields": [
				{
					"variable_abbrev": "OCCURRENCE_NAME",
					"variable_type": "identification",
					"required": "required"
				},
				{
					"variable_abbrev": "DESCRIPTION",
					"variable_type": "identification"
				},
				{
					"variable_abbrev": "CONTCT_PERSON_CONT"
				},
				{
					"variable_abbrev": "ECOSYSTEM"
				}
			]
		}
	}
	',
	1,
	'experiment_manager_occurrence'
);