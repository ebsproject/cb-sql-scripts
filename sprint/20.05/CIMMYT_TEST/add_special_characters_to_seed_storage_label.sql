
UPDATE operational.seed_storage SET label='E-045/IRRI 158' WHERE label = 'E-045' AND product_id=91778;
--rollback UPDATE operational.seed_storage SET label='E-045' WHERE label = 'E-045/IRRI 158' AND product_id=91778;

UPDATE operational.seed_storage SET label='H7170/IRRI 158' WHERE label = 'H7170' AND product_id=91778;
--rollback UPDATE operational.seed_storage SET label='H7170' WHERE label = 'H7170/IRRI 158' AND product_id=91778;

UPDATE operational.seed_storage SET label='141E1513\IRRI 158' WHERE label = '141E1513' AND product_id=91778;
--rollback UPDATE operational.seed_storage SET label='141E1513' WHERE label = '141E1513\IRRI 158' AND product_id=91778;

UPDATE operational.seed_storage SET label='141C2;IRRI 158' WHERE label = '141C2' AND product_id=91778;
--rollback UPDATE operational.seed_storage SET label='141C2' WHERE label = '141C2;IRRI 158' AND product_id=91778;