--ENABLE POLLINATION PROTOCOL

update master.item set is_void=false where abbrev = 'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT';

update platform.module set is_void=false where abbrev = 'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT_MOD'; 