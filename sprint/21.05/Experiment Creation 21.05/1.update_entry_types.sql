--OBSERVATION EXPERIMENT
update platform.config set config_value = '
{
	"Name": "Required and default entry level metadata variables for Observation data process",
	"Values": [{
			"default": "test",
			"disabled": false,
			"required": "required",
			"target_value": "",
			"target_column": "",
			"variable_type": "identification",
			"allowed_values": [
				"ENTRY_TYPE_CHECK",
				"ENTRY_TYPE_TEST"
			],
			"variable_abbrev": "ENTRY_TYPE",
			"api_resource_sort": "",
			"api_resource_filter": "",
			"api_resource_method": "",
			"api_resource_endpoint": "entries",
			"secondary_target_column": ""
		},
		{
			"disabled": false,
			"target_value": "",
			"target_column": "",
			"variable_type": "identification",
			"variable_abbrev": "ENTRY_CLASS",
			"api_resource_sort": "",
			"api_resource_filter": "",
			"api_resource_method": "",
			"api_resource_endpoint": "entries",
			"secondary_target_column": ""
		},
		{
			"disabled": false,
			"target_value": "",
			"target_column": "",
			"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION",
			"api_resource_sort": "",
			"api_resource_filter": "",
			"api_resource_method": "",
			"api_resource_endpoint": "entries",
			"secondary_target_column": ""
		}
	]
}
' where abbrev = 'OBSERVATION_ENTRY_LIST_ACT_VAL';

--BREEDING TRIAL EXPERIMENT
update platform.config set config_value = '
{
	"Name": "Required and default entry level metadata variables for Breeding Trial data process",
	"Values": [{
			"default": "test",
			"disabled": false,
			"required": "required",
			"target_value": "",
			"target_column": "",
			"variable_type": "identification",
			"allowed_values": [
				"ENTRY_TYPE_CHECK",
				"ENTRY_TYPE_TEST"
			],
			"variable_abbrev": "ENTRY_TYPE",
			"api_resource_sort": "",
			"api_resource_filter": "",
			"api_resource_method": "",
			"api_resource_endpoint": "entries",
			"secondary_target_column": ""
		},
		{
			"disabled": false,
			"target_value": "",
			"target_column": "",
			"variable_type": "identification",
			"variable_abbrev": "ENTRY_CLASS",
			"api_resource_sort": "",
			"api_resource_filter": "",
			"api_resource_method": "",
			"api_resource_endpoint": "entries",
			"secondary_target_column": ""
		},
		{
			"disabled": false,
			"target_value": "",
			"target_column": "",
			"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION",
			"api_resource_sort": "",
			"api_resource_filter": "",
			"api_resource_method": "",
			"api_resource_endpoint": "entries",
			"secondary_target_column": ""
		}
	]
}
' where abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_VAL';