-- Add Planting Instructions Manager in applications and left navigation

-- add planting instructions manager in applications
insert into platform.application (abbrev, label, action_label, icon, creator_id)
values
	('PLANTING_INSTRUCTIONS_MANAGER', 'Planting instructions manager', 'Planting instructions manager', 'grass',1);

insert into platform.application_action (application_id, module, creator_id)
select 
	id,
	'plantingInstruction',
	1
from
	platform.application
where
	abbrev = 'PLANTING_INSTRUCTIONS_MANAGER';

-- update default space to add PIM
update 
	platform.space
set 
	menu_data = '{
	"left_menu_items": [{
		"name": "experiment-creation",
		"label": "Experiment creation",
		"appAbbrev": "EXPERIMENT_CREATION"
	}, {
		"name": "experiment-manager",
		"label": "Experiment manager",
		"appAbbrev": "OCCURRENCES"
	}, {
		"name": "planting-instructions-manager",
		"label": "Planting instructions manager",
		"appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
	}, {
		"name": "data-collection-qc-quality-control",
		"label": "Data collection",
		"appAbbrev": "QUALITY_CONTROL"
	}, {
		"name": "seeds-harvest-manager",
		"label": "Harvest manager",
		"appAbbrev": "HARVEST_MANAGER"
	}, {
		"name": "searchs-germplasm",
		"label": "Germplasm",
		"appAbbrev": "GERMPLASM_CATALOG"
	}, {
		"name": "search-seeds",
		"label": "Seeds",
		"appAbbrev": "FIND_SEEDS"
	}, {
		"name": "search-traits",
		"label": "Traits",
		"appAbbrev": "TRAITS"
	}],
	"main_menu_items": [{
		"icon": "help_outline",
		"name": "help",
		"items": [{
			"url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
			"icon": "headset_mic",
			"name": "help_support-portal",
			"label": "Support portal",
			"tooltip": "Go to EBS Service Desk portal"
		}, {
			"url": "https://uat-b4rapi.b4rdev.org",
			"icon": "code",
			"name": "help_api",
			"label": "API",
			"tooltip": "Go to B4R API Documentation"
		}],
		"label": "Help"
	}]
}'
where
	abbrev = 'DEFAULT';

-- update admin space to add PIM
update 
	platform.space
set 
	menu_data = '
	{
		"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiment creation",
			"appAbbrev": "EXPERIMENT_CREATION"
		}, {
			"name": "experiment-manager",
			"label": "Experiment manager",
			"appAbbrev": "OCCURRENCES"
		}, {
			"name": "data-collection-qc-quality-control",
			"label": "Data collection",
			"appAbbrev": "QUALITY_CONTROL"
		}, {
			"name": "seeds-harvest-manager",
			"label": "Harvest manager",
			"appAbbrev": "HARVEST_MANAGER"
		}, {
			"name": "planting-instructions-manager",
			"label": "Planting instructions manager",
			"appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
		}, {
			"name": "searchs-germplasm",
			"label": "Germplasm",
			"appAbbrev": "GERMPLASM_CATALOG"
		}, {
			"name": "search-seeds",
			"label": "Seeds",
			"appAbbrev": "FIND_SEEDS"
		}, {
			"name": "search-traits",
			"label": "Traits",
			"appAbbrev": "TRAITS"
		}],
		"main_menu_items": [{
			"icon": "help_outline",
			"name": "help",
			"items": [{
				"url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
				"icon": "headset_mic",
				"name": "help_support-portal",
				"label": "Support portal",
				"tooltip": "Go to EBS Service Desk portal"
			}, {
				"url": "https://uat-b4rapi.b4rdev.org",
				"icon": "code",
				"name": "help_api",
				"label": "API",
				"tooltip": "Go to B4R API Documentation"
			}],
			"label": "Help"
		}, {
			"icon": "folder_special",
			"name": "data-management",
			"items": [{
				"name": "data-management_seasons",
				"label": "Seasons",
				"appAbbrev": "MANAGE_SEASONS"
			}],
			"label": "Data management"
		}, {
			"icon": "settings",
			"name": "administration",
			"items": [{
				"name": "administration_users",
				"label": "Persons",
				"appAbbrev": "PERSONS"
			}],
			"label": "Administration"
		}]
	}'
where
	abbrev = 'ADMIN';