-- Add fields configuration for Packing job creation
insert into platform.config (abbrev, name, config_value, usage, creator_id)
values
('PACKING_JOB_FIELDS', 'Fields configuration for packing job creation',
'
{
	"review": {
		"label": "Review tab additional info",
		"fields": [{
			"variable_type": "identification",
			"variable_abbrev": "PLANTING_JOB_DUE_DATE"
		}, {
			"variable_type": "identification",
			"variable_abbrev": "PLANTING_JOB_INSTRUCTIONS"
		}, {
			"variable_abbrev": "PLANTING_JOB_FACILITY",
			"variable_type": "identification",
			"target_column": "facilityDbId",
			"secondary_target_column": "facilityDbId",
			"target_value": "facilityName",
			"api_resource_method": "POST",
			"api_resource_endpoint": "facilities-search",
			"api_resource_filter": {
				"facilityType": "building"
			},
			"api_resource_sort": "sort=facilityName"
		}]
	},
	"envelopes": {
		"label": "Envelope tab",
		"fields": [{
			"fixed": true,
			"default": "1",
			"required": "required",
			"display_name": "Seeds / envelope",
			"variable_type": "identification",
			"variable_abbrev": "SEEDS_PER_ENVELOPE",
			"api_resource_endpoint": "planting-job-occurrences"
		}, {
			"fixed": true,
			"default": "g",
			"required": "required",
			"display_name": "Unit",
			"variable_type": "identification",
			"variable_abbrev": "PACKAGE_UNIT",
			"api_resource_endpoint": "planting-job-occurrences"
		}, {
			"fixed": true,
			"default": "1",
			"required": "required",
			"display_name": "Envelopes / plot",
			"variable_type": "identification",
			"variable_abbrev": "ENVELOPES_PER_PLOT",
			"api_resource_endpoint": "planting-job-occurrences"
		}]
	}
}
',
'planting_instructions_manager',
1
);