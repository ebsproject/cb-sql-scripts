-- CORB-1156 HM - Add hybrid formation package label pattern
Delete from platform.config where abbrev='HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT',
        'Harvest Manager Maize Package Label Pattern-Default V2',
$$			
		
{
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "sequence_name": "<sequence_name>",
                        "order_number": 4
                    }
                ]
            }
        }
    },
    "PLOT": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "germplasm",
                        "field_name" : "designation",
                        "order_number":0
                    }
                ]
            }
        }
    },
    "CROSS": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "cross",
                        "field_name" : "crossName",
                        "order_number":0
                    }
                ]
            }
        },
        "single cross": {
            "default" : {
                "default" : [
                    {
                        "type": "field",
                        "entity": "seed",
                        "field_name": "seedName",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "leading_zero" : "yes",
                        "max_digits" : 3,
                        "order_number": 2
                    }
                ]
            }
        },
        "hybrid formation": {
            "default" : {
                "default" : [
                    {
                        "type": "field",
                        "entity": "seed",
                        "field_name": "seedName",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "free-text",
                        "value": "PK",
                        "order_number": 2
                    },
                    {
                        "type": "counter",
                        "leading_zero" : "yes",
                        "max_digits" : 3,
                        "order_number": 2
                    }
                ]
            }
        }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'added hybrid formation package label pattern');