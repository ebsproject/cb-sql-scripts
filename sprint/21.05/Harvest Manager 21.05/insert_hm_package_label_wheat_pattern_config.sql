Delete from platform.config where abbrev='HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT',
        'Harvest Manager Wheat Package Label Pattern-Default',
$$			
	
{
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "sequence_name": "<sequence_name>",
                        "order_number": 4
                    }
                ]
            }
        }
    },
    "PLOT": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "seed",
                        "field_name" : "seedName",
                        "order_number":0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "free-text",
                        "value": "P",
                        "order_number": 2
                    },
                    {
                        "type" : "field",
                        "entity" : "plot",
                        "field_name" : "plotNumber",
                        "order_number": 3
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 4
                    },
                    {
                        "type": "free-text",
                        "value": "PK",
                        "order_number": 5
                    },
                    {
                        "type": "counter",
                        "leading_zero" : "yes",
                        "max_digits" : 3,
                        "order_number": 2
                    }
                ]
            }
        }
    },
    "CROSS": {
        "default": {
            "default": {
                "default": [
                    {
                        "type": "free-text",
                        "value": "",
                        "order_number": 0
                    }
                ]
            }
        },
        "single cross": {
            "default" : {
                "default" : [
                    {
                        "type": "field",
                        "entity": "seed",
                        "field_name": "seedName",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "free-text",
                        "value": "PK",
                        "order_number": 2
                    },
                    {
                        "type": "counter",
                        "leading_zero" : "yes",
                        "max_digits" : 3,
                        "order_number": 3
                    }
                ]
            }
        }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-1175 - j.bantay 2021-05-10');