-- CORB-1156 HM - Add hybrid formation seed naming pattern
DELETE from platform.config where abbrev='HM_NAME_PATTERN_SEED_MAIZE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_SEED_MAIZE_DEFAULT',
        'Harvest Manager Maize Seed Name Pattern-Default V2',
$$			
		
{
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "sequence_name": "<sequence_name>",
                        "order_number": 4
                    }
                ]
            }
        }
    },
    "PLOT": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "germplasm",
                        "field_name" : "designation",
                        "order_number":0
                    }
                ]
            }
        }
    },
    "CROSS": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "cross",
                        "field_name" : "crossName",
                        "order_number":0
                    }
                ]
            }
        },
        "single cross": {
            "default" : {
                "default" : {
                    "same_nursery" : [
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type" : "field",
                            "entity" : "maleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 8
                        }
                    ],
                    "different_nurseries" : [
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type" : "field",
                            "entity" : "maleCrossParent",
                            "field_name" : "sourceExperimentName",
                            "order_number": 8
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 9
                        },
                        {
                            "type" : "field",
                            "entity" : "maleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 10
                        }
                    ]
                }
            }
        },
        "hybrid formation": {
            "default" : {
                "default" : {
                    "same_nursery" : [
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type" : "field",
                            "entity" : "maleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 8
                        }
                    ],
                    "different_nurseries" : [
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type" : "field",
                            "entity" : "femaleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type" : "field",
                            "entity" : "maleCrossParent",
                            "field_name" : "sourceExperimentName",
                            "order_number": 8
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 9
                        },
                        {
                            "type" : "field",
                            "entity" : "maleCrossParent",
                            "field_name" : "entryNumber",
                            "order_number": 10
                        }
                    ]
                }
            }
        }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'added hybrid formation seed naming pattern');