-- Function: operational_data_terminal.validate_terminal_records(integer, integer)

-- DROP FUNCTION operational_data_terminal.validate_terminal_records(integer, integer);

CREATE OR REPLACE FUNCTION operational_data_terminal.validate_terminal_records(
    var_transaction_id integer,
    var_study_id integer)
  RETURNS character varying AS
$BODY$DECLARE
	r_variable RECORD;
	r_data_unit RECORD;	
BEGIN
	DELETE from operational_data_terminal.terminal where transaction_id=var_transaction_id and (value is null or trim(value)='') and is_generated=false;
	
	for r_variable in
	--start get all variables in the transaction
	select v.id as variable_id,v.scale_id,v.data_type from master.variable v  where v.is_void=false and v.id in (select distinct variable_id from operational_data_terminal.terminal where transaction_id=var_transaction_id)
	LOOP
	--start validates the data type
	IF r_variable.data_type = 'integer'
	THEN
		 update operational_data_terminal.terminal 
                set is_data_type_valid=TRUE 
                where 
                    value ~ '^\d+$' and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;

                    update operational_data_terminal.terminal 
                set is_data_type_valid=FALSE 
                where 
                    value !~ '^\d+$' and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
	

	ELSIF (r_variable.data_type = 'float' or r_variable.data_type = 'double precision') 
	THEN
		update operational_data_terminal.terminal 
                set is_data_type_valid=TRUE 
                where 
                    value ~ '^[-+]?[0-9]*\.?[0-9]+$' and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;

                update operational_data_terminal.terminal 
                set is_data_type_valid=FALSE
                where 
                    value !~ '^[-+]?[0-9]*\.?[0-9]+$' and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
	
	ELSIF (r_variable.data_type = 'character varying' or r_variable.data_type = 'text' or r_variable.data_type = 'varchar' ) 
	THEN
		update operational_data_terminal.terminal 
                set is_data_type_valid=TRUE 
                where 
                   transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
	
	ELSIF r_variable.data_type = 'date'
	THEN	/* valid date format is YYYY-mm-dd */
	
	raise notice 'date';
		update operational_data_terminal.terminal 
            set is_data_type_valid=TRUE 
            where 
                value ~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'
                and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;

		/*convert format from mm/dd/YYYY into YYYY-mm-dd */
        update operational_data_terminal.terminal 
        	set value =             
            substring( value from '^\d+\/\d+\/((19|20)\d\d)$') /*year*/ || '-' ||
			      substring( value from '^(0[1-9]|1[012])\/(\d+)\/(\d+)$') /*month*/ || '-' ||
			      substring( value from '^\d+\/((0[1-9]|[12][0-9]|3[01]))\/(.)+$') /*day*/
        	where 
            	value ~ '^(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$'
            
	            and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;

		update operational_data_terminal.terminal 
                set is_data_type_valid=FALSE 
                where 
                    value !~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$' and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
                
	

	ELSIF r_variable.data_type = 'time'
	THEN
		update operational_data_terminal.terminal 
                set is_data_type_valid=TRUE 
                where 
                    value ~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$' and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;

		update operational_data_terminal.terminal 
                set is_data_type_valid=FALSE 
                where 
                    value !~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$' and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
                
	END IF;

--end validates the data type

	--for scale values, if variable has scale values
	IF r_variable.scale_id is not NULL THEN
		--start validates the value
		IF EXISTS (SELECT 1 FROM master.scale_value where scale_id=r_variable.scale_id) THEN
			update operational_data_terminal.terminal 
				set is_data_value_valid=FALSE 
			where 
				value NOT IN (select value from master.scale_value where scale_id=r_variable.scale_id and is_void=false) and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;

			update operational_data_terminal.terminal 
				set is_data_value_valid=TRUE
			where 
				value IN (select value from master.scale_value where scale_id=r_variable.scale_id and is_void=false) and transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
		ELSE	-- no scale value, hence, value is valid
			 update operational_data_terminal.terminal 
				set is_data_value_valid=TRUE 
			where 
				transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
		END IF;
		--end validates the value
	ELSE
	-- no scale value, hence, value is valid
		 update operational_data_terminal.terminal 
                set is_data_value_valid=TRUE 
                where 
                    transaction_id=var_transaction_id and variable_id=r_variable.variable_id;
	END IF;
	
    END LOOP;

    -- start looping through the data unit in the transaction
	for r_data_unit in
	SELECT t.data_unit
            from operational_data_terminal.terminal t
            where t.transaction_id = var_transaction_id
            group by t.data_unit
	LOOP
	-- start check if the record is already in the operational, is_committed_operational = TRUE, alse is_committed_operational = FALSE
		IF r_data_unit.data_unit = 'entry_metadata' THEN 
			UPDATE operational_data_terminal.terminal t
			SET is_committed_operational = TRUE
			FROM operational.entry_metadata em,
				operational.entry e
			WHERE t.data_unit = 'entry_metadata'
				and em.variable_id = t.variable_id
				and t.transaction_id = var_transaction_id
				and e.key=t.key
				and em.entry_id = e.id
				and t.value<>em.value;
		
		ELSIF r_data_unit.data_unit = 'entry_data' THEN
			UPDATE operational_data_terminal.terminal t
			SET is_committed_operational = TRUE
			FROM operational.entry_data em,
				operational.entry e
			WHERE t.data_unit = 'entry_data'
				and em.variable_id = t.variable_id
				and t.transaction_id = var_transaction_id
				and e.key=t.key
				and em.entry_id = e.id
				and t.value<>em.value;
				
		ELSIF r_data_unit.data_unit = 'plot_data' THEN
			UPDATE operational_data_terminal.terminal t
			SET is_committed_operational = TRUE
			FROM 
				operational.plot_data pd, 
				operational.plot p 
			WHERE t.data_unit = 'plot_data'
				and pd.variable_id = t.variable_id
				and t.transaction_id = var_transaction_id
				and p.key=t.key
				and pd.plot_id = p.id
				and pd.study_id=var_study_id
				and t.value<>pd.value;
				
		ELSIF r_data_unit.data_unit = 'plot_metadata' THEN
			 UPDATE operational_data_terminal.terminal t
			SET is_committed_operational = TRUE
			FROM 
				operational.plot_metadata pd, 
				operational.plot p 
			WHERE t.data_unit = 'plot_metadata'
				and pd.variable_id = t.variable_id
				and t.transaction_id = var_transaction_id
				and p.key=t.key
				and pd.plot_id = p.id
				and pd.study_id=var_study_id
				and t.value<>pd.value;
		ELSE
			UPDATE operational_data_terminal.terminal t
			SET is_committed_operational = TRUE
				FROM operational.study_metadata sm,
				operational.study s
			WHERE t.data_unit = 'study_metadata'
				and sm.variable_id = t.variable_id
				and t.transaction_id = var_transaction_id
				and sm.study_id = s.id
				and s.id = var_study_id 
				and t.value<>sm.value;
	-- end check if the record is already in the operational, is_committed_operational = TRUE, alse is_committed_operational = FALSE	
		END IF;
-- end looping through the data unit in the transaction	
	END LOOP;

RETURN 'success';
	
END;	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;