DELETE FROM platform.config WHERE abbrev='HM_PACKAGE_LABEL_PATTERN_RICE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_PACKAGE_LABEL_PATTERN_RICE_DEFAULT',
        'Harvest Manager Rice Package Label Pattern-Default',
$$			
		
{
	"default": {
	  "default" : {
        "default" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "plotCode",
            "order_number":0
          }
        ]
      }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-53 update config - j.bantay 2021-02-04');
