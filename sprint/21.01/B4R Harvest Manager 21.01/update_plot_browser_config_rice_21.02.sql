DELETE FROM platform.config WHERE abbrev='HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE',
        'Harvest Manager Plot Browser Config for Rice',
$$			
		
{
    "name": "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE",
    "values": [
        {
            "germplasm_states": {
                "fixed": {
                    "methods": [
                        "Bulk"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "methods": [
                        "Bulk",
                        "Single Plant Selection"
                    ],
                    "display_columns": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                }
            }
        }
    ]
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-53 update config - j.bantay 2021-02-04');
