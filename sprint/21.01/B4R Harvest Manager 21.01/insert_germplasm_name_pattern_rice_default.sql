Delete from platform.config where abbrev='HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT',
        'Harvest Manager Rice Germplasm Name Pattern-Default',
$$			
		
{
	"default": {
	  "default" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ]
      },
      "progeny" : {
        "default" : [
          {
            "type" : "free-text",
            "value" : "IR XXXXX",
            "order_number" : 0
          }
        ],
        "bulk" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "germplasmDesignation",
            "order_number":0
          },
          {
            "type" : "delimiter",
            "value" : "-",
            "order_number" : 1
          },
          {
            "type" : "free-text",
            "value" : "B",
            "order_number" : 2
          }
        ],
        "single plant selection" : [
          {
            "type" : "field",
            "entity" : "plot",
            "field_name" : "germplasmDesignation",
            "order_number":0
          },
          {
            "type" : "delimiter",
            "value" : "-",
            "order_number" : 1
          },
          {
            "type" : "counter",
            "order_number" : 2
          }
        ]
      }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-54 update config - j.bantay 2021-02-04')
