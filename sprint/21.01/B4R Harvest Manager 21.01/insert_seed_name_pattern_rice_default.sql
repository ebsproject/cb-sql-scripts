DELETE FROM platform.config WHERE abbrev='HM_SEED_NAME_PATTERN_RICE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_SEED_NAME_PATTERN_RICE_DEFAULT',
        'Harvest Manager Rice Seed Name Pattern-Default',
$$			
		
{
	"default": {
	  "default" : {
        "default" : [
          {
            "type" : "field",
            "entity" : "germplasm",
            "field_name" : "designation",
            "order_number":0
          }
        ]
      }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-55 update config - j.bantay 2021-02-04');
