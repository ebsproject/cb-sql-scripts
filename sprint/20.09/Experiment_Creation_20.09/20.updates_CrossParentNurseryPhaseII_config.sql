
--void planting protocol
update master.item set is_void = true where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT';

update platform.module set is_void = true where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT_MOD';

update platform.module set  action_id = 'manage-crosses' where  abbrev= 'CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT_MOD';
