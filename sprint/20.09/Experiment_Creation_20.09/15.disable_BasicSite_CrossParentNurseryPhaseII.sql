
update platform.config set config_value = '{
   "Name": "Required and default place metadata variables for Cross Parent Nursery Phase II data process",
   "Values": [
    {
      "required": "required",
      "disabled": true,
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "disabled": true,
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "disabled": true,
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": true,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "disabled": true,
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
 }' where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_VAL'