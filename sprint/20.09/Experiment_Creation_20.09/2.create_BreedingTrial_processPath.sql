insert into master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage)
values('BREEDING_TRIAL_DATA_PROCESS','Breeding Trial Experiment',40,'Are experiment objectives such as yield, disease, physiological, etc.','Breeding Trial Experiment',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');

--create Specify basic information
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

--create Specify entry list
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol');

--create Specify design
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_DESIGN_ACT','Specify Design',30,'Specify Design','Design',1,'active','fa fa-th');

--create Specify experiment groups
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_EXPT_GROUP_ACT','Specify Experiment Groups',30,'Specify Experiment Groups','Experiment Groups',1,'active','fa fa-bars');

--create Specify Protocols
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_PROTOCOL_ACT','Specify Protocol',30,'Specify Protocol','Protocol',1,'active','fa fa-map-marker');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_PLANTING_PROTOCOL_ACT','Planting Protocol',20,'Planting Protocol','Planting Protocol',1,'active','fa fa-pagelines');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_TRAITS_PROTOCOL_ACT','Traits Protocol',20,'Traits Protocol','Traits Protocol',1,'active','fa fa-braille');

--create Specify locations
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker');

--create Preview and confirm
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');


--insert Specify Parents, Specify Crosses and Preview and Confirm in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS') as parent_id,
	id as child_id,
	case
		when abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT' then 1
		when abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT'then 2
		when abbrev = 'BREEDING_TRIAL_DESIGN_ACT'then 3
		when abbrev = 'BREEDING_TRIAL_EXPT_GROUP_ACT'then 4
        when abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT'then 5
		when abbrev = 'BREEDING_TRIAL_PLACE_ACT'then 6
		when abbrev = 'BREEDING_TRIAL_REVIEW_ACT'then 7
		else 8 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev in ('BREEDING_TRIAL_BASIC_INFO_ACT','BREEDING_TRIAL_ENTRY_LIST_ACT','BREEDING_TRIAL_DESIGN_ACT', 'BREEDING_TRIAL_EXPT_GROUP_ACT','BREEDING_TRIAL_PROTOCOL_ACT', 'BREEDING_TRIAL_PLACE_ACT', 'BREEDING_TRIAL_REVIEW_ACT');


--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT' then 1
		when abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'then 2
		else 3 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('BREEDING_TRIAL_PLANTING_PROTOCOL_ACT','BREEDING_TRIAL_TRAITS_PROTOCOL_ACT');
;





--insert Specify Parents, Specify Crosses and Preview and Confirm in platform.module
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('BREEDING_TRIAL_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
('BREEDING_TRIAL_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
('BREEDING_TRIAL_DESIGN_ACT_MOD','Specify Design','Specify Design','experimentCreation','create','specify-design',1,'added by j.antonio ' || now(), 'design generated'),
('BREEDING_TRIAL_EXPT_GROUP_ACT_MOD','Specify Experiment Groups','Specify Experiment Groups','experimentCreation','create','specify-experiment-groups',1,'added by j.antonio ' || now(), 'experiment group specified'),
('BREEDING_TRIAL_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
('BREEDING_TRIAL_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocos','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
('BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
('BREEDING_TRIAL_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'occurrences created'),
('BREEDING_TRIAL_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');

--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
	case
		when abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT' then (select id from platform.module where abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT_MOD')
		when abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT'then (select id from platform.module where abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_MOD')
		when abbrev = 'BREEDING_TRIAL_DESIGN_ACT' then (select id from platform.module where abbrev = 'BREEDING_TRIAL_DESIGN_ACT_MOD')
		when abbrev = 'BREEDING_TRIAL_EXPT_GROUP_ACT'then (select id from platform.module where abbrev = 'BREEDING_TRIAL_EXPT_GROUP_ACT_MOD')
        when abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT_MOD')
		when abbrev = 'BREEDING_TRIAL_PLACE_ACT'then (select id from platform.module where abbrev = 'BREEDING_TRIAL_PLACE_ACT_MOD')
		when abbrev = 'BREEDING_TRIAL_REVIEW_ACT'then (select id from platform.module where abbrev = 'BREEDING_TRIAL_REVIEW_ACT_MOD')
		else (select id from platform.module where abbrev = 'BREEDING_TRIAL_REVIEW_ACT_MOD') end as module_id,
	1,
	'added by l.gallardo ' || now()
from
	master.item
where
	abbrev in ('BREEDING_TRIAL_BASIC_INFO_ACT','BREEDING_TRIAL_ENTRY_LIST_ACT','BREEDING_TRIAL_DESIGN_ACT', 'BREEDING_TRIAL_EXPT_GROUP_ACT','BREEDING_TRIAL_PROTOCOL_ACT', 'BREEDING_TRIAL_PLACE_ACT', 'BREEDING_TRIAL_REVIEW_ACT');


--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT_MOD')
  when abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD')
  else (select id from platform.module where abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('BREEDING_TRIAL_PLANTING_PROTOCOL_ACT','BREEDING_TRIAL_TRAITS_PROTOCOL_ACT');
