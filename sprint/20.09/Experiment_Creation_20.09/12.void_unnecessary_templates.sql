/*
* Void the ff. experiment templates
* 1. Cross-Post Planning
* 2. Cross-Pre Planning for Hybrids
* 3. Selection and Advancement for IRRI
* 4. Seed Increase for IRRI
* 5. Cross-Parent Experiment
* 6. Nursery Cross list for IRRI
* 7. Trial for IRRI
* 8. Selection and Advancement
* 9. Seed Increase
* 10. Nursery Cross-list
* 11. Nursery Parent list
* 12. Cross Pre-Planning
* 13. Trial
*/

update master.item set (is_void, abbrev, name) = (true, 'VOIDED-'||abbrev, 'VOIDED-'||name) where abbrev in ('EXPT_CROSS_POST_PLANNING_DATA_PROCESS', 
'EXPT_CROSS_PRE_PLANNING_DATA_PROCESS','EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS','EXPT_SEED_INCREASE_IRRI_DATA_PROCESS',
'EXPT_CROSS_PARENT_DATA_PROCESS','EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS','EXPT_TRIAL_IRRI_DATA_PROCESS','EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS',
'EXPT_SEED_INCREASE_DATA_PROCESS','EXPT_NURSERY_CROSS_LIST_DATA_PROCESS','EXPT_NURSERY_PARENT_LIST_DATA_PROCESS','EXPT_NURSERY_CB_DATA_PROCESS',
'EXPT_TRIAL_DATA_PROCESS');
