--Void the previously created experiment type scale values
update master.scale_value set (is_void, value) = (true, 'VOIDED-'||value) where abbrev in ('EXPERIMENT_TYPE_TRIAL', 'EXPERIMENT_TYPE_NURSERY');

--Add the new Experiment Types
insert into master.scale_value (scale_id, order_number, value, description, display_name, abbrev) values 
(1875,1,'Breeding Trial','Breeding Trial', 'Breeding Trial','EXPERIMENT_TYPE_BREEDING_TRIAL'),
(1875,2,'Cross Parent Nursery','Cross Parent Nursery', 'Cross Parent Nursery','EXPERIMENT_TYPE_CROSS_PARENT_NURSERY'),
(1875,3,'Generation Nursery','Generation Nursery', 'Generation Nursery','EXPERIMENT_TYPE_GENERATION_NURSERY'),
(1875,4,'Intentional Crossing Nursery','Intentional Crossing Nursery', 'Intentional Crossing Nursery','EXPERIMENT_TYPE_INTENTIONAL_CROSSING_NURSERY');