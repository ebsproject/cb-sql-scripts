/**
* Add the HARVEST protocol to all the templates
*/

--For Breeding Trial
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'BREEDING_TRIAL_HARVEST_PROTOCOL_ACT' then 3
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('BREEDING_TRIAL_HARVEST_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('BREEDING_TRIAL_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'BREEDING_TRIAL_HARVEST_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('BREEDING_TRIAL_HARVEST_PROTOCOL_ACT');


--INTENTIONAL CROSSING NURSERY
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT' then 3
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT');

--GENERATION NURSERY
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'GENERATION_NURSERY_HARVEST_PROTOCOL_ACT' then 3
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_HARVEST_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('GENERATION_NURSERY_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'GENERATION_NURSERY_HARVEST_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_HARVEST_PROTOCOL_ACT');

--CROSS PARENT NURSERY (PHASE I)
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT' then 3
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT');


--CROSS PARENT NURSERY (PHASE II)
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT' then 2
        else 3 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT');



--CONFIGS
INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('BREEDING_TRIAL_HARVEST_PROTOCOL_ACT_VAL', 'Breeding Trial Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Breeding Trial data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL', 'Intentional Crossing Nursery Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Intentional Crossing Nursery data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('GENERATION_NURSERY_HARVEST_PROTOCOL_ACT_VAL', 'Generation Nursery Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Generation Nursery data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT_VAL', 'Cross Parent Nursery Phase I Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Cross Parent Nursery Phase I data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT_VAL', 'Cross Parent Nursery Phase II Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Cross Parent Nursery Phase II data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');