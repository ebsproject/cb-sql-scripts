update platform.config set config_value = '{
   "Name": "Required and default entry level metadata variables for Cross Parent Nursery Phase II data process",
   "Values": [
        {
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "display_name": "Parent Role",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": true,
            "variable_abbrev": "ENTRY_CLASS",
            "is_shown" : false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        }
    ]
 }' where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT_VAL';