INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Yield Trial',
             1,
             'Yield Trial',
             'Yield Trial',
             'EXPERIMENT_SUB_TYPE_YIELD_TRIAL'
         );
		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Disease Trial',
             2,
             'Disease Trial',
             'Disease Trial',
             'EXPERIMENT_SUB_TYPE_DISEASE_TRIAL'
         );
		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Physiology Trial',
             3,
             'Physiology Trial',
             'Physiology Trial',
             'EXPERIMENT_SUB_TYPE_PHYSIOLOGY_TRIAL'
         );
		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Other Trial',
             4,
             'Other Trial',
             'Other Trial',
             'EXPERIMENT_SUB_TYPE_OTHER_TRIAL'
         );		 
		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Breeding Crosses',
             5,
             'Breeding Crosses',
             'Breeding Crosses',
             'EXPERIMENT_SUB_TYPE_BREEDING_CROSSES'
         );		
		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Hybrid Crosses',
             6,
             'Hybrid Crosses',
             'Hybrid Crosses',
             'EXPERIMENT_SUB_TYPE_HYBRID_CROSSES'
         );		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Seed Increase',
             7,
             'Seed Increase',
             'Seed Increase',
             'EXPERIMENT_SUB_TYPE_SEED_INCREASE'
         );		 
		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Selection and Advancement',
             8,
             'Selection and Advancement',
             'Selection and Advancement',
             'EXPERIMENT_SUB_TYPE_SELECTION_AND_ADVANCEMENT'
         );	
		 
--
INSERT INTO 
 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
 	VALUES 
         (
             (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
             'Rapid Generation Advancement',
             9,
             'Rapid Generation Advancement',
             'Rapid Generation Advancement',
             'EXPERIMENT_SUB_TYPE_RAPID_GENERATION_ADVANCEMENT'
         );	