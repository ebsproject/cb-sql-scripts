/**
* Update the Breeding trial protocols label
*/
update master.item set (name, display_name) = ('Planting','Planting') where abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT';

update master.item set (name, display_name) = ('Traits','Traits') where abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT';