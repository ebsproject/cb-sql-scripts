/*
* Generation Nursery Process Path
*/

insert into master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage) 
values('GENERATION_NURSERY_DATA_PROCESS','Generation Nursery',40,'Are generation nurseries self-pollination occurs naturally and used for selection and advancement of segregating populations and seed increase','Generation Nursery',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');

--create specify basic information
--Basic, Entry List, Design, Protocols, Place, Review.
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

--create Specify Entry List
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol');


--create design tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_DESIGN_ACT','Planting Arrangement',30,'Planting Arrangement','Planting Arrangement',1,'active','fa fa-files-o');

--Additions in design tab
--Create design tabs
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_ADD_BLOCKS_ACT','Add blocks',20,'Add blocks','Add blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_ASSIGN_ENTRIES_ACT','Assign entries',20,'Assign entries','Assign entries',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_MANAGE_BLOCKS_ACT','Manage blocks',20,'Manage blocks','Manage blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_OVERVIEW_ACT','Overview',20,'Overview','Overview',1,'active','fa fa-random');



--create protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_PROTOCOL_ACT','Specify Protocol',30,'Specify Protocol','Protocol',1,'active','fa fa-files-o');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_PLANTING_PROTOCOL_ACT','Planting',20,'Planting Protocol','Planting',1,'active','fa fa-pagelines');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_TRAITS_PROTOCOL_ACT','Traits',20,'Traits Protocol','Traits',1,'active','fa fa-braille');


--create Specify locations
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker');

--create Preview and confirm
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');


--insert the sequence of activities for the cross pre-planning the data process
insert into master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select 
  (select id from master.item where abbrev='GENERATION_NURSERY_DATA_PROCESS') as root_id,
  (select id from master.item where abbrev='GENERATION_NURSERY_DATA_PROCESS') as parent_id,
   id as child_id,
   case 
     when abbrev = 'GENERATION_NURSERY_BASIC_INFO_ACT' then 1
     when abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT' then 2
     when abbrev = 'GENERATION_NURSERY_DESIGN_ACT' then 3
     when abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT' then 4
     when abbrev = 'GENERATION_NURSERY_PLACE_ACT' then 5
     when abbrev = 'GENERATION_NURSERY_REVIEW_ACT' then 6
     else 7 end as order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
from 
   master.item
where 
   abbrev in ('GENERATION_NURSERY_BASIC_INFO_ACT','GENERATION_NURSERY_ENTRY_LIST_ACT','GENERATION_NURSERY_DESIGN_ACT','GENERATION_NURSERY_PROTOCOL_ACT','GENERATION_NURSERY_PLACE_ACT','GENERATION_NURSERY_REVIEW_ACT');



--Additions for Design tab
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'GENERATION_NURSERY_DESIGN_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'GENERATION_NURSERY_ADD_BLOCKS_ACT' then 1
        when abbrev = 'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT'then 2
      when abbrev = 'GENERATION_NURSERY_MANAGE_BLOCKS_ACT'then 3
      when abbrev = 'GENERATION_NURSERY_OVERVIEW_ACT'then 4
        else 5 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_ADD_BLOCKS_ACT','GENERATION_NURSERY_ASSIGN_ENTRIES_ACT','GENERATION_NURSERY_MANAGE_BLOCKS_ACT','GENERATION_NURSERY_OVERVIEW_ACT');
;

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT' then 1
        when abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT'then 2
        else 3 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_PLANTING_PROTOCOL_ACT','GENERATION_NURSERY_TRAITS_PROTOCOL_ACT');
;



--insert the activities in the platform module
insert into platform.module(abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
values 
  ('GENERATION_NURSERY_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
  ('GENERATION_NURSERY_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
  ('GENERATION_NURSERY_DESIGN_ACT_MOD','Planting Arrangement','Planting Arrangement','experimentCreation','create','specify-cross-design',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_ADD_BLOCKS_ACT_MOD','Add Blocks','Add Blocks','experimentCreation','create','add-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_ASSIGN_ENTRIES_ACT_MOD','Assign Entries','Assign Entries','experimentCreation','create','assign-entries',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_MANAGE_BLOCKS_ACT_MOD','Manage Blocks','Manage Blocks','experimentCreation','create','manage-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_OVERVIEW_ACT_MOD','Overview','Overview','experimentCreation','create','overview',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('GENERATION_NURSERY_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocol','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('GENERATION_NURSERY_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'occurrences created'),
  ('GENERATION_NURSERY_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');


--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    case
        when abbrev = 'GENERATION_NURSERY_BASIC_INFO_ACT' then (select id from platform.module where abbrev = 'GENERATION_NURSERY_BASIC_INFO_ACT_MOD')
        when abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT_MOD')
        when abbrev = 'GENERATION_NURSERY_DESIGN_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_DESIGN_ACT_MOD')
      when abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT_MOD')
        when abbrev = 'GENERATION_NURSERY_PLACE_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_PLACE_ACT_MOD')
        when abbrev = 'GENERATION_NURSERY_REVIEW_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_REVIEW_ACT_MOD')
        else (select id from platform.module where abbrev = 'GENERATION_NURSERY_REVIEW_ACT_MOD') end as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
    abbrev in ('GENERATION_NURSERY_BASIC_INFO_ACT','GENERATION_NURSERY_ENTRY_LIST_ACT', 'GENERATION_NURSERY_DESIGN_ACT','GENERATION_NURSERY_PROTOCOL_ACT', 'GENERATION_NURSERY_PLACE_ACT','GENERATION_NURSERY_REVIEW_ACT');


--Additions for Design tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
case
  when abbrev = 'GENERATION_NURSERY_ADD_BLOCKS_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_ADD_BLOCKS_ACT_MOD')
  when abbrev = 'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT_MOD')
  when abbrev = 'GENERATION_NURSERY_MANAGE_BLOCKS_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_MANAGE_BLOCKS_ACT_MOD')
  when abbrev = 'GENERATION_NURSERY_OVERVIEW_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_OVERVIEW_ACT_MOD')
  else (select id from platform.module where abbrev = 'GENERATION_NURSERY_OVERVIEW_ACT_MOD') end as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_ADD_BLOCKS_ACT','GENERATION_NURSERY_ASSIGN_ENTRIES_ACT','GENERATION_NURSERY_MANAGE_BLOCKS_ACT','GENERATION_NURSERY_OVERVIEW_ACT');


--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
case
  when abbrev = 'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT_MOD')
  when abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD')
  else (select id from platform.module where abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD') end as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_PLANTING_PROTOCOL_ACT','GENERATION_NURSERY_TRAITS_PROTOCOL_ACT');
