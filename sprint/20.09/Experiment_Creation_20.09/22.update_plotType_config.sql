/**
* Update the seeding rate variable
*
"PLOT_TYPE_1R"
"PLOT_TYPE_2R"
"PLOT_TYPE_3R"
"PLOT_TYPE_4R"
"PLOT_TYPE_5R"
"PLOT_TYPE_6R"
"PLOT_TYPE_10R"
"PLOT_TYPE_20R"
"PLOT_TYPE_50R"
"PLOT_TYPE_DIRECT_SEEDED_UNKNOWN"
*/

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 1,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 0.2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_1R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 2,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_2R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 3,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 0.6,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_3R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 4,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 0.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_4R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 5,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 1,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area (sqm)",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_5R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 6,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 1.2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_6R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 10,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_10R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_20R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 50,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 10,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_50R';

update platform.config set config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}' where abbrev = 'PLOT_TYPE_DIRECT_SEEDED_UNKNOWN';
