/*
* INTENTIONAL CROSSING NURSERY CONFIG
*/

INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT_VAL', 'Cross Parent Nursery Phase I Basic Information', 
     '{
    "Name": "Required experiment level metadata variables for Cross Parent Nursery Phase I data process",
    "Values": [
        {
            "default": false,
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_TYPE"
        },
        {
            "default": "RICE",
            "disabled": true,
            "required": "required",
            "target_column": "cropDbId",
            "target_value" : "cropCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "crops",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=cropCode",
            "variable_type" : "identification",
            "variable_abbrev": "CROP"
        },
        {
            "disabled": true,
            "required": "required",
            "target_column": "programDbId",
            "target_value":"programCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "programs",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=programCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROGRAM"
        },
        {
            "default" : "EXPT-XXXX",
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_CODE"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_NAME"
        },
        {
            "default": "HB",
            "disabled": false,
            "required": "required",
            "allowed_values": [
                "HB",
                "F1"
            ],
            "target_column": "stageDbId",
            "target_value":"stageCode",
            "api_resource_method": "POST",
            "api_resource_endpoint": "stages-search",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=stageCode",
            "variable_type" : "identification",
            "variable_abbrev": "STAGE"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_YEAR"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "seasonDbId",
            "target_value":"seasonCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "seasons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=seasonCode",
            "variable_type" : "identification",
            "variable_abbrev": "SEASON"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "stewardDbId",
            "secondary_target_column": "personDbId",
            "target_value":"personName",
            "api_resource_method": "GET",
            "api_resource_endpoint": "persons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=personName",
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_STEWARD"
        },
        {
            "disabled": false,
            "target_column": "pipelineDbId",
            "target_value":"pipelineCode",
            "api_resource_method" : "GET",
            "api_resource_endpoint" : "pipelines",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=pipelineCode",
            "variable_type" : "identification",
            "variable_abbrev": "PIPELINE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_OBJECTIVE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "PLANTING_SEASON"
        },
        {
            "disabled": false,
            "allow_new_val": true,
            "target_column": "projectDbId",
            "target_value": "projectCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "projects",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=projectCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROJECT"
        },
        {
            "disabled": false,
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_SUB_TYPE"
        },
        {
            "default": "Crossing Block",
            "disabled": false,
            "allowed_values": [
                "Crossing Block"
            ],
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type": "identification",
            "variable_abbrev": "DESCRIPTION"
        }
    ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio'),

('CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_VAL', 'Cross Parent Nursery Phase I Entry List',
 '{
   "Name": "Required and default entry level metadata variables for Cross Parent Nursery Phase I data process",
   "Values": [
        {
            "disabled": false,
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "display_name": "Parent Role",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        }
    ]
 }'::json, 1, 'experiment_creation', 1,'added by j.antonio'),


('CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT_VAL', 'Cross Parent Nursery Phase I Site',
 '{
   "Name": "Required and default place metadata variables for Cross Parent Nursery Phase I data process",
   "Values": [
    {
      "required": "required",
      "target_column": "occurrenceName",
      "secondary_target_column":"",
      "target_value":"",
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "OCCURRENCE_NAME"
    },
    {
      "required": "required",
      "target_column": "siteDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"geospatialObjectName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "site"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "SITE"
    },
    {
      "target_column": "fieldDbId",
      "secondary_target_column":"geospatialObjectDbId",
      "target_value":"scaleName",
      "api_resource_method" : "POST",
      "api_resource_endpoint" : "geospatial-objects-search",
      "api_resource_filter" : {"geospatialObjectType": "field"},
      "api_resource_sort": "", 
      "variable_type" : "identification",
      "variable_abbrev": "FIELD"
    },
    {
      "disabled": false,
      "target_column": "description",
      "secondary_target_column": "",
      "target_value": "",
      "api_resource_filter" : "",
      "api_resource_sort": "", 
      "api_resource_method" : "",
      "api_resource_endpoint" : "",
      "variable_type" : "identification",
      "variable_abbrev": "DESCRIPTION"
    },
    {
      "allow_new_val": true,
      "target_column": "contactPerson",
      "secondary_target_column": "personDbId",
      "target_value":"personName",
      "api_resource_filter" : "",
      "api_resource_sort": "sort=personName",
      "api_resource_method": "GET",
      "api_resource_endpoint": "persons",
      "variable_type" : "metadata",
      "variable_abbrev": "CONTCT_PERSON_CONT"
    }
  ]
 }
'::json, 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL', 'Cross Parent Nursery Phase I Protocol variables', 
      '{
      "Name": "Required experiment level protocol variables for Cross Parent Nursery Phase I data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "ESTABLISHMENT"
          },{
              "default": false,
              "variable_abbrev": "PLANTING_TYPE",
              "disabled": false
          },{
              "variable_abbrev": "PLOT_TYPE",
              "required": "required",
              "disabled": false
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');
