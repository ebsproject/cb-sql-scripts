INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_CROP_BULK_HARVEST_CODE',
        'Harvest Method Code for Bulk',
$$			
{
	"rice": {
		"default": "B"
	},
	"maize": {
		"default": "B"
	},

	"wheat": {

		"default": "0",
		"no_plant_default": "99",
		"F1TOP": "TOP"
	}
}
$$,
        1,
        'harvest_manager',
        1,
        'B4R 6725 update config - a.caneda 2020-09-07')


