UPDATE platform.space
	SET menu_data='{
    "left_menu_items": [
        {
            "name": "experiment-creation",
            "label": "Experiment creation",
            "appAbbrev": "EXPERIMENT_CREATION"
        },
        {
            "name": "experiment-manager",
            "label": "Experiment manager",
            "appAbbrev": "OCCURRENCES"
        },
        {
            "name": "data-collection-qc",
            "items": [
                {
                    "name": "data-collection-qc-quality-control",
                    "label": "Quality control",
                    "appAbbrev": "QUALITY_CONTROL"
                }
            ],
            "label": "Data collection & QC"
        },
        {
            "name": "seeds",
            "items": [
                {
                    "name": "seeds-find-seeds",
                    "label": "Find seeds",
                    "appAbbrev": "FIND_SEEDS"
                },
                {
                    "name": "seeds-harvest-manager",
                    "label": "Harvest manager",
                    "appAbbrev": "HARVEST_MANAGER"
                }
            ],
            "label": "Seeds"
        }
    ],
    "main_menu_items": [
        {
            "icon": "folder_special",
            "name": "data-management",
            "items": [
                {
                    "name": "data-management_germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                },
                {
                    "name": "data-management_traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }
            ],
            "label": "Data management"
        }
    ]
}'
	WHERE abbrev='DEFAULT';