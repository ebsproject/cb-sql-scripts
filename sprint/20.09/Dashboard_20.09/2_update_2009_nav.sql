-- update navigation items
update platform.space set menu_data = '
{
"left_menu_items": [{
      "name": "experiment-creation",
      "label": "Experiment creation",
      "appAbbrev": "EXPERIMENT_CREATION"
    }, {
      "name": "experiment-manager",
      "label": "Experiment manager",
      "appAbbrev": "OCCURRENCES"
    }, {
		"name": "data-collection-qc-quality-control",
		"label": "Data collection",
		"appAbbrev": "QUALITY_CONTROL"	
	}, {
		"name": "seeds-harvest-manager",
		"label": "Harvest manager",
		"appAbbrev": "HARVEST_MANAGER"
	},{
		"name": "search",
		"items": [
		{
			"name": "searchs-germplasm",
			"label": "Germplasm",
			"appAbbrev": "GERMPLASM_CATALOG"
		},{
			"name": "search-seeds",
			"label": "Seeds",
			"appAbbrev": "FIND_SEEDS"
		},{
			"name": "search-traits",
			"label": "Traits",
			"appAbbrev": "TRAITS"
		}],
		"label": "Search"
	}
]
}' where abbrev = 'DEFAULT';

update platform.space set menu_data = '
{
	"left_menu_items": [{
		"name": "experiment-creation",
		"label": "Experiment creation",
		"appAbbrev": "EXPERIMENT_CREATION"
	}, {
		"name": "experiment-manager",
		"label": "Experiment manager",
		"appAbbrev": "OCCURRENCES"
	}, {
		"name": "data-collection-qc-quality-control",
		"label": "Data collection",
		"appAbbrev": "QUALITY_CONTROL"
	}, {
		"name": "seeds-harvest-manager",
		"label": "Harvest manager",
		"appAbbrev": "HARVEST_MANAGER"
	}, {
		"name": "search",
		"items": [{
			"name": "searchs-germplasm",
			"label": "Germplasm",
			"appAbbrev": "GERMPLASM_CATALOG"
		}, {
			"name": "search-seeds",
			"label": "Seeds",
			"appAbbrev": "FIND_SEEDS"
		}, {
			"name": "search-traits",
			"label": "Traits",
			"appAbbrev": "TRAITS"
		}],
		"label": "Search"
	}],
	"main_menu_items": [{
		"icon": "folder_special",
		"name": "data-management",
		"items": [{
			"name": "data-management_seasons",
			"label": "Seasons",
			"appAbbrev": "MANAGE_SEASONS"
		}],
		"label": "Data management"
	}, {
		"icon": "settings",
		"name": "administration",
		"items": [{
			"name": "administration_users",
			"label": "Persons",
			"appAbbrev": "PERSONS"
		}],
		"label": "Administration"
	}]
}' where abbrev = 'ADMIN';

-- update label of Quality Control and Find Seeds
update platform.application set label = 'Data Collection', action_label = 'Data Collection' where abbrev = 'QUALITY_CONTROL';

update platform.application set label = 'Seeds' where abbrev = 'FIND_SEEDS';
