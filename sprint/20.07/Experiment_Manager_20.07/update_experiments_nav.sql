-- Update navigation for Experiment creation and Experiment manager

update 
	platform.space 
set 
	menu_data = 
	'{
		"left_menu_items": [{
				"name": "experiment-creation",
				"label": "Experiment creation",
				"appAbbrev": "EXPERIMENT_CREATION"
			},
			{
				"name": "experiment-manager",
				"label": "Experiment manager",
				"appAbbrev": "OCCURRENCES"
			}, {
				"name": "data-collection-qc",
				"items": [{
					"name": "data-collection-qc-quality-control",
					"label": "Quality control",
					"appAbbrev": "QUALITY_CONTROL"
				}],
				"label": "Data collection & QC"
			}, {
				"name": "seeds",
				"items": [{
					"name": "seeds-find-seeds",
					"label": "Find seeds",
					"appAbbrev": "FIND_SEEDS"
				}, {
					"name": "seeds-harvest-manager",
					"label": "Harvest manager",
					"appAbbrev": "HARVEST_MANAGER"
				}],
				"label": "Seeds"
			}
		],
		"main_menu_items": [{
			"icon": "folder_special",
			"name": "data-management",
			"items": [{
				"name": "data-management_seasons",
				"label": "Seasons",
				"appAbbrev": "MANAGE_SEASONS"
			}, {
				"name": "data-management_germplasm",
				"label": "Germplasm",
				"appAbbrev": "GERMPLASM_CATALOG"
			}, {
				"name": "data-management_traits",
				"label": "Traits",
				"appAbbrev": "TRAITS"
			}],
			"label": "Data management"
		}, {
			"icon": "settings",
			"name": "administration",
			"items": [{
				"name": "administration_users",
				"label": "Persons",
				"appAbbrev": "PERSONS"
			}],
			"label": "Administration"
		}]
	}'
where
	abbrev = 'ADMIN';


update 
	platform.space 
set 
	menu_data = 
'{
	"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiment creation",
			"appAbbrev": "EXPERIMENT_CREATION"
		},
		{
			"name": "experiment-manager",
			"label": "Experiment manager",
			"appAbbrev": "OCCURRENCES"
		}, {
			"name": "data-collection-qc",
			"items": [{
				"name": "data-collection-qc-quality-control",
				"label": "Quality control",
				"appAbbrev": "QUALITY_CONTROL"
			}],
			"label": "Data collection & QC"
		}, {
			"name": "seeds",
			"items": [{
				"name": "seeds-find-seeds",
				"label": "Find seeds",
				"appAbbrev": "FIND_SEEDS"
			}, {
				"name": "seeds-harvest-manager",
				"label": "Harvest manager",
				"appAbbrev": "HARVEST_MANAGER"
			}],
			"label": "Seeds"
		}
	]
}'
where
	abbrev = 'DEFAULT';

-- Update tool labels
update platform.application set label = 'Experiment creation', action_label = 'Create Experiments' where abbrev = 'EXPERIMENT_CREATION';

update platform.application set label = 'Persons', action_label = 'Manage Persons' where abbrev = 'PERSONS';

update platform.application set label = 'Experiment manager', action_label = 'Manage Experiments' where abbrev = 'OCCURRENCES';