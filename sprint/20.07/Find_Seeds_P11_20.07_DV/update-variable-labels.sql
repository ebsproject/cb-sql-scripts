UPDATE master.variable
	SET label = 'YEAR'
WHERE
	abbrev = 'YEAR';
	
UPDATE master.variable
	SET label = 'SEASON'
WHERE
	abbrev = 'SEASON';

UPDATE master.variable
	SET label = 'STAGE'
WHERE
	abbrev = 'EVALUATION_STAGE';

	
UPDATE master.variable
	SET label = 'EXPERIMENT'
WHERE
	abbrev = 'EXPERIMENT_NAME';

UPDATE master.variable
	SET label = 'PLOT NO.'
WHERE
	abbrev = 'PLOTNO';

UPDATE master.variable
	SET label = 'ENTRY NO.'
WHERE
	abbrev = 'ENTNO';

UPDATE master.variable
	SET label = 'ENTRY CODE'
WHERE
	abbrev = 'ENTCODE';
	
UPDATE master.variable
	SET label = 'PACKAGE QTY.'
WHERE
	abbrev = 'VOLUME';
	
UPDATE master.variable
	SET label = 'GERMPLASM'
WHERE
	abbrev = 'DESIGNATION';

	
UPDATE master.variable
	SET label = 'SEED'
WHERE
	abbrev = 'GID';
