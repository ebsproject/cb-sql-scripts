CREATE OR REPLACE FUNCTION operational_data_terminal.terminal_commit(var_transaction_id integer, var_study_id integer, var_user_id integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
DECLARE
r_data_unit RECORD;	

BEGIN
    -- start looping through the data unit in the transaction
    for r_data_unit in
    SELECT t.data_unit from operational_data_terminal.terminal t where t.transaction_id = var_transaction_id group by t.data_unit
    LOOP
    -- start check if the record is already in the operational, is_committed_operational = TRUE, alse is_committed_operational = FALSE
    IF r_data_unit.data_unit = 'entry_metadata' THEN 
        
        update operational.entry_metadata pd 
        set 
            is_void=(
                case 
                    when (tx.value is not null and (tx.value<>pd.value)) OR (tx.value is null and pd.value is not null) then true
                    else false
                end),
            is_suppress =tx.is_suppress,
            suppress_remarks =tx.suppress_remarks
        from
            operational.entry p,
            operational_data_terminal.terminal tx 
        where 
            p.study_id = var_study_id 
            and p.key=tx.key 
            and p.is_void = false 
            and pd.is_void = false
            and pd.entry_id=p.id             
            and pd.variable_id=tx.variable_id 
            and tx.transaction_id = var_transaction_id 
            and tx.data_unit='entry_metadata' 
            and tx.is_data_type_valid=TRUE
            and tx.is_data_value_valid=TRUE
            ;

        insert into operational.entry_metadata (study_id,entry_id,variable_id,value,creation_timestamp,creator_id,notes,remarks,is_suppress,suppress_remarks, transaction_id)
            select 
                p.study_id,
                p.id,
                tx.variable_id,
                tx.value,
                now(),
                var_user_id,
                'Created via B4R Data Collection',
                tx.remarks,
                tx.is_suppress,
                tx.suppress_remarks,
                json_build_object('insert',tx.transaction_id)
            from 
                operational.entry p,
                operational_data_terminal.terminal tx
            where 
                tx.transaction_id=var_transaction_id 
                AND p.key=tx.key 
                AND tx.data_unit='entry_metadata' 
                AND p.study_id=var_study_id 
                AND p.is_void=false
                AND tx.is_data_type_valid=TRUE
                AND tx.is_data_value_valid=TRUE
                AND tx.is_void=FALSE
                AND tx.value is not null
                AND not exists(
                    select 1 
                    from 
                        operational.entry_metadata pd 
                    where
                        pd.entry_id=p.id
                        AND p.key=tx.key 
                        AND pd.variable_id=tx.variable_id
                        AND tx.value =pd.value
                        AND pd.is_void =FALSE
                );

    ELSIF r_data_unit.data_unit = 'study_metadata' THEN

        update operational.study_metadata pd 
        set 
            is_void=(
                case 
                    when (tx.value is not null and (tx.value<>pd.value)) OR (tx.value is null and pd.value is not null) then true
                    else false
                end),
            is_suppress =tx.is_suppress,
            suppress_remarks =tx.suppress_remarks
        from
            operational_data_terminal.terminal tx 
        where 
            pd.is_void = false
            and pd.study_id=var_study_id             
            and pd.variable_id=tx.variable_id 
            and tx.transaction_id = var_transaction_id 
            and tx.data_unit='study_metadata' 
            and tx.is_data_type_valid=TRUE
            and tx.is_data_value_valid=TRUE;

        insert into operational.study_metadata (study_id,variable_id,value,creation_timestamp,creator_id,notes,remarks,is_suppress,suppress_remarks, transaction_id)

            select 
                var_study_id as study_id,
                tx.variable_id,
                tx.value,
                now(),
                var_user_id,
                'Created via B4R Data Collection',
                tx.remarks,
                tx.is_suppress,
                tx.suppress_remarks,
                json_build_object('insert',tx.transaction_id)
            from 
                operational_data_terminal.terminal tx
            where 
                tx.transaction_id=var_transaction_id 
                AND tx.data_unit='study_metadata'
                AND tx.is_data_type_valid=TRUE
                AND tx.is_data_value_valid=TRUE
                AND tx.is_void=FALSE
                AND tx.value is not null
                AND not exists(
                    select 1 
                    from 
                        operational.study_metadata pd 
                    where
                        pd.study_id=var_study_id
                        AND pd.variable_id=tx.variable_id
                        AND tx.value =pd.value
                        AND pd.is_void =FALSE
                );


    ELSIF r_data_unit.data_unit = 'entry_data' THEN
    
        update operational.entry_data pd 
        set 
            is_void=(
                case 
                    when (tx.value is not null and (tx.value<>pd.value)) OR (tx.value is null and pd.value is not null) then true
                    else false
                end),
            is_suppress =tx.is_suppress,
            suppress_remarks =tx.suppress_remarks
        from
            operational.entry p,
            operational_data_terminal.terminal tx 
        where 
            p.study_id = var_study_id 
            and p.key=tx.key 
            and p.is_void = false 
            and pd.is_void = false
            and pd.entry_id=p.id             
            and pd.variable_id=tx.variable_id 
            and tx.transaction_id = var_transaction_id 
            and tx.data_unit='entry_data' 
            and tx.is_data_type_valid=TRUE
            and tx.is_data_value_valid=TRUE;

        insert into operational.entry_data (study_id,entry_id,variable_id,value,creation_timestamp,creator_id,notes,remarks,is_suppress,suppress_remarks, transaction_id)

            select 
                p.study_id,
                p.id,
                tx.variable_id,
                tx.value,
                now(),
                var_user_id,
                'Created via B4R Data Collection',
                tx.remarks,
                tx.is_suppress,
                tx.suppress_remarks,
                json_build_object('insert',tx.transaction_id)
            from 
                operational.entry p,
                operational_data_terminal.terminal tx
            where 
                tx.transaction_id=var_transaction_id 
                AND p.key=tx.key
                AND tx.data_unit='entry_data' 
                AND p.study_id=var_study_id 
                AND p.is_void=false
                AND tx.is_data_type_valid=TRUE
                AND tx.is_data_value_valid=TRUE
                AND tx.is_void=FALSE
                AND tx.value is not null
                AND not exists(
                    select 1 
                    from 
                        operational.entry_data pd 
                    where
                        pd.entry_id=p.id
                        AND p.key=tx.key 
                        AND pd.variable_id=tx.variable_id
                        AND tx.value =pd.value
                        AND pd.is_void =FALSE
                );


    ELSIF r_data_unit.data_unit = 'plot_data' THEN
        update operational.plot_data pd 
        set 
            is_void=(
                case 
                    when (tx.value is not null and (tx.value<>pd.value)) OR (tx.value is null and pd.value is not null) then true
                    else false
                end),
            is_suppress =tx.is_suppress,
            suppress_remarks =tx.suppress_remarks
        from
            operational.plot p,
            operational_data_terminal.terminal tx 
        where 
            p.study_id = var_study_id 
            and p.key=tx.key 
            and p.is_void = false 
            and pd.is_void = false
            and pd.plot_id=p.id             
            and pd.variable_id=tx.variable_id 
            and tx.transaction_id = var_transaction_id 
            and tx.data_unit='plot_data' 
            and tx.is_data_type_valid=TRUE
            and tx.is_data_value_valid=TRUE;

        insert into operational.plot_data (study_id,entry_id,plot_id,variable_id,value,creation_timestamp,creator_id,notes, remarks,is_suppress,suppress_remarks, transaction_id)

            select 
                p.study_id,
                p.entry_id,
                p.id,
                tx.variable_id,
                tx.value,
                now(),
                var_user_id,
                'Created via B4R Data Collection',
                tx.remarks,
                tx.is_suppress,
                tx.suppress_remarks,
                json_build_object('insert',tx.transaction_id)
            from 
                operational.plot p,
                operational_data_terminal.terminal tx
            where 
                tx.transaction_id=var_transaction_id 
                AND p.key=tx.key
                AND tx.data_unit='plot_data' 
                AND p.study_id=var_study_id 
                AND p.is_void=false
                AND tx.is_data_type_valid=TRUE
                AND tx.is_data_value_valid=TRUE
                AND tx.is_void=FALSE
                AND tx.value is not null
                AND not exists(
                    select 1 
                    from 
                        operational.plot_data pd 
                    where
                        pd.plot_id=p.id
                        AND p.key=tx.key 
                        AND pd.variable_id=tx.variable_id
                        AND tx.value =pd.value
                        AND pd.is_void =FALSE
                );

    ELSIF r_data_unit.data_unit = 'plot_metadata' THEN
        update operational.plot_metadata pd 
        set 
            is_void=(
                case 
                    when (tx.value is not null and (tx.value<>pd.value)) OR (tx.value is null and pd.value is not null) then true
                    else false
                end),
            is_suppress =tx.is_suppress,
            suppress_remarks =tx.suppress_remarks
        from
            operational.plot p,
            operational_data_terminal.terminal tx 
        where 
            p.study_id = var_study_id 
            and p.key=tx.key 
            and p.is_void = false 
            and pd.is_void = false
            and pd.plot_id=p.id             
            and pd.variable_id=tx.variable_id 
            and tx.transaction_id = var_transaction_id 
            and tx.data_unit='plot_metadata' 
            and tx.is_data_type_valid=TRUE
            and tx.is_data_value_valid=TRUE
            ;

        insert into operational.plot_metadata (study_id,entry_id,plot_id,variable_id,value,creation_timestamp,creator_id,notes,remarks,is_suppress,suppress_remarks, transaction_id)
            select 
            p.study_id,
            p.entry_id,
            p.id,
            tx.variable_id,
            tx.value,
            now(),
            var_user_id,
            'Created via B4R Data Collection',
            tx.remarks,
            tx.is_suppress,
            tx.suppress_remarks,
            json_build_object('insert',tx.transaction_id)
        
        from 
            operational.plot p,
            operational_data_terminal.terminal tx
        where 
            tx.transaction_id=var_transaction_id 
            AND p.key=tx.key 
            AND tx.data_unit='plot_metadata' 
            AND p.study_id=var_study_id 
            AND p.is_void=false
            AND tx.is_data_type_valid=TRUE
            AND tx.is_data_value_valid=TRUE
            AND tx.is_void=FALSE
            AND tx.value is not null
            and not exists(
                select 1 
				from 
				    operational.plot_metadata pd 
				where
                    pd.plot_id=p.id
                    AND p.key=tx.key 
                    AND pd.variable_id=tx.variable_id
                    AND tx.value =pd.value
                    AND pd.is_void =FALSE

            );

    -- end check if the record is already in the operational, is_committed_operational = TRUE, alse is_committed_operational = FALSE	
    END IF;
-- end looping through the data unit in the transaction	
END LOOP;
RETURN 'success';

END;	
$function$
;
