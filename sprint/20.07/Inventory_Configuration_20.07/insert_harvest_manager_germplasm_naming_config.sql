INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVEST_MANAGER_GERMPLASM_NAMING_CONVENTION',
        'Harvest Manager Germplasm Naming Convention',
$$			
{
  "F1": {
    "harvest_method": "Single Seed numbering",
    "germplasm_state": "progeny",
    "delimeter": ":",
    "pattern": "{designation}{delimeter}{sequence}"
  },
  "F2-Fn": {
    "Bulk": {
      "germplasm_state": "progeny",
      "bulk_code": "B",
      "delimeter": "-",
      "pattern": "{designation}{delimeter}{bulk_code}"
    },
    "Bulk fixedline": {
      "harvest_method": "Bulk fixedline",
      "germplasm_state": "progeny",
      "bulk_code": "",
      "delimeter": "",
      "pattern": "{designation}"
    },
    "Modified Bulk": {
      "germplasm_state": "progeny",
      "bulk_code": "",
      "delimeter": "",
      "pattern": "{designation}"
    },
    "Plant Specific": {
      "germplasm_state": "progeny",
      "delimeter": "-",
      "pattern": "{designation}{delimeter}{sequence}"
    },
    "Plant Specific Bulk": {
      "germplasm_state": "progeny",
      "delimeter_specific": "-",
      "pattern_specific": "{designation}{delimeter}{sequence}",
      "bulk_code": "B",
      "delimeter_bulk": "-",
      "pattern_bulk": "{designation}{delimeter}{bulk_code}"
    },
    "Single Plant Selection": {
      "germplasm_state": "progeny",
      "delimeter": "-",
      "pattern_specific": "{designation}{delimeter}{sequence}"
    },
    "Single Plant Selection and Bulk": {
      "germplasm_state": "progeny",
      "delimeter_specific": "-",
      "pattern_specific": "{designation}{delimeter_specific}{sequence}",
      "bulk_code": "B",
      "delimeter_bulk": "-",
      "pattern_bulk": "{designation}{delimeter}{bulk_code}"
    },
    "Panicle Selection": {
      "germplasm_state": "progeny",
      "delimeter_specific": "-",
      "pattern_specific": "{designation}{delimeter_specific}{sequence}"
    }
  }
}
$$,
        1,
        'harvest_manager',
        1,
        'B4R 6729 create config - a.caneda 2020-07-23')
