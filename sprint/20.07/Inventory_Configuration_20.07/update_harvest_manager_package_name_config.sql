UPDATE
    platform.config
SET
    config_value = $$			
		
		{ "pattern": [
       {
        "type":"field",
        "reference_data_session":"program",
        "reference_data_key":"programCode",
        "order_number":0
       },
       {
        "type":"delimeter",
        "value":"-",
        "order_number":1
       },
       {
        "type":"free_text",
        "value":"abc",
        "order_number":2
       },
       {
        "type":"delimeter",
        "value":"-",
        "order_number":3
       },
	   {
        "type":"field",
        "reference_data_session":"experiment",
        "reference_data_key":"experimentYear",
        "order_number":4
       },
	   {
        "type":"counter",
        "reset_per_year":"false",
	      "trailing_zero":"2",		
        "order_number":5
       }
	 ] 
	}
$$
WHERE
    abbrev = 'HARVEST_MANAGER_PACKAGE_PATTERN_DEFAULT';
