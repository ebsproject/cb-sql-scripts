INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVEST_MANAGER_PACKAGE_PATTERN_DEFAULT',
        'Harvest Manager Package Pattern Default',
$$			
		
		{ "pattern": [
       {
        "type":"field",
        "reference_data_session":"program",
        "reference_data_key":"program_abbrev",
        "order_number":0
       },
       {
        "type":"delimeter",
        "value":"-",
        "order_number":1
       },
       {
        "type":"free_text",
        "value":"abc",
        "order_number":2
       },
       {
        "type":"delimeter",
        "value":"-",
        "order_number":3
       },
	   {
        "type":"field",
        "reference_data_session":"experiment",
        "reference_data_key":"experiment_year",
        "order_number":4
       },
	   {
        "type":"counter",
        "reset_per_year":"false",
	      "trailing_zero":"2",		
        "order_number":5
       }
	 ] 
	}
$$,
        1,
        'harvest_manager',
        1,
        'B4R 6726 create config - a.caneda 2020-07-23')