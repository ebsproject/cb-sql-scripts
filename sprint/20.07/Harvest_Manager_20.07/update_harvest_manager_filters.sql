UPDATE
	platform.config
SET
	config_value = $$
	
	{
		"name": "Harvest Manager Filters",
		"values": [
			{
				"disabled": "true",
				"required": "false",
				"input_type": "single",
				"field_label": "Program",
				"input_field": "selection",
				"order_number": "1",
				"default_value": "",
				"allowed_values": "",
				"basic_parameter": "true",
				"variable_abbrev": "PROGRAM",
				"field_description": "Program that manages the experiment"
			},
			{
				"disabled": "false",
				"required": "false",
				"input_type": "single",
				"field_label": "Experiment",
				"input_field": "selection",
				"order_number": "3",
				"default_value": "",
				"allowed_values": "",
				"basic_parameter": "true",
				"variable_abbrev": "EXPERIMENT_NAME",
				"field_description": "Name of the experiment"
			},
			{
				"disabled": "false",
				"required": "true",
				"input_type": "single",
				"field_label": "Location Code",
				"input_field": "selection",
				"order_number": "5",
				"default_value": "",
				"allowed_values": "",
				"basic_parameter": "true",
				"variable_abbrev": "LOCATION_NAME",
				"field_description": "Location Code"
			}
		]
	}
	
	$$
 WHERE
 	abbrev = 'HARVEST_MANAGER_FILTERS';
