INSERT INTO
    master.scale_value (
        scale_id,
        value,
        order_number,
        description,
        display_name,
        creation_timestamp,
        creator_id,
        is_void,
        scale_value_status,
        abbrev,
        notes
    )
VALUES
    (
        540,
        'Single Seed Descent',
        9,
        'Single Seed Descent method',
        'Single Seed Descent',
        now(),
        467,
        false,
        'show',
        'HV_METH_DISC_SINGLE_SEED_DESCENT',
        'added by j.bantay 2020-07-30')