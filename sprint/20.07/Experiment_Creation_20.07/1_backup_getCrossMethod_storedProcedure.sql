-- FUNCTION: operational.get_cross_method(json, integer, integer)

-- DROP FUNCTION operational.get_cross_method(json, integer, integer);

CREATE OR REPLACE FUNCTION operational.get_cross_method(
	var_parent_list json,
	experiment_id_val integer,
	user_id integer)
    RETURNS character varying
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
DECLARE
  crossListArr integer[];
  crossesArr text[];
  female varchar;
  male varchar;
  femaleGen varchar;
  maleGen varchar;
  femaleInfo RECORD;
  maleInfo RECORD;
  cross_method varchar;
  i json;
  
  crossing_method_id integer;
  crossno integer;
  var_scale_id integer;
  var_female_gen_pos integer;
  var_male_gen_pos integer;
  var_female_gen_proc varchar;
  var_male_gen_proc varchar;
  var_f1_parent_product_id integer;
  var_fn_parent_product_id integer;
  var_f1_parents_product_ids integer[];
  var_f1_parent_gids integer[];
  var_fn_parent_gids integer[];
  var_f1_parent_gpids integer[];
  var_f1_parent_gpid1s integer[];
  var_f1_parent_gpid2s integer[];
  var_f1_parent_id integer;
  var_fn_parent_id integer;
  var_is_backcross boolean;
  scaleValueInfo RECORD;
  scale_value_id integer;
  record_exists boolean;
  crossing_list_id integer;
  cross_id_list varchar;
  query_string varchar;
  temp_cross_id integer;
  fGenerationArray text[];
  cross_method_val varchar;

BEGIN
  fGenerationArray := ARRAY['F1','F2','F3','F4','F5','F6','F7','F8','F9','F10','F11','F12'];

  crossno = 1;
  FOR i IN SELECT * FROM json_array_elements(var_parent_list) LOOP
    record_exists = false;
    female := i->>'femaleEntryId';
    male := i->>'maleEntryId';

    if(female is null)
    then
      female := i->>'femaleentryid';
    end if;

    if(male is null)
    then
      male := i->>'maleentryid';
    end if;

    SELECT 
        el.id,
        mp.generation
    into femaleInfo
      FROM 
        master.product mp,
        operational.entry_list el
      WHERE
        el.id = female::integer
        and mp.id = el.product_id
        and el.is_void = false
        and mp.is_void = false
     limit 1;

    SELECT 
        el.id,
        mp.generation
    into maleInfo
      FROM 
        master.product mp,
        operational.entry_list el
      WHERE
        el.id = male::integer
        and mp.id = el.product_id
        and el.is_void = false
        and mp.is_void = false
    limit 1;

    femaleGen := femaleInfo.generation;
    maleGen := maleInfo.generation;

    
    select position('F' in femaleGen) into var_female_gen_pos;
    select position('F' in maleGen) into var_male_gen_pos;

    select regexp_replace(substring(femaleGen from var_female_gen_pos for 3), '[^\w]+','') into var_female_gen_proc;  
    select regexp_replace(substring(maleGen from var_male_gen_pos for 3), '[^\w]+','') into var_male_gen_proc;

    femaleGen := var_female_gen_proc;
    maleGen := var_male_gen_proc;
    cross_method := null;
    var_f1_parent_id := null;
    var_fn_parent_id := null;

    
    
    SELECT scale_id into var_scale_id from master.variable where abbrev = 'CROSS_METHOD' and is_void = false;
	
	IF (female <> male)
	THEN 
    
    IF (femaleGen = 'F1' AND ((maleGen != 'F1') AND (SELECT fGenerationArray @> ARRAY[maleGen::text]) = true))
    THEN
      
      var_f1_parent_id := femaleInfo.id;
      var_fn_parent_id := maleInfo.id;      

		ELSIF (maleGen = 'F1' AND ((femaleGen != 'F1') AND (SELECT fGenerationArray @> ARRAY[femaleGen::text]) = true))
    THEN
      
      var_f1_parent_id := maleInfo.id;
      var_fn_parent_id := femaleInfo.id;

    END IF;

    
    IF(var_f1_parent_id is not null and var_fn_parent_id is not null) THEN
      
      select product_id into var_f1_parent_product_id from operational.entry_list where id = var_f1_parent_id;
      
      select product_id into var_fn_parent_product_id from operational.entry_list where id = var_fn_parent_id;

      
      select 
        ARRAY[female.product_id::integer, male.product_id::integer] into var_f1_parents_product_ids
      from 
        operational.cross c
      left join operational.entry female
        on c.female_entry_id = female.id
      left join operational.entry male
        on c.male_entry_id = male.id
      where 
        c.product_id = var_f1_parent_product_id;

      
      IF(var_f1_parents_product_ids is not null) THEN 
        
        
        select var_fn_parent_product_id = ANY (var_f1_parents_product_ids) into var_is_backcross;

        if(var_is_backcross) then
          cross_method := 'BC'; 
        END IF;
      ELSE
        

        
        select array_agg(gid::INTEGER) into var_f1_parent_gids from master.product_gid where product_id = var_f1_parent_product_id and is_void = false;
        
        select array_agg(gid::INTEGER) into var_fn_parent_gids from master.product_gid where product_id = var_fn_parent_product_id and is_void = false;

        
        select
          array_agg(g.gpid1) into var_f1_parent_gpid1s
        from 
          master.product_gid pg
        left join gms.germplsm g
        on 
          g.gid = pg.gid
        where
          pg.product_id = var_f1_parent_product_id;

        select
          array_agg(g.gpid2) into var_f1_parent_gpid2s
        from 
          master.product_gid pg
        left join gms.germplsm g
        on
          g.gid = pg.gid
        where
          pg.product_id = var_f1_parent_product_id;

        
        select array_cat(var_f1_parent_gpid1s, var_f1_parent_gpid2s) into var_f1_parent_gpids;

        

        select var_fn_parent_gids && var_f1_parent_gpids into var_is_backcross;
        
        IF(var_is_backcross) THEN
          cross_method := 'BC'; 
        END IF;   
      END IF;
    END IF;
	END IF;

    
    IF(cross_method is null) THEN
	  IF (female = male)
	  THEN 
	  	 cross_method := 'SLF'; 
      ELSIF ((femaleGen != 'F1' AND (SELECT fGenerationArray @> ARRAY[femaleGen::text]) = true) AND (maleGen != 'F1' AND (SELECT fGenerationArray @> ARRAY[maleGen::text]) = true))
      THEN
        cross_method := 'SC'; 
      ELSIF (femaleGen = 'F1' AND maleGen = 'F1')
      THEN
        cross_method := 'C2W'; 
      ELSIF (femaleGen = 'F1' AND (maleGen != 'F1' AND (SELECT fGenerationArray @> ARRAY[maleGen::text]) = true))
      THEN
        cross_method := 'C3W'; 
      ELSE
        cross_method := null;
      END IF;
    END IF;

    raise notice 'method: %',cross_method;
	
    
    
	
	cross_method_val := cross_method;
	SELECT id into scaleValueInfo FROM master.cross_method where is_void = false and abbrev ilike '%'||cross_method_val||'%';
    
    
    SELECT id INTO crossing_list_id FROM operational.crossing_list where experiment_id = experiment_id_val and female_entry_id = femaleInfo.id and male_entry_id = maleInfo.id and is_void = false;

    IF (crossing_list_id is not null) THEN 
      record_exists := true;
    ELSE 
      record_exists := false;
    END IF;

    
    IF (record_exists = false)
    THEN 
	  raise notice 'Cross No: %',crossno;
      IF (cross_method is not null) 
      THEN
        INSERT into operational.crossing_list (experiment_id,female_entry_id,male_entry_id,crossing_method_id,crossno,is_method_autofilled,creator_id)
      VALUES (experiment_id_val,femaleInfo.id,maleInfo.id,scaleValueInfo.id,crossno,true,user_id);
      ELSE
        INSERT into operational.crossing_list (experiment_id,female_entry_id,male_entry_id,crossing_method_id,crossno,is_method_autofilled,creator_id)
      VALUES (experiment_id_val,femaleInfo.id,maleInfo.id,scaleValueInfo.id,crossno,false,user_id);
	  
    END IF; 
    crossno := crossno+1;
    END IF;
  END LOOP;
  RETURN 'SUCCESS';

END; $BODY$;

ALTER FUNCTION operational.get_cross_method(json, integer, integer)
    OWNER TO postgres;
