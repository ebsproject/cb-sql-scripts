
--EXPT_TRIAL_ENTRY_LIST_ACT_VAL
update platform.config set config_value = '
{
    "Name": "Required and default entry level metadata variables for trial data process",
    "Values": [{
            "default": "entry",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_TYPE",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "allowed_values": [
                "ENTRY_TYPE_CHECK",
                "ENTRY_TYPE_ENTRY"
            ]
        },
        {
            "disabled": false,
            "variable_abbrev": "ENTRY_CLASS",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        }
    ]
}
' where abbrev = 'EXPT_TRIAL_ENTRY_LIST_ACT_VAL';

update platform.config set config_value = '
{
	"Name": "Required and default entry level metadata variables for IRRI Trial data process",
	"Values": [
		{
			"default": "entry",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "ENTRY_TYPE",
			"target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "allowed_values": [
                "ENTRY_TYPE_CHECK",
                "ENTRY_TYPE_ENTRY"
            ]
		},
		{
            "disabled": false,
            "variable_abbrev": "ENTRY_CLASS",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        }
	]
}
' where abbrev = 'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_VAL';
