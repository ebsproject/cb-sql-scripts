--Create the Cross-Parent Template Process Path
insert into master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage) 
values('EXPT_CROSS_PARENT_DATA_PROCESS','Cross-Parent Experiment',40,'Parent role (Female or Male) is unknown before planting and Cross List is prepared with assigned Parent Role only before making crosses.','Cross-Parent Experiment',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');

--create specify basic information
--Basic, Parent list, Design, Protocols, Place, Review.
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

--create Specify parent list
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_ENTRY_LIST_ACT','Specify Parent List',30,'Specify Parent List','Parent List',1,'active','fa fa-list-ol');

--create Specify crosses
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_CROSSES_ACT','Crosses',30,'Crosses','Crosses',1,'active','fa fa-random');

--additionals for crosses
--create matrix and manage
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_MATRIX_ACT','Matrix',20,'Matrix','Matrix',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_MANAGE_ACT','Manage',20,'Manage','Manage',1,'active','fa fa-random');


--create protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_PROTOCOLS_ACT','Protocols',30,'Protocols','Protocols',1,'active','fa fa-files-o');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT','Planting Protocol',20,'Planting Protocol','Planting',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT','Traits Protocol',20,'Traits Protocol','Traits',1,'active','fa fa-random');



--create Specify locations
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker');

--create Preview and confirm
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_CROSS_PARENT_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');


--insert the sequence of activities for the cross post-planning the data process
insert into master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select 
  (select id from master.item where abbrev='EXPT_CROSS_PARENT_DATA_PROCESS') as root_id,
  (select id from master.item where abbrev='EXPT_CROSS_PARENT_DATA_PROCESS') as parent_id,
   id as child_id,
   case 
     when abbrev = 'EXPT_CROSS_PARENT_BASIC_INFO_ACT' then 1
     when abbrev = 'EXPT_CROSS_PARENT_ENTRY_LIST_ACT' then 2
	   when abbrev = 'EXPT_CROSS_PARENT_CROSSES_ACT' then 3
     when abbrev = 'EXPT_CROSS_PARENT_PROTOCOLS_ACT' then 4
     when abbrev = 'EXPT_CROSS_PARENT_PLACE_ACT' then 5
     when abbrev = 'EXPT_CROSS_PARENT_REVIEW_ACT' then 6
     else 7 end as order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
from 
   master.item
where 
   abbrev in ('EXPT_CROSS_PARENT_BASIC_INFO_ACT','EXPT_CROSS_PARENT_ENTRY_LIST_ACT','EXPT_CROSS_PARENT_CROSSES_ACT','EXPT_CROSS_PARENT_PROTOCOLS_ACT','EXPT_CROSS_PARENT_PLACE_ACT','EXPT_CROSS_PARENT_REVIEW_ACT');


--Additions for crosses
--insert Specify Parents, Specify Crosses and Preview and Confirm in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_CROSS_PARENT_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_CROSS_PARENT_CROSSES_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_CROSS_PARENT_MATRIX_ACT' then 1
		when abbrev = 'EXPT_CROSS_PARENT_MANAGE_ACT'then 2
		else 3 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_CROSS_PARENT_MATRIX_ACT','EXPT_CROSS_PARENT_MANAGE_ACT');
;



--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_CROSS_PARENT_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_CROSS_PARENT_PROTOCOLS_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT' then 1
		when abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT'then 2
		else 3 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT','EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT');
;



--insert the activities in the platform module
insert into platform.module(abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
values 
  ('EXPT_CROSS_PARENT_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
  ('EXPT_CROSS_PARENT_ENTRY_LIST_ACT_MOD','Specify Parent List','Specify Parent List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
  ('EXPT_CROSS_PARENT_CROSSES_ACT_MOD','Manage Crosses','Manage Crosses','experimentCreation','create','manage-crosses',1,'added by j.antonio ' || now(), 'crosses created'),
  ('EXPT_CROSS_PARENT_MANAGE_ACT_MOD','Manage','Manage','experimentCreation','create','manage-crosslist',1,'added by j.antonio ' || now(), 'crosses created'),
  ('EXPT_CROSS_PARENT_MATRIX_ACT_MOD','Matrix','Matrix','experimentCreation','create','manage-crossing-matrix',1,'added by j.antonio ' || now(), 'crosses created'),
  ('EXPT_CROSS_PARENT_PROTOCOLS_ACT_MOD','Protocols','Protocols','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT_MOD','Planting Protocols','Planting Protocols','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD','Traits Protocols','Traits Protocols','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_CROSS_PARENT_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'location rep studies created'),
  ('EXPT_CROSS_PARENT_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'finalized');


--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
	case
		when abbrev = 'EXPT_CROSS_PARENT_BASIC_INFO_ACT' then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_BASIC_INFO_ACT_MOD')
		when abbrev = 'EXPT_CROSS_PARENT_ENTRY_LIST_ACT' then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_ENTRY_LIST_ACT_MOD')
    when abbrev = 'EXPT_CROSS_PARENT_CROSSES_ACT' then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_CROSSES_ACT_MOD')
    when abbrev = 'EXPT_CROSS_PARENT_PROTOCOLS_ACT' then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_PROTOCOLS_ACT_MOD')
		when abbrev = 'EXPT_CROSS_PARENT_PLACE_ACT' then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_PLACE_ACT_MOD')
		when abbrev = 'EXPT_CROSS_PARENT_REVIEW_ACT' then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_REVIEW_ACT_MOD')
		else (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_REVIEW_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev in ('EXPT_CROSS_PARENT_BASIC_INFO_ACT','EXPT_CROSS_PARENT_ENTRY_LIST_ACT','EXPT_CROSS_PARENT_CROSSES_ACT','EXPT_CROSS_PARENT_PROTOCOLS_ACT', 'EXPT_CROSS_PARENT_PLACE_ACT','EXPT_CROSS_PARENT_REVIEW_ACT');


--Additions of Crosses' tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_CROSS_PARENT_MATRIX_ACT'then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_MATRIX_ACT_MOD')
  when abbrev = 'EXPT_CROSS_PARENT_MANAGE_ACT'then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_MANAGE_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_MANAGE_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_CROSS_PARENT_MATRIX_ACT','EXPT_CROSS_PARENT_MANAGE_ACT');



--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT_MOD')
  when abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT','EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT');
