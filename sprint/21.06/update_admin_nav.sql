-- update ADMIN navigation in platform.space
UPDATE 
    platform.space
SET 
    menu_data = 
    '
        {
            "left_menu_items": [{
                "name": "experiment-creation",
                "label": "Experiment creation",
                "appAbbrev": "EXPERIMENT_CREATION"
            }, {
                "name": "experiment-manager",
                "label": "Experiment manager",
                "appAbbrev": "OCCURRENCES"
            }, {
                "name": "planting-instructions-manager",
                "label": "Planting instructions manager",
                "appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data collection",
                "appAbbrev": "QUALITY_CONTROL"
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            }, {
                "name": "searchs-germplasm",
                "label": "Germplasm",
                "appAbbrev": "GERMPLASM_CATALOG"
            }, {
                "name": "search-seeds",
                "label": "Seed search",
                "appAbbrev": "FIND_SEEDS"
            }, {
                "name": "search-traits",
                "label": "Traits",
                "appAbbrev": "TRAITS"
            }, {
                "name": "list-manager",
                "label": "List manager",
                "appAbbrev": "LISTS"
            }],
            "main_menu_items": [{
                "icon": "help_outline",
                "name": "help",
                "items": [{
                    "url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
                    "icon": "headset_mic",
                    "name": "help_support-portal",
                    "label": "Support portal",
                    "tooltip": "Go to EBS Service Desk portal"
                }, {
                    "url": "https://cbapi-dev.ebsproject.org",
                    "icon": "code",
                    "name": "help_api",
                    "label": "API",
                    "tooltip": "Go to B4R API Documentation"
                }],
                "label": "Help"
            }, {
                "icon": "folder_special",
                "name": "data-management",
                "items": [{
                    "name": "data-management_seasons",
                    "label": "Seasons",
                    "appAbbrev": "MANAGE_SEASONS"
                }],
                "label": "Data management"
            }, {
                "icon": "settings",
                "name": "administration",
                "items": [{
                    "name": "administration_users",
                    "label": "Persons",
                    "appAbbrev": "PERSONS"
                }],
                "label": "Administration"
            }]
        }
    '
WHERE
    abbrev = 'ADMIN';
