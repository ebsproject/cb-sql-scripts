with not_found_msgs as(
 
	select 
		msg,
		row_number() over () as order_number
	from 
		unnest(
			ARRAY[
			'The germplasm you have requested does not exist.',

			'The cross data requested does not exist.',

			'The seed you have requested does not exist.',

			'The planting job entry you have provided does not exist.',

			'The germplasm ID you have provided does not exist.',

			'The package ID you have provided does not exist.',

			'The planting job you have requested does not exist.',

			'The experiment ID you have provided does not exist.',

			'The summary for the experiment ID does not exist.',

			'The summary that you are requesting cannot be retrieved since the planting job ID that you have provided does not exist.',

			'The planting job occurrence ID you have provided does not exist.'

			]::text[]
		) as msg
)
insert into api.messages(
  http_status_code, 
  code,
  "type",
  message,
  url
)

select 
'404',
(select max(code)::int from api.messages m where http_status_code =404)+ order_number,
'ERR',
msg,
'/responses.html#'||(select max(code)::int from api.messages m where http_status_code =404)+ order_number
from not_found_msgs