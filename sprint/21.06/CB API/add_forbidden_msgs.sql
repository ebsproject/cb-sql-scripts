with forbidden_msgs as(
 
	select 
		msg,
		row_number() over () as order_number
	from 
		unnest(
			ARRAY[
			'User is not allowed to delete seeds and related data already used in an experiment.',
			
			'User is not allowed to delete seed relation if the seed is a parent seed.',
			
			'User is not allowed to delete germplasm relation if the germplasm is a parent germplasm.',
			
			'User is not allowed to delete seeds and related data of a germplasm already used in an experiment.',
			
			'User is not allowed to delete seeds with seed attribute and seed relation records.',
      
      'User is not allowed to delete the planting job record when the status is already packed.',
      
      'User is not allowed to delete the planting job occurrence record when the status is already packed.'
			]::text[]
		) as msg
)
insert into api.messages(
  http_status_code, 
  code,
  "type",
  message,
  url
)

select 
'403',
(select max(code)::int from api.messages m where http_status_code =403)+ order_number,
'ERR',
msg,
'/responses.html#'||(select max(code)::int from api.messages m where http_status_code =403)+ order_number
from forbidden_msgs