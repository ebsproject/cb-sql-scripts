with invalid_msgs as(
 
	select 
		msg,
		row_number() over () as order_number
	from 
		unnest(
			ARRAY[
			'Invalid request, planting job entry ID must be an integer.',
      'Invalid value provided for isSufficient, value must be a valid boolean value: true | false',
      'Invalid value provided for isReplaced, value must be a valid boolean value: true | false',
      'Invalid request, unit value must be one of the following: kg, seeds, pan, g, panicles, plant.',
      'Invalid request, replacement type value must be one of the following: Replace with check, Replace with filler, Replace with package',
      'Invalid format, replacement germplasm ID must be an integer.',
      'Invalid format, replacement package ID must be an integer.',
      'Invalid format, replacement entry ID must be an integer.',
      'Required parameter is missing. Ensure that experimentDbId is not empty.',
      'Invalid request, planting job ID must be an integer.',
      'Invalid value for plantingJobStatus, make sure that it is one of the following: packing cancelled, packing on hold, packed, packing, ready for packing.',
      'Invalid value for plantingJobType, make sure that it is one of the following: planting job, packing job',
      'Invalid format, planting job due date must be in format YYYY-MM-DD.',
      'Invalid request, planting job occurrence ID must be an integer.',
      'Invalid format, noOfRanges field must be an integer.',
      'Invalid format, entryListIdList must be a valid JSON string.',
      'Invalid format, layoutOrderData field must be a valid JSON string.',
      'Required parameters are missing. Ensure that entryName, entryType, entryStatus, entryListDbId, and germplasmDbId fields are not empty.',
      'Invalid format, packageDbId must be an integer.',
      'Invalid request, you have provided an invalid value for entry role.',
      'Invalid format, seed ID must be an integer.',
      'Required parameters are missing. Ensure that the crossDbId, germplasmDbId, parentRole, and orderNumber fields are not empty.',
      'Invalid format, germplasm ID must be an integer.',
      'Invalid request, you have provided an invalid value for parent role.'
      'Invalid format, order number must be an integer.' ,
      'Invalid request, please provide a valid request body.',
      'Invalid request, occurrence ID must be an integer.',
      'Invalid request, a pattern for the experiment that you have provided does not exist.',
      'Required parameters are missing. Ensure that the offset field is not empty.',
      'Ensure that there is a pattern to generate plot code for occurrence ID: ',
      'Required parameters are missing. Ensure that offset field of the occurrenceDbId is not empty.',
      'Invalid request. User has provided an invalid format for germplasmDbId, it must be an integer.',
      'Invalid request. User has provided an invalid format for crossDataDbId, it must be an integer.',
      'Invalid request. User has provided an invalid format for seedDbId, it must be an integer.'

			]::text[]
		) as msg
)

insert into api.messages(
  http_status_code, 
  code,
  "type",
  message,
  url
)

select 
'400',
(select max(code)::int from api.messages m where http_status_code =400)+ order_number,
'ERR',
msg,
'/responses.html#'||(select max(code)::int from api.messages m where http_status_code =400)+ order_number
from invalid_msgs