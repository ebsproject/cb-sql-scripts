with internal_error_msgs as(
 
	select 
		msg,
		row_number() over () as order_number
	from 
		unnest(
			ARRAY[
			'Error encountered while retrieving valid replacement types.',
			
			'Error encountered while retrieving valid units.'
			]::text[]
		) as msg
)
insert into api.messages(
  http_status_code, 
  code,
  "type",
  message,
  url
)

select 
'500',
(select max(code)::int from api.messages m where http_status_code =500)+ order_number,
'ERR',
msg,
'/responses.html#'||(select max(code)::int from api.messages m where http_status_code =500)+ order_number
from internal_error_msgs