with unauthorized_msgs as(
 
	select 
		msg,
		row_number() over () as order_number
	from 
		unnest(
			ARRAY[
			'Unauthorized request. User is not a program team member.',
			
			'Unauthorized request. User is not a program team member or a producer.'

			]::text[]
		) as msg
)
insert into api.messages(
  http_status_code, 
  code,
  "type",
  message,
  url
)

select 
'401',
(select max(code)::int from api.messages m where http_status_code =401)+ order_number,
'ERR',
msg,
'/responses.html#'||(select max(code)::int from api.messages m where http_status_code =401)+ order_number
from unauthorized_msgs