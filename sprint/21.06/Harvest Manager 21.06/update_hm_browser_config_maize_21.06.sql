-- CORB-1450 HM - Add backcross configuration
--------------------------------------------------------------------------------------
-- MAIZE
DELETE FROM platform.config WHERE abbrev='HM_BROWSER_CONFIG_MAIZE';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
(
    'HM_BROWSER_CONFIG_MAIZE',
    'Harvest Manager Browser Config for Maize',
$$
    {
        "name" : "HM_BROWSER_CONFIG_MAIZE",
        "values": [
            {
                "CROSS_METHOD_SELFING": {
                    "fixed" : {
                        "display_column" : [
                            "harvestDate",
                            "harvestMethod"
                        ],
                        "harvest_method" : [
                            "Bulk"
                        ]
                    },
                    "not_fixed" : {
                        "display_column" : [
                            "harvestDate",
                            "harvestMethod",
                            "numericVar"
                        ],
                        "harvest_method" : [
                            "Bulk",
                            "Individual ear"
                        ]
                    }
                },
                "CROSS_METHOD_SINGLE_CROSS": {
                    "fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]
                    },
                    "not_fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]                    
                    }
                },
                "CROSS_METHOD_HYBRID_FORMATION": {
                    "fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]
                    },
                    "not_fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]                    
                    }
                },
                "CROSS_METHOD_THREE_WAY_CROSS": {
                    "fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]
                    },
                    "not_fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]                    
                    }
                },
                "CROSS_METHOD_BACKCROSS": {
                    "fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]
                    },
                    "not_fixed" : {
                        "harvest_method": [
                            "Bulk"
                        ],
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]                    
                    }
                }
            }
        ]
    }
$$,
    1,
    'harvest_manager',
    1,
    'add configuration for backcross'
);
