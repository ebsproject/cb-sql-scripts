-- CORB-1526 HM - Add top cross configuration
--------------------------------------------------------------------------------------
-- WHEAT
DELETE FROM platform.config WHERE abbrev='HM_BROWSER_CONFIG_WHEAT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_BROWSER_CONFIG_WHEAT',
        'Harvest Manager Browser Config for Wheat',
$$	
{
    "name": "HM_BROWSER_CONFIG_WHEAT",
    "values": [
        {
            "CROSS_METHOD_SELFING": {
                "fixed": {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "harvest_method": [
                        "Bulk",
                        "Selected bulk",
                        "Single Plant Selection",
                        "Individual spike"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                }
            },
            "CROSS_METHOD_SINGLE_CROSS": {
                "fixed": {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                }
            },
            "CROSS_METHOD_TOP_CROSS": {
                "fixed": {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                }
            }
        }
    ]
}
$$,
        1,
        'harvest_manager',
        1,
        'created wheat browser configuration');