-- CORB-1442 HM - Update cross name pattern RICE configuration
--------------------------------------------------------------------------------------
-- RICE
DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_CROSS_RICE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_CROSS_RICE_DEFAULT',
        'Harvest Manager Rice Cross Name Pattern-Default',
$$					
{
    "PLOT": {
        "default": {
            "default": {
                "default": [
                    {
                        "type": "free-text",
                        "value": "IR XXXXX",
                        "order_number": 0
                    }
                ]
            }
        }
    },
    "CROSS": {
        "default": {
            "default": {
                "bulk": [
                    {
                        "type": "free-text",
                        "value": "IR ",
                        "order_number": 0
                    },
                    {
                        "type": "db-sequence",
                        "schema": "germplasm",
                        "order_number": 1,
                        "sequence_name": "cross_number_seq"
                    }
                ],
                "default": [
                    {
                        "type": "free-text",
                        "value": "IR ",
                        "order_number": 0
                    },
                    {
                        "type": "db-sequence",
                        "schema": "germplasm",
                        "order_number": 1,
                        "sequence_name": "cross_name_seq"
                    }
                ]
            }
        },
        "single cross": {
            "default": {
                "bulk": [
                    {
                        "type": "free-text",
                        "value": "IR ",
                        "order_number": 0
                    },
                    {
                        "type": "db-sequence",
                        "schema": "germplasm",
                        "order_number": 1,
                        "sequence_name": "cross_number_seq"
                    }
                ],
                "default": [
                    {
                        "type": "free-text",
                        "value": "IR ",
                        "order_number": 0
                    },
                    {
                        "type": "db-sequence",
                        "schema": "germplasm",
                        "order_number": 1,
                        "sequence_name": "cross_name_seq"
                    }
                ],
                "single seed numbering": [
                    {
                        "type": "free-text",
                        "value": "IR ",
                        "order_number": 0
                    },
                    {
                        "type": "db-sequence",
                        "schema": "germplasm",
                        "order_number": 1,
                        "sequence_name": "cross_number_seq"
                    }
                ]
            }
        }
    },
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "order_number": 4,
                        "sequence_name": "<sequence_name>"
                    }
                ]
            }
        }
    }
}
$$,
1,
'harvest_manager',
1,
'created rice name pattern config');

