-- CORB-1526 HM - Add backcross and single seed numbering harvest method configuration
--------------------------------------------------------------------------------------
-- RICE
DELETE FROM platform.config WHERE abbrev='HM_BROWSER_CONFIG_RICE';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_BROWSER_CONFIG_RICE',
        'Harvest Manager Browser Config for Rice',
$$			
		
{
    "name": "HM_BROWSER_CONFIG_RICE",
    "values": [
		{
            "CROSS_METHOD_SELFING": {
                "fixed": {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                },
                "not_fixed": {
                    "harvest_method": [
                        "Bulk",
                        "Panicle Selection",
                        "Single Plant Selection",
                        "Single Plant Selection and Bulk",
                        "Plant-specific",
                        "Plant-Specific and Bulk",
                        "Single Seed Descent"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                }
            },
            "CROSS_METHOD_SINGLE_CROSS": {
                "fixed" : {
                    "harvest_method": [
                        "Bulk",
                        "Single seed numbering"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                },
                "not_fixed" : {
                    "harvest_method": [
                        "Bulk",
                        "Single seed numbering"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]                    
                }
            }, 
            "CROSS_METHOD_THREE_WAY_CROSS": {
                "fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                },
                "not_fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]                    
                }
            },  
            "CROSS_METHOD_DOUBLE_CROSS": {
                "fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                },
                "not_fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]                    
                }
            },
            "CROSS_METHOD_COMPLEX_CROSS": {
                "fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                },
                "not_fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]                    
                }
            },
            "CROSS_METHOD_BACKCROSS": {
                "fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]
                },
                "not_fixed" : {
                    "harvest_method": [
                        "Bulk"
                    ],
                    "display_column": [
                        "harvestDate",
                        "harvestMethod",
                        "numericVar"
                    ]                    
                }
            } 
        }
    ]
}
$$,
        1,
        'harvest_manager',
        1,
        'created rice browser config');

