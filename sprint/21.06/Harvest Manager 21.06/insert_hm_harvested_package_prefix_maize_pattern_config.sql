-- CORB-1454 - Insert Harvested Package Prefix Pattern Maize Default
Delete from platform.config where abbrev='HARVESTED_PACKAGE_PREFIX_PATTERN_MAIZE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVESTED_PACKAGE_PREFIX_PATTERN_MAIZE_DEFAULT',
        'HM: Harvested Package Prefix Pattern Maize Default',
$$			
		
{
    "pattern" : [
        {
            "type": "field",
            "field_name": "programCode",
            "order_number": 0
        },
        {
            "type": "field",
            "field_name": "experimentYearYY",
            "order_number": 1
        },
        {
            "type": "field",
            "field_name": "experimentSeasonCode",
            "order_number": 2
        }
    ]
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-1454 - Insert Harvested Package Prefix Pattern Maize Default');