-- CORB-1446 HM - Add single seed numbering germplasm name config

UPDATE
    platform.config
SET
    config_value = 
$$			
		
{
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "sequence_name": "<sequence_name>",
                        "order_number": 4
                    }
                ]
            }
        }
    },
    "PLOT": {
        "default": {
            "default": {
                "default": [
                    {
                        "type": "free-text",
                        "value": "IR XXXXX",
                        "order_number": 0
                    }
                ]
            },
            "progeny": {
                "default": [
                    {
                        "type": "free-text",
                        "value": "IR XXXXX",
                        "order_number": 0
                    }
                ],
                "bulk": [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "free-text",
                        "value": "B",
                        "order_number": 2
                    }
                ],
                "plant-specific": [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 2
                    }
                ],
                "panicle selection": [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 2
                    }
                ],
                "single seed descent": [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "free-text",
                        "value": "B RGA",
                        "order_number": 2
                    }
                ],
                "single plant selection": [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 2
                    }
                ]
            }
        }
    },
    "CROSS" : {
        "default": {
            "default": {
                "default": [
                    {
                        "type": "field",
                        "entity": "cross",
                        "field_name": "crossName",
                        "order_number": 0
                    }
                ]
            }
        },
        "single cross": {
            "default": {
                "default": [
                    {
                        "type": "field",
                        "entity": "cross",
                        "field_name": "crossName",
                        "order_number": 0
                    }
                ],
                "bulk": [
                    {
                        "type": "field",
                        "entity": "cross",
                        "field_name": "crossName",
                        "order_number": 0
                    }
                ],
                "single seed numbering": [
                    {
                        "type": "field",
                        "entity": "cross",
                        "field_name": "crossName",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": ":",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 2
                    }
                ]
            }
        }
    }
}
$$

WHERE
	abbrev = 'HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT';
