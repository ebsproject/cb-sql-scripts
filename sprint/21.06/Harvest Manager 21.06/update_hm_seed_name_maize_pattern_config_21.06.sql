-- CORB-1450 HM - Add configuration for recurrent parent of backcross
--------------------------------------------------------------------------------------
-- MAIZE
DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_SEED_MAIZE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
(
    'HM_NAME_PATTERN_SEED_MAIZE_DEFAULT',
    'Harvest Manager Maize Seed Name Pattern-Default V2',
$$
{
    "PLOT": {
        "default": {
            "default": {
                "bulk": {
                    "single_occurrence": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "entryNumber",
                            "order_number": 6
                        }
                    ],
                    "multiple_occurrence": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "occurrenceName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "entryNumber",
                            "order_number": 6
                        }
                    ]
                },
                "default": {
                    "single_occurrence": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": ":",
                            "order_number": 7
                        },
                        {
                            "type": "counter",
                            "order_number": 8
                        }
                    ],
                    "multiple_occurrence": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "occurrenceName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": ":",
                            "order_number": 7
                        },
                        {
                            "type": "counter",
                            "order_number": 8
                        }
                    ]
                }
            }
        }
    },
    "CROSS": {
        "default": {
            "default": {
                "default": {
                    "same_nursery": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 8
                        }
                    ],
                    "different_nurseries": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 8
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 9
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 10
                        }
                    ]
                }
            }
        },
        "single cross": {
            "default": {
                "default": {
                    "same_nursery": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 8
                        }
                    ],
                    "different_nurseries": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 8
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 9
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 10
                        }
                    ]
                }
            }
        },
        "hybrid formation": {
            "default": {
                "default": {
                    "same_nursery": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 8
                        }
                    ],
                    "different_nurseries": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 8
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 9
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 10
                        }
                    ]
                }
            }
        },
        "three-way cross": {
            "default": {
                "default": {
                    "same_nursery": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 8
                        }
                    ],
                    "different_nurseries": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "originSiteCode",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentYearYY",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "experimentSeasonCode",
                            "order_number": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 3
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 4
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 5
                        },
                        {
                            "type": "field",
                            "entity": "femaleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 6
                        },
                        {
                            "type": "delimiter",
                            "value": "/",
                            "order_number": 7
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "sourceExperimentName",
                            "order_number": 8
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 9
                        },
                        {
                            "type": "field",
                            "entity": "maleCrossParent",
                            "field_name": "entryNumber",
                            "order_number": 10
                        }
                    ]
                }
            }
        }
    },
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "order_number": 4,
                        "sequence_name": "<sequence_name>"
                    }
                ]
            }
        }
    }
}
$$,
    1,
    'harvest_manager',
    1,
    'add configuration for recurrent parent of backcross'
);