-- CORB-1450 HM - Add configuration for recurrent parent of backcross
--------------------------------------------------------------------------------------
-- MAIZE
DELETE FROM platform.config WHERE abbrev='HM_RECURRENT_PARENT_PATTERN_BACKROSS_MAIZE_DEFAULT';
DELETE FROM platform.config WHERE abbrev='HM_RECURRENT_PARENT_PATTERN_BACKCROSS_MAIZE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
(
    'HM_RECURRENT_PARENT_PATTERN_BACKCROSS_MAIZE_DEFAULT',
    'Harvest Manager Recurrent Parent Pattern for Maize Backcrosses',
$$
    {
        "pattern": [
            [
                {
                    "type": "field",
                    "entity": "recurrentParentGermplasm",
                    "field_name": "designation",
                    "order_number": 0
                },
                {
                    "type": "delimiter",
                    "value": "<",
                    "order_number": 1
                },{
                    "type": "counter",
                    "order_number": 2
                }
            ],
            [
                {
                    "type": "field",
                    "entity": "recurrentParentGermplasm",
                    "field_name": "designation",
                    "order_number": 0
                },
                {
                    "type": "delimiter",
                    "value": "<",
                    "order_number": 1
                },{
                    "type": "counter",
                    "order_number": 2
                }
            ]
        ],
        "delimiter_backcross_number" : "<",
        "delimiter_parentage" : "/"
    }
$$,
    1,
    'harvest_manager',
    1,
    'add configuration for recurrent parent of backcross'
);
