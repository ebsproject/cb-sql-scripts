Delete from platform.config where abbrev='HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD',
        'Background processing threshold values for HARVEST MANAGER tool features',
$$			
{
    "deleteHarvestData": {
        "size": "2000",
        "description": "Delete harvest data of plots or crosses"
    },
    "deleteSeeds": {
        "size": "200",
        "description": "Delete seeds of plots or crosses"
    },
    "deletePackages": {
        "size": "200",
        "description": "Delete packages and related records"
    },
    "createList": {
        "size": "500",
        "description": "Create new germplasm, seed, or package list"
    }
}

$$,
        1,
        'harvest_manager_bg_process',
        1,
        'CORB-1455 - k.delarosa 2021-06-18');
