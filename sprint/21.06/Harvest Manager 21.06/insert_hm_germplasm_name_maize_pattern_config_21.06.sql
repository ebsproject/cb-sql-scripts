Delete from platform.config where abbrev='HM_NAME_PATTERN_GERMPLASM_MAIZE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_GERMPLASM_MAIZE_DEFAULT',
        'Harvest Manager Maize Germplasm Name Pattern - Default',
$$			
		
{
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "sequence_name": "<sequence_name>",
                        "order_number": 4
                    },
                    {
                        "type": "free-text-repeater",
                        "value": "ABC",
                        "delimiter": "*",
                        "minimum": "2",
                        "order_number": 5
                    }
                ]
            }
        }
    },
    "PLOT": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 0
                    }
                ],
                "bulk" : [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 2
                    },
                    {
                        "type": "free-text-repeater",
                        "value": "B",
                        "delimiter": "*",
                        "minimum": "2",
                        "order_number": 3
                    }
                ],
                "individual ear" : [
                    {
                        "type": "field",
                        "entity": "plot",
                        "field_name": "germplasmDesignation",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 2
                    }
                ]
            }
        }
    },
    "CROSS" : {
        "default": {
            "default": {
                "default": [
                    {
                        "type": "free-text",
                        "value": "",
                        "order_number": 0
                    }
                ]
            }
        }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-1450 - j.bantay 2021-06-15');