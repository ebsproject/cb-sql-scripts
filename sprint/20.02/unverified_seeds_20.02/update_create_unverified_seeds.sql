-- Function: operational.create_seed_lots_from_webform(character varying, integer)

-- DROP FUNCTION operational.create_seed_lots_from_webform(character varying, integer);

CREATE OR REPLACE FUNCTION operational.create_seed_lots_from_webform(
    tbl_name character varying,
    usr_id integer)
  RETURNS character varying AS
$BODY$DECLARE

	seed_lot_rec RECORD;
	r_seedstorage_metadata_variables RECORD;
	var_seed_lot_id numeric;
	var_raw_tbl_name character varying;
	var_new_seed_storage_id integer;
	var_seed_storage_metadata_id integer;
	var_metadata_column character varying;
	var_met_value character varying;
	var_transfer character varying;
	var_ss_gid integer;
	var_receiver_id integer;
	var_save_gid integer;
	
	germuid integer;
	var_glocn integer;
	var_method_id integer;
	var_gpid1 integer;
	var_gpid2 integer;
	gdate integer;
	var_count integer;
	var_arr VARCHAR[];
	var_max NUMERIC;
	var_new_gid numeric;
	lgid numeric;
	var_new_nid numeric;
	var_name varchar;
	var_date integer;
	var_new_seed_storage_log_id INTEGER;
	var_program_place_id INTEGER;
	var_new_seed_lot_id NUMERIC;
	var_method_type CHARACTER VARYING;
	var_gid_type CHARACTER VARYING;
	var_new_product_gid_id INTEGER;
	var_name_type CHARACTER VARYING;
	designation_val CHARACTER VARYING;
	var_gnpgs INTEGER;
	var_aval CHARACTER VARYING;
	var_new_aid INTEGER;
	var_ntype INTEGER; 
	var_nlocn INTEGER;
	var_ndate INTEGER;
	var_product_from_web_form INTEGER;
	var_volume INTEGER;
	var_transaction_type CHARACTER VARYING;
BEGIN

	var_raw_tbl_name := substring(tbl_name from 16);
	var_product_from_web_form = 0;

	execute 'alter table temporary_data.' || tbl_name || ' add column if not exists is_created boolean';
	
	FOR seed_lot_rec in execute '
		select * from temporary_data.' || tbl_name
	LOOP

	if seed_lot_rec.is_created is NULL or seed_lot_rec.is_created = false
	then
		var_seed_lot_id := nextval('operational.seed_storage_seed_lot_id_seq');
		var_ss_gid := null;
		select
			pg.gid
		into var_ss_gid
		from
			master.product p,
			master.product_gid pg,
			operational.seed_storage ss
		where
			p.id = seed_lot_rec.product_id
			and pg.product_id = p.id
			and pg.gid = ss.gid
			and ss.product_id = p.id
			and ss.is_void = false
			and pg.is_void = false
		order by gid desc limit 1;

		--check if product created from unverified tool, if not, transfer
		select
			id
		into var_product_from_web_form
		from
			master.product
		where
			id = seed_lot_rec.product_id
			and notes ilike '%product created via product web form%';

		if var_ss_gid is null AND var_product_from_web_form is null
		then
			var_transfer = 'true';
		elsif var_ss_gid is not null
		then
			var_transfer = 'true';
		else
			var_transfer = 'false';
		end if;

		var_save_gid := seed_lot_rec.gid;
		--- For GMS --
		RAISE NOTICE 'var_transfer %',var_transfer;
		if var_transfer = 'true'
		then
			---for gms.germplsm
			--getting the user id
			germuid := NULL;
			select value into germuid
			from master.user_metadata 
			where user_id = usr_id and variable_id = 582;

			IF germuid IS NULL THEN germuid = 2; END IF;

			--getting the location id
			var_glocn := 9000;
			var_method_id := NULL;
			var_gpid1 := seed_lot_rec.gpid1;
			var_gpid2 := seed_lot_rec.gpid2;
			var_method_id := seed_lot_rec.methn;
			
			--select methn, gpid1 into var_method_id, var_gpid1
			--from gms.germplsm
			--where gid = var_ss_gid;

			var_receiver_id := null;
			select id into var_receiver_id
			from master.program where abbrev = seed_lot_rec.seed_manager;
			
			select mppm.value into var_glocn
			from master.program_place mpp, master.program_place_metadata mppm
			where mpp.program_id = var_receiver_id and mpp.place_id = 10001
			and mpp.id = mppm.program_place_id and mppm.variable_id = 583;

			--if receiver is not in program_place table
			if var_glocn IS NULL
			then
				var_glocn := 9000;
			end if;

			--getting the harvest date
			gdate := NULL;
			if seed_lot_rec.harvest_date IS NULL
			then
				gdate = to_char(now(), 'YYYYMMDD');
			else
				gdate = to_char(seed_lot_rec.harvest_date, 'YYYYMMDD');
			end if;

			var_count := NULL;
			--getting the next available gid
			SELECT COUNT(*) INTO var_count FROM gms.germplsm;
			
			var_max := NULL;
			
			IF var_count <= 0
			THEN
				SELECT ugid INTO var_new_gid FROM gms.instln;
			ELSE
				SELECT MAX(gid) INTO var_max FROM gms.germplsm WHERE gid >= 300000001 AND gid < 400000000;

				IF var_max Is NULL
				THEN
					var_new_gid := 300000001;
				ELSE
					var_new_gid := var_max + 1;
				END IF;
				
			END IF;

			--seed_lot_rec.gid = var_new_gid;
			RAISE NOTICE 'var_new_gid %',var_new_gid;
			var_save_gid := var_new_gid;
			--getting the lgid
			lgid := -(var_new_gid);

			--get gnpgs value
			IF var_method_id in (
				select mid from gms.methods where mname in (
				'Backcross',
				'Single cross',
				'Three-way cross',
				'Double cross',
				'DOUBLE CROSS CF'
				)
			)
			THEN 
				var_gnpgs = 2;
			ELSE
				var_gnpgs = -1;
			END IF;
		
			--creating new record in germplasm
			INSERT INTO gms.germplsm (gid,methn,gnpgs,gpid1,gpid2,germuid,lgid,glocn,gdate,gref,grplce,mgid)
			VALUES (
				var_new_gid,var_method_id,var_gnpgs,var_gpid1,var_gpid2,germuid,lgid,var_glocn,gdate,0,0,0
			);


			---for gms.names
			
			var_ntype := null; 
			var_nlocn := null;
			var_ndate := null;
			
			--getting the next available nid
			var_max := NULL;
			var_new_nid := NULL;
			
			SELECT MAX(nid) INTO var_max FROM gms.names WHERE nid >= 300000001 AND nid < 400000000;

			IF var_max IS NULL
			THEN 
				var_new_nid := 300000001;
			ELSE
				var_new_nid := var_max + 1;
			END IF;

			--getting the old name
			var_name := NULL;

			select gn.nval, gn.ntype into var_name, var_ntype
			from gms.names gn, master.product mp
			where gn.gid = var_ss_gid
			and gn.nval = mp.designation;
			
			IF var_name IS NULL
			THEN
				select nval, ntype into var_name, var_ntype
				from gms.names where gid = var_ss_gid;
			END IF;

			IF var_name IS NULL
			THEN
				select designation into var_name
				from master.product where id = seed_lot_rec.product_id;
			END IF;
					
			-- get the ntype, nlocn, ndate of the given source (gpid2) if the method is import (methn = 62)-- RBIMS-6047
			IF var_method_id = 62 
			THEN 
				IF var_gpid2 IS NOT NULL
				THEN
					select gn.ntype, gn.nlocn, gn.ndate into var_ntype, var_nlocn, var_ndate
					from gms.names gn, master.product mp
					where gn.gid = var_gpid2
					and mp.designation = gn.nval;
				ELSE
					select gn.ntype, gn.nlocn, gn.ndate into var_ntype, var_nlocn, var_ndate
					from gms.names gn, master.product mp
					where gn.gid = var_ss_gid
					and gn.nval = mp.designation;
					
					IF var_ntype IS NULL
					THEN
						select gn.ntype, gn.nlocn, gn.ndate into var_ntype, var_nlocn, var_ndate
						from gms.names gn where gid = var_ss_gid;
					END IF;
				END IF;
			END IF;

			IF var_ntype IS NULL
			THEN
				var_ntype := 2; -- default
			END IF;
			
			IF var_nlocn IS NULL
			THEN
				var_nlocn := 9000; -- default
			END IF;

			--getting the current date
			var_date := to_char(current_timestamp, 'YYYYMMDD');
			IF var_ndate IS NULL
			THEN
				var_ndate := var_date; -- default
			END IF;
			
			--creating new record in names
			
			INSERT INTO gms.names (nid,gid,ntype,nstat,nuid,nval,nlocn,ndate,nref)
			VALUES (
				var_new_nid, var_new_gid, var_ntype, 1, germuid, var_name, var_nlocn, var_ndate, 0
			);

			var_aval := seed_lot_rec.remarks;  -- remarks
			
			-- add attributes (RBIMS-6008)
			IF var_aval IS NOT NULL THEN
				
				--getting the next available aid
				var_max := NULL;
				var_new_aid := NULL;

				SELECT MAX(aid) INTO var_max FROM gms.atributs WHERE aid >= 300000001 AND aid < 400000000;	
				
				IF var_max IS NULL
				THEN 
					var_new_aid := 300000001;
				ELSE
					var_new_aid := var_max + 1;
				END IF;

				--creating the first record in atributs
				INSERT INTO gms.atributs ( aid,gid,atype,auid,aval,alocn,aref,adate )
				VALUES (
					var_new_aid, var_new_gid, 103, germuid, var_aval, 9000, 0, var_date
				);
			END IF;
		end if;
		--update gpids and method in gms created in product creation tool
		if var_transfer = 'false'
		then
			update gms.germplsm set gpid1 = seed_lot_rec.gpid1, gpid2 = seed_lot_rec.gpid2, methn = seed_lot_rec.methn where gid = seed_lot_rec.gid;
		end if;

		if seed_lot_rec.volume::integer is null
		then
			var_volume = 0;
		else
			var_volume = seed_lot_rec.volume::integer;
		end if;

		if seed_lot_rec.volume::integer is null
		then
			var_transaction_type = 'deposit';
		else
			var_transaction_type = seed_lot_rec.transaction_type;
		end if;
		
		INSERT INTO operational.seed_storage (
		product_id, seed_lot_id, key_type, seed_manager, gid, volume, unit,
		harvest_date, label,
		original_storage_id, creation_timestamp, creator_id, notes, is_void,
		product_gid_id, seed_storage_status, temp_seed_lot_id, remarks
		) VALUES(
		seed_lot_rec.product_id, var_seed_lot_id, 'custom_key', seed_lot_rec.seed_manager,
		var_save_gid, var_volume, seed_lot_rec.unit, seed_lot_rec.harvest_date, seed_lot_rec.label,
		NULL, CURRENT_TIMESTAMP, usr_id, 'seed lot created via seed lot web form', FALSE, seed_lot_rec.product_gid_id, 'unverified', seed_lot_rec.label, seed_lot_rec.remarks
		) returning id into var_new_seed_storage_id;
		
		-- insert to seed storage metadata
		FOR r_seedstorage_metadata_variables in execute '
			select 
				v.*
			from 
				master.variable v,
				master.variable_set_member vsm,
				master.variable_set vs
			where
				vs.abbrev = ''SEEDLOTS_CREATE_WEBFORM_METADATA''
				and vsm.variable_set_id = vs.id
				and vsm.variable_id = v.id
				and vsm.is_void = false
				and v.is_void = false
			'
		LOOP
			var_seed_storage_metadata_id := null;
			var_metadata_column := null;
			
			select column_name into var_metadata_column from information_schema.columns where table_schema = 'temporary_data' and table_name = tbl_name and column_name = lower(r_seedstorage_metadata_variables.abbrev);
			if var_metadata_column IS NOT NULL
			then
				select ssm.id into var_seed_storage_metadata_id from operational.seed_storage_metadata ssm, operational.seed_storage ss where 
				ss.id = var_new_seed_storage_id and ss.id = ssm.seed_storage_id and ss.is_void = false and ssm.variable_id = r_seedstorage_metadata_variables.id and ssm.is_void = false;
				var_met_value := lower(r_seedstorage_metadata_variables.abbrev);

				var_metadata_column = lower(r_seedstorage_metadata_variables.abbrev);
				execute 'select ' || quote_ident(var_metadata_column) || ' from ' || 'temporary_data.' || tbl_name || ' where id = ' || seed_lot_rec.id into var_met_value;
				if var_seed_storage_metadata_id IS NULL and var_met_value IS NOT NULL
				then
						execute '
							insert into operational.seed_storage_metadata(
								seed_storage_id,
								variable_id,
								value,
								creator_id,
								notes
							)
							values(
								$1,
								$2,
								$3,
								$4,
								$5
							) '
							using
								var_new_seed_storage_id,
								r_seedstorage_metadata_variables.id,
								var_met_value,
								usr_id,
								'created via seed lot web form'
							;
				else
					if var_met_value IS NOT NULL
					then
						execute 'update operational.seed_storage_metadata
								set value = ' || var_met_value || ',
								notes = ' || 'created via seed lot web form' || '
								where id = ' || var_seed_storage_metadata_id;
						else -- do nothing
					end if;
				end if;
			end if;
		
		END LOOP; --end loop seed_storage_metadata
		
		--creating seed storage log
		INSERT INTO operational.seed_storage_log (
		seed_storage_id, encoder_id, encode_timestamp, transaction_type,
		volume, unit, event_timestamp, creation_timestamp, creator_id, notes, is_void
		) VALUES(
		var_new_seed_storage_id, usr_id, CURRENT_TIMESTAMP, var_transaction_type, var_volume,
		seed_lot_rec.unit, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, usr_id, 'created via seed lot web form', FALSE
		);

		if var_transfer = 'true'
		then

			-- get gid_type --added by jpr
			var_method_type := null;
			select mtype into var_method_type from gms.methods where mid = seed_lot_rec.methn;

			--gms.germplsm g
			--join gms.methods m on m.mid = g.methn
			--where g.gid = CAST(var_ss_gid as INTEGER) and g.grplce = 0;
			
			if var_method_type = 'GEN' THEN
				var_gid_type = 'cross';
			elsif var_method_type = 'DER' THEN
				var_gid_type = 'derivative';
			else
				var_gid_type = 'fixed';
			end if;

			
			--insert new product_gid --added by jpr
			INSERT INTO master.product_gid(product_id, gid, gid_type, creator_id, notes)
			values(seed_lot_rec.product_id,var_save_gid, var_gid_type,usr_id,'created via seed lot web form')
			returning id into var_new_product_gid_id;
			
			--update product_gid_id of new seed storage record
			UPDATE operational.seed_storage SET product_gid_id = var_new_product_gid_id where id = var_new_seed_storage_id;
			UPDATE master.product SET product_gid_id = var_new_product_gid_id where id = seed_lot_rec.product_id;
						
			--select designation
			select designation into designation_val from master.product where id = seed_lot_rec.product_id;

			-- get name_type
			var_name_type := null;
			select lower(name_type) into var_name_type from master.product_name
			where product_id = CAST(seed_lot_rec.product_id as INTEGER)
			and value = designation_val;

			if var_name_type IS NOT NULL
			then
				var_name_type = replace(replace(lower(trim(var_name_type)), ' ', '_'), E'''','');
			end if;

			-- insert to master.product_name --added by jpr
			insert into master.product_name(product_id, name_type, value, notes, product_gid_id, creator_id)
			values(seed_lot_rec.product_id, var_name_type, designation_val, 'created via seed lot web form', var_new_product_gid_id, usr_id);
		end if;

	end if;
	END LOOP;
	
	execute 'update temporary_data.' || tbl_name || ' set is_created = true';
	 
RETURN 'success';

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION operational.create_seed_lots_from_webform(character varying, integer)
  OWNER TO testb4radmin;
