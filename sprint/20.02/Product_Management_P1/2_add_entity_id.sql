ALTER TABLE platform.config ADD column entity_id integer;

ALTER TABLE platform.config
    ADD CONSTRAINT config_entity_id_fkey FOREIGN KEY (entity_id)
    REFERENCES dictionary.entity (id) MATCH SIMPLE
    ON UPDATE NO ACTION;
	
UPDATE platform.config
SET entity_id = (SELECT id FROM dictionary.entity WHERE abbrev = 'PRODUCT')
WHERE
	abbrev = 'PRODUCT_QUERY_FIELDS'