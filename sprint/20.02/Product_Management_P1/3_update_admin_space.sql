UPDATE platform.space
SET
	menu_data = '{
		"left_menu_items": [
			{
				"name": "experiment-creation",
				"label": "Experiments",
				"appAbbrev": "EXPERIMENT_CREATION"
			},
			{
				"name": "operational-studies",
				"label": "Operational studies",
				"appAbbrev": "OPERATIONAL_STUDIES"
			},
			{
				"name": "tasks",
				"label": "Tasks",
				"appAbbrev": "TASKS"
			},
			{
				"name": "data-collection-qc",
				"items": [
					{
						"name": "data-collection-qc-quality-control",
						"label": "Quality control",
						"appAbbrev": "QUALITY_CONTROL"
					}
				],
				"label": "Data collection & QC"
			},
			{
				"name": "seeds",
				"items": [
					{
						"name": "seeds-find-seeds",
						"label": "Find seeds",
						"appAbbrev": "FIND_SEEDS"
					},
					{
						"name": "seeds-harvest-manager",
						"label": "Harvest manager",
						"appAbbrev": "HARVEST_MANAGER"
					}
				],
				"label": "Seeds"
			}
		],
		"main_menu_items": [
			{
				"icon": "folder_special",
				"name": "data-management",
				"items": [
					{
						"name": "data-management_seasons",
						"label": "Seasons",
						"appAbbrev": "MANAGE_SEASONS"
					},
					{
						"name": "data-management_products",
						"label": "Products",
						"appAbbrev": "PRODUCT_CATALOG"
					}
				],
				"label": "Data management"
			}
		]
	}'
WHERE
	abbrev = 'ADMIN';