select *
	-- config_value->>'Values', '{"field_label":"Contact person","variable_abbrev":"CONTCT_PERSON_CONT","allow_new_val":true}'::jsonb, abbrev, config_value
from platform.config 
where  abbrev ilike '%_PLACE_ACT_VAL'
;

update platform.config
set config_value = '{
    "Name": "Required and default place metadata variables for trial data process",
    "Values": [
        {
            "disabled": false,
            "field_label": "Location",
            "order_number": 1,
            "variable_abbrev": "LOCATION",
            "field_description": "Location"
        },
        {
            "disabled": false,
            "field_label": "Remarks",
            "order_number": 2,
            "variable_abbrev": "REMARKS",
            "field_description": "Place Remarks"
        }
    ]
}'
where abbrev in (
	'EXPT_TRIAL_PLACE_ACT_VAL'
);
-- UPDATE 1