-- B4RSUPPORT-2077 Void unreleased applications

select *
from platform.application
where abbrev in (
	'FIND_SEEDS',
	'HARVEST_MANAGER',
	'SEED_INVENTORY_CONFIG'
);
-- 57	FIND_SEEDS	Find seeds	Find Seeds	search		true		2019-11-14 05:50:16.593826	1				false	[{"actor_id": "1", "new_data": {"id": 57, "icon": "search", "label": "Find seeds", "notes": null, "abbrev": "FIND_SEEDS", "is_void": false, "remarks": null, "is_public": true, "creator_id": 1, "description": null, "modifier_id": null, "action_label": "Find Seeds", "open_new_tab": false, "creation_timestamp": "2019-11-14 05:50:16.593826", "modification_timestamp": null}, "row_data": null, "action_type": "INSERT", "log_timestamp": "2019-11-14 05:50:16.593826+00", "transaction_id": "51031"}]	false

update platform.application
set is_void = true
where abbrev in (
	'FIND_SEEDS',
	'HARVEST_MANAGER',
	'SEED_INVENTORY_CONFIG'
) and is_void = false;
-- UPDATE 1