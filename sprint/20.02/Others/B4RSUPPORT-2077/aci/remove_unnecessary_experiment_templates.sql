-- B4RSUPPORT-2077 Can't create experiment

select * from master.item where process_type = 'experiment_creation_data_process' and is_void = false;

/*
RETAIN	1554	EXPT_DEFAULT_DATA_PROCESS		40				2019-10-04 01:59:42.154152	1				false		8	experiment_creation_data_process	active						fa fa-th-list			experiment_creation	667986590634607809	9d81ec06-c776-4675-9033-85a818b22836	ITEM;667986590634607809
VOID	1566	EXPT_NURSERY_PARENT_LIST_DATA_PROCESS	Nursery-Parent List	40	Nursery-Parent List	Nursery-Parent List		2019-11-14 06:19:27.480156	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	724483534844069319	5b7d5c10-7460-42c7-bfd0-90d6d2b89914	ITEM;724483534844069319
VOID	1580	EXPT_NURSERY_CROSS_LIST_DATA_PROCESS	Nursery-Cross List	40	Nursery-Cross List	Nursery-Cross List		2019-11-22 14:19:11.978263	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	730523202563868234	7c48ee71-be51-4e49-91e2-c9c6d82a23e1	ITEM;730523202563868234
VOID	1605	EXPT_SEED_INCREASE_DATA_PROCESS	Seed Increase Experiment	40	Applies to experiments Seed Increase (or Seed Multiplication or Seed Regeneration).	Seed Increase Experiment		2020-02-21 11:11:55.713909	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796383537488987747	56c92216-aa82-4bbe-ad1d-20f562ccc0ec	ITEM;796383537488987747
VOID	1619	EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS	Selection and Advancement Experiment	40	Applies to experiments where segregating populations (F1-Fn) are selfed for their selection and advancement. 	Seed and Advancement Experiment		2020-02-21 11:12:37.561766	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796383888543843953	f97a7f58-3f02-4d2f-8eca-278ddc7ac926	ITEM;796383888543843953
VOID	1542	EXPT_NURSERY_CB_DATA_PROCESS	Cross Pre-planning Experiment	40	Create Crossing Block	Cross Pre-planning Experiment		2019-10-04 01:59:42.154152	1			test void	true		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	667986070238921909	0cde34e5-5d4c-48c9-abbf-7946e99fea63	ITEM;667986070238921909
RETAIN	1547	EXPT_TRIAL_DATA_PROCESS	Trial Experiment	40	Create Trial Experiment	Trial Experiment		2019-10-04 01:59:42.154152	1				false		8	experiment_creation_data_process	active	trial					fa fa-th-list			experiment_creation	667986301898720442	5e452d4b-6638-472a-88de-b7346c333632	ITEM;667986301898720442
VOID	1679	EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS	Nursery Cross-list for IRRIHQ	40	Nursery cross-list experiment for IRRIHQ.	Nursery Cross-list for IRRIHQ		2020-02-21 14:04:43.8694	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796470511918909101	b8e63269-543f-47cf-a148-e0736851e2b2	ITEM;796470511918909101
VOID	1591	EXPT_CROSS_PRE_PLANNING_DATA_PROCESS	Cross Pre-planning Experiment for Hybrids	40	Parent role (Female or Male) of entries is known and planting arrangement is designed before planting for making crosses.	Cross Pre-planning Experiment for Hybrids		2020-02-21 11:11:17.261133	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796383215114782293	402565e3-1d96-4b7c-9bdd-e72d71e9a6ea	ITEM;796383215114782293
VOID	1633	EXPT_CROSS_POST_PLANNING_DATA_PROCESS	Cross Post-planning Experiment	40	Parent role (Female or Male) is unknown before planting and Cross List is prepared with assigned Parent Role only before making crosses.	Cross Post-planning Experiment		2020-02-21 11:13:25.522546	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796384290861483647	12ac64e8-5ee3-463f-b86d-4c03247fa664	ITEM;796384290861483647
VOID	1690	EXPT_SEED_INCREASE_IRRI_DATA_PROCESS	Seed Increase for IRRI Experiment	40	Applies to experiments Seed Increase (or Seed Multiplication or Seed Regeneration).	Seed Increase for IRRI Experiment		2020-02-21 14:05:06.211731	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796470699303634616	e2429641-6e96-400c-9553-4aa6897b9ffd	ITEM;796470699303634616
VOID	1704	EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS	Selection and Advancement for IRRI Experiment	40	Applies to experiments where segregating populations (F1-Fn) are selfed for their selection and advancement. 	Selection and Advancement for IRRI Experiment		2020-02-21 14:05:19.245028	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796470808632362694	aaafc0b3-2ecc-49df-a690-187b2cb48a72	ITEM;796470808632362694
VOID	1647	EXPT_TRIAL_IRRI_DATA_PROCESS	Trial Experiment for IRRI	40	Trial Experiment for IRRI	Trial Experiment for IRRI		2020-02-21 11:15:16.180337	1				false		8	experiment_creation_data_process	active	trial					fa fa-th-list			experiment_creation	796385219128067725	2b7a5e2e-b8be-40d8-bafc-9a711fc6317f	ITEM;796385219128067725
*/

update master.item
set is_void = true,
	notes = z_admin.append_text(notes, 'B4RSUPPORT-2077 remove unnecessary experiment templates - a.flores 2020-02-24')
where
	process_type = 'experiment_creation_data_process'
	and abbrev not in (
		'EXPT_DEFAULT_DATA_PROCESS',
		'EXPT_TRIAL_DATA_PROCESS'
	)
	and is_void = false
;
-- UPDATE 10