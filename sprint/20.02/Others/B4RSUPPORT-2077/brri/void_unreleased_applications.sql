-- B4RSUPPORT-2077 Void unreleased applications

select *
from platform.application
where abbrev in (
	'FIND_SEEDS',
	'HARVEST_MANAGER',
	'SEED_INVENTORY_CONFIG'
);
-- 55	FIND_SEEDS			2019-11-22 14:12:21.238585	1				false	Find Seeds	[{"actor_id": "1", "new_data": {"id": 55, "icon": "search", "label": "Find seeds", "notes": null, "abbrev": "FIND_SEEDS", "is_void": false, "remarks": null, "is_public": true, "creator_id": 1, "description": null, "modifier_id": null, "action_label": "Find Seeds", "open_new_tab": false, "creation_timestamp": "2019-11-22 14:12:21.238585", "modification_timestamp": null}, "row_data": null, "action_type": "INSERT", "log_timestamp": "2019-11-22 14:12:21.238585+00", "transaction_id": "72740"}]	search	true	Find seeds	false

update platform.application
set is_void = true
where abbrev in (
	'FIND_SEEDS',
	'HARVEST_MANAGER',
	'SEED_INVENTORY_CONFIG'
) and is_void = false;
-- UPDATE 1