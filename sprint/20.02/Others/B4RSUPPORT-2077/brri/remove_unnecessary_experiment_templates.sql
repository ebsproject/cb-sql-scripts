-- B4RSUPPORT-2077 Can't create experiment

select * from master.item where process_type = 'experiment_creation_data_process' and is_void = false;

/*
RETAIN	1554	EXPT_DEFAULT_DATA_PROCESS		40				2019-08-28 15:30:06.898	1				false		8	experiment_creation_data_process	active						fa fa-th-list			experiment_creation	667986590634607809	9d81ec06-c776-4675-9033-85a818b22836	ITEM;667986590634607809
VOID	1564	EXPT_NURSERY_PARENT_LIST_DATA_PROCESS	Nursery-Parent List	40	Nursery-Parent List	Nursery-Parent List		2019-11-22 14:02:35.766461	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	730514845740304344	58edab26-54c9-45b4-9f8b-a5bc84e61365	ITEM;730514845740304344
VOID	1569	EXPT_NURSERY_CROSS_LIST_DATA_PROCESS	Nursery-Cross List	40	Nursery-Cross List	Nursery-Cross List		2019-11-22 14:02:57.16673	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	730515025256515549	41b57ed4-5aac-4756-a175-3adba77dd8f6	ITEM;730515025256515549
VOID	1596	EXPT_SEED_INCREASE_DATA_PROCESS	Seed Increase Experiment	40	Applies to experiments Seed Increase (or Seed Multiplication or Seed Regeneration).	Seed Increase Experiment		2020-02-21 23:10:28.202067	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796745191259112527	03ea318c-2509-4046-9018-5648a5db3a52	ITEM;796745191259112527
VOID	1610	EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS	Selection and Advancement Experiment	40	Applies to experiments where segregating populations (F1-Fn) are selfed for their selection and advancement. 	Seed and Advancement Experiment		2020-02-21 23:10:48.351086	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796745360281175133	6170c949-0476-4864-b3fa-3e0d14ad61f2	ITEM;796745360281175133
RETAIN	1547	EXPT_TRIAL_DATA_PROCESS	Trial Experiment	40	Create Trial Experiment	Trial Experiment		2019-08-28 15:29:32.478	1				false		8	experiment_creation_data_process	active	trial					fa fa-th-list			experiment_creation	667986301898720442	5e452d4b-6638-472a-88de-b7346c333632	ITEM;667986301898720442
VOID	1582	EXPT_CROSS_PRE_PLANNING_DATA_PROCESS	Cross Pre-planning Experiment for Hybrids	40	Parent role (Female or Male) of entries is known and planting arrangement is designed before planting for making crosses.	Cross Pre-planning Experiment for Hybrids		2020-02-21 23:10:01.397223	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796744966427640897	7bb9fb95-47f0-46f5-892a-f297f1bcd507	ITEM;796744966427640897
VOID	1624	EXPT_CROSS_POST_PLANNING_DATA_PROCESS	Cross Post-planning Experiment	40	Parent role (Female or Male) is unknown before planting and Cross List is prepared with assigned Parent Role only before making crosses.	Cross Post-planning Experiment		2020-02-21 23:11:09.858407	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796745540694967403	85e26312-f7e6-4f89-baf2-91ea0bef6ffd	ITEM;796745540694967403
VOID	1667	EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS	Nursery Cross-list for IRRIHQ	40	Nursery cross-list experiment for IRRIHQ.	Nursery Cross-list for IRRIHQ		2020-02-21 23:35:32.844934	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796757813111030934	fd87bde3-cf91-4dd8-aa26-3457d4b91ede	ITEM;796757813111030934
VOID	1678	EXPT_SEED_INCREASE_IRRI_DATA_PROCESS	Seed Increase for IRRI Experiment	40	Applies to experiments Seed Increase (or Seed Multiplication or Seed Regeneration).	Seed Increase for IRRI Experiment		2020-02-21 23:35:56.966693	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796758015461033121	d198b320-25f8-4eae-b30f-767bd0f1b839	ITEM;796758015461033121
VOID	1692	EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS	Selection and Advancement for IRRI Experiment	40	Applies to experiments where segregating populations (F1-Fn) are selfed for their selection and advancement. 	Selection and Advancement for IRRI Experiment		2020-02-21 23:36:15.233913	1				false		8	experiment_creation_data_process	active	nursery					fa fa-th-list			experiment_creation	796758168704124079	6cf60b6f-3e6b-43b6-8747-835920548a33	ITEM;796758168704124079
VOID	1638	EXPT_TRIAL_IRRI_DATA_PROCESS	Trial Experiment for IRRI	40	Trial Experiment for IRRI	Trial Experiment for IRRI		2020-02-21 23:12:09.621221	1				false		8	experiment_creation_data_process	active	trial					fa fa-th-list			experiment_creation	796746042023347321	104bd414-0ad2-47ac-873c-24acfe17c700	ITEM;796746042023347321
*/

update master.item
set is_void = true,
	notes = z_admin.append_text(notes, 'B4RSUPPORT-2077 remove unnecessary experiment templates - a.flores 2020-02-24')
where
	process_type = 'experiment_creation_data_process'
	and abbrev not in (
		'EXPT_DEFAULT_DATA_PROCESS',
		'EXPT_TRIAL_DATA_PROCESS'
	)
	and is_void = false
;
-- UPDATE 10