-- B4R-2077 Update basic info selection

select * from master.item where process_type = 'experiment_creation_data_process';

select * from platform.config where abbrev = 'EXPT_DEFAULT_BASIC_INFO_ACT_VAL';

update platform.config
set config_value = '{
    "Name": "Required experiment level metadata variables for the default data process",
    "Values": [
        {
            "disabled": false,
            "required": "required",
            "field_label": "Experiment Template",
            "order_number": 1,
            "variable_abbrev": "EXPERIMENT_TEMPLATE",
            "field_description": "Experiment Template"
        }
    ]
}'
where abbrev = 'EXPT_DEFAULT_BASIC_INFO_ACT_VAL';
-- UPDATE 1