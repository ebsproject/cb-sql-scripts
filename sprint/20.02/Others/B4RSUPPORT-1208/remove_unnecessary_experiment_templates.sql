select * from master.item where process_type = 'experiment_creation_data_process';

/*
RETAIN	1314	EXPT_DEFAULT_DATA_PROCESS	
RETAIN	1319	EXPT_TRIAL_DATA_PROCESS	Trial Experiment
VOID	1326	EXPT_NURSERY_CB_DATA_PROCESS	Nursery-Crossing Experiment
VOID	1332	EXPT_NURSERY_PARENT_LIST_DATA_PROCESS	Nursery-Parent List
VOID	1337	EXPT_NURSERY_CROSS_LIST_DATA_PROCESS	Nursery-Cross List
VOID	1375	EXPT_SEED_INCREASE_DATA_PROCESS	Seed Increase Experiment
VOID	1391	EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS	Selection and Advancement Experiment
VOID	1407	EXPT_CROSS_POST_PLANNING_DATA_PROCESS	Cross Post-Planning Experiment
VOID	1420	EXPT_TRIAL_IRRI_DATA_PROCESS	Trial Experiment for IRRI
*/

update master.item
set is_void = true,
	notes = z_admin.append_text(notes, 'B4RSUPPORT-2108 remove unnecessary experiment templates - a.flores 2020-02-24')
where
	process_type = 'experiment_creation_data_process'
	and abbrev not in (
		'EXPT_DEFAULT_DATA_PROCESS',
		'EXPT_TRIAL_DATA_PROCESS'
	)
	and is_void = false
;

/*
UPDATE 7

Query returned successfully in 140 msec.
*/