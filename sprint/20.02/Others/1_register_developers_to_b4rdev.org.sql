-- B4R-4524
-- Register developers to b4rdev.org

with t as (
	select
		*
	from (
		values
			('a.crisostomo@irri.org', 'Albert', 'Crisostomo'),
			('a.caneda@irri.org', 'Alex', 'Caneda'),
			('a.flores@irri.org', 'Argem Gerald', 'Flores'),
			('d.pagtananan@irri.org', 'Dolores', 'Pagtananan'),
			('e.banasihan@irri.org', 'Erica', 'Banasihan'),
			('e.tenorio@irri.org', 'Eugenia', 'Tenorio'),
			('e.capili@irri.org', 'Eunice Anne', 'Capili'),
			('g.romuga@irri.org', 'Gene Christhopher', 'Romuga'),
			('j.lagare@irri.org', 'Jack Elendil', 'Lagare'),
			('jp.ramos@irri.org', 'Jahzeel', 'Ramos'),
			('j.bantay@irri.org', 'Jaymar', 'Bantay'),
			('j.antonio@irri.org', 'Joanie', 'Antonio'),
			('j.f.castillo@irri.org', 'Jose Miguel', 'Castillo'),
			('k.delarosa@irri.org', 'Karen Mae', 'DelaRosa'),
			('k.ana@irri.org', 'Kenichii', 'Ana'),
			('l.gallardo@irri.org', 'Larise', 'Gallardo'),
			('l.b.go@irri.org', 'Lois Angelica', 'Go (IRRI)'),
			('m.pasang@irri.org', 'Maria Relliza', 'Pasang'),
			('m.karkkainen@irri.org', 'Marko', 'Karkkainen'),
			('n.carumba@irri.org', 'Nikki', 'Carumba'),
			('p.sinohin@irri.org', 'Philip Joshua', 'Sinohin'),
			('r.lat@irri.org', 'Renee Arianne', 'Lat'),
			('r.e.carpio@irri.org', 'RuthErica', 'Carpio'),
			('v.calaminos@irri.org', 'Viana Carla', 'Calaminos')
		) t (
			email,
			first_name,
			last_name
		)
)
select
	format($$INSERT INTO master.user (username, email, first_name, last_name, display_name, valid_end_date, user_type) VALUES ('%s', '%s', '%s', '%s', '%s', '2020-12-31', 1);$$,
		substring(t.email from '(.*)@'),
		t.email,
		t.first_name,
		t.last_name,
		(t.last_name || ', ' || t.first_name)
    )
from
	t