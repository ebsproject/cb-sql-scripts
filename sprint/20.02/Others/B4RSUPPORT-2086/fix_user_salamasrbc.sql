select * from z_admin.get_user('salamasrbc@gmail.com');
-- 28	28	salamasrbc@gmail.com	salamasrbc	0	1	Salam	M A		Salam, M A	f20a6487-2bb9-45bc-a8b9-541e775a7438	

select * from z_admin.get_program('ASRBC');
-- 112	112	ASRBC	Advanced Seed Research & Biotech Centre	Rice breeding and seed in Bangladesh		20	CREATED	true	21b1f178-c199-4c2d-bbe6-a062aab98ef2	false	

select * from master.program_team where program_id = 112 and is_void = false;
-- 2	112	8		2019-05-31 06:02:07.593588	5			created using program mgt tool	false

select * from master.team_member where member_id = 28 and is_void = false;
-- 80	8	28	20	100		2019-05-31 06:05:21.858645	5	2019-05-31 06:06:47.581548	5		false
-- updated user_role_id to 76

select * from master.team_member where team_id = 8 and member_id = 28 and is_void = false;
-- 71	7	28	20	136		2019-05-28 03:02:24.908613	5	2019-05-28 03:02:29.535073	5		false
-- updated role_id to 21

select * from master.user_role where user_id = 28 and is_void = false;
-- 76	28	21		2019-05-27 05:31:44.159571	5				false

select * from master.role where is_void = false and abbrev = 'B4R_USER';
-- 21	B4R_USER	B4R User	Default role assigned to all Breeding4Rice users	Breeding4Rice User	1		2019-04-26 07:43:19.72598	1				false	dbcd962e-fcd9-49d8-b0f0-18c696d730d7	

delete from master.team_member where team_id = 8 and member_id = 28 and is_void = false;
-- DELETE 1

select * from master.role where is_void = false;

select * from master.team_member where role_id not in (select id from master.role where is_void = false);
-- 82	8	32	22	102		2019-05-31 06:05:34.63003	5	2019-05-31 06:06:36.56141	5		false
-- 98	8	41	22	118		2019-08-01 05:51:12.40483	5	2019-08-01 05:51:30.661191	5		false
-- 100	8	42	22	120		2019-08-01 05:53:37.096193	5	2019-08-01 05:53:40.69734	5		false
-- updated role_id to 21

select * from master.user_role where id in (102, 118, 120);
-- 102	32	22		2019-05-31 06:05:34.622499	5	2019-05-31 06:06:36.555065	5		false
-- 118	41	22		2019-08-01 05:51:12.397721	5	2019-08-01 05:51:30.656349	5		false
-- 120	42	22		2019-08-01 05:53:37.089915	5	2019-08-01 05:53:40.692134	5		false
-- updated role_id to 21


select * from master.user_role where role_id not in (select id from master.role where is_void = false);
-- 100	28	20		2019-05-31 06:05:21.851106	5	2019-05-31 06:06:47.576237	5		true
-- 91	28	20		2019-05-28 03:02:24.899708	5	2019-05-28 03:02:29.529567	5		true
-- updated role_id to 21
