-- B4R-4528
-- CIMMYT-20.01  error on commit | Curate data and add unique constraints to temp tables

-- duplicate temp_entry records
with t as (
	select
		ote.study_id,
		ote.entno,
		array_agg(ote.id order by ote.id asc) temp_entry_id_list,
		min(ote.id) retain_temp_entry_id,
		count(1)
	from
		operational.temp_entry ote
	-- where
		-- ote.is_void = false
	group by
		ote.study_id,
		ote.entno
), u as (
	select
		t.*
	from
		t
	where
		t.count > 1
	order by
		t.study_id,
		t.entno
)--/*
select
	u.*
from
	u
order by
	u.study_id desc,
	u.entno asc
;--*/
/*
update
	operational.temp_entry ote
set
	is_void = true,
	entno = ote.id,
	notes = z_admin.append_text(ote.notes, 'B4R-4528 fix duplicates - a.flores 2020-02-17')
from
	u
where
	ote.id = any(u.temp_entry_id_list)
	and ote.id <> u.retain_temp_entry_id
	-- and ote.is_void = false
;--*/


-- duplicate temp_plot records
with t as (
	select
		otp.study_id,
		otp.plotno,
		array_agg(otp.id order by otp.id asc) temp_plot_id_list,
		min(otp.id) retain_temp_plot_id,
		count(1)
	from
		operational.temp_plot otp
	-- where
		-- otp.is_void = false
	group by
		otp.study_id,
		otp.plotno
), u as (
	select
		t.*
	from
		t
	where
		t.count > 1
	order by
		t.study_id,
		t.plotno
)--/*
select
	u.*
from
	u
order by
	u.study_id desc,
	u.plotno asc
;--*/
/*
update
	operational.temp_plot otp
set
	is_void = true,
	plotno = otp.id,
	notes = z_admin.append_text(otp.notes, 'B4R-4528 fix duplicates - a.flores 2020-02-17')
from
	u
where
	otp.id = any(u.temp_plot_id_list)
	and otp.id <> u.retain_temp_plot_id
	-- and otp.is_void = false
;--*/


------------------------

/*
-- operational.temp_entry
-- add unique index to operational.temp_entry study_id + entno
CREATE UNIQUE INDEX
    temp_entry_study_id_entno_uidx
ON
    operational.temp_entry (
        study_id,
        entno
    )
;
 
-- add unique constraint to previously defined unique index
ALTER TABLE
    operational.temp_entry
ADD CONSTRAINT
    temp_entry_study_id_entno_ukey
UNIQUE USING INDEX
    temp_entry_study_id_entno_uidx
;


------------------------


-- operational.temp_plot
-- add unique index to operational.temp_plot study_id + plotno
CREATE UNIQUE INDEX
    temp_plot_study_id_plotno_uidx
ON
    operational.temp_plot (
        study_id,
        plotno
    )
;
 
-- add unique constraint to previously defined unique index
ALTER TABLE
    operational.temp_plot
ADD CONSTRAINT
    temp_plot_study_id_plotno_ukey
UNIQUE USING INDEX
    temp_plot_study_id_plotno_uidx
;--*/