/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Larise L. Gallardo <l.gallardo@irri.org>
 * @date 2020-02-13 01:44:36
 */

--  Create variable set for mandatory variables of data export tool

set role b4radmin;

-- Create record for variable set containing mandatory variables
insert into master.variable_set (abbrev, name, description, display_name, creator_id, notes, varsetusage, usage, access_type)
values ('DATA_EXPORT_MANDATORY_VARIABLES', 'Data Export Mandatory Variables', 'Mandatory variables for data export', 'Data Export Mandatory Variables',1, 'added by l.gallardo', 'APPLICATION', 'data export', 'PRIVATE');

-- Populate variable set members
insert into master.variable_set_member (variable_set_id, variable_id, order_number, creator_id)
select t. vs_id, t.v_id, t.v_order, 1
	from (
	SELECT 
	    (select id from master.variable_set vs where vs.abbrev='DATA_EXPORT_MANDATORY_VARIABLES') as vs_id,
            v.id as v_id,
            (row_number() over (order by v.id)) as v_order
        from
            master.variable v
            where v.is_void = false 
             and lower(v.abbrev) IN ('study_id','study_name','location','study','design','plot_area_sqm_cont','dist_bet_rows','rows_per_plot_cont','hills_per_row_cont','dist_bet_hills','product_id','entry_id','entno','entcode','designation','gid','product_preferred_name','line_name','cross_name','cultivar_name','international_testing_number','local_common_name','elite_lines','derivative_name','alternative_cultivar_name','release_name','plot_id','plotno','plot_code','rep','design_y','design_x','plot_area_ha_cont') order by label ASC
	) as t;