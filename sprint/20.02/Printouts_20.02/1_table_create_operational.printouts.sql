/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Maria Relliza A. Pasang <m.pasang@irri.org>
* @date 2020-02-12 14:23:02
*/


----------------------------------------------------------------------------------------------------

-- Printouts template catalog

-- drop table if exists operational.printouts;

create table operational.printouts (
    -- primary key column
    id serial not null, -- Identifier of the record within the table
    
    -- columns
    abbrev varchar NOT NULL, -- Abbreviation of the record
    "name" varchar NOT NULL, -- Printouts template name
    "description" text, -- More information about the record
    material_type varchar NOT NULL, -- Type of the material used if rolled pages or flat pages
    material_size_width float NOT NULL, -- Material size width
    material_size_length float NOT NULL, -- Material size length
    material_size_unit varchar NOT NULL, -- Material size unit (in,mm,etc.)

    -- audit columns
    remarks text, -- Additional details about the record
    creation_timestamp timestamp not null default current_timestamp, -- Timestamp when the record was added to the table
    creator_id integer not null, -- ID of the user who added the record to the table
    modification_timestamp timestamp, -- Timestamp when the record was last modified
    modifier_id integer, -- ID of the user who last modified the record
    notes text, -- Additional details added by an admin; can be technical or advanced details
    is_void boolean not null default false, -- Indicator whether the record is deleted (true) or not (false)
    event_log jsonb, -- Historical transactions of the record
    record_uuid uuid not null default uuid_generate_v4(), -- Universally unique identifier (UUID) of the record
    
    -- primary key constraint
    constraint printouts_id_pkey primary key (id), -- Primary key constraint
    
    -- audit foreign relations
    constraint printouts_creator_id_fkey foreign key (creator_id)
        references master.user (id) match simple
        on update cascade on delete restrict, -- Creator foreign key relation
    constraint printouts_modifier_id_fkey foreign key (modifier_id)
        references master.user (id) match simple
        on update cascade on delete restrict -- Modifier foreign key relation
);

/*-- indices
create index printouts_column_name_idx
    on operational.printouts
    using btree (column_name);
--*/

create index printouts_is_void_idx
    on operational.printouts
    using btree (is_void);

create index printouts_creator_id_idx
    on operational.printouts
    using btree (creator_id);

create index printouts_modifier_id_idx
    on operational.printouts
    using btree (modifier_id);

create index printouts_record_uuid_idx
    on operational.printouts
    using btree (record_uuid);

-- comments
comment on table operational.printouts is 'Printouts template catalog';

comment on column operational.printouts.id is 'Identifier of the record within the table';

comment on column operational.printouts.abbrev is 'Abbreviation of the record';
comment on column operational.printouts.name is 'Printouts template name';
comment on column operational.printouts.description is 'More information about the record';
comment on column operational.printouts.material_type is 'Type of the material used if rolled pages or flat pages';
comment on column operational.printouts.material_size_width is 'Material size width';
comment on column operational.printouts.material_size_length is 'Material size length';
comment on column operational.printouts.material_size_unit is 'Material size unit(in,mm,etc.)';

comment on column operational.printouts.remarks is 'Additional details about the record';
comment on column operational.printouts.creation_timestamp is 'Timestamp when the record was added to the table';
comment on column operational.printouts.creator_id is 'ID of the user who added the record to the table';
comment on column operational.printouts.modification_timestamp is 'Timestamp when the record was last modified';
comment on column operational.printouts.modifier_id is 'ID of the user who last modified the record';
comment on column operational.printouts.notes is 'Additional details added by an admin; can be technical or advanced details';
comment on column operational.printouts.is_void is 'Indicator whether the record is deleted (true) or not (false)';
comment on column operational.printouts.event_log is 'Historical transactions of the record';
comment on column operational.printouts.record_uuid is 'Universally unique identifier (UUID) of the record';
--*/


-- uncomment below to audit this table
--/*
SELECT * FROM z_admin.audit_table(
    in_table_name := 'operational.printouts'
);
--*/
