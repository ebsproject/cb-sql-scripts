/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>.
*/

/**
* @author Maria Relliza A. Pasang <m.pasang@irri.org>
* @date 2020-02-18 01:44 pm
*/

-- FUNCTION: operational.get_sampling_tag_gsl_values(integer, integer, integer)

-- DROP FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer);

CREATE OR REPLACE FUNCTION operational.get_sampling_tag_gsl_values(
	studyid integer,
	from_page integer,
	to_page integer,
	OUT page_number bigint,
	OUT sample_name character varying,
	OUT sample_no integer)
    RETURNS SETOF record 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE

  plot_rec varchar;
  result varchar; 
  start_query varchar;
  end_query varchar;
BEGIN
  /**
    * Get the plot values of the given study id for the Sampling Tags - GSL
    * @param study id
    * @param from_page
    * @param to_page
    * @return query study and sow order 
    * @author Maria Relliza Pasang <m.pasang@irri.org>
    * @date 2020-02-18 01:44 pm
  **/

  if(from_page is not NULL)
  then
    start_query = 'select * from ( ';
    end_query = ' ) as t where t.page_number between ' || from_page || ' and ' || to_page;
  else
    start_query = '';
    end_query = '';
  end if;

  plot_rec = start_query || 
    'select
      row_number () over (order by sample_no) as page_number,
      sample_name,
      sample_no
    from seed_warehouse.sample
    where id in (select sample_id from operational.entry where study_id = ''' || studyid || ''' and is_void = false)
    order by sample_no'
	|| end_query;
  
  return query execute plot_rec;
END;$BODY$;

COMMENT ON FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer)
    IS 'Function for the Sampling Tags - GSL values';
