/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Maria Relliza A. Pasang <m.pasang@irri.org>
* @date 2020-02-12 04:13:05
*/

-- Replace current printouts function/stored procedures' names 
-- with operational.get_<template abbrev>_values 

ALTER FUNCTION operational.get_harvest_label_cross_values(integer, integer, integer)
    RENAME TO get_harv_lbl_hb_crosses_values;
	
ALTER FUNCTION operational.get_harvest_label_nursery_values(integer, integer, integer)
    RENAME TO get_harv_lbl_nurseries_values;
	
ALTER FUNCTION operational.get_harvest_label_rga_values(integer, integer, integer)
    RENAME TO get_harv_lbl_rga_values;
	
ALTER FUNCTION operational.get_harvest_label_trial_values(integer, integer, integer)
    RENAME TO get_harv_lbl_trials_values;

ALTER FUNCTION operational.get_harvest_tag_multiple_bags_values(integer, integer, integer)
    RENAME TO get_harv_tag_mult_bag_values;
	
ALTER FUNCTION operational.get_harvest_tag_selection_values(integer, integer, integer)
    RENAME TO get_harv_tag_selection_values;