/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Maria Relliza A. Pasang <m.pasang@irri.org>
 * @date 2020-02-12 03:23:23
 */

--  Populate operational.printouts table

INSERT INTO
	operational.printouts (abbrev, name, description, material_type, material_size_width, material_size_length, material_size_unit, creator_id, notes)
VALUES
    ('HARV_TAG_MULT_BAG', 'Harvest Tag - Multiple Bags', null, 'rolled', 102 , 70, 'mm', 1, 'added by m.pasang'), -- Harvest Tag - Multiple Bags
    ('HARV_TAG_SELECTION', 'Harvest Tag - Selection', null, 'rolled', 102 , 70, 'mm', 1, 'added by m.pasang'), -- Harvest Tag - Selection
    ('HARV_LBL_TRIALS', 'Harvest Label - Trials', null, 'rolled', 66.675 , 34.925, 'mm', 1, 'added by m.pasang'), -- Harvest Label - Trials
    ('HARV_LBL_NURSERIES', 'Harvest Label - Nurseries', null, 'rolled', 66.675 , 34.925, 'mm', 1, 'added by m.pasang'), -- Harvest Label - Nurseries
    ('HARV_LBL_HB_CROSSES', 'Harvest Label - HB Crosses', null, 'rolled', 66.675 , 34.925, 'mm', 1, 'added by m.pasang'), -- Harvest Label - HB Crosses
    ('HARV_LBL_RGA', 'Harvest Label - RGA', null, 'rolled', 66.675 , 34.925, 'mm', 1, 'added by m.pasang'), -- Harvest Label - RGA
    ('FIELD_TAG_DEFAULT', 'Field Tag', null, 'rolled', 3.5 , 2, 'in', 1, 'added by m.pasang'), -- Field Tag
    ('FIELD_TAG_MET_INGER', 'Field Tag - MET/INGER', null, 'rolled', 3.5 , 2, 'in', 1, 'added by m.pasang'), -- Field Tag - MET/INGER
    ('FIELD_TAG_MET_INGER_OFFSITE', 'Field Tag - MET/INGER (Off-site)', null, 'rolled', 3.5 , 2, 'in', 1, 'added by m.pasang'), -- Field Tag - MET/INGER (Off-site)
    ('SEEDBED_TAG', 'Seedbed Tag', null, 'rolled', 3.5 , 2, 'in', 1, 'added by m.pasang'), -- Seedbed Tag
    ('SAMPLING_TAG_GSL', 'Sampling Tag - GSL', null, 'rolled', 3.5 , 2, 'in', 1, 'added by m.pasang'), -- Sampling Tag - GSL
    ('SAMPLING_LBL_GSL', 'Sampling Label - GSL', null, 'rolled', 66.675 , 34.925, 'mm', 1, 'added by m.pasang'); -- Sampling Label - GSL