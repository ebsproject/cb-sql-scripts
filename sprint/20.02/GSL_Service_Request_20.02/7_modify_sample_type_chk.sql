﻿
alter table seed_warehouse.sample drop constraint sample_sample_type_chk;

ALTER TABLE seed_warehouse.sample
  ADD CONSTRAINT sample_sample_type_chk CHECK (sample_type::text = ANY (ARRAY['seed'::text, 'tissue'::text, 'leaf'::text]));