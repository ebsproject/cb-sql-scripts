﻿--drop table seed_warehouse.plate_layout

create table seed_warehouse.plate_layout
(
	id serial not null,
	program_id integer,
	abbrev varchar,
	name varchar,
	number serial not null,
	status varchar,
	creator_id integer,
	creation_timestamp timestamp without time zone,
	modifier_id integer,
	modification_timestamp timestamp without time zone,
	notes text,
	is_void boolean default false
);


ALTER TABLE seed_warehouse.plate_layout
  ADD CONSTRAINT plate_layout_id_pkey PRIMARY KEY(id);
