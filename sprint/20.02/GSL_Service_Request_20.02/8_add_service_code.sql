﻿alter table operational.platform add column service_code varchar;

update operational.platform pl
set service_code = t.service_code
from
(
	select 
		id,
		case when id > 2 then 'NT'
		when id = 1 then 'NF'
		else 'RC'
		end as service_code
	from 
		operational.platform
) as t
where t.id = pl.id
;