﻿--drop table seed_warehouse.temp_sample

create table seed_warehouse.temp_sample
(
	id serial not null,
	service_request_id integer,
	data_id integer,
	list_member_id integer,
	sample_type varchar,
	product_id integer,
	sample_name varchar,
	sample_rep_no integer,
	sample_no integer,
	plate_id integer,
	row varchar,
	row_index integer,
	col integer,
	plate_layout_no integer,
	creator_id integer,
	creation_timestamp timestamp without time zone,
	modifier_id integer,
	modification_timestamp timestamp without time zone,
	is_void boolean default FALSE
)