﻿
alter table operational.platform add column reserved_wells varchar;


update operational.platform
set
	reserved_wells = (
		case when abbrev != '1K_RICA_SNP' then 'H11,H12'
		else
			'G12,H12'
		end
	)
;