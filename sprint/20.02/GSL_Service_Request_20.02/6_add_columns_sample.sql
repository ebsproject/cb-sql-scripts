﻿alter table seed_warehouse.sample add column plate_id integer, add column sample_rep_no integer, add column plate_layout_no integer, add column row varchar, add column col integer, add column row_index integer;


alter table seed_warehouse.sample add column sample_barcode serial NOT NULL;

ALTER SEQUENCE seed_warehouse.sample_sample_barcode_seq RESTART WITH 10000000;