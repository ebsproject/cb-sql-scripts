INSERT INTO operational.platform (abbrev, vendor_id, name, contact_name, contact_email, 
								  contact_phone, shipping_address, plate_orientation, 
								  creation_timestamp, creator_id, service_code, reserved_wells)
SELECT
	'1K_RICA_SNP_V2',
	vendor_id,
	'1k RiCA SNP genotyping V2',
	contact_name,
	contact_email,
	contact_phone,
	shipping_address,
	plate_orientation,
	now(),
	1,
	'DRT',
	'H11,H12'
FROM
	operational.platform
limit 1
;

INSERT INTO operational.platform (abbrev, vendor_id, name, contact_name, contact_email, 
								  contact_phone, shipping_address, plate_orientation, 
								  creation_timestamp, creator_id, service_code, reserved_wells)
SELECT
	'1K_RICA_SNP_V3',
	vendor_id,
	'1k RiCA SNP genotyping V3',
	contact_name,
	contact_email,
	contact_phone,
	shipping_address,
	plate_orientation,
	now(),
	1,
	'AGR',
	'H11,H12'
FROM
	operational.platform
limit 1
;


UPDATE
	operational.platform
SET
	service_code = CASE 
		WHEN service_code = 'NF' THEN 'INF'
		WHEN service_code = 'RC' THEN 'RiCA'
		ELSE 'TGP'
	END
;