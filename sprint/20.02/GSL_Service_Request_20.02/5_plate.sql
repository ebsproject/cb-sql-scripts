﻿--drop table seed_warehouse.plate;

create table seed_warehouse.plate
(
	id serial not null,
	plate_layout_id integer,
	plate_name varchar,
	plate_no integer,
	plate_barcode serial,
	record_uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
	creator_id integer,
	creation_timestamp timestamp without time zone,
	modifier_id integer,
	modification_timestamp timestamp without time zone,
	notes text,
	is_void boolean default false
);

ALTER SEQUENCE seed_warehouse.plate_plate_barcode_seq RESTART WITH 10000000;

