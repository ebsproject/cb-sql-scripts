UPDATE
  api.messages set message = 'There is at least one(1) plot that does not exist in the location.'
WHERE
  id in (select id from api.messages m where message ='There is at least one(1) plot that do not exist in the location.');

UPDATE
  api.messages set message = 'There is at least one(1) invalid record in occurrences. The dataUnit should be one of the following: plot, cross'
WHERE
  id in (select id from api.messages m where message ='There is at least one(1) invalid record in occurrences. The dataUnit should be one of the following: plot');
