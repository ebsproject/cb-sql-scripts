Delete from platform.config where abbrev='HM_PACKAGE_LABEL_PATTERN_RICE_DEFAULT';
Delete from platform.config where abbrev='HM_LABEL_PATTERN_PACKAGE_RICE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_LABEL_PATTERN_PACKAGE_RICE_DEFAULT',
        'Harvest Manager Rice Package Label Pattern-Default',
$$			
	
{
    "harvest_mode": {
        "cross_method": {
            "germplasm_state/germplasm_type": {
                "harvest_method": [
                    {
                        "type": "free-text",
                        "value": "ABC",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "entity": "<entity>",
                        "field_name": "<field_name>",
                        "order_number": 1
                    },
                    {
                        "type": "delimiter",
                        "value": "-",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 3
                    },
                    {
                        "type": "db-sequence",
                        "schema": "<schema>",
                        "sequence_name": "<sequence_name>",
                        "order_number": 4
                    }
                ]
            }
        }
    },
    "PLOT": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "plot",
                        "field_name" : "plotCode",
                        "order_number":0
                    }
                ]
            }
        }
    },
    "CROSS": {
        "default": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "cross",
                        "field_name" : "crossName",
                        "order_number":0
                    }
                ]
            }
        },
        "single cross": {
            "default" : {
                "default" : [
                    {
                        "type" : "field",
                        "entity" : "cross",
                        "field_name" : "crossName",
                        "order_number":0
                    }
                ],
                "bulk" : [
                    {
                        "type" : "field",
                        "entity" : "cross",
                        "field_name" : "crossName",
                        "order_number":0
                    }
                ]
            }
        }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-983 - j.bantay 2021-04-15');