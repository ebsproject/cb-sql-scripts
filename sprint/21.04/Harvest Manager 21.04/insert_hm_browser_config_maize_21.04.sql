-- CORB-988 HM - Update harvest data browser configuration
--------------------------------------------------------------------------------------
-- MAIZE
DELETE FROM platform.config WHERE abbrev='HM_BROWSER_CONFIG_MAIZE';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
(
    'HM_BROWSER_CONFIG_MAIZE',
    'Harvest Manager Browser Config for Maize',
$$
    {
        "name" : "HM_BROWSER_CONFIG_MAIZE",
        "values": [
            {
                "plot" : {
                    "germplasm_state" : {
                        "fixed" : {
                            "display_column" : [
                                "harvestDate",
                                "harvestMethod"
                            ],
                            "harvest_method" : [
                                "Bulk"
                            ]
                        },
                        "not_fixed" : {
                            "display_column" : [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ],
                            "harvest_method" : [
                                "Bulk",
                                "Individual ear"
                            ]
                        }
                    }
                },
                "cross" : {
                    "cross_method" : {
                        "CROSS_METHOD_SINGLE_CROSS": {
                            "harvest_method": [
                                "Bulk"
                            ]
                        }
				    },
                    "display_column": [
                        "harvestDate",
                        "harvestMethod"
                    ]
                }
            }
        ]
    }
$$,
    1,
    'harvest_manager',
    1,
    'created maize browser configuration'
);
