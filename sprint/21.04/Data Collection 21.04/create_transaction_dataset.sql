
CREATE OR REPLACE FUNCTION data_terminal.create_transaction_dataset(var_transaction_id integer, var_user_id integer, var_dataset json, var_variables text[])
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$

DECLARE
    tableName text; 
    columnString text; 
    items RECORD; 
    locationId integer;	
    insertedCount integer;	
    updatedCount integer; 
    insertedCrossCount integer;	
    updatedCrossCount integer; 
begin
    insertedCrossCount := 0;	
    updatedCrossCount := 0; 
    updatedCount := 0;
    insertedCount := 0;
    tableName := 'dataset_'||var_transaction_id||'_'||var_user_id;
    columnString := 'plot_id, cross_id, '|| array_to_string(var_variables, ',');

    SELECT location_id INTO locationId FROM data_terminal."transaction" t WHERE id = var_transaction_id;
    
    -- create temporary table
    execute '
    CREATE TEMP TABLE dataset_'||var_transaction_id||'_'||var_user_id||'(
        id serial NOT NULL,
        plot_id text,
        cross_id text, 
        '||(SELECT string_agg(a.concat,',') FROM (SELECT concat(unnest(var_variables),' text'))a)||'
    ) ON COMMIT DROP
    ';

    -- insert json data to temporary table
    execute ' 
	    WITH data AS (
			SELECT 
			'''||var_dataset||'''
			::jsonb AS jsondata
		)
		INSERT INTO '||tableName ||'('|| columnString || ') 
		SELECT 
			(elems->>''plot_id'')::int as plot_id,
			(elems->>''cross_id'')::int as cross_id,
            '||(SELECT STRING_AGG(b.concat, ',') from (SELECT CONCAT('(elems->>''',unnest(var_variables),''')::text as ',unnest(var_variables)))b )||'
		FROM 
			data,
		    jsonb_array_elements(jsondata) AS elems
    ';
    
    -- verify if plot_id and cross_id is not null
    FOR items IN EXECUTE format('SELECT * FROM %I WHERE plot_id IS NULL AND cross_id IS NULL ORDER BY id ASC', tableName)
    LOOP
        return 'Record '|| items.id || ' does not have PLOT_ID or CROSS_ID.';
    END LOOP;
   
    -- verify if plot_id exists
    FOR items IN EXECUTE format('SELECT * FROM %I WHERE plot_id IS NOT NULL ORDER BY id ASC', tableName)
    LOOP
        if  (items.cross_id !~ E'^\\d+$') then 
            return 'Record '|| items.id || ' must be an integer';

        elsif NOT EXISTS(SELECT 1 FROM experiment.plot WHERE location_id = locationId AND id = items.plot_id::integer AND is_void = FALSE) then 
            return 'Record '|| items.id || ' with PLOT_ID of '|| items.plot_id || ' is not a plot of location.';

        end if;
    END LOOP;

    -- verify if cross_id exists
    FOR items IN EXECUTE format('SELECT * FROM %I WHERE cross_id IS NOT NULL ORDER BY id asc', tableName)
    LOOP
        if  (items.cross_id !~ E'^\\d+$') then 
            return 'Record '|| items.id || ' must be an integer';

        elsif NOT EXISTS(
            SELECT 1 
            FROM 
                germplasm.cross "cross"
                JOIN experiment.experiment as expt
                    ON expt.id = "cross".experiment_id
                JOIN experiment.occurrence AS occ
                    ON occ.experiment_id = expt.id
                JOIN experiment.location_occurrence_group AS logrp
                    ON logrp.occurrence_id = occ.id
                JOIN experiment.location AS loc
                    ON loc.id = logrp.location_id
            WHERE 
                loc.id = locationId 
                AND "cross".id = items.cross_id::integer 
                AND "cross".is_void = FALSE) 
            then 

            return 'Record '|| items.id || ' with CROSS_ID of '|| items.cross_id || ' is not a cross of location.';

        end if;
    END LOOP;
    
    -- update transaction dataset plot data records if already existing but with a different uploaded value
    EXECUTE (
    SELECT '
        WITH rows AS (
            UPDATE data_terminal.transaction_dataset td
            SET 
                is_suppressed = FALSE,
                value = a.value,
                modification_timestamp = now(),
                modifier_id = '|| var_user_id ||'
                
            FROM
            (
                SELECT 
                    DISTINCT ON (u.variable,u.val,t.plot_id)
                    '|| var_transaction_id ||' AS transaction_id,
                    u.val AS value,
                    '||var_user_id||' AS creator_id,
                    ''plot_data'' AS entity,
                    t.plot_id::integer AS entity_id,
					(SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id
                FROM  
                    ' || tableName || ' t
                        LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', tempVar, tempVar), ', ') || ') u(variable, val) ON true,
                    master.variable v
                WHERE 
                    lower(v.abbrev) = u.variable
                    AND trim(u.val) <> ''''
                    AND plot_id IS NOT NULL
                    AND NOT EXISTS(
                      SELECT 1 
                      FROM
                          experiment.plot_data pd
                      WHERE 
                          t.plot_id::int = pd.plot_id
                          AND pd.data_value = u.val
                          AND pd.variable_id = v.id
                          AND pd.is_void = FALSE
                    )
            )a
            WHERE 
                td.entity_id = a.entity_id
                AND td.transaction_id = '||var_transaction_id||'
                AND td.variable_id = a.variable_id
                AND td.entity = ''plot_data''
                AND td.value IS NOT NULL 
                AND td.value <> a.value
                AND td.is_void = FALSE
            returning id 
        ) SELECT count(1) FROM rows' 
    FROM   
          (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(tempVar)
      ) INTO updatedCount;

    -- update transaction dataset cross data records if already existing but with a different uploaded value
    EXECUTE (
    SELECT '
        WITH rows AS (
            UPDATE data_terminal.transaction_dataset td
            SET 
                is_suppressed = FALSE,
                value = a.value,
                modification_timestamp = now(),
                modifier_id = '|| var_user_id ||'
                
            FROM
            (
                SELECT 
                    DISTINCT ON (u.variable,u.val,t.cross_id)
                    '|| var_transaction_id ||' AS transaction_id,
                    u.val AS value,
                    '||var_user_id||' AS creator_id,
                    ''cross_data'' AS entity,
                    t.cross_id::integer AS entity_id,
					(SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id
                FROM  
                    ' || tableName || ' t
                        LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', tempVar, tempVar), ', ') || ') u(variable, val) ON true,
                    master.variable v
                WHERE 
                    lower(v.abbrev) = u.variable
                    AND trim(u.val) <> ''''
                    AND cross_id IS NOT NULL
                    AND NOT EXISTS(
                      SELECT 1 
                      FROM
                          germplasm.cross_data cd
                      WHERE 
                          t.cross_id::int = cd.cross_id
                          AND cd.data_value = u.val
                          AND cd.variable_id = v.id
                          AND cd.is_void = FALSE
                    )
            )a
            WHERE 
                td.entity_id = a.entity_id
                AND td.transaction_id = '||var_transaction_id||'
                AND td.variable_id = a.variable_id
                AND td.entity = ''cross_data''
                AND td.value IS NOT NULL 
                AND td.value <> a.value
                AND td.is_void = FALSE
            returning id 
        ) SELECT count(1) FROM rows' 
    FROM   
          (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(tempVar)
      ) INTO updatedCrossCount;
         
    -- insert transaction dataset plot data records if not existing
    EXECUTE (
    SELECT '
        WITH rows AS (
          INSERT INTO data_terminal.transaction_dataset 
            (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
              is_void, is_generated, is_suppressed)
            SELECT 
                DISTINCT ON (u.variable,value,t.plot_id)
                '|| var_transaction_id ||' AS transaction_id,
                ''plot'' AS data_unit,
                (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
                u.val AS value,
                '||var_user_id||' AS creator_id,
                ''plot_data'' AS entity,
                t.plot_id::integer AS entity_id,
                FALSE AS is_void,
                FALSE AS is_generated,
                FALSE AS is_suppressed
            FROM  
                ' || tableName || ' t
                    LEFT   JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true,
                master.variable v
            WHERE 
                lower(v.abbrev) = u.variable
                AND plot_id IS NOT NULL
                AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        data_terminal.transaction_dataset td
                    WHERE 
                        td.transaction_id= '||var_transaction_id||'
                        AND t.plot_id::int = td.entity_id
                        AND td.value IS NOT NULL 
                        AND td.value = u.val
                        AND td.variable_id = v.id
                        AND td.is_void = FALSE
                        AND td.entity = ''plot_data''
                ) AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        experiment.plot_data pd
                    WHERE 
                        t.plot_id::int = pd.plot_id
                        AND pd.data_value = u.val
                        AND pd.variable_id = v.id
                        AND pd.is_void = FALSE
                )
                AND trim(u.val) <> ''''
            RETURNING id 
        ) SELECT count(1) FROM rows' 
    FROM   
        (SELECT unnest(var_variables) except SELECT unnest(array['plot_id']) )t(y)
    )INTO insertedCount;

    -- insert transaction dataset cross data records if not existing
    EXECUTE (
    SELECT '
        WITH rows AS (
          INSERT INTO data_terminal.transaction_dataset 
            (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
              is_void, is_generated, is_suppressed)
            SELECT 
                DISTINCT ON (u.variable,value,t.cross_id)
                '|| var_transaction_id ||' AS transaction_id,
                ''cross'' AS data_unit,
                (SELECT id FROM master.variable WHERE abbrev = upper(u.variable)) AS variable_id, 
                u.val AS value,
                '||var_user_id||' AS creator_id,
                ''cross_data'' AS entity,
                t.cross_id::integer AS entity_id,
                FALSE AS is_void,
                FALSE AS is_generated,
                FALSE AS is_suppressed
            FROM  
                ' || tableName || ' t
                    LEFT   JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true,
                master.variable v
            WHERE 
                lower(v.abbrev) = u.variable
                AND cross_id IS NOT NULL
                AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        data_terminal.transaction_dataset td
                    WHERE 
                        td.transaction_id= '||var_transaction_id||'
                        AND t.cross_id::int = td.entity_id
                        AND td.value IS NOT NULL 
                        AND td.value = u.val
                        AND td.variable_id = v.id
                        AND td.is_void = FALSE
                        AND td.entity = ''cross_data''
                ) AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        germplasm.cross_data cd
                    WHERE 
                        t.cross_id::int = cd.cross_id
                        AND cd.data_value = u.val
                        AND cd.variable_id = v.id
                        AND cd.is_void = FALSE
                )
                AND trim(u.val) <> ''''
            RETURNING id 
        ) SELECT count(1) FROM rows' 
    FROM   
        (SELECT unnest(var_variables) except SELECT unnest(array['cross_id']) )t(y)
    )INTO insertedCrossCount;
          
    return insertedCount + updatedCount + insertedCrossCount + updatedCrossCount;
END;
$function$
;
