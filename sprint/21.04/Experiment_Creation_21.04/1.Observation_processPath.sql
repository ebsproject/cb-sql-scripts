insert into master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage)
values('OBSERVATION_DATA_PROCESS','Observation',40,'Are experiment objectives such as yield, disease, physiological, etc.','Breeding Trial Experiment',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');

--create Specify basic information
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

--create Specify entry list
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol');

--create Specify design
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_DESIGN_ACT','Planting Arrangement',30,'Planting Arrangement','Planting Arrangement',1,'active','fa fa-files-o');

--Additions in design tab
--Create design tabs
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_ADD_BLOCKS_ACT','Add blocks',20,'Add blocks','Add blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_ASSIGN_ENTRIES_ACT','Assign entries',20,'Assign entries','Assign entries',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_MANAGE_BLOCKS_ACT','Manage blocks',20,'Manage blocks','Manage blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_OVERVIEW_ACT','Overview',20,'Overview','Overview',1,'active','fa fa-random');



--create Specify Protocols
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_PROTOCOL_ACT','Specify Protocol',30,'Specify Protocol','Protocol',1,'active','fa fa-map-marker');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_PLANTING_PROTOCOL_ACT','Planting Protocol',20,'Planting Protocol','Planting',1,'active','fa fa-pagelines');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_POLLINATION_PROTOCOL_ACT','Pollination Protocol',20,'Pollination Protocol','Pollination',1,'active','fa fa-crosshairs');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_TRAITS_PROTOCOL_ACT','Traits Protocol',20,'Traits Protocol','Traits',1,'active','fa fa-braille');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_MANAGEMENT_PROTOCOL_ACT','Management Protocol',20,'Management Protocol','Management',1,'active','fa fa-tags');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper');


--create Specify locations
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker');

--create Preview and confirm
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('OBSERVATION_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');


--insert Specify Parents, Specify Crosses and Preview and Confirm in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'OBSERVATION_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'OBSERVATION_DATA_PROCESS') as parent_id,
	id as child_id,
	case
		when abbrev = 'OBSERVATION_BASIC_INFO_ACT' then 1
		when abbrev = 'OBSERVATION_ENTRY_LIST_ACT'then 2
		when abbrev = 'OBSERVATION_DESIGN_ACT'then 3
        when abbrev = 'OBSERVATION_PROTOCOL_ACT'then 4
		when abbrev = 'OBSERVATION_PLACE_ACT'then 5
		when abbrev = 'OBSERVATION_REVIEW_ACT'then 6
		else 7 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev in ('OBSERVATION_BASIC_INFO_ACT','OBSERVATION_ENTRY_LIST_ACT','OBSERVATION_DESIGN_ACT','OBSERVATION_PROTOCOL_ACT', 'OBSERVATION_PLACE_ACT', 'OBSERVATION_REVIEW_ACT');


--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'OBSERVATION_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'OBSERVATION_PROTOCOL_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'OBSERVATION_PLANTING_PROTOCOL_ACT' then 1
        when abbrev = 'OBSERVATION_POLLINATION_PROTOCOL_ACT'then 2
		when abbrev = 'OBSERVATION_TRAITS_PROTOCOL_ACT'then 3
        when abbrev = 'OBSERVATION_MANAGEMENT_PROTOCOL_ACT'then 4
        when abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT'then 5
		else 6 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('OBSERVATION_PLANTING_PROTOCOL_ACT','OBSERVATION_POLLINATION_PROTOCOL_ACT','OBSERVATION_TRAITS_PROTOCOL_ACT','OBSERVATION_MANAGEMENT_PROTOCOL_ACT','OBSERVATION_HARVEST_PROTOCOL_ACT');
;





--insert Specify Parents, Specify Crosses and Preview and Confirm in platform.module
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('OBSERVATION_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
('OBSERVATION_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
('OBSERVATION_DESIGN_ACT_MOD','Planting Arrangement','Planting Arrangement','experimentCreation','create','specify-cross-design',1,'added by j.antonio ' || now(), 'design generated'),
('OBSERVATION_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
('OBSERVATION_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocol','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
('OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD','Pollination Protocol','Pollination Protocol','experimentCreation','protocol','pollination-protocol',1,'added by j.antonio ' || now(), 'protocol specified'),
('OBSERVATION_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
('OBSERVATION_MANAGEMENT_PROTOCOL_ACT_MOD','Management Protocol','Management Protocol','experimentCreation','protocol','management-protocol',1,'added by j.antonio ' || now(), 'protocol specified'),
('OBSERVATION_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified'),
('OBSERVATION_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'occurrences created'),
('OBSERVATION_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');

--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
	case
		when abbrev = 'OBSERVATION_BASIC_INFO_ACT' then (select id from platform.module where abbrev = 'OBSERVATION_BASIC_INFO_ACT_MOD')
		when abbrev = 'OBSERVATION_ENTRY_LIST_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_ENTRY_LIST_ACT_MOD')
		when abbrev = 'OBSERVATION_DESIGN_ACT' then (select id from platform.module where abbrev = 'OBSERVATION_DESIGN_ACT_MOD')
        when abbrev = 'OBSERVATION_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_PROTOCOL_ACT_MOD')
		when abbrev = 'OBSERVATION_PLACE_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_PLACE_ACT_MOD')
		when abbrev = 'OBSERVATION_REVIEW_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_REVIEW_ACT_MOD')
		else (select id from platform.module where abbrev = 'OBSERVATION_REVIEW_ACT_MOD') end as module_id,
	1,
	'added by l.gallardo ' || now()
from
	master.item
where
	abbrev in ('OBSERVATION_BASIC_INFO_ACT','OBSERVATION_ENTRY_LIST_ACT','OBSERVATION_DESIGN_ACT', 'OBSERVATION_PROTOCOL_ACT', 'OBSERVATION_PLACE_ACT', 'OBSERVATION_REVIEW_ACT');


--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'OBSERVATION_PLANTING_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_PLANTING_PROTOCOL_ACT_MOD')
  when abbrev = 'OBSERVATION_POLLINATION_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD')
  when abbrev = 'OBSERVATION_TRAITS_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_TRAITS_PROTOCOL_ACT_MOD')
  when abbrev = 'OBSERVATION_MANAGEMENT_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_MANAGEMENT_PROTOCOL_ACT_MOD')
  when abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT'then (select id from platform.module where abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT_MOD')
  else (select id from platform.module where abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('OBSERVATION_PLANTING_PROTOCOL_ACT','OBSERVATION_POLLINATION_PROTOCOL_ACT','OBSERVATION_TRAITS_PROTOCOL_ACT','OBSERVATION_MANAGEMENT_PROTOCOL_ACT','OBSERVATION_HARVEST_PROTOCOL_ACT');
