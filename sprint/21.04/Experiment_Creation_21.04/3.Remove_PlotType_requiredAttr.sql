--Breeding Trial
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol variables for Breeding Trial data process",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "ESTABLISHMENT"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_TYPE"
        },
        {
            "disabled": false,
            "variable_abbrev": "PLOT_TYPE"
        }
    ]
}
' where abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT_VAL';


--Intentional Crossing Nursery
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol variables for Intentional Crossing Nursery data process",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "ESTABLISHMENT"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_TYPE"
        },
        {
            "disabled": false,
            "variable_abbrev": "PLOT_TYPE"
        }
    ]
}
' where abbrev = 'INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT_VAL';


--Generation Nursery
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol variables for Generation Nursery data process",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "ESTABLISHMENT"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_TYPE"
        },
        {
            "disabled": false,
            "variable_abbrev": "PLOT_TYPE"
        }
    ]
}
' where abbrev = 'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT_VAL';


--Cross Parent Nursery Phase I
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol variables for Cross Parent Nursery Phase I data process",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "ESTABLISHMENT"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_TYPE"
        },
        {
            "disabled": false,
            "variable_abbrev": "PLOT_TYPE"
        }
    ]
}
' where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL';

--Cross Parent Nursery Phase II
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol variables for Cross Parent Nursery Phase II data process",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "ESTABLISHMENT"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_TYPE"
        },
        {
            "disabled": false,
            "variable_abbrev": "PLOT_TYPE"
        }
    ]
}
' where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT_VAL';