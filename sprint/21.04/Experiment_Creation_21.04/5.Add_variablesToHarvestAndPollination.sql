--OBSERVATION 
--HARVEST Protocol
INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('OBSERVATION_HARVEST_PROTOCOL_ACT_VAL', 'Harvest Protocol', 
  '{
      "Name": "Required experiment level harvest protocol variables for Observation data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HARVEST_INSTRUCTIONS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');


--POLLINATION Protocol
INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('OBSERVATION_POLLINATION_PROTOCOL_ACT_VAL', 'Observation Pollination Protocol variables', 
      '{
      "Name": "Required experiment level pollination protocol variables for Observation data process",
      "Values": [
         {
              "default": false,
              "disabled": false,
              "variable_abbrev": "POLLINATION_INSTRUCTIONS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');