alter table platform.user_filter add column type varchar;
update platform.user_filter set type = 'filter';

ALTER TABLE platform.user_filter RENAME TO user_data_browser_configuration;