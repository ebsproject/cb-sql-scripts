with t as (
	select
		ist.table_schema,
		ist.table_name,
		ist.table_schema || '.' || ist.table_name full_table_name,
		z_admin.query(format($$
			select
				count(1)
			from
				%s.%s
		$$,
			ist.table_schema,
			ist.table_name
		))::integer record_count,
		'select * from ' || ist.table_schema || '.' || ist.table_name
	from
		information_schema."tables" ist
	where
		ist.table_type = 'BASE TABLE'
		and ist.table_schema not in (
			'pg_catalog', 'information_schema', 'public'
		)
	order by
		ist.table_schema,
		ist.table_name
), u as (
	select
		t.full_table_name
		/*format($$('%s'),$$,
			t.full_table_name
		)*/
	from
		t
	where
		t.record_count > 0
), v as (
	-- from template database with data
	select
		t.full_table_name
	from (
		values
		('api.client'),
		('api.messages'),
		('api.oauth_clients'),
		-- ('audit.event_log'),
		('dictionary.column'),
		('dictionary.constraint'),
		('dictionary.constraint_column'),
		('dictionary.database'),
		('dictionary.entity'),
		('dictionary.schema'),
		('dictionary.table'),
		-- ('dms.scaletab'),
		('gms.methods'),
		-- ('gms.rr_pending_changes'),
		('gms.udflds'),
		('gms.users'),
		-- ('master.country'),
		('master.cross_method'),
		('master.entity'),
		('master.entity_record'),
		('master.facility_type'),
		('master.formula'),
		('master.formula_parameter'),
		-- ('master.institute'),
		('master.instruction'),
		('master.item'),
		-- ('master.item_action'),
		('master.item_metadata'),
		('master.item_record'),
		('master.item_relation'),
		('master.method'),
		('master.phase'),
		('master.property'),
		-- ('master.property_method_scale'),
		('master.record'),
		('master.record_variable'),
		('master.role'),
		('master.scale'),
		('master.scale_value'),
		('master.tooltip'),
		('master.user'),
		('master.user_metadata'),
		('master.variable'),
		('master.variable_relation'),
		('master.variable_relation_value'),
		-- ('master.variable_result'),
		('master.variable_set'),
		('master.variable_set_member'),
		('platform.application'),
		('platform.application_action'),
		('platform.config'),
		('platform.dashboard_widget'),
		('platform.gearman_worker'),
		-- ('platform.menu_item'),
		-- ('platform.menu_item_relation'),
		-- ('platform.plan_config'),
		('platform.space')
		-- ('platform.viewer_config')
	) t (
		full_table_name
	)
), w as (
	select
		*
	from
		u
	except
		select
			*
		from
			v
)
select
	w.*
from
	w
order by
	w.full_table_name
;