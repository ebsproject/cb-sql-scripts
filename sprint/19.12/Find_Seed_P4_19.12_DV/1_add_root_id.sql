/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Gene Christhopher Romuga <g.romuga@irri.org>
 * @date 2019-11-12 8:47:11
 */


--/*
BEGIN;

SAVEPOINT sp1;
--*/

set role b4radmin;

/**
 * B4R-3603 Find Seed: Add root_id column to the master.facility table
 *
 * master.facility
 * - root_id integer
 */
-- add root id column
ALTER TABLE
    master.facility
ADD COLUMN IF NOT EXISTS
    root_id
    INTEGER
    --NOT NULL
    --DEFAULT 1
;

COMMENT ON COLUMN
    master.facility.root_id
IS
    'Root id'
;

--Create root_id index
CREATE INDEX IF NOT EXISTS
	facility_root_id_idx
ON
	master.facility
USING
	btree (
		root_id
	)
;

--Add reference 
ALTER TABLE IF EXISTS
	master.facility
ADD CONSTRAINT
	facility_root_id_fkey
FOREIGN KEY (
	root_id
)
REFERENCES
	master.facility (
		id
	)
ON UPDATE CASCADE
ON DELETE RESTRICT
;


--/*
ROLLBACK TO sp1;

END;
--*/
