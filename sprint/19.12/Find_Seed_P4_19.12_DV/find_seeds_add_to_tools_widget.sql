﻿insert into platform.application (abbrev, label, action_label, icon, creator_id, is_public)
values
('FIND_SEEDS', 'Find seeds', 'Find Seeds', 'search', 1, true);

insert into platform.application_action(application_id,module,controller,action,params,creator_id)
values
((select id from platform.application where abbrev = 'FIND_SEEDS'),'seedInventory','find-seeds',null,null,1);