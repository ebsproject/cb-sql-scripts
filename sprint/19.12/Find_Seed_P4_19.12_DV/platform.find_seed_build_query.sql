/* 
Builds query for Find seed tool based on config dataset and query parameters

Example usage: 

select * from platform.find_seed_build_query('
[{"abbrev":"PROGRAM","values":[126],"operator":"in","filter_type":"search"},
{"abbrev":"EXPERIMENT_TYPE","values":["'Trial'"],"operator":"in","filter_type":"search"},
{"abbrev":"includeSeedlistOption","values":false,"operator":"in","filter_type":"search"},
{"abbrev":"userId","values":29,"operator":"in","filter_type":"search"}]
')

Function: platform.find_seed_build_query(json)
DROP FUNCTION platform.find_seed_build_query(json);
*/
set role b4radmin;

CREATE OR REPLACE FUNCTION platform.find_seed_build_query(IN filters json)
  RETURNS TABLE(main_sql text, count_sql text, working_list_sql text) AS
$BODY$

DECLARE
    abbrev_arr text[];
    table_arr text[];
    config json;
    column_config json;
    r_rec RECORD;
    config_abbrev_rec RECORD;
    cond_values text;
    column_config_rec RECORD;
    main_table text;
    main_table_alias text;
    main_table_primary_key text;
    metadata_column text;
    metadata_condition text;
    crosstab_column text;
    metadata_column_arr integer[];
    from_joined_table text;
    select_column text;
    from_table text;
    condition_text text;
    operator text;
    order_by text;
    order_by_values text;
    final_order_by text;
    main_order_by text;
    group_by text;
    userId int;
    seedListTable text;
    seedListTableExist boolean;
    includeSeedlistOption boolean;
    working_list_condition text;

BEGIN    
    -- get config for search parameters
    select (config_value->>'values')::json into config from platform.config where 
        config.abbrev='FIND_SEED_FILTERS';
    
    -- get config for data browser columns
    select (config_value->>'values')::json into column_config from platform.config where 
        config.abbrev='FIND_SEED_DATA_BROWSER_COLUMNS';
    
    -- get main table
    select (config_value->>'main_table')::text into main_table from platform.config where 
        config.abbrev='FIND_SEED_DATA_BROWSER_COLUMNS';
    
    -- get main table alias
    select (config_value->>'main_table_alias')::text into main_table_alias from platform.config where 
        config.abbrev='FIND_SEED_DATA_BROWSER_COLUMNS';

    -- get main table primary key
    select (config_value->>'main_table_primary_key')::text into main_table_primary_key from platform.config where 
        config.abbrev='FIND_SEED_DATA_BROWSER_COLUMNS';
    
    -- get the user ID
    select values into userId from json_to_recordset(filters) as x(abbrev text, values text, operator text, filter_type text)
    where abbrev='userId';

    -- get the value if to include records that already in the working list
    select values into includeSeedlistOption from json_to_recordset(filters) as x(abbrev text, values text, operator text, filter_type text)
    where abbrev='includeSeedlist';

    seedListTable:='';
    from_joined_table:='';
    main_order_by:='';
    
    order_by:='ss.product_id';
    final_order_by:=' ORDER BY '||order_by;

    select_column := 'SELECT 
        '||main_table_alias||'.'|| main_table_primary_key ;
    
    from_table:=',master.program prog, master.product pr'; 
    
    condition_text:= ' WHERE 
        '|| main_table_alias || '.is_void=false and 
        pr.id=ss.product_id and 
        prog.id=ss.program_id'; 
    
    working_list_condition:= ' WHERE 
        '|| main_table_alias || '.is_void=false and 
        pr.id=ss.product_id and 
        prog.id=ss.program_id'; 
    
    metadata_column:='';
    metadata_condition:='';
    crosstab_column:='id integer ';
    
    -- mandatory tables
    table_arr:=array_append(table_arr,'master.program_prog');
    table_arr:=array_append(table_arr,'master.product_pr');
    table_arr:=array_append(table_arr,'operational.seed_storage_'||main_table_alias);

    -- working list temp table
    execute 'select exists (
    select 1
    from   information_schema.tables
    where  table_schema = ''temporary_data''
    and    table_name = ''temp_findseed_'||userId||'''
    )' into seedListTableExist;
    
    -- checks if the temporary table for working list is existing
    if(seedListTableExist) THEN

        from_joined_table:=from_joined_table||' left join temporary_data.temp_findseed_'||userId||' 
            as workingList on workingList.seed_storage_id=ss.id';
        raise notice 'seed list table: %',from_joined_table;
       
        select_column:=select_column||'
            ,(case when workingList.seed_storage_id is null then ''excludeRow'' else ''includeRow'' 
                end) as includeSeedlistOption'; 
        
        if(includeSeedlistOption='false') THEN
            condition_text:=condition_text||' and workingList.seed_storage_id is null';
        END IF;
	else
        -- if table does not exist, includeSeedlistOption column will be set to excludeRow
        -- this column is added as html class in the row 
	 	select_column:=select_column||',''excludeRow''  as includeSeedlistOption'; 
		
    END IF;
    
    raise notice '-- build the query using the search query parameters';

    FOR r_rec in select * from json_to_recordset(filters) as x(abbrev text, values text, operator text, filter_type text)
    LOOP
        raise notice '%',r_rec;
        execute '
            Select * from 
            json_to_recordset($$'||config||'$$) 
                as x(
                disabled text, 
                required text, 
                input_type text, 
                field_label text, 
                input_field text, 
                order_number text, 
                target_table text, 
                target_table_alias text,
                default_value text, 
                target_column text, 
                allowed_values text,
                basic_parameter text, 
                variable_abbrev text, 
                reference_column text, 
                field_description text, 
                primary_attribute text, 
                secondary_attribute text, 
                secondary_target_table text, 
                secondary_target_table_alias text, 
                secondary_target_column text,
                filter_column text) where variable_abbrev=$$'||r_rec.abbrev||'$$' 
                into config_abbrev_rec;

        raise notice '%',config_abbrev_rec.variable_abbrev;

        if(config_abbrev_rec.variable_abbrev is not null and r_rec.filter_type='search' 
            and exists(select 1 from master.variable where abbrev=r_rec.abbrev)) then 
            -- use case: the target column is in the find seeds data filters config
            raise notice '-- FILTER by search query parameters';
            raise notice 'SEARCH ---- target: %  target column: %  ref column: %  2nd target table: %',
                config_abbrev_rec.target_table, config_abbrev_rec.target_column, 
                config_abbrev_rec.reference_column, config_abbrev_rec.secondary_target_table;

            if r_rec.operator='ilike any' then -- used for not exact matching/column filtering. assumes all cols are text
            
                select '::text '||r_rec.operator||' (ARRAY['||string_agg('$$%'||replace(value,'"','')||'%$$', ',')||'])' 
                    into cond_values from (select value::text from json_array_elements(r_rec.values::json))a ;
                
            else -- used for exact match(operator is 'in')
                 -- by default, columns are cast to text
            
                select ' '||r_rec.operator||' ('||string_agg('$$'||regexp_replace(value,'["|'']','','g')||'$$', ',')||')' 
                    into cond_values from (select value::text from json_array_elements(r_rec.values::json))a ;
                
            end if;
            -- search param has 2nd target table
            IF (config_abbrev_rec.secondary_target_table <> '' )THEN
                raise notice '-- search param has 2nd target table';
                raise notice 'table arr: %',table_arr;

                IF (config_abbrev_rec.secondary_target_table||'_'||config_abbrev_rec.secondary_target_table_alias=any(table_arr) ) THEN
                    -- table is already in the array (for building the FROM query), prevent multiple declaring of tables
                ELSE
                    -- add the 2ndary target table to the array
                    table_arr:=array_append(table_arr,config_abbrev_rec.secondary_target_table||'_'||config_abbrev_rec.secondary_target_table_alias);

                    from_joined_table :=from_joined_table || '
                        left join '||config_abbrev_rec.secondary_target_table ||' as ' || config_abbrev_rec.secondary_target_table_alias ||
                            ' on '||main_table_alias||'.'||config_abbrev_rec.reference_column||' = '||
                                config_abbrev_rec.secondary_target_table_alias||'.id ';
                    
                END IF;

                IF  config_abbrev_rec.target_table||'_'||config_abbrev_rec.target_table_alias=any(table_arr) THEN
                -- checks if the target table or the primary table is in the array
                ELSE
                    table_arr:=array_append(table_arr,config_abbrev_rec.target_table||'_'||
                        config_abbrev_rec.target_table_alias);

                    from_joined_table :=from_joined_table || ' 
                    left join '||config_abbrev_rec.target_table || ' as '||config_abbrev_rec.target_table_alias||
                    ' on '||config_abbrev_rec.target_table_alias||'.'||config_abbrev_rec.filter_column||' = '||
                    config_abbrev_rec.secondary_target_table_alias||'.'||config_abbrev_rec.secondary_target_column;
                    
                END IF;

                condition_text:=condition_text||' and 
                    '||config_abbrev_rec.target_table_alias||'.'||
                    config_abbrev_rec.filter_column||'::text '||cond_values; 

                raise notice 'tables: %',from_joined_table;
                raise notice 'condition_text: %',condition_text;
                
                
            ELSIF config_abbrev_rec.secondary_target_table = '' THEN 
                raise notice 'NO 2ndary table ';
                -- no 2ndary target table, examples are the columns of operational.seed_storage, 
                -- operational.study columns such as source study name, source study
                IF config_abbrev_rec.target_table ='operational.seed_storage' THEN 
                    config_abbrev_rec.target_table_alias := main_table_alias;
                END IF;

                if(config_abbrev_rec.target_table||'_'||config_abbrev_rec.target_table_alias=any(table_arr) ) THEN

                ELSE 
                    table_arr:=array_append(table_arr,config_abbrev_rec.target_table||'_'||
                        config_abbrev_rec.target_table_alias);

                    from_joined_table :=from_joined_table || '
                            left join '||config_abbrev_rec.target_table || 
                            ' as ' || config_abbrev_rec.target_table_alias || ' on '||main_table_alias||'.'||
                            config_abbrev_rec.reference_column||' = '||config_abbrev_rec.target_table_alias||'.id ';
                END IF;

                IF r_rec.abbrev='VOLUME' THEN
                    select replace(value::text,'"','') into cond_values from json_array_elements(r_rec.values::json);
                    condition_text:=condition_text||' and 
                        '||config_abbrev_rec.target_table_alias||'.'||
                        config_abbrev_rec.filter_column||''||cond_values; 
                
                ELSIF(r_rec.abbrev='FACILITY') THEN 

                            select ' '||r_rec.operator||' ('||string_agg(''||replace(value,'"','')||'', ',')||')' 
                                into cond_values from (select value::text from json_array_elements(r_rec.values::json))a ;

                            condition_text:=condition_text||' 
                                and '||
                                '(with recursive fac(id,name,facility_type,level,parent_id,depth) as (
                                    select 
                                        a.id, a.name,a.facility_type, a.level, a.parent_id, 1::int as depth
                                    from
                                        master.facility a
                                    where 
                                        a.id= ss.facility_id
                                    union all
                                    select 
                                        b.id, b.name, b.facility_type, b.level, b.parent_id, fac.depth+1 as depth
                                    from 
                                        master.facility b,fac
                                    where 
                                        b.id = fac.parent_id
                                )
                               select id from fac order by depth desc limit 1)'
                            ||cond_values;

                ELSIF r_rec.abbrev in ('PLOTNO','ENTNO','REP') then -- expected input value: [" 1 -5 ","11","25"]
                    raise notice 'RANGE input: %',r_rec.abbrev;
                   
                    execute '
                    select ''(''||string_agg(value, '' or '')||'')''
                    from 
                    (
                        select 
                            (case 
                                when value[2] is  null then 
                                '''||config_abbrev_rec.target_table_alias||'.'||
                                    config_abbrev_rec.filter_column||' =''||value[1] 

                                when value[2]::int=value[1]::int then 
                                '''||config_abbrev_rec.target_table_alias||'.'||
                                    config_abbrev_rec.filter_column||' =''||value[1] 
                                
                                when value[2]::int > value[1]::int then 
                                    ''('||config_abbrev_rec.target_table_alias||'.'||
                                    config_abbrev_rec.filter_column||' >=''||value[1]||'' and '||config_abbrev_rec.target_table_alias||'.'||
                                    config_abbrev_rec.filter_column||' <=''||value[2]||'')'' 
                            else 
                                ''('||config_abbrev_rec.target_table_alias||'.'||
                                    config_abbrev_rec.filter_column||' >=''||value[2]||'' and '||config_abbrev_rec.target_table_alias||'.'||
                                    config_abbrev_rec.filter_column||' <=''||value[1]||'')'' 
                            end) as value
                    from (
                        select 
                            regexp_matches(
                                regexp_replace(jsonval::text,''[\"*|\s*]'','''',''g''), 
                                ''^(-?\d+(?:[.]\d+)?)-?(-?\d+(?:[.]\d+)?)?$'',
                                ''g'') 
                            as value 
                        from 
                            json_array_elements('''||r_rec.values::json||''') as jsonval
                    )a
                    )range_values ' into cond_values;

                    condition_text:=condition_text||' and '||cond_values; 
                
                ELSE 
                    condition_text:=condition_text||' and 
                        '||config_abbrev_rec.target_table_alias||'.'||
                        config_abbrev_rec.filter_column||' '||cond_values; 

                END IF;

            END IF;
        
        ELSIF exists(select 1 from master.variable where abbrev=r_rec.abbrev)  THEN 
            -- use case: column is not specified in the find seeds data filter config
            -- use case: column filtering and input list search
            raise notice 'COLUMN FILTERS/INPUT LIST ----';

            IF r_rec.operator='ilike any' then -- used for column filtering 
            
                select replace(REGEXP_REPLACE(value,'\\''+', '''', 'g'),'"','') into cond_values from 
                    (select value::text from json_array_elements(r_rec.values::json))a ;

                select r_rec.operator||' (ARRAY['||string_agg('E''%'||replace(value,'"','')||'%''', ',')||'])'
                    into cond_values from (select value::text from json_array_elements(r_rec.values::json))a ;
                
            else -- used for exact match(operator is 'in')
                 -- by default columns are cast to text
            
                select r_rec.operator|| ' ('||string_agg(''''||replace(value,'"','')||'''', ',')||')',
                    ''''||string_agg(replace(value,'"',''), ',')||''''
                        into cond_values ,order_by_values 
                    from (select value::text from json_array_elements(r_rec.values::json))a ;
                
            end if;

            execute '
                Select * from 
                json_to_recordset($$'||column_config||'$$) 
                    as x(
                    hidden  text,
                    field_label text,
                    field_description text, 
                    required  text,
                    disabled  text,
                    order_number text,
                    variable_abbrev text,
                    primary_attribute  text,
                    secondary_attribute  text,
                    target_table text,
                    target_table_alias text,
                    secondary_target_table  text,
                    secondary_target_table_alias  text,
                    target_column  text,
                    secondary_target_column  text,
                    reference_column text
                    ) 
                where variable_abbrev=$$'||r_rec.abbrev||'$$ ' 
            into column_config_rec;

            raise notice ' var: %',column_config_rec.variable_abbrev;
            raise notice ' var: %',column_config_rec.target_table;

            IF (column_config_rec.secondary_target_table <> '' )THEN

                IF  column_config_rec.target_table||'_'||column_config_rec.target_table_alias=any(table_arr) THEN
                
                ELSE
                    table_arr:=array_append(table_arr,column_config_rec.target_table||'_'||
                        column_config_rec.target_table_alias);

                    from_joined_table :=from_joined_table || ' 
                    left join '||
                    column_config_rec.target_table || ' '||column_config_rec.target_table_alias||' on '||
                    main_table_alias||'.'||column_config_rec.reference_column||' = '||
                    column_config_rec.target_table_alias||'.id';
                    
                END IF;   

                IF (column_config_rec.secondary_target_table||'_'||column_config_rec.secondary_target_table_alias=any(table_arr)) THEN
                    
                ELSE
                    table_arr:=array_append(table_arr,column_config_rec.secondary_target_table||'_'||column_config_rec.secondary_target_table_alias);

                    from_joined_table :=from_joined_table || ' 
                        left join '||column_config_rec.secondary_target_table || ' as '|| column_config_rec.secondary_target_table_alias||
                        ' on '||column_config_rec.target_table_alias||'.'||column_config_rec.target_column||' = '||
                        column_config_rec.secondary_target_table_alias||'.id ';
                END IF;

                condition_text:=condition_text||' 
                        and '||
                        column_config_rec.secondary_target_table_alias||'.'||
                        column_config_rec.secondary_target_column||'::text '||cond_values; 
     
            ELSIF column_config_rec.secondary_target_table = '' THEN

                IF  column_config_rec.target_table||'_'||column_config_rec.target_table_alias=any(table_arr) THEN
                    IF(column_config_rec.variable_abbrev='DESIGNATION') THEN 
                        -- from find seeds inputlist search by designation,exact match
                        select 
                            string_agg(''''||replace(value,'"','')||'''', ','),
                            ''''||string_agg(replace(value,'"',''), ',')||''''
                            
                            into cond_values ,order_by_values
                        from 
                            (select z_admin.normalize_text(desig_index.value)as value 
                            from (
                                select replace(value,'"','')as value from (
                                    select value::text from json_array_elements(r_rec.values::json)
                                )a
                            ) as desig_index
                            )a ;

                        if r_rec.operator='ilike any' then -- used for column filtering 
                        
                             cond_values:=  r_rec.operator||' (ARRAY['||cond_values||'])';
                            
                        else-- used for exact match(operator is 'in')
                            -- by default columns are cast to text
                            cond_values:=  r_rec.operator||' ('||cond_values||')';
                        end if;

                        condition_text:=condition_text||' 
                            and pr.system_product_name '||cond_values;

                        order_by:='
                            array_position((SELECT
                                array_agg(id) 
                            from (
                                select id
                                FROM master.product
                                    JOIN UNNEST(''{' ||BTRIM(order_by_values, '''')||'}''::text[]) WITH ORDINALITY t(system_product_name, ord) USING (system_product_name)
                                WHERE
                                    system_product_name '||cond_values||' 
                                ORDER BY t.ord
                                )a),ss.product_id)';
                        final_order_by:= ' position (pr.system_product_name in '||order_by_values||')';
                        group_by:= 'pr.system_product_name';

                    ELSE 
                        condition_text:=condition_text||' 
                        and '||column_config_rec.target_table_alias||'.'||
                        column_config_rec.target_column||'::text '||cond_values; 
                    END IF;
                    
                ELSE
                    
                    table_arr:=array_append(table_arr,column_config_rec.target_table||'_'||
                        column_config_rec.target_table_alias);
                
                    IF column_config_rec.target_table='operational.seed_storage_metadata' THEN
                        -- special case for seed_storage_metadata , append to column as parameter in crosstab

                        metadata_column_arr:=array_append(metadata_column_arr,(select id from master.variable where 
                            abbrev=column_config_rec.variable_abbrev));
                        
                    ELSIF column_config_rec.target_table <> 'operational.seed_storage' THEN 
                        --default is using left join to get values from other table
                    
                        IF(r_rec.abbrev='FACILITY') THEN 
                            condition_text:=condition_text||' 
                                and '||
                                '(with recursive fac(id,name,facility_type,level,parent_id,depth) as (
                                    select 
                                        a.id, a.name,a.facility_type, a.level, a.parent_id, 1::int as depth
                                    from
                                        master.facility a
                                    where 
                                        a.id= ss.facility_id
                                    union all
                                    select 
                                        b.id, b.name, b.facility_type, b.level, b.parent_id, fac.depth+1 as depth
                                    from 
                                        master.facility b,fac
                                    where 
                                        b.id = fac.parent_id
                                    )
                                    select name::text from fac order by depth desc limit 1
                                )'
                            ||cond_values; 
                
                        ELSE
                            condition_text:=condition_text||' 
                                and '||column_config_rec.target_table_alias||'.'||
                                column_config_rec.target_column||'::text '||cond_values; 
                        END IF;
                            
                            from_joined_table :=from_joined_table || ' 
                                left join '||column_config_rec.target_table || ' as ' || 
                                column_config_rec.target_table_alias ||
                                ' on '||main_table_alias||'.'||column_config_rec.reference_column||' = '||
                                column_config_rec.target_table_alias||'.id '; 

                    ELSE -- operational.seed storage columns

                        IF(r_rec.abbrev='GID') THEN 

                            IF r_rec.operator='ilike any' then -- used for column filtering 
            
                                condition_text:=condition_text||' 
                                    and '||main_table_alias||'.'||
                                    column_config_rec.target_column||'::text '||cond_values; 
                
                            else 
                                -- used for exact match(operator is 'in')
                                -- by default columns are cast to text
                                -- from find seeds inputlist search by gid

                                condition_text:=condition_text||' 
                                    and '||main_table_alias||'.'||
                                    column_config_rec.target_column||' '||cond_values; 

                                order_by:= 'position (ss.product_id::text in (
                                    select string_agg(product_id::text, '','' order by position (
                                        gid::text in '|| order_by_values ||')) 
                                    from master.product_gid where gid  
                                '||cond_values||'))';
                                final_order_by:=' order by position (
                                        ss.gid::text in '|| order_by_values ||')';
                                        main_order_by:=' order by row_number,rank_number';
                            end if;
                        ELSIF(r_rec.abbrev='LABEL') THEN 

                            IF r_rec.operator='ilike any' then -- used for column filtering 
            
                                condition_text:=condition_text||' 
                                    and '||main_table_alias||'.'||
                                    column_config_rec.target_column||'::text '||cond_values; 
                
                            else 
                                -- used for exact match(operator is 'in')
                                -- by default columns are cast to text
                                -- from find seeds inputlist search by gid

                                condition_text:=condition_text||' 
                                    and '||main_table_alias||'.'||
                                    column_config_rec.target_column||' '||cond_values; 
                                
                                final_order_by:= ' ORDER BY position (ss.label::text in '||order_by_values||')';
                                group_by:= 'ss.label';
                            end if;
                        ELSE
                            IF r_rec.operator='ilike any' then -- used for column filtering 
                
                                condition_text:=condition_text||' 
                                    and '||main_table_alias||'.'||
                                    column_config_rec.target_column||'::text '||cond_values; 
                            
                            else -- used for exact match(operator is 'in')
                            -- by default columns are cast to text
                            
                                condition_text:=condition_text||' 
                                    and '||main_table_alias||'.'||
                                    column_config_rec.target_column||' '||cond_values; 
                            END IF;

                        END IF;
                        
                    END IF;
                    
                END IF;
            
            ELSIF (r_rec.abbrev='SYNONYM') THEN   

                -- from find seeds inputlist search by designation, return synonyms
                select 
                    string_agg(''''||replace(value,'"','')||'''', ','),
                    ''''||string_agg(replace(value,'"',''), ',')||''''
                    
                    into cond_values ,order_by_values
                from 
                    (select z_admin.normalize_text(desig_index.value)as value 
                    from (
                        select replace(value,'"','')as value from (
                            select value::text from json_array_elements(r_rec.values::json)
                        )a
                    ) as desig_index
                    )a ;

                if r_rec.operator='ilike any' then -- used for column filtering 
                
                     cond_values:=  r_rec.operator||' (ARRAY['||cond_values||'])';
                    
                else-- used for exact match(operator is 'in')
                    -- by default columns are cast to text
                    cond_values:=  r_rec.operator||' ('||cond_values||')';
                end if;

                condition_text:=condition_text||' 
                    and pr.id in ( 
                        select px.id 
                        from 
                            master.product_name pn,
                            master.product px 
                        where 
                            pn.system_product_name '||cond_values||' 
                            and pn.product_id=px.id
                            and pn.is_void=false
                            and px.is_void=false
                    )';

                order_by:='
                    array_position((SELECT
                        array_agg(id) 
                    from (
                        select px.id
                        FROM 
                            master.product px,
                            master.product_name pn
                                JOIN UNNEST(''{' ||BTRIM(order_by_values, '''')||'}''::text[]) 
                                WITH ORDINALITY t(system_product_name, ord) USING (system_product_name)
                        WHERE
                            pn.system_product_name '||cond_values||'
                            and pn.product_id=px.id
                            and pn.is_void=false
                            and px.is_void=false
                        ORDER BY t.ord
                        )a),ss.product_id)
                    ';
                final_order_by:= order_by; 
                
                group_by:= 'synonym';
            
            END IF;
    END IF;
    END LOOP;
    -- Retain order as inputted in INPUT List
    IF(group_by='ss.label') then 
        order_by:='
            array_position((SELECT
                array_agg(product_id) 
            from (
                select ss.product_id
                FROM '|| 
                main_table|| ' ' || main_table_alias ||
                from_joined_table|| 
                from_table ||
                condition_text|| 
                final_order_by||'
                )a),ss.product_id)';
        main_order_by:=' order by row_number,rank_number';

    ELSIF(group_by='pr.system_product_name') then 

        final_order_by:= ' ORDER BY '||final_order_by;
        main_order_by:=' order by row_number,rank_number';--||final_order_by;

    ELSIF(group_by='synonym') then 

        final_order_by:= ' ORDER BY '||final_order_by;
        main_order_by:=' order by row_number,rank_number';--||final_order_by;
        
    END IF;

    raise notice '-- build columns based on config dataset for data browser columns';
    
    FOR column_config_rec in 
    execute '
        Select * from 
        json_to_recordset($$'||column_config||'$$) 
            as x(
                hidden  text,
                field_label text,
                field_description text, 
                order_number text,
                variable_abbrev text,
                additional_columns  text,
                target_table text,
                target_table_alias text,
                secondary_target_table  text,
                secondary_target_table_alias  text,
                target_column  text,
                secondary_target_column  text,
                reference_column text
                ) order by order_number::int' 
            
        LOOP
            raise notice '%',column_config_rec.variable_abbrev;

            crosstab_column:=crosstab_column||' , 
                '|| column_config_rec.variable_abbrev|| ' text';

            IF(column_config_rec.additional_columns<>'') THEN
                select_column:= select_column|| ', 
                    '||column_config_rec.additional_columns;
            END IF;

            IF(column_config_rec.target_table_alias<>'' and column_config_rec.target_table_alias is not null and 
                    column_config_rec.target_table<>'operational.seed_storage_metadata') then

                IF column_config_rec.secondary_target_table is not null and 
                    column_config_rec.secondary_target_table <> '' THEN

                    select_column:= select_column|| ', 
                        '||column_config_rec.secondary_target_table_alias||'.'||
                        column_config_rec.secondary_target_column || ' as '|| column_config_rec.variable_abbrev; 
                ELSE 
                    IF column_config_rec.variable_abbrev='FACILITY' THEN 
                        select_column:= select_column|| ', 
                            '||
                            '(with recursive fac(id,name,facility_type,level,parent_id,depth) as (
                                select 
                                    a.id, a.name,a.facility_type, a.level, a.parent_id, 1::int as depth
                                from
                                    master.facility a
                                where 
                                    a.id= ss.facility_id

                                union all
                                select 
                                    b.id, b.name, b.facility_type, b.level, b.parent_id, fac.depth+1 as depth
                                from master.facility b,fac
                                where 
                                    b.id = fac.parent_id
                             )
                                select name from fac order by depth desc limit 1)' 
                            ||
                            ' as '|| column_config_rec.variable_abbrev; 
                    ELSE   
                        select_column:= select_column|| ', 
                            '||column_config_rec.target_table_alias||'.'||
                            column_config_rec.target_column || ' as '|| column_config_rec.variable_abbrev; 
                    END IF;
                END IF;
                    
            elsif column_config_rec.target_table<>'operational.seed_storage_metadata' THEN 
           
                select_column:= select_column|| ', 
                    '||column_config_rec.target_table||'.'||
                    column_config_rec.target_column || ' as '|| column_config_rec.variable_abbrev; 

            end if;

            raise notice 'table: %',table_arr;

            IF (column_config_rec.secondary_target_table <> '' )THEN

                IF  column_config_rec.target_table||'_'||column_config_rec.target_table_alias=any(table_arr) THEN
        
                ELSE
                    -- target table will be added as joined table
                    table_arr:=array_append(table_arr,column_config_rec.target_table||'_'||
                        column_config_rec.target_table_alias);

                    from_joined_table :=from_joined_table || ' 
                        left join '||column_config_rec.target_table || ' as ' || 
                            column_config_rec.target_table_alias ||' on '||main_table_alias||'.'||
                            column_config_rec.reference_column||' = '||column_config_rec.target_table_alias||'.id ';

                END IF;

                IF (column_config_rec.secondary_target_table|| '_'||column_config_rec.secondary_target_table_alias=any(table_arr)) THEN
                
                ELSE
                    table_arr:=array_append(table_arr,column_config_rec.secondary_target_table|| '_'||column_config_rec.secondary_target_table_alias);
                    
                    from_joined_table :=from_joined_table || ' 
                        left join '||column_config_rec.secondary_target_table || ' as '||column_config_rec.secondary_target_table_alias || 
                            ' on '||column_config_rec.target_table_alias||'.'||column_config_rec.target_column||' = '||
                            column_config_rec.secondary_target_table_alias||'.id ';
                END IF; 
                
                -- secondary table will be added as joined table
                raise notice '-- secondary table will be added as joined table';                        
                
            ELSIF column_config_rec.secondary_target_table = '' THEN
                IF  column_config_rec.target_table||'_'||column_config_rec.target_table_alias=any(table_arr) THEN
                ELSE
                    table_arr:=array_append(table_arr,column_config_rec.target_table||'_'||
                        column_config_rec.target_table_alias);
                        
                    IF column_config_rec.target_table='operational.seed_storage_metadata' THEN
                        -- special case for seed_storage_metadata , append to column as parameter in crosstab
                        metadata_column_arr:=array_append(metadata_column_arr,(select id from master.variable 
                            where abbrev=column_config_rec.variable_abbrev));
                        
                    ELSIF column_config_rec.target_table <>'operational.seed_storage' THEN
                        --default is using left join to get values from other table
                        
                        from_joined_table :=from_joined_table || ' 
                            left join '||column_config_rec.target_table || ' as ' || 
                                column_config_rec.target_table_alias ||
                            ' on '||main_table_alias||'.'||column_config_rec.reference_column||' = '||
                                column_config_rec.target_table_alias||'.id '; 

                    END IF;
                END IF;
            END IF;

        END LOOP;
    raise notice 'select column: %',select_column;
    raise notice 'main_table: %',main_table;
    raise notice 'main_table_alias: %',main_table_alias;
    raise notice 'from_joined_table: %',from_joined_table;
    raise notice 'condition_text: %',condition_text;
    raise notice 'final_order_by: %',final_order_by;
    main_sql:='
        select 
            *, 
            (case when row_number::int%2=0 then 0 else 1 end) as product_group,
            max(rank_number) over (partition by product_id  ORDER BY row_number) as product_count 
        from (
            '||select_column||',
            dense_rank () OVER (ORDER BY '||order_by||') as row_number, 
            row_number () OVER (partition by pr.id '||final_order_by||') as rank_number 
            FROM '|| 
                main_table|| ' ' || main_table_alias ||
                from_joined_table|| 
                from_table ||
            condition_text|| 
            ')a' || main_order_by;
    
    count_sql:='select count(*) 
        FROM '|| main_table|| ' ' || main_table_alias ||
        from_joined_table|| from_table ||
        condition_text;

    working_list_sql:=select_column||' 
        FROM '|| main_table|| ' ' || main_table_alias ||
        from_joined_table|| from_table || working_list_condition;
    
    RETURN NEXT;
    
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
    COST 100
    ROWS 1000;
    --ALTER FUNCTION platform.find_seed_build_query(json) OWNER TO ncarumba;
    GRANT EXECUTE ON FUNCTION platform.find_seed_build_query(json) TO public;
    --GRANT EXECUTE ON FUNCTION platform.find_seed_build_query(json) TO ncarumba;
