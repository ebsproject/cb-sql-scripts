/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Argem Gerald Flores <a.flores@irri.org>
 * @date 2019-11-21 20:07:29
 */


--/*
BEGIN;

SAVEPOINT sp1;
--*/

set role b4radmin;

--/*
DO

$BODY$

DECLARE
    var_updated_variable_count integer;
BEGIN
--*/
    /**
     * B4R-3830 Add descriptions to variables used in Find Seeds to show in tooltips
     */
    
	/*
    select
		mv.id,
		mv.abbrev,
		mv.label,
		mv.name,
		mv.description,
		'(''' || array_to_string(array[mv.abbrev, mv.name, mv.description], ''', ''') || ''')'
	from
		master.variable mv
	where
		mv.abbrev in (
			'SEED_MANAGER',
			'LABEL',
			'GID',
			'VOLUME',
			'UNIT',
			'DESIGNATION',
			'SOURCE_HARV_YEAR',
			'SOURCE_STUDY',
			'SOURCE_STUDY_NAME',
			'SOURCE_ENTRY',
			'SRC_ENTRY',
			'PLOT_CODE',
			'SRC_PLOT',
			'REP',
			'SRC_SEASON',
			'HARVEST_DATE',
			'FACILITY',
			'SUB_FACILITY',
			'CONTAINER'
		)
		order by
			mv.abbrev
	;
	--*/
	
	with t_variables as (
		select
			t.*
		from (
				values
				('CONTAINER', 'container', 'Container where seeds or materials are being kept for storage, processing or transit'),
				('DESIGNATION', 'designation', 'Standard preferred name of product'),
				('FACILITY', 'facility', 'Facility where seeds, materials, and packages are being kept for storage or processing'),
				('GID', 'germplasm id', 'IRIS Germplasm identification number'),
				('HARVEST_DATE', 'harvest date', 'Date when the material was harvested'),
				('LABEL', 'label', 'Label printed in seed packages for proper identification'),
				('PLOT_CODE', 'Plot code', 'Plot code, which identifies a plot among other plots in a study'),
				('REP', 'Replication', 'Replication number of an entry in a study'),
				('SEED_MANAGER', 'seed manager', 'Manages the seeds for usage in experiments'),
				('SOURCE_ENTRY', 'SOURCE ENTRY CODE', 'Source of seeds based on the entry code of the product in a study'),
				('SOURCE_HARV_YEAR', 'Seed source harvest year', 'Year when the seeds were harvested'),
				('SOURCE_STUDY', 'SOURCE_STUDY', 'Study where harvested seeds came from'),
				('SOURCE_STUDY_NAME', 'SOURCE STUDY NAME', 'System-generated name of the study where harvested seeds came from'),
				('SRC_ENTRY', 'Seed source''s entry number ', 'Entry number of the product in the study, where the seeds are harvested from'),
				('SRC_PLOT', 'Seed source plot number or code', 'Plot number of the product in the study, where the seeds are harvested from'),
				('SRC_SEASON', 'SOURCE STUDY SEASON', 'Season of the study when the seeds are harvested'),
				('SUB_FACILITY', 'sub-facility', 'Facility within a parent facility where seeds and materials are being stored'),
				('UNIT', 'unit', 'Unit of measurement'),
				('VOLUME', 'volume', 'Amount of seeds or materials a package has')
			) t (
				abbrev, name, description
			)
	), t_update_variables as (
		update
			master.variable mv
		set
			description = t.description
		from
			t_variables t
		where
			mv.abbrev = t.abbrev
			and (
				mv.description is null
				or mv.description <> t.description
			)
			and mv.is_void = false
		returning
			*
	)
	select
		count(1)
	into
		var_updated_variable_count
	from
		t_update_variables t
	;
	
	raise notice '+ Variable (description) updated: %', var_updated_variable_count;
--/*
END;

$BODY$;
--*/


/*
DEV

NOTICE:  + Variable (description) updated: 18
DO

Query returned successfully in 180 msec.
*/


--/*
ROLLBACK TO sp1;

END;
--*/
