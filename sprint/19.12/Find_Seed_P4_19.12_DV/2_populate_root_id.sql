/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Gene Christhopher Romuga <g.romuga@irri.org>
 * @date 2019-14-12 8:47:11
 */


--/*

set role b4radmin;

do $$
declare
	var_record record;
	var_record_o record;
	var_parent_id integer;
	var_id integer;
	var_root_id integer;
begin

	for var_record_o in
		select id, parent_id from master.facility order by id
		loop
			
			--Get parent id and parent_id 
			var_parent_id = var_record_o.parent_id;
			var_id = var_record_o.id;
			
			if var_parent_id is null then
			
				--update root_id equal to id if null
				update master.facility set root_id = var_id where id = var_id;
				raise notice '!POPULATED ID % ROOT_ID %', var_id, var_id;
			else 
			
					for var_record in 
						WITH RECURSIVE traverse AS (
							SELECT mf.id, mf.parent_id FROM master.facility mf 
							WHERE id = var_parent_id

							UNION ALL

							SELECT mf2.id, 
									mf2.parent_id
							FROM master.facility mf2
							INNER JOIN traverse tv
							ON mf2.id = tv.parent_id
						)
						select t.* from traverse t order by id asc limit 1
						
					loop
					
						--Get root id
						var_root_id = var_record.id;
						update master.facility set root_id = var_root_id where id = var_id;
						raise notice '!POPULATED ID % ROOT_ID %', var_id, var_root_id;
						
					end loop;
			end if;
		end loop;
end; $$

--select id, parent_id, root_id from master.facility order by id asc;
