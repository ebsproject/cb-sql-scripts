set role b4radmin;

--****UPDATE the CROSS LIST Experiment Data Process
--create protocol
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_CROSS_LIST_PROTOCOLS_ACT','Protocols',30,'Protocols','Protocols',1,'active','fa fa-files-o');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_CROSS_LIST_TRAITS_PROTOCOLS_ACT','Traits Protocol',20,'Traits Protocol','Traits Protocol',1,'active','fa fa-random');


--update existing records
update master.item_relation set order_number = 7 where child_id =  (select id from master.item where abbrev = 'EXPT_NURSERY_CROSS_LIST_REVIEW_ACT');
update master.item_relation set order_number = 6 where child_id =  (select id from master.item where abbrev = 'EXPT_NURSERY_CROSS_LIST_PLACE_ACT');


--insert protocol tab
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_NURSERY_CROSS_LIST_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_NURSERY_CROSS_LIST_DATA_PROCESS') as parent_id,
	id as child_id,
	5,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev = 'EXPT_NURSERY_CROSS_LIST_PROTOCOLS_ACT';


--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_NURSERY_CROSS_LIST_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_NURSERY_CROSS_LIST_PROTOCOLS_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_NURSERY_CROSS_LIST_TRAITS_PROTOCOLS_ACT'then 1
		else 2 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_NURSERY_CROSS_LIST_TRAITS_PROTOCOLS_ACT');
;






--insert to platform.module
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('EXPT_NURSERY_CROSS_LIST_PROTOCOLS_ACT_MOD','Protocols','Protocols','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
('EXPT_NURSERY_CROSS_LIST_TRAITS_PROTOCOLS_ACT_MOD','Traits Protocols','Traits Protocols','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocols specified');


--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
	(select id from platform.module where abbrev = 'EXPT_NURSERY_CROSS_LIST_PROTOCOLS_ACT_MOD'),
	1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev = 'EXPT_NURSERY_CROSS_LIST_PROTOCOLS_ACT';


--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_NURSERY_CROSS_LIST_TRAITS_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_CROSS_POST_PLANNING_TRAITS_PROTOCOLS_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_NURSERY_CROSS_LIST_TRAITS_PROTOCOLS_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_NURSERY_CROSS_LIST_TRAITS_PROTOCOLS_ACT');
