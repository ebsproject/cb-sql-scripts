/*
* This file is part of Breeding4Results.
*
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-14 20:20:25
*/


--/*
BEGIN;

SAVEPOINT sp1;
--*/


set role b4radmin;


DO

$BODY$

DECLARE
    var_updated_count integer = 0;
BEGIN
	/**
	 * B4R-3783 Populate design column in operational.experiment table
	 */
    
    with t_variable as (
		select
			mv.*
		from
			master.variable mv
		where
			mv.abbrev = 'DESIGN'
			and mv.is_void = false
	), t_experiment as (
		select
			oexp.id experiment_id,
			oexp.experiment_name,
			oexp.design old_design,
			(
				select
					oexpd.value
				from
					operational.experiment_data oexpd,
					t_variable tv
				where
					oexpd.variable_id = tv.id
					and oexpd.experiment_id = oexp.id
					and oexpd.is_void = false
			) new_design
		from
			operational.experiment oexp
		order by
			oexp.id asc
	), t_update_experiment as (
		update
			operational.experiment oexp
		set
			design = t.new_design
		from
			t_experiment t
		where
			oexp.id = t.experiment_id
			and oexp.design is null
			and t.new_design is not null
		returning
			*
	)
	select
		count(1)
	into
		var_updated_count
	from
		t_update_experiment t
	;
	
	raise notice '+ Experiment (design) updated: % records', var_updated_count;
END;

$BODY$;


/*
DEV
NOTICE:  + Experiment (design) updated: 67 records
DO

Query returned successfully in 216 msec.
*/


--/*
ROLLBACK TO sp1;

END;
--*/
