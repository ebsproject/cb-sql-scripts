set role b4radmin;

-- FUNCTION: operational.get_cross_method(json, integer, integer)

-- DROP FUNCTION operational.get_cross_method(json, integer, integer);

CREATE OR REPLACE FUNCTION operational.get_cross_method(
	var_parent_list json,
	experiment_id_val integer,
	user_id integer)
    RETURNS character varying
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
DECLARE
  crossListArr integer[];
  crossesArr text[];
  female varchar;
  male varchar;
  femaleGen varchar;
  maleGen varchar;
  femaleInfo RECORD;
  maleInfo RECORD;
  cross_method varchar;
  i json;
  --product_id integer;
  crossing_method_id integer;
  crossno integer;
  var_scale_id integer;
  var_female_gen_pos integer;
  var_male_gen_pos integer;
  var_female_gen_proc varchar;
  var_male_gen_proc varchar;
  var_f1_parent_product_id integer;
  var_fn_parent_product_id integer;
  var_f1_parents_product_ids integer[];
  var_f1_parent_gids integer[];
  var_fn_parent_gids integer[];
  var_f1_parent_gpids integer[];
  var_f1_parent_gpid1s integer[];
  var_f1_parent_gpid2s integer[];
  var_f1_parent_id integer;
  var_fn_parent_id integer;
  var_is_backcross boolean;
  scaleValueInfo RECORD;
  scale_value_id integer;
  record_exists boolean;
  crossing_list_id integer;
  cross_id_list varchar;
  query_string varchar;
  temp_cross_id integer;
  fGenerationArray text[];
  cross_method_val varchar;

BEGIN
  fGenerationArray := ARRAY['F1','F2','F3','F4','F5','F6','F7','F8','F9','F10','F11','F12'];

  crossno = 1;
  FOR i IN SELECT * FROM json_array_elements(var_parent_list) LOOP
    record_exists = false;
    female := i->>'femaleEntryId';
    male := i->>'maleEntryId';

    if(female is null)
    then
      female := i->>'femaleentryid';
    end if;

    if(male is null)
    then
      male := i->>'maleentryid';
    end if;

    SELECT 
        el.id,
        mp.generation
    into femaleInfo
      FROM 
        master.product mp,
        operational.entry_list el
      WHERE
        el.id = female::integer
        and mp.id = el.product_id
        and el.is_void = false
        and mp.is_void = false
     limit 1;

    SELECT 
        el.id,
        mp.generation
    into maleInfo
      FROM 
        master.product mp,
        operational.entry_list el
      WHERE
        el.id = male::integer
        and mp.id = el.product_id
        and el.is_void = false
        and mp.is_void = false
    limit 1;

    femaleGen := femaleInfo.generation;
    maleGen := maleInfo.generation;

    -- process special generation
    select position('F' in femaleGen) into var_female_gen_pos;
    select position('F' in maleGen) into var_male_gen_pos;

    select regexp_replace(substring(femaleGen from var_female_gen_pos for 3), '[^\w]+','') into var_female_gen_proc;  
    select regexp_replace(substring(maleGen from var_male_gen_pos for 3), '[^\w]+','') into var_male_gen_proc;

    femaleGen := var_female_gen_proc;
    maleGen := var_male_gen_proc;
    cross_method := null;
    var_f1_parent_id := null;
    var_fn_parent_id := null;

    -- Check the Cross Method
    -- Get the scale id
    SELECT scale_id into var_scale_id from master.variable where abbrev = 'CROSS_METHOD' and is_void = false;
	-- 	if selfing
	IF (female <> male)
	THEN 
    -- if backcross and female is F1
    IF (femaleGen = 'F1' AND ((maleGen != 'F1') AND (SELECT fGenerationArray @> ARRAY[maleGen::text]) = true))
    THEN
      --raise notice 'f1/fn';
      var_f1_parent_id := femaleInfo.id;
      var_fn_parent_id := maleInfo.id;      

		ELSIF (maleGen = 'F1' AND ((femaleGen != 'F1') AND (SELECT fGenerationArray @> ARRAY[femaleGen::text]) = true))
    THEN
      --raise notice 'fn/f1';
      var_f1_parent_id := maleInfo.id;
      var_fn_parent_id := femaleInfo.id;

    END IF;

    -- check if F1 and Fn parents are set
    IF(var_f1_parent_id is not null and var_fn_parent_id is not null) THEN
      -- get product id of F1 parent
      select product_id into var_f1_parent_product_id from operational.entry_list where id = var_f1_parent_id;
      -- get product id of Fn paren
      select product_id into var_fn_parent_product_id from operational.entry_list where id = var_fn_parent_id;

      -- get product ids of F1 female parent
      select 
        ARRAY[female.product_id::integer, male.product_id::integer] into var_f1_parents_product_ids
      from 
        operational.cross c
      left join operational.entry female
        on c.female_entry_id = female.id
      left join operational.entry male
        on c.male_entry_id = male.id
      where 
        c.product_id = var_f1_parent_product_id;

      -- check if product id of male parent equals product ids of parents of F1 parent
      IF(var_f1_parents_product_ids is not null) THEN 
        
        -- check if product id of Fn male parent in product ids parents of f1 female product id 
        select var_fn_parent_product_id = ANY (var_f1_parents_product_ids) into var_is_backcross;

        if(var_is_backcross) then
          cross_method := 'BC'; -- 'BACKCROSS';
        END IF;
      ELSE
        -- check in gms.germplsm table

        -- get GIDs of F1 female parent
        select array_agg(gid::INTEGER) into var_f1_parent_gids from master.product_gid where product_id = var_f1_parent_product_id and is_void = false;
        -- get GIDs of Fn male parent
        select array_agg(gid::INTEGER) into var_fn_parent_gids from master.product_gid where product_id = var_fn_parent_product_id and is_void = false;

        -- get GPID1 and GPID2 of F1 female parent
        select
          array_agg(g.gpid1) into var_f1_parent_gpid1s
        from 
          master.product_gid pg
        left join gms.germplsm g
        on 
          g.gid = pg.gid
        where
          pg.product_id = var_f1_parent_product_id;

        select
          array_agg(g.gpid2) into var_f1_parent_gpid2s
        from 
          master.product_gid pg
        left join gms.germplsm g
        on
          g.gid = pg.gid
        where
          pg.product_id = var_f1_parent_product_id;

        -- get all gpid1s and gpid2s of f1 female parent
        select array_cat(var_f1_parent_gpid1s, var_f1_parent_gpid2s) into var_f1_parent_gpids;

        -- check if any of the gids of Fn male parent is in GPIDs of F1 female parent

        select var_fn_parent_gids && var_f1_parent_gpids into var_is_backcross;
        
        IF(var_is_backcross) THEN
          cross_method := 'BC'; --'BACKCROSS';
        END IF;   
      END IF;
    END IF;
	END IF;

    -- if not Backcross
    IF(cross_method is null) THEN
	  IF (female = male)
	  THEN 
	  	 cross_method := 'SLF'; --'SELFING';
      ELSIF ((femaleGen != 'F1' AND (SELECT fGenerationArray @> ARRAY[femaleGen::text]) = true) AND (maleGen != 'F1' AND (SELECT fGenerationArray @> ARRAY[maleGen::text]) = true))
      THEN
        cross_method := 'SC'; --'SINGLE CROSS';
      ELSIF (femaleGen = 'F1' AND maleGen = 'F1')
      THEN
        cross_method := 'C2W'; --'DOUBLE CROSS';
      ELSIF (femaleGen = 'F1' AND (maleGen != 'F1' AND (SELECT fGenerationArray @> ARRAY[maleGen::text]) = true))
      THEN
        cross_method := 'C3W'; --'THREE-WAY CROSS';
      ELSE
        cross_method := null;
      END IF;
    END IF;

    raise notice 'method: %',cross_method;
	
    --Retrieve the scale values
    --SELECT id, value into scaleValueInfo FROM master.scale_value where scale_id = var_scale_id and value ilike '%'||cross_method||'%';  --Please do not remove
	--raise notice 'test: %',(SELECT id, abbrev from master.cross_method where is_void = false and abbrev ilike '%'||cross_method||'%');
	cross_method_val := cross_method;
	SELECT id into scaleValueInfo FROM master.cross_method where is_void = false and abbrev ilike '%'||cross_method_val||'%';
    
    --Check if a record with the same experiment_id, female_entry_id, male_entry_id exists in the database
    SELECT id INTO crossing_list_id FROM operational.crossing_list where experiment_id = experiment_id_val and female_entry_id = femaleInfo.id and male_entry_id = maleInfo.id and is_void = false;

    IF (crossing_list_id is not null) THEN 
      record_exists := true;
    ELSE 
      record_exists := false;
    END IF;

    --Creating new record in the crossing_list table
    IF (record_exists = false)
    THEN 
	  raise notice 'Cross No: %',crossno;
      IF (cross_method is not null) 
      THEN
        INSERT into operational.crossing_list (experiment_id,female_entry_id,male_entry_id,crossing_method_id,crossno,is_method_autofilled,creator_id)
      VALUES (experiment_id_val,femaleInfo.id,maleInfo.id,scaleValueInfo.id,crossno,true,user_id);
      ELSE
        INSERT into operational.crossing_list (experiment_id,female_entry_id,male_entry_id,crossing_method_id,crossno,is_method_autofilled,creator_id)
      VALUES (experiment_id_val,femaleInfo.id,maleInfo.id,scaleValueInfo.id,crossno,false,user_id);
	  
    END IF; 
    crossno := crossno+1;
    END IF;
  END LOOP;
  RETURN 'SUCCESS';

END; $BODY$;