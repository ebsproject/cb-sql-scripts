/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Gene Christhopher Romuga <g.romuga@irri.org>
 * @date 2019-11-12 15:09:03
 */


/**
 * B4R-3770 Nursery Creation: Populate abbrev column in master.scale_value table
 *
 */

set role b4radmin;

do $$
declare 
   var_record record;
   var_record_sv record;
   val_scale_value varchar(128);
begin
    
   for var_record in 
  	   -- get id, abbrev, scale_id from master.variable
       select id, abbrev, scale_id from master.variable order by id
	   --select id, abbrev, scale_id from master.variable where abbrev = 'EXPERIMENT_TYPE' order by id
   loop

	   if var_record.id is not null then
	   		
			for var_record_sv in 
				-- get value from master.scale_value
				select value, scale_id from master.scale_value where scale_id = var_record.scale_id order by scale_id, value
			loop
			
				if var_record_sv.value is not null then
					
					val_scale_value = var_record_sv.value;
					
					--Special case
					if var_record_sv.value = '1/3' and var_record_sv.scale_id in (67, 68) then
						val_scale_value = var_record_sv.value || ' 1';
						--raise notice '% %', val_scale_value, var_record_sv.scale_id;
					elsif var_record_sv.value = '1-3' and var_record_sv.scale_id in (67, 68) then
						val_scale_value = var_record_sv.value || ' 2';
						--raise notice '% %', val_scale_value, var_record_sv.scale_id;
					end if;
				
					--replace special characters by _
					val_scale_value = upper(regexp_replace(val_scale_value,'[^\w]+|-%=/ ','_','g'));
					
					--check last character and replace _ by empty character
					if right(val_scale_value, 1) = '_' then
						val_scale_value = replace(val_scale_value, '_', '');	
					end if;
			
					--update abbrev column
					update master.scale_value 
						set abbrev = var_record.abbrev||'_'||val_scale_value
						where value = var_record_sv.value and scale_id = var_record.scale_id;
						
					raise notice '!POPULATED %', var_record.abbrev||'_'||val_scale_value;
					
				end if;
				
			end loop;
			
	   end if;
	   
   end loop;

end;
$$

--select value, abbrev from master.scale_value where abbrev is not null order by abbrev
--update master.scale_value set abbrev = null
