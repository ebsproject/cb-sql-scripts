/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Argem Gerald Flores <a.flores@irri.org>
 * @date 2019-11-18 16:34:04
 */


--/*
BEGIN;

SAVEPOINT sp1;
--*/


set role b4radmin;


DO

$BODY$
	
DECLARE
	var_cross_method record;
BEGIN
    /**
     * B4R-3602 Insert selfing record to master.cross_method table
     */
	
	insert into
		master.cross_method (
			abbrev,
			"name",
			description,
			display_name,
			creator_id
		)
		select
			'SLF' abbrev,
			'selfing' "name",
			'Selfing a single plant or population' description,
			'Selfing' display_name,
			1 creator_id
	returning
		*
	into
		var_cross_method
	;
	
	if (var_cross_method.id is not null) then
		raise notice '+ Cross method added: %', 'SELFING';
	else
		raise notice '! Cross method not added: %', 'SELFING';
	end if;
END;

$BODY$;


--/*
ROLLBACK TO sp1;

END;
--*/
