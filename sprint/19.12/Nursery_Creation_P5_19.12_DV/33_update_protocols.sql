
-- B4R-3458 Experiment creation: Protocols > Load protocols

--planting protocol
update master.item set display_name = 'Planting', item_icon = 'fa fa-pagelines' where abbrev ilike '%planting_protocols%';
-- changed from 'create' to 'protocol' controller id
update platform.module set controller_id = 'protocol' where abbrev ilike '%planting_protocols%' or abbrev ilike '%traits_protocols%';

--traits protocol
update master.item set display_name = 'Traits', item_icon = 'fa fa-braille' where abbrev ilike '%traits_protocols%';

--process path protocol
update master.item set display_name = 'Process Path', item_icon = 'fa fa-list-ol' where abbrev ilike '%process_path_protocols%';
    