set role b4radmin;

insert into master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage,subject_entity_id)
values('EXPT_TRIAL_IRRI_DATA_PROCESS','Trial Experiment for IRRI',40,'Trial Experiment for IRRI','Trial Experiment for IRRI',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation',(select id from master.entity where abbrev='EXPERIMENT'));

--create Specify basic information
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

--create Specify entry list
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol');

--create Specify design
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_DESIGN_ACT','Specify Design',30,'Specify Design','Design',1,'active','fa fa-th');

--create Specify experiment groups
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_EXPT_GROUP_ACT','Specify Experiment Groups',30,'Specify Experiment Groups','Experiment Groups',1,'active','fa fa-bars');

--create Specify Protocols
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_PROTOCOLS_ACT','Specify Protocols',30,'Specify Protocols','Protocols',1,'active','fa fa-map-marker');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT','Planting Protocol',20,'Planting Protocol','Planting Protocol',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_TRAITS_PROTOCOLS_ACT','Traits Protocol',20,'Traits Protocol','Traits Protocol',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT','Process Path Protocol',20,'Process Path Protocol','Process Path Protocol',1,'active','fa fa-random');


--create Specify locations
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_PLACE_ACT','Specify Place',30,'Specify Place','Place',1,'active','fa fa-map-marker');

--create Preview and confirm
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_TRIAL_IRRI_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');


--insert Specify Parents, Specify Crosses and Preview and Confirm in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_TRIAL_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_TRIAL_IRRI_DATA_PROCESS') as parent_id,
	id as child_id,
	case
		when abbrev = 'EXPT_TRIAL_IRRI_BASIC_INFO_ACT' then 1
		when abbrev = 'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT'then 2
		when abbrev = 'EXPT_TRIAL_IRRI_DESIGN_ACT'then 3
		when abbrev = 'EXPT_TRIAL_IRRI_EXPT_GROUP_ACT'then 4
        when abbrev = 'EXPT_TRIAL_IRRI_PROTOCOLS_ACT'then 5
		when abbrev = 'EXPT_TRIAL_IRRI_PLACE_ACT'then 6
		when abbrev = 'EXPT_TRIAL_IRRI_REVIEW_ACT'then 7
		else 8 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev in ('EXPT_TRIAL_IRRI_BASIC_INFO_ACT','EXPT_TRIAL_IRRI_ENTRY_LIST_ACT','EXPT_TRIAL_IRRI_DESIGN_ACT', 'EXPT_TRIAL_IRRI_EXPT_GROUP_ACT','EXPT_TRIAL_IRRI_PROTOCOLS_ACT', 'EXPT_TRIAL_IRRI_PLACE_ACT', 'EXPT_TRIAL_IRRI_REVIEW_ACT');


--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_TRIAL_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_TRIAL_IRRI_PROTOCOLS_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT' then 1
		when abbrev = 'EXPT_TRIAL_IRRI_TRAITS_PROTOCOLS_ACT'then 2
		when abbrev = 'EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT'then 3
		else 4 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT','EXPT_TRIAL_IRRI_TRAITS_PROTOCOLS_ACT','EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT');
;





--insert Specify Parents, Specify Crosses and Preview and Confirm in platform.module
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('EXPT_TRIAL_IRRI_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
('EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
('EXPT_TRIAL_IRRI_DESIGN_ACT_MOD','Specify Design','Specify Design','experimentCreation','create','specify-design',1,'added by j.antonio ' || now(), 'design generated'),
('EXPT_TRIAL_IRRI_EXPT_GROUP_ACT_MOD','Specify Experiment Groups','Specify Experiment Groups','experimentCreation','create','specify-experiment-groups',1,'added by j.antonio ' || now(), 'experiment group specified'),
('EXPT_TRIAL_IRRI_PROTOCOLS_ACT_MOD','Protocols','Protocols','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
('EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT_MOD','Planting Protocols','Planting Protocols','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
('EXPT_TRIAL_IRRI_TRAITS_PROTOCOLS_ACT_MOD','Traits Protocols','Traits Protocols','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
('EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD','Process Path Protocols','Process Path Protocols','experimentCreation','protocol','process-path-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
('EXPT_TRIAL_IRRI_PLACE_ACT_MOD','Specify Locations','Specify Locations','experimentCreation','create','specify-locations',1,'added by j.antonio ' || now(), 'location rep studies created'),
('EXPT_TRIAL_IRRI_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'finalized');

--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
	case
		when abbrev = 'EXPT_TRIAL_IRRI_BASIC_INFO_ACT' then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_BASIC_INFO_ACT_MOD')
		when abbrev = 'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_MOD')
		when abbrev = 'EXPT_TRIAL_IRRI_DESIGN_ACT' then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_DESIGN_ACT_MOD')
		when abbrev = 'EXPT_TRIAL_IRRI_EXPT_GROUP_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_EXPT_GROUP_ACT_MOD')
        when abbrev = 'EXPT_TRIAL_IRRI_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_PROTOCOLS_ACT_MOD')
		when abbrev = 'EXPT_TRIAL_IRRI_PLACE_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_PLACE_ACT_MOD')
		when abbrev = 'EXPT_TRIAL_IRRI_REVIEW_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_REVIEW_ACT_MOD')
		else (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_REVIEW_ACT_MOD') end as module_id,
	1,
	'added by l.gallardo ' || now()
from
	master.item
where
	abbrev in ('EXPT_TRIAL_IRRI_BASIC_INFO_ACT','EXPT_TRIAL_IRRI_ENTRY_LIST_ACT','EXPT_TRIAL_IRRI_DESIGN_ACT', 'EXPT_TRIAL_IRRI_EXPT_GROUP_ACT','EXPT_TRIAL_IRRI_PROTOCOLS_ACT', 'EXPT_TRIAL_IRRI_PLACE_ACT', 'EXPT_TRIAL_IRRI_REVIEW_ACT');


--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT_MOD')
  when abbrev = 'EXPT_TRIAL_IRRI_TRAITS_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_TRAITS_PROTOCOLS_ACT_MOD')
  when abbrev = 'EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT','EXPT_TRIAL_IRRI_TRAITS_PROTOCOLS_ACT','EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT');
