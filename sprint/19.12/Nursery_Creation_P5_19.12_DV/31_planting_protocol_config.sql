-- TRIAL EXPERIMENT
  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_TRIAL_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment Trial protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for trial data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

-- TRIAL EXPERIMENT FOR IRRI
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment IRRI trial protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for IRRI trial data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

-- CROSS POST-PLANNING
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_CROSS_POST_PLANNING_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment Cross Pre-Planning protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for cross post-planning data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

-- CROSS PRE-PLANNING
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_NURSERY_CB_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment Nursery crossing protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for nursery crossing data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

-- CROSS PRE-PLANNING FOR HYBRID
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_CROSS_PRE_PLANNING_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment Cross Pre-Planning for Hybrid Protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for cross pre-planning for Hybrid data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

-- NURSERY-PARENT LIST
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment Nursery-Parent List trial protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for Nursery-Parent List data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

-- SEED INCREASE
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_SEED_INCREASE_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment seed increase protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for seed increase data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

-- SELECTION AND ADVANCEMENT
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_SELECTION_ADVANCEMENT_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment Selection and Advancement Protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for Selection and Advancement data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');



