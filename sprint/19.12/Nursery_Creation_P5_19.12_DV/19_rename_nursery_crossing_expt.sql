set role b4radmin;

--Rename "Nursery-Crossing Experiment" to Nursery > "Cross Pre-planning Experiment"
update master.item set (name,display_name) = ('Cross Pre-planning Experiment','Cross Pre-planning Experiment') where abbrev = 'EXPT_NURSERY_CB_DATA_PROCESS'