/*
* This file is part of Breeding4Results.
*
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-12 15:04:32
*/

--/*
begin;

savepoint sp1;
--*/

set role b4radmin;

-- add locrep_block_id column to operational.plot table
ALTER TABLE
    operational.plot
ADD COLUMN IF NOT EXISTS
    locrep_block_id
    integer
    -- other_properties: NOT NULL DEFAULT default_value
;


COMMENT ON COLUMN
	operational.plot.locrep_block_id
IS
	'Reference ID to block containing the plot'
;


-- add foreign key constraint to locrep_block_id column in operational.plot table referencing operational.locrep_block.id column
ALTER TABLE IF EXISTS
    operational.plot
ADD CONSTRAINT
    plot_locrep_block_id_fkey
FOREIGN KEY (
    locrep_block_id
)
REFERENCES
    operational.locrep_block (
        id
    )
ON UPDATE CASCADE
ON DELETE RESTRICT
;


---


-- add locrep_block_id column to operational.plot table
ALTER TABLE
    operational.temp_plot
ADD COLUMN IF NOT EXISTS
    locrep_block_id
    integer
    -- other_properties: NOT NULL DEFAULT default_value
;


COMMENT ON COLUMN
	operational.temp_plot.locrep_block_id
IS
	'Reference ID to block containing the plot'
;


-- add foreign key constraint to locrep_block_id column in operational.plot table referencing operational.locrep_block.id column
ALTER TABLE IF EXISTS
    operational.temp_plot
ADD CONSTRAINT
    temp_plot_locrep_block_id_fkey
FOREIGN KEY (
    locrep_block_id
)
REFERENCES
    operational.locrep_block (
        id
    )
ON UPDATE CASCADE
ON DELETE RESTRICT
;


--/*
rollback to sp1;

end;
--*/
