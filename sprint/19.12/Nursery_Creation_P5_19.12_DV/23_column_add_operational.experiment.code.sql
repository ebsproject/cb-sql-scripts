/*
* This file is part of Breeding4Results.
*
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-13 09:45:03
*/

--/*
begin;

savepoint sp1;
--*/


/**
 * B4R-3604 Add code column to operational.experiment table
 */

set role b4radmin;

-- drop code column from operational.experiment table
/*
ALTER TABLE
    operational.experiment
DROP COLUMN IF EXISTS
    code
;
*/


-- add code column to operational.experiment table
ALTER TABLE
    operational.experiment
ADD COLUMN IF NOT EXISTS
    code
    varchar(128)
	-- not null
    -- other_properties: NOT NULL DEFAULT default_value
;


COMMENT ON COLUMN
	operational.experiment.code
IS
	'Short name identifier of the experiment'
;


--/*
rollback to sp1;

end;
--*/
