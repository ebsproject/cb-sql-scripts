/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-12 14:29:54
*/


--/*
begin;

savepoint sp1;
--*/


set role b4radmin;


--/*
----------------------------------------------------------------------------------------------------

-- Blocks and sub-blocks in the locrep level

-- drop table if exists operational.locrep_block;

create table operational.locrep_block (
    -- primary key column
    id serial not null, -- Identifier of the record within the table
    
    -- columns
    "locrep_id" integer NOT NULL, -- Reference ID to the locrep containing the block
    "parent_id" integer, -- Reference ID to the parent block, if any
    "order_number" integer NOT NULL DEFAULT 1, -- Sequence number of the block among its sibling blocks
    "code" varchar NOT NULL, -- Short identifier of the block
    "name" varchar NOT NULL, -- Name of the block
    "block_type" varchar NOT NULL, -- Type of block: parent, cross, sub-block
    "no_of_blocks" integer NOT NULL DEFAULT 0, -- Total no. of children blocks the parent block has
    "no_of_rows_in_block" integer NOT NULL, -- Total no. of rows in a block
    "no_of_cols_in_block" integer NOT NULL, -- Total no. of columns in a block
    "no_of_reps_in_block" integer NOT NULL DEFAULT 1, -- Total no. of reps in a block
    "plot_numbering_order" varchar NOT NULL, -- Ordering of plot number: row-serpentine, column-serpentine, row-order, column-order
    "starting_corner" varchar NOT NULL, -- Starting corner where the ordering of the plot number will begin
    "entry_ids" integer[], -- Array list of entry ids belonging to the block
	
    -- audit columns
    remarks text, -- Additional details about the record
    creation_timestamp timestamp not null default current_timestamp, -- Timestamp when the record was added to the table
    creator_id integer not null, -- ID of the user who added the record to the table
    modification_timestamp timestamp, -- Timestamp when the record was last modified
    modifier_id integer, -- ID of the user who last modified the record
    notes text, -- Additional details added by an admin; can be technical or advanced details
    is_void boolean not null default false, -- Indicator whether the record is deleted (true) or not (false)
    event_log jsonb, -- Historical transactions of the record
    record_uuid uuid not null default uuid_generate_v4(), -- Universally unique identifier (UUID) of the record
    
    -- primary key constraint
    constraint locrep_block_id_pkey primary key (id), -- Primary key constraint
    
    /*-- foreign relations
    constraint locrep_block_column_name_fkey foreign key (column_name)
        references foreign_schema.foreign_table (id) match simple
        on update cascade on delete restrict, -- Column foreign key relation
    --*/
    
    constraint locrep_block_locrep_id_fkey foreign key (locrep_id)
        references operational.study (id) match simple
        on update cascade on delete restrict, -- Locrep foreign key relation
    
    constraint locrep_block_parent_id_fkey foreign key (parent_id)
        references operational.locrep_block (id) match simple
        on update cascade on delete restrict, -- Parent block foreign key relation
    
    -- audit foreign relations
    constraint locrep_block_creator_id_fkey foreign key (creator_id)
        references master.user (id) match simple
        on update cascade on delete restrict, -- Creator foreign key relation
    constraint locrep_block_modifier_id_fkey foreign key (modifier_id)
        references master.user (id) match simple
        on update cascade on delete restrict -- Modifier foreign key relation
);

/*-- indices
create index locrep_block_column_name_idx
    on operational.locrep_block
    using btree (column_name);
--*/

create index locrep_block_parent_id_idx
    on operational.locrep_block
    using btree (parent_id);

create index locrep_block_locrep_id_order_number_idx
    on operational.locrep_block
    using btree (locrep_id, order_number);

create index locrep_block_is_void_idx
    on operational.locrep_block
    using btree (is_void);

create index locrep_block_creator_id_idx
    on operational.locrep_block
    using btree (creator_id);

create index locrep_block_modifier_id_idx
    on operational.locrep_block
    using btree (modifier_id);

create index locrep_block_record_uuid_idx
    on operational.locrep_block
    using btree (record_uuid);

-- comments
comment on table operational.locrep_block is 'Blocks and sub-blocks in the locrep level';

comment on column operational.locrep_block.id is 'Identifier of the record within the table';

comment on column operational.locrep_block.locrep_id is 'Reference ID to the locrep containing the block';
comment on column operational.locrep_block.parent_id is 'Reference ID to the parent block, if any';
comment on column operational.locrep_block.order_number is 'Sequence number of the block among its sibling blocks';
comment on column operational.locrep_block.code is 'Short identifier of the block';
comment on column operational.locrep_block.name is 'Name of the block';
comment on column operational.locrep_block.block_type is 'Type of block: parent, cross, sub-block';
comment on column operational.locrep_block.no_of_blocks is 'Total no. of children blocks the parent block has';
comment on column operational.locrep_block.no_of_rows_in_block is 'Total no. of rows in a block';
comment on column operational.locrep_block.no_of_cols_in_block is 'Total no. of columns in a block';
comment on column operational.locrep_block.no_of_reps_in_block is 'Total no. of reps in a block';
comment on column operational.locrep_block.plot_numbering_order is 'Ordering of plot number: row-serpentine, column-serpentine, row-order, column-order';
comment on column operational.locrep_block.starting_corner is 'Starting corner where the ordering of the plot number will begin';
comment on column operational.locrep_block.entry_ids is 'Array list of entry ids belonging to the block';

comment on column operational.locrep_block.remarks is 'Additional details about the record';
comment on column operational.locrep_block.creation_timestamp is 'Timestamp when the record was added to the table';
comment on column operational.locrep_block.creator_id is 'ID of the user who added the record to the table';
comment on column operational.locrep_block.modification_timestamp is 'Timestamp when the record was last modified';
comment on column operational.locrep_block.modifier_id is 'ID of the user who last modified the record';
comment on column operational.locrep_block.notes is 'Additional details added by an admin; can be technical or advanced details';
comment on column operational.locrep_block.is_void is 'Indicator whether the record is deleted (true) or not (false)';
comment on column operational.locrep_block.event_log is 'Historical transactions of the record';
comment on column operational.locrep_block.record_uuid is 'Universally unique identifier (UUID) of the record';
--*/


-- uncomment below to audit this table
--/*
SELECT * FROM z_admin.audit_table(
    in_table_name := 'operational.locrep_block'
);
--*/


--/*
rollback to sp1;

end;
--*/