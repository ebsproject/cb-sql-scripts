--NEW - list of all FLAT plot types when no establishment is applied

--For RICE crop plot types only -- no planting type 
--NEW - list of all FLAT plot types when no establishment is applied
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('RICE_PLOT_TYPE', 'Experiment Protocol RICE Plot types with no planting type selected',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R",
                "PLOT_TYPE_2R",
                "PLOT_TYPE_3R",
                "PLOT_TYPE_4R",
                "PLOT_TYPE_5R",
                "PLOT_TYPE_6R",
                "PLOT_TYPE_10R",
                "PLOT_TYPE_20R",
                "PLOT_TYPE_50R",
                "PLOT_TYPE_DIRECT_SEEDED_UNKNOWN",
				"PLOT_TYPE_1R_X_12H",
                "PLOT_TYPE_1R_X_25H",
                "PLOT_TYPE_2R_X_12H",
                "PLOT_TYPE_2R_X_25H",
                "PLOT_TYPE_4R_X_25H",
                "PLOT_TYPE_5R_X_12H",
                "PLOT_TYPE_5R_X_25H",
                "PLOT_TYPE_6R_X_25H",
                "PLOT_TYPE_7R_X_25H",
                "PLOT_TYPE_7R_X_26H",
                "PLOT_TYPE_7R_X_32H",
                "PLOT_TYPE_10R_X_25H",
                "PLOT_TYPE_10R_X_26H",
                "PLOT_TYPE_TRANSPLANTED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLANTING_TYPE_FLAT_PLOT_TYPE', 'Experiment Protocol FLAT Plot types',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R",
                "PLOT_TYPE_2R",
                "PLOT_TYPE_3R",
                "PLOT_TYPE_4R",
                "PLOT_TYPE_5R",
                "PLOT_TYPE_6R",
                "PLOT_TYPE_10R",
                "PLOT_TYPE_20R",
                "PLOT_TYPE_50R",
                "PLOT_TYPE_DIRECT_SEEDED_UNKNOWN",
				"PLOT_TYPE_1R_X_12H",
                "PLOT_TYPE_1R_X_25H",
                "PLOT_TYPE_2R_X_12H",
                "PLOT_TYPE_2R_X_25H",
                "PLOT_TYPE_4R_X_25H",
                "PLOT_TYPE_5R_X_12H",
                "PLOT_TYPE_5R_X_25H",
                "PLOT_TYPE_6R_X_25H",
                "PLOT_TYPE_7R_X_25H",
                "PLOT_TYPE_7R_X_26H",
                "PLOT_TYPE_7R_X_32H",
                "PLOT_TYPE_10R_X_25H",
                "PLOT_TYPE_10R_X_26H",
                "PLOT_TYPE_TRANSPLANTED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLANTING_TYPE_CONTAINER_PLOT_TYPE', 'Experiment Protocol Plot types container',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R",
                "PLOT_TYPE_2R",
                "PLOT_TYPE_3R",
                "PLOT_TYPE_4R",
                "PLOT_TYPE_5R",
                "PLOT_TYPE_6R",
                "PLOT_TYPE_10R",
                "PLOT_TYPE_20R",
                "PLOT_TYPE_50R",
                "PLOT_TYPE_DIRECT_SEEDED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLANTING_TYPE_BED_PLOT_TYPE', 'Experiment Protocol Plot types bed',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R",
                "PLOT_TYPE_2R",
                "PLOT_TYPE_3R",
                "PLOT_TYPE_4R",
                "PLOT_TYPE_5R",
                "PLOT_TYPE_6R",
                "PLOT_TYPE_10R",
                "PLOT_TYPE_20R",
                "PLOT_TYPE_50R",
                "PLOT_TYPE_DIRECT_SEEDED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');



INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('ESTABLISHMENT_DIRECT_SEEDED_PLOT_TYPE', 'Experiment Protocol Plot types for Direct seeded',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R",
                "PLOT_TYPE_2R",
                "PLOT_TYPE_3R",
                "PLOT_TYPE_4R",
                "PLOT_TYPE_5R",
                "PLOT_TYPE_6R",
                "PLOT_TYPE_10R",
                "PLOT_TYPE_20R",
                "PLOT_TYPE_50R",
                "PLOT_TYPE_DIRECT_SEEDED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('ESTABLISHMENT_TRANSPLANTED_PLOT_TYPE', 'Experiment Protocol Plot types for transplanted',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R_X_12H",
                "PLOT_TYPE_1R_X_25H",
                "PLOT_TYPE_2R_X_12H",
                "PLOT_TYPE_2R_X_25H",
                "PLOT_TYPE_4R_X_25H",
                "PLOT_TYPE_5R_X_12H",
                "PLOT_TYPE_5R_X_25H",
                "PLOT_TYPE_6R_X_25H",
                "PLOT_TYPE_7R_X_25H",
                "PLOT_TYPE_7R_X_26H",
                "PLOT_TYPE_7R_X_32H",
                "PLOT_TYPE_10R_X_25H",
                "PLOT_TYPE_10R_X_26H",
                "PLOT_TYPE_TRANSPLANTED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('MAIZE_FLAT_PLOT_TYPE', 'Experiment Protocol Plot types for Maize',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_2R5M",
                "PLOT_TYPE_MAIZE_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('WHEAT_FLAT_PLOT_TYPE', 'Experiment Protocol Plot types for Wheat-flat',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_6SS_4M",
                "PLOT_TYPE_6SS_5M",
                "PLOT_TYPE_FLAT_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('WHEAT_BED_PLOT_TYPE', 'Experiment Protocol Plot types for Wheat-bed ',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1SD",
                "PLOT_TYPE_1SD_0_7M",
                "PLOT_TYPE_1SD_1_5M",
                "PLOT_TYPE_1SD_3M",
                "PLOT_TYPE_1SD_5M",
                "PLOT_TYPE_1ST",
                "PLOT_TYPE_1ST_2_8M",
                "PLOT_TYPE_2SD",
                "PLOT_TYPE_2SD_3M",
                "PLOT_TYPE_2SD_5M",
                "PLOT_TYPE_2ST",
                "PLOT_TYPE_2ST_2_8M",
                "PLOT_TYPE_4SD",
                "PLOT_TYPE_4ST",
                "PLOT_TYPE_8SD",
                "PLOT_TYPE_BED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_1R',  '{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"field_label": "Rows per plot",
			"order_number": 1,
			"variable_abbrev": "ROWS_PER_PLOT_CONT",
			"field_description": "Number of rows per plot"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"field_label": "Dist. bet. rows (cm)",
			"order_number": 2,
			"variable_abbrev": "DIST_BET_ROWS",
			"field_description": "Distance between rows"
		},
		{
			"unit": "m",
			"default": 0.2,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Width (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_WIDTH",
			"field_description": "Plot Width"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}
	]
}', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_2R',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_3R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_3R',  '{
  	"Values": [
        {
  			"default": 3,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 0.6,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_4R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_4R',  '{
  	"Values": [
        {
  			"default": 4,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
			{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"computed": "computed",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_5R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_5R',  '{
  	"Values": [
        {
  			"default": 5,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 1,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_6R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_6R',  '{
  	"Values": [
        {
  			"default": 6,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 1.2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_10R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_10R',  '{
  	"Values": [
        {
  			"default": 10,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_20R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_20R',  '{
  	"Values": [
        {
  			"default": 20,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}


  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_50R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_50R',  '{
  	"Values": [
        {
  			"default": 50,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 10,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (cm)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_DIRECT_SEEDED_UNKNOWN', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_DIRECT_SEEDED_UNKNOWN',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": false,
            "unit": "cm",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot Width"
  		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"field_label": "Plot Length (m)",
			"order_number": 3,
			"variable_abbrev": "PLOT_LN",
			"field_description": "Plot Length"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"field_label": "Plot Area (sqm)",
			"order_number": 4,
			"variable_abbrev": "PLOT_AREA_2",
			"field_description": "Plot Area"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"field_label": "Seeding Rate",
			"order_number": 5,
			"variable_abbrev": "SEEDING_RATE",
			"field_description": "Seeding Rate"
		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

--SAmple
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1R_X_12H', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_1R_X_12H',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 12,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1R_X_25H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1R_X_25H',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 25,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2R_X_12H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2R_X_12H',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 12,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2R_X_25H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2R_X_25H',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 25,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		} 

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_4R_X_25H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_4R_X_25H',  '{
  	"Values": [
        {
  			"default": 4,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 25,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_5R_X_12H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_5R_X_12H',  '{
  	"Values": [
        {
  			"default": 5,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 12,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_5R_X_25H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_5R_X_25H',  '{
  	"Values": [
        {
  			"default": 5,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 25,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
 			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_6R_X_25H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_6R_X_25H',  '{
  	"Values": [
        {
  			"default": 6,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 25,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_7R_X_25H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_7R_X_25H',  '{
  	"Values": [
        {
  			"default": 7,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 25,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_7R_X_26H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_7R_X_26H',  '{
  	"Values": [
        {
  			"default": 7,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 26,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_7R_X_32H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_7R_X_32H',  '{
  	"Values": [
        {
  			"default": 7,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 32,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_10R_X_25H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_10R_X_25H',  '{
  	"Values": [
        {
  			"default": 10,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 25,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_10R_X_26H', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_10R_X_26H',  '{
  	"Values": [
        {
  			"default": 10,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 26,
  			"disabled": true,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": 20,
            "unit": "cm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 20,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_TRANSPLANTED_UNKNOWN', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_TRANSPLANTED_UNKNOWN',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Hills per row",
  			"order_number": 2,
  			"variable_abbrev": "HILLS_PER_ROW_CONT",
  			"field_description": "Number of hills per row"
  		},
		{
  			"default": false,
            "unit": "cm",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Dist. bet. rows (cm)",
  			"order_number": 3,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Dist. bet. hills (m)",
  			"order_number": 4,
  			"variable_abbrev": "DIST_BET_HILLS",
  			"field_description": "Distance between hills"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 6,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot width"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot Area (sqm)",
  			"order_number": 7,
  			"variable_abbrev": "PLOT_AREA_1",
  			"field_description": "Plot Area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 8,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2R5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2R5M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
		{
  			"default": 1.5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 15,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_MAIZE_UNKNOWN', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_MAIZE_UNKNOWN',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_6SS_4M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_6SS_4M',  '{
  	"Values": [
        {
  			"default": 6,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
		{
  			"default": 0.18,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. plots",
  			"order_number": 3,
  			"variable_abbrev": "DISTANCE_BET_PLOTS_IN_M",
  			"field_description": "Distance betweem plots"
  		},
		{
  			"default": 1.3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 5.2,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_6SS_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_6SS_5M',  '{
  	"Values": [
        {
  			"default": 6,
  			"disabled": true,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
		{
  			"default": 0.18,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Dist. bet. plots",
  			"order_number": 3,
  			"variable_abbrev": "DISTANCE_BET_PLOTS_IN_M",
  			"field_description": "Distance betweem plots"
  		},
		{
  			"default": 1.3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 6.5,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_FLAT_UNKNOWN', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_FLAT_UNKNOWN',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_0_7M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_0_7M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 0.7,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.28,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_1_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_1_5M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 1.5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.6,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_3M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_3M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.2,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_5M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1ST', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1ST',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1ST_2_8M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1ST_2_8M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 2.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.12,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2SD',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2SD_3M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2SD_3M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed width",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2.4,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2SD_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2SD_5M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 4,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2ST', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2ST',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2ST_2_8M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2ST_2_8M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 2.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2.24,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_4SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_4SD',  '{
  	"Values": [
        {
  			"default": 4,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_4ST', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_4ST',  '{
  	"Values": [
        {
  			"default": 4,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_8SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_8SD',  '{
  	"Values": [
        {
  			"default": 8,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_BED_UNKNOWN', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_BED_UNKNOWN',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');
