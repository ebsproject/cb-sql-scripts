-- add previous variables to new variable set for planting protocols

insert into master.variable_set_member (variable_set_id,variable_id,order_number,notes,creator_id)
select 
(select id from master.variable_set where abbrev = 'PLANTING_PROTOCOL') as variable_set_id,
id as variable_id,
row_number() over () + (
	select 
		max(order_number)
	from 
		master.variable_set vs,
		master.variable_set_member vsm
	where
		vs.abbrev = 'PLANTING_PROTOCOL'
		and vsm.variable_set_id = vs.id
),
'added by jp.ramos for saving planting protocols',
1
from master.variable where abbrev in (
	'DIST_BET_ROWS',
	'ROWS_PER_PLOT_CONT',
	'PLOT_LN',
	'PLOT_AREA_SQM_CONT',
	'SEEDING_RATE',
	'HILLS_PER_ROW_CONT',
	'DIST_BET_HILLS',
	'PLOT_AREA_SQM_CONT'
);