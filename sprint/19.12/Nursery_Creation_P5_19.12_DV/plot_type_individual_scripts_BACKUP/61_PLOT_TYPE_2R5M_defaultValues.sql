INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2R5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2R5M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": false,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
		{
  			"default": 1.5,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 15,
            "unit": "sqm",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');