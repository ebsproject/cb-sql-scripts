INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('ESTABLISHMENT_DIRECT_SEEDED_PLOT_TYPE', 'Experiment Protocol Plot types for Direct seeded',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R",
                "PLOT_TYPE_2R",
                "PLOT_TYPE_3R",
                "PLOT_TYPE_4R",
                "PLOT_TYPE_5R",
                "PLOT_TYPE_6R",
                "PLOT_TYPE_10R",
                "PLOT_TYPE_20R",
                "PLOT_TYPE_50R",
                "PLOT_TYPE_DIRECT_SEEDED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');