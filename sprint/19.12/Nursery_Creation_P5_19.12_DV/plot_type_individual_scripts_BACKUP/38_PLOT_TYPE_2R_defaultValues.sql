INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2R', 'Experiment Protocol Plot types default values for Plot type PLOT_TYPE_2R',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": false,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
        {
  			"default": 20,
            "unit": "cm",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
        {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_RICE",
  			"field_description": "Plot Width"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');