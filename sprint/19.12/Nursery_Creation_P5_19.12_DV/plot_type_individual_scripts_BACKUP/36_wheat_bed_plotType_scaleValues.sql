INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('WHEAT_BED_PLOT_TYPE', 'Experiment Protocol Plot types for Wheat-bed ',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1SD",
                "PLOT_TYPE_1SD_0_7M",
                "PLOT_TYPE_1SD_1_5M",
                "PLOT_TYPE_1SD_3M",
                "PLOT_TYPE_1SD_5M",
                "PLOT_TYPE_1ST",
                "PLOT_TYPE_1ST_2_8M",
                "PLOT_TYPE_2SD",
                "PLOT_TYPE_2SD_3M",
                "PLOT_TYPE_2SD_5M",
                "PLOT_TYPE_2ST",
                "PLOT_TYPE_2ST_2_8M",
                "PLOT_TYPE_4SD",
                "PLOT_TYPE_4ST",
                "PLOT_TYPE_8SD",
                "PLOT_TYPE_BED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');