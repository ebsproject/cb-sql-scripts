INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('ESTABLISHMENT_TRANSPLANTED_PLOT_TYPE', 'Experiment Protocol Plot types for transplanted',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1R_X_12H",
                "PLOT_TYPE_1R_X_25H",
                "PLOT_TYPE_2R_X_12H",
                "PLOT_TYPE_2R_X_25H",
                "PLOT_TYPE_4R_X_25H",
                "PLOT_TYPE_5R_X_12H",
                "PLOT_TYPE_5R_X_25H",
                "PLOT_TYPE_6R_X_25H",
                "PLOT_TYPE_7R_X_25H",
                "PLOT_TYPE_7R_X_26H",
                "PLOT_TYPE_7R_X_32H",
                "PLOT_TYPE_10R_X_25H",
                "PLOT_TYPE_10R_X_26H",
                "PLOT_TYPE_TRANSPLANTED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');