INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('MAIZE_FLAT_PLOT_TYPE', 'Experiment Protocol Plot types for Maize',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_2R5M",
                "PLOT_TYPE_MAIZE_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');