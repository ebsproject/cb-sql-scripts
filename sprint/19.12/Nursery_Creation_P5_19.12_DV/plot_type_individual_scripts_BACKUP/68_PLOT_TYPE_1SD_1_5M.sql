INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_1_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_1_5M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": false,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Bed width",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 1.5,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN_1",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.6,
            "unit": "sqm",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');