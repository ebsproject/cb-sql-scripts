/*
* This file is part of Breeding4Results.
*
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-14 20:13:53
*/

--/*
begin;

savepoint sp1;
--*/


/**
 * B4R-3605 Add design column to operational.experiment table
 */

set role b4radmin;

-- drop design column from operational.experiment table
/*
ALTER TABLE
    operational.experiment
DROP COLUMN IF EXISTS
    design
;
*/


-- add design column to operational.experiment table
ALTER TABLE
    operational.experiment
ADD COLUMN IF NOT EXISTS
    design
    varchar(64)
    -- other_properties: NOT NULL DEFAULT default_value
;


/*
ALTER TABLE
	operational.experiment
ALTER COLUMN
	design
SET DATA TYPE
	varchar(64)
;


ALTER TABLE
	operational.study
ALTER COLUMN
	design
SET DATA TYPE
	varchar(64)
;
*/


COMMENT ON COLUMN
	operational.experiment.design
IS
	'Experimental design'
;


-- add check constraint
ALTER TABLE operational.experiment
    ADD CONSTRAINT experiment_design_chk CHECK (design = any(array['Systematic arrangement', 'Strip-Split-Split Plot Design', 'Strip-Split Plot Design', 'Strip Plot Desig', 'Split-Split-Split Plot Design in Latin Square Design', 'Split-Split-Split Plot Design in CRD', 'Split-Split Plot Design in CRD', 'Split Plot Design in RCBD', 'Row-Column', 'Repeated checks', 'Pedigree order', 'PBIB', 'Latin Square Desig', 'Complete Randomized Design', 'BIBD', 'Augmented RCBD', 'Alpha-lattice', 'Split-Split-Split Plot Design in RCBD', 'Split-Split Plot Design in RCBD', 'Split-Split Plot Design in Latin Square Design', 'Split Plot Design in Latin Square Design', 'Split Plot Design in CRD', 'RCBD', 'Random', 'P-REP', 'Balanced Incomplete Design', 'Augmented LSD', 'Augmented Alpha-lattice']::text[]))
    NOT VALID;


--/*
rollback to sp1;

end;
--*/
