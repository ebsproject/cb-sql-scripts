/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Gene Romuga <g.romuga@irri.org>
 * @date 2019-12-10 10:00:45
 */

do $$
declare 
	scale_id_no integer;
begin

	--Get scale_id from master.variable
	select mv.scale_id::integer from master.variable mv where mv.abbrev = 'ESTABLISHMENT' into scale_id_no;
	
	--insert scale_value
	insert into master.scale_value (scale_id, value, order_number, description, creator_id, scale_value_status, abbrev)
	                       values (scale_id_no, 'single seed', '4', 'single seed', '1', 'show', 'ESTABLISHMENT_SINGLE_SEED');

end $$
