/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Argem Gerald Flores <a.flores@irri.org>
 * @date 2019-11-18 13:35:45
 */


--/*
BEGIN;

SAVEPOINT sp1;
--*/


set role b4radmin;

/*
DO

$BODY$

DECLARE
	in_variable_set varchar = 'PLANTING_PROTOCOL';
	var_creator_id integer = 1;
	var_order_number integer = 1;
    var_variable record;
	var_variable_set record;
	var_variable_set_member record;
BEGIN
    /**
     * B4R-3606 Create PLANTING_PROTOCOL variable set
     */
	
	select
		mvs.*
	into
		var_variable_set
	from
		master.variable_set mvs
	where
		mvs.abbrev = in_variable_set
		and mvs.is_void = false
	;
	
	if (var_variable_set.id is null) then
		raise notice '+ Variable set to be created: %', in_variable_set;
		
		-- create variable set
		insert into
			master.variable_set (
				abbrev,
				"name",
				description,
				display_name,
				creator_id,
				varsetusage,
				"usage"
			)
			select
				'PLANTING_PROTOCOL' abbrev,
				'Planting Protocol' "name",
				null::text descriptionn,
				'Planting Protocol' display_name,
				var_creator_id creator_id,
				'STUDY' varsetusage,
				null "usage"
		returning
			*
		into
			var_variable_set
		;

		-- variable set created
		if (var_variable_set.id is not null) then
			raise notice '+ Variable set created: %', in_variable_set;
		else
			-- variable set not created
			raise notice '! Variable set not created: %', in_variable_set;
		end if;
	else
		raise notice '+ Variable set exists: %', var_variable_set.abbrev;
	end if;
	
	-- void existing members
	update
		master.variable_set_member t
	set
		is_void = true
	where
		t.variable_set_id = var_variable_set.id
		and t.is_void = false
	;
	
	-- add each variable as a member to the variable set
	for var_variable in (
		select
			t.*,
			zagv.*
		from (
				values
				(1, 'ROWS_PER_PLOT_CONT'),
				(2, 'HILLS_BY_PLOT'),
				(3, 'PLOT_DENSITY'),
				(4, 'DISTANCE_BET_PLOTS'),
				(5, 'ALLEY_LENGTH'),
				(6, 'PLOT_SIZE_SQM_CONT'),
				(7, 'PLOT_LN'),
				(8, 'PLOT_LN_DSR'),
				(9, 'PLOT_UNIT_MEASUREMENT')
			) t (
				order_number, variable_abbrev
			),
			z_admin.get_variable(t.variable_abbrev) zagv
		order by
			t.order_number
	) loop
		insert into
			master.variable_set_member (
				variable_set_id,
				variable_id,
				order_number,
				creator_id
			)
		select
			var_variable_set.id,
			var_variable.id,
			var_order_number,
			var_creator_id
		returning
			*
		into
			var_variable_set_member
		;
		
		-- variable added
		if (var_variable_set_member.id is not null) then
			raise notice '+ Variable set member created: % | %', var_order_number, var_variable.abbrev;

			var_order_number = var_order_number + 1;
		else
			-- variable not added
			raise notice '! Variable set member not created: % | %', var_order_number, var_variable.abbrev;
		end if;
	end loop;
END;

$BODY$;
*/

-- delete from master.variable_set where abbrev = 'PLANTING_PROTOCOL';

--/*
ROLLBACK TO sp1;

END;
--*/
