/*
* This file is part of Breeding4Results.
*
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-13 08:44:32
*/

--/*
begin;

savepoint sp1;
--*/


/**
 * B4R-3601 Add trait_protocol option to platform.list.type column
 */

set role b4radmin;

-- ALTER TABLE platform.list DROP CONSTRAINT list_type_chk;

ALTER TABLE platform.list
    ADD CONSTRAINT list_type_chk CHECK (type::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'designation'::text, 'study'::text, 'variable'::text, 'trait_protocol'::text]));


--/*
rollback to sp1;

end;
--*/
