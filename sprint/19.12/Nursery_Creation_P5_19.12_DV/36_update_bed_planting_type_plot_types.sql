INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('ESTABLISHMENT_DIRECT_SEEDED_PLANTING_TYPE_BED_PLOT_TYPE', 'Experiment Protocol Plot types for Direct Seeded bed planting type',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1SD",
                "PLOT_TYPE_1SD_0_7M",
                "PLOT_TYPE_1SD_1_5M",
                "PLOT_TYPE_1SD_3M",
                "PLOT_TYPE_1SD_5M",
                "PLOT_TYPE_1ST",
                "PLOT_TYPE_1ST_2_8M",
                "PLOT_TYPE_2SD",
                "PLOT_TYPE_2SD_3M",
                "PLOT_TYPE_2SD_5M",
                "PLOT_TYPE_2ST",
                "PLOT_TYPE_2ST_2_8M",
                "PLOT_TYPE_4SD",
                "PLOT_TYPE_4ST",
                "PLOT_TYPE_8SD",
                "PLOT_TYPE_BED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


delete from platform.config where abbrev in ('PLOT_TYPE_1SD','PLOT_TYPE_1SD_0_7M','PLOT_TYPE_1SD_1_5M','PLOT_TYPE_1SD_3M','PLOT_TYPE_1SD_5M','PLOT_TYPE_1ST','PLOT_TYPE_1ST_2_8M','PLOT_TYPE_2SD','PLOT_TYPE_2SD_3M',
'PLOT_TYPE_2SD_5M','PLOT_TYPE_2ST','PLOT_TYPE_2ST_2_8M','PLOT_TYPE_4SD','PLOT_TYPE_4ST','PLOT_TYPE_8SD','PLOT_TYPE_BED_UNKNOWN');
			   


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_0_7M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_0_7M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 0.7,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.28,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_1_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_1_5M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 1.5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 0.6,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_3M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_3M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.2,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1SD_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1SD_5M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1ST', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1ST',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_1ST_2_8M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_1ST_2_8M',  '{
  	"Values": [
        {
  			"default": 1,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 2.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 1.12,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2SD',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2SD_3M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2SD_3M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2.4,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2SD_5M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2SD_5M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 5,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 4,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2ST', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2ST',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed" : "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_2ST_2_8M', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_2ST_2_8M',  '{
  	"Values": [
        {
  			"default": 2,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": 0.4,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": 0.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": 2.8,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": 2.24,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_4SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_4SD',  '{
  	"Values": [
        {
  			"default": 4,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_4ST', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_4ST',  '{
  	"Values": [
        {
  			"default": 4,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 3,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_8SD', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_8SD',  '{
  	"Values": [
        {
  			"default": 8,
  			"disabled": true,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": 2,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('PLOT_TYPE_BED_UNKNOWN', 'Experiment Protocol Plot types defaultvalues for Plot type PLOT_TYPE_BED_UNKNOWN',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "No of beds per plot",
  			"order_number": 1,
  			"variable_abbrev": "NO_OF_BEDS_PER_PLOT",
  			"field_description": "Number of beds per plot"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "No of rows per bed",
  			"order_number": 2,
  			"variable_abbrev": "NO_OF_ROWS_PER_BED",
  			"field_description": "Number of rows per bed"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Bed width (m)",
  			"order_number": 3,
  			"variable_abbrev": "BED_WIDTH",
  			"field_description": "Bed width"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot width (m)",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_MAIZE_BED",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Plot length (m)",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area (sqm)",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_4",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length (m)",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');
