-- UPDATE TRIAL EXPERIMENT ORDER NUMBER
update master.item_relation 
set order_number = 1 
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_TRIAL_BASIC_INFO_ACT');

update master.item_relation 
set order_number = 2
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_TRIAL_ENTRY_LIST_ACT');

update master.item_relation 
set order_number = 3
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DESIGN_ACT');

update master.item_relation 
set order_number = 4
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_TRIAL_EXPT_GROUP_ACT');

update master.item_relation 
set order_number = 5
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_TRIAL_PROTOCOLS_ACT');

update master.item_relation 
set order_number = 6
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_TRIAL_PLACE_ACT');

update master.item_relation 
set order_number = 7
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_TRIAL_REVIEW_ACT');

-- check:
select * from master.item_relation r, master.item i
where root_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_TRIAL_DATA_PROCESS')
and child_id = i.id order by order_number;

-- UPDATE DISPLAY NAME TO PARENT LIST
update master.item set name = 'Specify Parent List', description = 'Specify Parent List', display_name = 'Parent List'
where abbrev in ('EXPT_NURSERY_PARENT_LIST_ENTRY_LIST_ACT','EXPT_NURSERY_CROSS_LIST_ENTRY_LIST_ACT');

update platform.module set name = 'Specify Parent List', description = 'Specify Parent List'
where abbrev in ('EXPT_NURSERY_PARENT_LIST_ENTRY_LIST_ACT_MOD','EXPT_NURSERY_CROSS_LIST_ENTRY_LIST_ACT_MOD');

--create design tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_DESIGN_ACT','Design',30,'Design','Design',1,'active','fa fa-files-o');
select * from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DESIGN_ACT';

--Additions in design tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT','Add blocks',20,'Add blocks','Add blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT','Assign entries',20,'Assign entries','Assign entries',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT','Manage blocks',20,'Manage blocks','Manage blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT','Overview',20,'Overview','Overview',1,'active','fa fa-random');

-- check:
select * from master.item where abbrev IN( 'EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT','EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT' ,'EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT','EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT');


--create protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT','Protocols',30,'Protocols','Protocols',1,'active','fa fa-files-o');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT','Planting Protocol',20,'Planting Protocol','Planting',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_NURSERY_PARENT_LIST_TRAITS_PROTOCOLS_ACT','Traits Protocol',20,'Traits Protocol','Traits',1,'active','fa fa-random');

-- insert the sequence of activities for the cross pre-planning the data process
insert into master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select 
  (select id from master.item where abbrev='EXPT_NURSERY_PARENT_LIST_DATA_PROCESS') as root_id,
  (select id from master.item where abbrev='EXPT_NURSERY_PARENT_LIST_DATA_PROCESS') as parent_id,
   id as child_id,
   case 
     when abbrev = 'EXPT_NURSERY_PARENT_LIST_DESIGN_ACT' then 3
     when abbrev = 'EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT' then 4
    end,
   0,
   1,
   1,
   'added by j.antonio ' || now()
from 
   master.item
where 
   abbrev in ('EXPT_NURSERY_PARENT_LIST_DESIGN_ACT','EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT');

-- update order number
update master.item_relation 
set order_number = 3
where root_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DESIGN_ACT');

update master.item_relation 
set order_number = 4
where root_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT');

update master.item_relation 
set order_number = 5
where root_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_PLACE_ACT');

update master.item_relation 
set order_number = 6
where root_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and child_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_REVIEW_ACT');

---check:
select * from master.item_relation r, master.item i
where root_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and parent_id = (select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS')
and child_id = i.id order by order_number;

-- --Additions for Design tab
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DESIGN_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT' then 1
		when abbrev = 'EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT'then 2
        when abbrev = 'EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT'then 3
        when abbrev = 'EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT'then 4
		else 5 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT','EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT','EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT','EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT');
;


--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT' then 1
		when abbrev = 'EXPT_NURSERY_PARENT_LIST_TRAITS_PROTOCOLS_ACT'then 2
		else 3 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT','EXPT_NURSERY_PARENT_LIST_TRAITS_PROTOCOLS_ACT');
;


--insert the activities in the platform module
insert into platform.module(abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
values 
  ('EXPT_NURSERY_PARENT_LIST_DESIGN_ACT_MOD','Specify Design','Specify Design','experimentCreation','create','specify-cross-design',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT_MOD','Add Blocks','Add Blocks','experimentCreation','create','add-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT_MOD','Assign Entries','Assign Entries','experimentCreation','create','assign-entries',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT_MOD','Manage Blocks','Manage Blocks','experimentCreation','create','manage-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT_MOD','Overview','Overview','experimentCreation','create','overview',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT_MOD','Protocols','Protocols','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT_MOD','Planting Protocols','Planting Protocols','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_NURSERY_PARENT_LIST_TRAITS_PROTOCOLS_ACT_MOD','Traits Protocols','Traits Protocols','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocols specified');


--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
	case
		when abbrev = 'EXPT_NURSERY_PARENT_LIST_DESIGN_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_DESIGN_ACT_MOD')
        when abbrev = 'EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT_MOD')
	end,
    1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev in ('EXPT_NURSERY_PARENT_LIST_DESIGN_ACT','EXPT_NURSERY_PARENT_LIST_PROTOCOLS_ACT');


--Additions for Design tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT_MOD')
  when abbrev = 'EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT_MOD')
  when abbrev = 'EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT_MOD')
  when abbrev = 'EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_NURSERY_PARENT_LIST_ADD_BLOCKS_ACT','EXPT_NURSERY_PARENT_LIST_ASSIGN_ENTRIES_ACT','EXPT_NURSERY_PARENT_LIST_MANAGE_BLOCKS_ACT','EXPT_NURSERY_PARENT_LIST_OVERVIEW_ACT');



--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT_MOD')
  when abbrev = 'EXPT_NURSERY_PARENT_LIST_TRAITS_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_NURSERY_PARENT_LIST_TRAITS_PROTOCOLS_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_CROSS_POST_PLANNING_TRAITS_PROTOCOLS_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_NURSERY_PARENT_LIST_PLANTING_PROTOCOLS_ACT','EXPT_NURSERY_PARENT_LIST_TRAITS_PROTOCOLS_ACT');
