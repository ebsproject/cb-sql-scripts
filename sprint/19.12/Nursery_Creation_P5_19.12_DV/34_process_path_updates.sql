update platform.config set abbrev='EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT_VAL' where abbrev='EXPT_TRIAL_IRRI_PROTOCOLS_ACT_VAL';

-- B4R-3880 Remove design tab in nursery-cross list
update master.item set is_void = true where abbrev = 'EXPT_NURSERY_CROSS_LIST_DESIGN_ACT';

-- B4R-3881 Replace design with planting arrangement for nursery type experiments
update master.item 
set name = 'Planting Arrangement', description = 'Planting Arrangement', display_name = 'Planting Arrangement' 
where abbrev ilike '%design_act%' and abbrev not ilike '%trial%' and abbrev <> 'EXPERIMENT_CREATION_DESIGN_ACT' and is_void = false;

-- check
select * from master.item where abbrev ilike '%design_act%' and abbrev not ilike '%trial%' and abbrev <> 'EXPERIMENT_CREATION_DESIGN_ACT' and is_void = false;

update platform.module 
set name = 'Planting Arrangement', description = 'Planting Arrangement'
where abbrev ilike '%design_act%' and abbrev not ilike '%trial%' and abbrev <> 'EXPERIMENT_CREATION_DESIGN_ACT' and is_void = false;

-- check
select * from platform.module where abbrev ilike '%design_act%' and abbrev not ilike '%trial%' and abbrev <> 'EXPERIMENT_CREATION_DESIGN_ACT' and is_void = false;