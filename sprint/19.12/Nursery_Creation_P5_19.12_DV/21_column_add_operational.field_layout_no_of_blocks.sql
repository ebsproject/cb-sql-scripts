/*
* This file is part of Breeding4Results.
*
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-13 08:44:32
*/

--/*
begin;

savepoint sp1;
--*/

/**
 * B4R-3592 Add no_of_blocks column to operational.field_layout and operational.field_layout_study tables
 */

set role b4radmin;

-- drop no_of_blocks column from operational.field_layout table
/*
ALTER TABLE
    operational.field_layout
DROP COLUMN IF EXISTS
    no_of_blocks
;
*/


-- add no_of_blocks column to operational.field_layout table
ALTER TABLE
    operational.field_layout
ADD COLUMN IF NOT EXISTS
    no_of_blocks
    integer
	not null
	default 0
    -- other_properties: NOT NULL DEFAULT default_value
;


COMMENT ON COLUMN
	operational.field_layout.no_of_blocks
IS
	'Total no. of experiment blocks'
;


---


-- drop no_of_blocks column from operational.field_layout_study table
/*
ALTER TABLE
    operational.field_layout_study
DROP COLUMN IF EXISTS
    no_of_blocks
;
*/


-- add no_of_blocks column to operational.field_layout_study table
ALTER TABLE
    operational.field_layout_study
ADD COLUMN IF NOT EXISTS
    no_of_blocks
    integer
	not null
	default 0
    -- other_properties: NOT NULL DEFAULT default_value
;


COMMENT ON COLUMN
	operational.field_layout_study.no_of_blocks
IS
	'Total no. of locrep blocks'
;


--/*
rollback to sp1;

end;
--*/
