/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Gene Christhopher Romuga <g.romuga@irri.org>
 * @date 2019-11-12 8:59:03
 */


--/*
BEGIN;

SAVEPOINT sp1;
--*/

/**
 * B4R-3590 Nursery Creation: Add abbrev column to the master.scale_value table
 *
 * master.scale_value
 * - abbrev character varying
 */

set role b4radmin;

-- add column abbrev to master.scale_value table
ALTER TABLE
    master.scale_value
ADD COLUMN IF NOT EXISTS
    abbrev
    character varying (128)
    --NOT NULL
    --DEFAULT 1
;

COMMENT ON COLUMN
    master.scale_value.abbrev
IS
    'Short name identifier of the scale value'
;

--/*
ROLLBACK TO sp1;

END;
--*/
