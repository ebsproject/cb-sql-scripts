set role b4radmin;

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_SEED_INCREASE_BASIC_INFO_ACT_VAL', 'Seed Increase Experiment Basic Information',  '{
  	"Values": [{
  			"default": false,
  			"disabled": true,
  			"required": "required",
  			"field_label": "Experiment Template",
  			"order_number": 1,
  			"variable_abbrev": "EXPERIMENT_TEMPLATE",
  			"field_description": "Experiment Template"
  		},{
  			"default": false,
  			"variable_abbrev": "EXPERIMENT_TYPE",
  			"order_number": 2,
  			"field_label": "Experiment Type",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Experiment Type"
  		},{
  			"field_label": "Crop",
  			"order_number": 3,
  			"variable_abbrev": "CROP",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Program Crop"
  		},
  		{
  			"field_label": "Program",
  			"order_number": 4,
  			"variable_abbrev": "PROGRAM",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Program"
  		},
  		{
  			"field_label": "Project",
  			"order_number": 5,
  			"variable_abbrev": "PROJECT",
  			"allow_new_val": true,
  			"disabled": false,
  			"field_description": "Project where the experiment is under"
  		},
  		{
  			"field_label": "Experiment Code",
  			"order_number": 6,
  			"variable_abbrev": "EXPERIMENT_CODE",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Auto-generated code for the experiment"
  		},
  		{
  			"field_label": "Experiment Name",
  			"order_number": 7,
  			"variable_abbrev": "EXPERIMENT_NAME",
  			"required": "required",
  			"disabled": false,
  			"field_description": "User inputted name for the experiment"
  		},
  		{
  			"default": "BRE",
  			"variable_abbrev": "PHASE",
  			"order_number": 8,
  			"field_label": "Experiment sub-type",
  			"required": "required",
  			"disabled": false,
  			"allowed_values": [
				"BRE",
				"SEM"
  			],
  			"field_description": "Experiment Sub-type/Phase of the experiment"
  		},
  		{
  			"variable_abbrev": "EXPERIMENT_SUB_SUBTYPE",
  			"order_number": 9,
  			"field_label": "Experiment Sub Sub-type",
  			"disabled": false,
  			"field_description": "Experiment Sub Sub-type/Objective of the experiment"
  		},
  		{
  			"field_label": "Evaluation Year",
  			"order_number": 10,
  			"variable_abbrev": "YEAR",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Evaluation Year"
  		},
  		{
  			"field_label": "Evaluation Season",
  			"order_number": 11,
  			"variable_abbrev": "SEASON",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Evaluation Season"
  		},
  		{
  			"field_label": "Experiment Steward",
  			"order_number": 12,
  			"variable_abbrev": "EXPERIMENT_STEWARD",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Steward of the experiment"
  		},
  		{
  			"field_label": "Remarks",
  			"disabled": false,
  			"field_description": "Experiment Comment",
  			"order_number": 13,
  			"variable_abbrev": "REMARKS"
  		}
  	],
  	"Name": "Required experiment level metadata variables for seed increase data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio'),


('EXPT_SEED_INCREASE_ENTRY_LIST_ACT_VAL','Seed Increase Experiment Entry List',
 '{
   "Name": "Required and default entry level metadata variables for seed increase data process",
   "Values": [
     {
            "fixed": true,
            "disabled": true,
            "field_label": "Parent Role",
            "order_number": 1,
            "variable_abbrev": "PARENT_TYPE",
            "field_description": "Parent Role",
            "default": "female-and-male"
     },
     {
       "disabled": false,
       "field_label": "Remarks",
       "field_description": "Entry Remarks",
       "order_number": 2,
       "variable_abbrev": "REMARKS"
     }
   ]
 }', 1, 'experiment_creation', 1,'added by j.antonio'),


('EXPT_SEED_INCREASE_PLACE_ACT_VAL', 'Seed Increase Experiment Place',
 '{
   "Name": "Required and default place metadata variables for seed increase data process",
   "Values": [
     {
       "disabled": false,
       "field_label": "Remarks",
       "field_description": "Place Remarks",
       "order_number": 1,
       "variable_abbrev": "REMARKS"
     }
   ]
 }
', 1, 'experiment_creation', 1,'added by j.antonio');
