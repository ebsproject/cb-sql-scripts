
---For RICE crop
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('RICE_PLANTING_TYPE', 'Experiment Crop-Planting type relation-RICE',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Planting Type",
  			"order_number": 1,
  			"variable_abbrev": "PLANTING_TYPE",
  			"field_description": "Planting Type",
            "planting_type_abbrevs" : [
				"PLANTING_TYPE_CONTAINER",
                "PLANTING_TYPE_FLAT",
				"PLANTING_TYPE_BED"
            ]
  		}
  	],
  	"Name": "Required experiment level planting protocol variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


--For WHEAT crop
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('WHEAT_PLANTING_TYPE', 'Experiment Crop-Planting type relation-WHEAT',  '{
  	"Values": [
      {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Planting Type",
  			"order_number": 1,
  			"variable_abbrev": "PLANTING_TYPE",
  			"field_description": "Planting Type",
            "planting_type_abbrevs" : [
				"PLANTING_TYPE_BED",
                "PLANTING_TYPE_FLAT"
            ]
  		}
  	],
  	"Name": "Required experiment level planting protocol variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


--For WHEAT crop
INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('MAIZE_PLANTING_TYPE', 'Experiment Crop-Planting type relation-MAIZE',  '{
  	"Values": [
      {
  			"default": "Flat",
  			"disabled": false,
            "required": "required",
  			"field_label": "Planting Type",
  			"order_number": 1,
  			"variable_abbrev": "PLANTING_TYPE",
  			"field_description": "Planting Type",
            "planting_type_abbrevs" : [
				"PLANTING_TYPE_BED",
                "PLANTING_TYPE_FLAT"
            ]
  		}
  	],
  	"Name": "Required experiment level planting protocol variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');
