/*
* This file is part of Breeding4Results.
*
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
*/


/**
* @author Argem Gerald R. Flores <a.flores@irri.org>
* @date 2019-11-13 09:54:24
*/


--/*
BEGIN;

SAVEPOINT sp1;
--*/


set role b4radmin;


DO

$BODY$

DECLARE
    var_updated_count integer = 0;
BEGIN
	/**
	 * B4R-3777 Populate code column in operational.experiment table
	 */
    
    with t_variable as (
		select
			mv.*
		from
			master.variable mv
		where
			mv.abbrev = 'EXPERIMENT_CODE'
			and mv.is_void = false
	), t_experiment as (
		select
			oexp.id experiment_id,
			oexp.experiment_name,
			oexp.code old_experiment_code,
			(
				select
					oexpd.value
				from
					operational.experiment_data oexpd,
					t_variable tv
				where
					oexpd.variable_id = tv.id
					and oexpd.experiment_id = oexp.id
					and oexpd.is_void = false
			) new_experiment_code
		from
			operational.experiment oexp
		order by
			oexp.id asc
	), t_update_experiment as (
		update
			operational.experiment oexp
		set
			code = t.new_experiment_code
		from
			t_experiment t
		where
			oexp.id = t.experiment_id
			and oexp.code is null
			and t.new_experiment_code is not null
		returning
			*
	)
	select
		count(1)
	into
		var_updated_count
	from
		t_update_experiment t
	;
	
	raise notice '+ Experiment (code) updated: % records', var_updated_count;
END;

$BODY$;


/*
DEV
NOTICE:  + Experiment (code) updated: 165 records
DO

Query returned successfully in 234 msec.
*/


--/*
ROLLBACK TO sp1;

END;
--*/
