insert into platform.application (abbrev, label, action_label, icon, creator_id, is_public)
values
('HARVEST_MANAGER', 'Harvest manager', 'Harvest manager', 'search', 1, true);

insert into platform.application_action(application_id,module,controller,action,params,creator_id)
values
((select id from platform.application where abbrev = 'HARVEST_MANAGER'),'seedInventory','harvest-manager',null,null,1);