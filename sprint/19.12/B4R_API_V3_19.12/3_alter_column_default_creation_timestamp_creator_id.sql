/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Argem Gerald Flores <a.flores@irri.org>
 * @date 2019-11-27 15:10:23
 */


--/*
BEGIN;

SAVEPOINT sp1;
--*/

/**
 * B4R-3824 Alter column default values of creation_timestamp and creator_id
 */

set role b4radmin;


-- set default value of creation_timestamp = now()
alter table
	api.messages
alter column
	creation_timestamp
set default
	now()
;

-- set default value of creator_id = 1
alter table
	api.messages
alter column
	creator_id
set default
	1
;


--/*
ROLLBACK TO sp1;

END;
--*/