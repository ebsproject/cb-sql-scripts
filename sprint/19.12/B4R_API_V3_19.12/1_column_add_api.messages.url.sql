/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Argem Gerald Flores <a.flores@irri.org>
 * @date 2019-11-21 09:53:54
 */


--/*
BEGIN;

SAVEPOINT sp1;
--*/

/**
 * B4R-3824 Add url column to api.messages table
 */

set role b4radmin;

alter table api.messages add column url varchar;

comment on column api.messages.url is 'Reference info of the API message';

--/*
ROLLBACK TO sp1;

END;
--*/