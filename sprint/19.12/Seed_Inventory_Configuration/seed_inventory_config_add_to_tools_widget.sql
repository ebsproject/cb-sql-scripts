insert into platform.application (abbrev, label, action_label, icon, creator_id, is_public)
values
('SEED_INVENTORY_CONFIG', 'Seed inventory config', 'Seed inventory config', 'settings', 1, true);

insert into platform.application_action(application_id,module,controller,action,params,creator_id)
values
((select id from platform.application where abbrev = 'SEED_INVENTORY_CONFIG'),'seedInventory','configuration',null,null,1);