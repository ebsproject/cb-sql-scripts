--Objective: Set an experiment year to 2018 or 2017 (to have test data other 2019)

select id, * from operational.experiment
where is_void = false
--and year > 2019
and program_id = 126
--1. From the results above, take note of the experiment.id targeted to be updated. Then proceed with updating the year.
and id in (117, 118, 119)

update operational.experiment
set year = 2019 --2018 --or 2017
where is_void = false
and year > 2019
and program_id = 126
and id = 119 --118 --117
--2. Link the experiment.id to some studies by populating study.experiment_id

select * from operational.study
where is_void = false
and year = 2019 --2018 --2017
and program_id = 126
and study_status like '%committed%'
and experiment_id is null
--From the results above, take note of the study.id and another key column, for instance study.place_id, targeted to be updated. 
--Then proceed with updating the study.experiment_id.
and creator_id = 181

update operational.study
set experiment_id = 119
select * from operational.study
where is_void = false
and year = 2019 --2018 --2017
and program_id = 126
and study_status like '%committed%'
and experiment_id is null
--and place_id = 10001
--and id in (6134, 4779)
--and id in (6048, 4761)
--and id in (7856)
--3. Assuming there are already seedlot created for the studies above, update seed_storage.source_experiment_id.

update operational.seed_storage
set source_experiment_id = 118 --117
--select * from operational.seed_storage
where is_void = false 
and source_experiment_id is null
and year = 2018 --2017 --in (2017, 2018, 2019, 2020)
and program_id = 126
and source_study_id in (
    select id from operational.study
    where is_void = false
    and year in (2017, 2018, 2019, 2020)
    and program_id = 126
    and study_status like '%committed%'
    and experiment_id is not null
)