/**
 * B4R-3642 Build SQL queries for experiment-locrep-blocks resource
 */


-- all experiment locrep blocks
select
	olb.id "locRepBlockDbId",
	olb.locrep_id "locRepDbId",
	olb.parent_id "parentDbId",
	olb.order_number "orderNumber",
	olb.code "code",
	olb.name "name",
	olb.block_type "blockType",
	olb.no_of_blocks "noOfBlocks",
	olb.no_of_rows_in_block "noOfRowsInBlock",
	olb.no_of_cols_in_block "noOfColsInBlock",
	olb.no_of_reps_in_block "noOfRepsInBlock",
	olb.plot_numbering_order "plotNumberingOrder",
	olb.starting_corner "startingCorner"
from
	operational.locrep_block olb
where
	olb.is_void = false
order by
	olb.id asc
;

-- single locrep experiment block
select
	olb.id "locRepBlockDbId",
	olb.locrep_id "locRepDbId",
	olb.parent_id "parentDbId",
	olb.order_number "orderNumber",
	olb.code "code",
	olb.name "name",
	olb.block_type "blockType",
	olb.no_of_blocks "noOfBlocks",
	olb.no_of_rows_in_block "noOfRowsInBlock",
	olb.no_of_cols_in_block "noOfColsInBlock",
	olb.no_of_reps_in_block "noOfRepsInBlock",
	olb.plot_numbering_order "plotNumberingOrder",
	olb.starting_corner "startingCorner"
from
	operational.locrep_block olb
where
	olb.is_void = false
	and olb.id = 82
;