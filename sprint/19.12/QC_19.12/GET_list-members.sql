-- B4R-3644 Build SQL queries for list-members resource
-- https://riceinfo.atlassian.net/browse/B4R-3644

/*
BEGIN;

SAVEPOINT sp1;
--*/


DO

$BODY$

DECLARE
	var_list_id integer = 787;
    var_list record;
	var_row record;
	var_list_json json;
	var_list_member_json json;
BEGIN
    /**
     * B4R-3644 Build SQL queries for list-members resource
     *
     * https://riceinfo.atlassian.net/browse/B4R-3644
     */
    
    select
		pl.id "traitProtocolDbId",
		pl.abbrev "abbrev",
		pl.name "name",
		pl.display_name "displayName",
		pl.type "type",
		pl.entity_id "entityDbId",
		de.name "entity",
		de.abbrev "entityAbbrev",
		pl.description "description",
		pl.remarks "remarks",
		pl.creation_timestamp "creationTimestamp",
		pl.creator_id "creatorDbId",
		muc.display_name "creator",
		pl.modification_timestamp "modificationTimestamp",
		pl.modifier_id "modifierDbId",
		mum.display_name "modifier"
	into
		var_list
	from
		platform.list pl
		join dictionary.entity de
			on pl.entity_id = de.id
		join master.user muc
			on pl.creator_id = muc.id
		left join master.user mum
			on pl.modifier_id = mum.id
	where
		pl.is_void = false
		-- and pl.type = 'trait_protocol'
		-- and pl.id = var_list_id
	order by
		pl.id desc
	limit
		1
	;
	
	if (var_list.abbrev is not null) then
		if (var_list.type = 'seed') then
			for var_row in (
				select
					plm.id list_member_id,
					plm.data_id,
					plm.order_number,
					plm.creation_timestamp
				from
					platform.list_member plm
				where
					plm.list_id = var_list."traitProtocolDbId"
					and plm.is_void = false
			) loop
				with t as (
					select
						var_row.order_number "orderNumber",
						var_row.creation_timestamp "creationTimestamp",
						oss.id "seedStorageDbId",
						oss.label "label",
						oss.product_id "productDbId",
						mp.designation "designation",
						oss.gid "gid",
						oss.volume "volume",
						os.id "studyDbId",
						os.study "study",
						os.name "studyName",
						oss.container_id "containerDbId",
						swc.label "container_label",
						swc.type "container_type",
						oss.year "year"
					from
						operational.seed_storage oss
						join master.product mp
							on oss.product_id = mp.id
						left join operational.study os
							on oss.source_study_id = os.id
						left join seed_warehouse.container swc
							on oss.container_id = swc.id
					where
						oss.is_void = false
						and oss.id = var_row.data_id
				)
				select
					row_to_json(t.*)
				into
					var_list_member_json
				from
					t
				;

				raise notice '%', var_list_member_json;
			end loop;
		end if;
	else
		raise notice '! List not found: %', var_list_id;
	end if;
	
	var_list_json = row_to_json(var_list);
	
	raise notice '%', var_list_json;
END;

$BODY$;


/*
ROLLBACK TO sp1;

END;
--*/


/*select
	pl.id "traitProtocolDbId",
	pl.abbrev "abbrev",
	pl.name "name",
	pl.display_name "displayName",
	pl.type "type",
	pl.entity_id "entityDbId",
	de.name "entity",
	de.abbrev "entityAbbrev",
	pl.description "description",
	pl.remarks "remarks",
	pl.creation_timestamp "creationTimestamp",
	pl.creator_id "creatorDbId",
	muc.display_name "creator",
	pl.modification_timestamp "modificationTimestamp",
	pl.modifier_id "modifierDbId",
	mum.display_name "modifier",
	(select row_to_json(t) from (
		select
			plm.id "listMemberDbId",
			plm.order_number "orderNumber",
			plm.creation_timestamp "creationTimestamp",
			oss.id "seedStorageDbId",
			oss.label "label",
			oss.product_id "productDbId",
			mp.designation "designation",
			oss.gid "gid",
			oss.volume "volume",
			os.id "studyDbId",
			os.name "studyName",
			swc.id "containerDbId",
			swc.label "container_label",
			swc.type "container_type",
			oss.year "year"
		from
			platform.list_member plm
			join operational.seed_storage oss
				on plm.data_id = oss.id
			join master.product mp
				on oss.product_id = mp.id
			left join operational.study os
				on oss.source_study_id = os.id
			left join seed_warehouse.container swc
				on oss.container_id = swc.id
	) t ) "members"
from
	platform.list pl
	join dictionary.entity de
		on pl.entity_id = de.id
	join master.user muc
		on pl.creator_id = muc.id
	left join master.user mum
		on pl.modifier_id = mum.id
where
	pl.is_void = false
	-- and pl.type = 'trait_protocol'
	and pl.id = 787
;*/