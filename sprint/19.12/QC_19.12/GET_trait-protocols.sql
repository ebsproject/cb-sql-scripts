-- GET call for /v3/trait-protocols
-- https://riceinfo.atlassian.net/browse/B4R-3741

select
	pl.id "traitProtocolDbId",
	pl.abbrev "abbrev",
	pl.name "name",
	pl.display_name "displayName",
	pl.type "type",
	pl.entity_id "entityDbId",
	de.name "entity",
	de.abbrev "entityAbbrev",
	pl.description "description",
	pl.remarks "remarks",
	pl.creation_timestamp "creationTimestamp",
	pl.creator_id "creatorDbId",
	muc.display_name "creator",
	pl.modification_timestamp "modificationTimestamp",
	pl.modifier_id "modifierDbId",
	mum.display_name "modifier"
from
	platform.list pl
	join dictionary.entity de
		on pl.entity_id = de.id
	join master.user muc
		on pl.creator_id = muc.id
	left join master.user mum
		on pl.modifier_id = mum.id
where
	pl.type = 'trait_protocol'
	and pl.is_void = false
	and pl.id = 962
;

	