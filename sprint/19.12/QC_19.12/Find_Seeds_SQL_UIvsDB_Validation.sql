/*
To verify filter dependencies, each select statement will give the list of distinct values 
that should be displaying in a specific search filter
Example:

--Uncommenting
SELECT DISTINCT oe.year as SOURCE_EXP_YEAR
FROM
operational.seed_storage ss
left join master.product as pr on ss.product_id = pr.id
left join operational.study as s on ss.source_study_id = s.id
left join operational.study as ssn on ss.source_study_id = ssn.id
left join operational.entry as e on ss.source_entry_id = e.id
left join operational.plot as p on ss.source_plot_id = p.id
left join operational.experiment as oe on ss.source_experiment_id = oe.id
left join seed_warehouse.container as swc on ss.container_id = swc.id
left join master.facility as mf on ss.facility_id = mf.id
WHERE
ss.is_void=false
and ss.program_id = 126
--will give the distinct Source Experiment Year dropdown values for program 126 (MET)

*/

--SELECT DISTINCT oe.year as SOURCE_EXP_YEAR
--SELECT DISTINCT oe.experiment_name as EXPERIMENT_NAME
--SELECT DISTINCT oe.experiment_type as EXPERIMENT_TYPE
--SELECT DISTINCT oe.phase_id as SOURCE_EXP_SUBTYPE
--SELECT DISTINCT oe.season_id as SOURCE_EXP_SEASON
--SELECT DISTINCT s.study as SOURCE_STUDY
--SELECT DISTINCT ssn.name as SOURCE_STUDY_NAME
--SELECT DISTINCT s.study as SOURCE_STUDY_LOCREP

--SELECT DISTINCT ss.creator_id, u.username
--SELECT DISTINCT mf.name as FACILITY_NAME
--SELECT DISTINCT ss.facility_id, mf.name as FACILITY_NAME
--SELECT DISTINCT mf.facility_type as FACILITY_TYPE, ss.program_id
--SELECT DISTINCT ss.harvest_date as HARVEST_DATE, oe.year
--SELECT DISTINCT ss.modifier_id
--SELECT DISTINCT e.entno as SRC_ENTRY_NO
--SELECT DISTINCT e.entcode as SRC_ENTRY_CODE
--SELECT DISTINCT ss.year as SOURCE_HARV_YEAR
--SELECT DISTINCT p.plotno as SRC_PLOT_NO
--SELECT DISTINCT p.rep as SRC_PLOT_REP
--SELECT DISTINCT ss.volume as VOLUME 
--SELECT DISTINCT ss.unit as UNIT

--To get a list of unique GID, Seedlot Code, and Designations
--SELECT DISTINCT ss.gid as GID
--SELECT DISTINCT ss.label as LABEL
--SELECT DISTINCT pr.designation as DESIGNATION


/*
To verify the actual records count satisfying a set of filter values, uncomment SELECT count(*) statement and add the conditions corresponding to the set filter values
*/
--SELECT count(*)
FROM
operational.seed_storage ss
left join master.product as pr on ss.product_id = pr.id
left join operational.study as s on ss.source_study_id = s.id
left join operational.study as ssn on ss.source_study_id = ssn.id
left join operational.entry as e on ss.source_entry_id = e.id
left join operational.plot as p on ss.source_plot_id = p.id
left join operational.experiment as oe on ss.source_experiment_id = oe.id
left join seed_warehouse.container as swc on ss.container_id = swc.id
left join master.facility as mf on ss.facility_id = mf.id
WHERE
ss.is_void=false
and ss.program_id = 126
--the following are the corresponding condition for a set filter in the Find Seeds basic search parameters
--and oe.year in (2017, 2018, 2019) --= 2019 --= 2019 --
--and oe.experiment_name in ('MET2 MOD1', 'MET-Philippines LYS')
--and oe.experiment_type = 'Trial'
--and oe.phase_id in (128, 129) --= 129 --128
--and oe.season_id in (11, 12)
--and s.study in ('AYT_EARLY 2017WS', 'AYT_EARLY 2019DS', 'AYT_LATE 2019DS')

--and ss.creator_id = 19
--and ss.facility_id = 4327
--and ss.modifier_id = 6
--and e.entno in --input range condition
--and e.entcode in --list of entry codes --('171CN82') --('SI-99')
--and ss.year in --list of distinct year values
--and p.plotno in --input range condition
--and p.rep in --input range condition
--and ss.volume in --input range condition
--and mf.name like '%PGF%'
--and mf.facility_type in ('Seed Warehouse', 'Building')

/*
To get all the query results column info run the following statement and add the same conditions from the above WHERE conditions
To get the query total records that will match the set filters, uncomment SELECT count(8)
*/
SELECT * 
--SELECT count(*) 
FROM
ss.product_id as PRODUCT_ID,
ss.seed_manager as SEED_MANAGER,
ss.label as LABEL, 
ss.gid as GID,
ss.volume as VOLUME, 
ss.unit as UNIT,
pr.designation as DESIGNATION,
ss.year as SOURCE_HARV_YEAR,
oe.year as SOURCE_EXP_YEAR,
oe.experiment_name as EXPERIMENT_NAME,
oe.experiment_type as EXPERIMENT_TYPE,
oe.phase_id as SOURCE_EXP_SUBTYPE,
oe.season_id as SOURCE_EXP_SEASON,
s.study as SOURCE_STUDY,
ssn.name as SOURCE_STUDY_NAME,
ssn.study as SOURCE_STUDY_LOCREP,
e.entcode as SOURCE_ENTRY,
e.entno as SRC_ENTRY,
p.code as PLOT_CODE,
p.plotno as SRC_PLOT,
p.rep as REP,
s.season_id as SRC_SEASON,
ss.harvest_date as HARVEST_DATE,
ss.product_id as PRODUCT_ID,
ss.source_study_id as SOURCE_STUDY_ID,
ss.container_id as CONTAINER_ID,
swc.type as CONTAINER_TYPE,
swc.label as CONTAINER_LABEL,
ss.facility_id as FACILITY_ID,
mf.name as FACILITY_NAME,
mf.facility_type as FACILITY_TYPE
WHERE
ss.is_void=false
and ss.program_id = 126
--and ss.gid as in () --your GID input list
--and ss.label in () --your Seedlot Code input list
--and pr.designation in () --your Designation input list

/* IRSEA sample input list
label	gid	designation	other names
126	301205270	IR16M1618	IR 115976-B-223-2-1-B
119	301205271	IR18M1002	IR 126392-B-13-3-1-B
112	301205273	IR 99674-9-2-2	IR14M123, IR14M123
111	301205274	IR16M2110	IR 115976-B-211-1-1-B
122	301205284	IR17M1430	IR 120687-B-60-1-2-B
134	301205285	IR17M1467	IR 120690-B-32-1-2-B
116	301205286	IR15M2601	IR 105914-B-46-2-1-B
113	301205288	IR17M1656	IR 124044-B-86-3-1-B
162	301205289	IR17M1308	IR 120626-B-55-1-1-B
169	301205297	IR18M1003	IR 126392-B-92-2-1-B
66	301205269	IR17M1650	IR 124047-B-91-1-1-B
83	301205272	IR17M1644	IR 124029-B-62-2-2-B
52	301205275	IR17M1172	IR 120558-B-24-1-1-B
17	301205276	IR17M1293	IR 120623-B-134-3-2-B
6	301205277	IR17M1641	IR 124015-B-132-1-1-B
1	301205278	IR17M1647	IR 124041-B-45-1-2-B
19	301205279	IR17M1643	IR 124029-B-13-1-1-B
98	301205280	IR17M1648	IR 124045-B-153-1-1-B
4	301205281	IR16M1191	IR 109441-B-20-1-3-B
22	301205282	IR17M1560	IR 120702-B-14-1-1-B
26	301205283	IR16M1949	IR 115978-B-269-2-1-B
75	301205287	IR13V163	IR 95044:8-B-5-22-19-GBS, IR 9
68	301205296	IR17M1651	IR 124048-B-3-2-1-B
14	301205298	IR16M2050	IR 115932-B-163-3-2-B
57	301205299	IR17M1213	IR 120612-B-103-2-1-B
*/

