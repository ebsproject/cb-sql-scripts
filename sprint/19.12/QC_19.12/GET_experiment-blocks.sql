/**
 * B4R-3641 Build SQL queries for experiments-blocks resource
 */

-- all experiment blocks default ordering by id
select
	oeb.id "experimentBlockDbId",
	oeb.experiment_id "experimentDbId",
	oeb.parent_id "parentDbId",
	oeb.order_number "orderNumber",
	oeb.code "code",
	oeb.name "name",
	oeb.block_type "blockType",
	oeb.no_of_blocks "noOfBlocks",
	oeb.no_of_rows_in_block "noOfRowsInBlock",
	oeb.no_of_cols_in_block "noOfColsInBlock",
	oeb.no_of_reps_in_block "noOfRepsInBlock",
	oeb.plot_numbering_order "plotNumberingOrder",
	oeb.starting_corner "startingCorner",
	oeb.entry_list_ids "entryListIds",
	oeb.remarks "remarks"
from
	operational.experiment_block oeb
where
	oeb.is_void = false
order by
	oeb.id asc
;

-- single experiment block
select
	oeb.id "experimentBlockDbId",
	oeb.experiment_id "experimentDbId",
	oeb.parent_id "parentDbId",
	oeb.order_number "orderNumber",
	oeb.code "code",
	oeb.name "name",
	oeb.block_type "blockType",
	oeb.no_of_blocks "noOfBlocks",
	oeb.no_of_rows_in_block "noOfRowsInBlock",
	oeb.no_of_cols_in_block "noOfColsInBlock",
	oeb.no_of_reps_in_block "noOfRepsInBlock",
	oeb.plot_numbering_order "plotNumberingOrder",
	oeb.starting_corner "startingCorner",
	oeb.entry_list_ids "entryListIds",
	oeb.remarks "remarks"
from
	operational.experiment_block oeb
where
	oeb.is_void = false
	and oeb.id = 291
;