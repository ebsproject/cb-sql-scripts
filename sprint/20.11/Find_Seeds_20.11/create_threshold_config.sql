INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage)
    VALUES ('FIND_SEEDS_BG_PROCESSING_THRESHOLD', 'Background processing threshold values for Find Seeds tool features', 
    '{
        "deleteWorkingList": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "addWorkingList": {
            "size": "500",
            "description": "Update entries of an Experiment"
        }
  }'::json, 1, 'find_seeds_bg_process');