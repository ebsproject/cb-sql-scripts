Delete from platform.config where abbrev='HM_PACKAGE_NAME_PATTERN_WHEAT_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_PACKAGE_NAME_PATTERN_WHEAT_DEFAULT',
        'Harvest Manager WHEAT Package Name Pattern-Default',
$$			
		
		{
     "pattern": [
      {
        "type": "seedField",
        "order_number": 0,
        "label":"Seed Name",
        "seedInfoField":"seedName"
      },
      {
        "type": "delimeter",
        "order_number": 1,
        "value": "-"     
      },
      {
        "type": "free-text",
        "order_number": 2,
        "value":"P"
        
      },
      {
        "type": "plotField",
        "order_number": 3,
        "label":"Plot No",
        "plotInfoField": "plotNumber"
      },
		  {
        "type": "delimeter",
        "order_number": 4,
        "value": "-"     
      },
		  {
        "type": "free-text",
        "order_number": 5,
        "value":"PK001"
        
      }
    ]
}
$$,
        1,
        'harvest_manager',
        1,
        'B4R 6725 update config - a.caneda 2020-09-07')


