INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage)
    VALUES ('HARVEST_MANAGER_BG_PROCESSING_THRESHOLD', 'Background processing threshold values for HARVEST MANAGER tool features', 
    '{
        "deletePlotData": {
            "size": "2000",
            "description": "Delete plot data of plots"
        },
        "deleteSeedlots": {
            "size": "500",
            "description": "Delete seedlots of plots"
        }
  }'::json, 1, 'harvest_manager_bg_process');
