Delete from platform.config where abbrev='HM_SEED_NAME_PATTERN_MAIZE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_SEED_NAME_PATTERN_MAIZE_DEFAULT',
        'Harvest Manager Maize Seed Name Pattern-Default',
$$			
		
{
	"bulk": [{
			"type": "field",
			"label": "Source Nursery Site Code",
			"order_number": 0,
			"plotInfoField": "nurserySiteCode"
		},
		{
			"type": "field",
			"label": "Experiment Year - YY",
			"order_number": 1,
			"plotInfoField": "experimentYearYY"
		},
		{
			"type": "field",
			"label": "Source Season Code",
			"order_number": 2,
			"plotInfoField": "seasonCode"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number": 3
		},
		{
			"type": "field",
			"label": "Experiment Name",
			"order_number": 4,
			"plotInfoField": "experimentName"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number": 5
		},
		{
			"type": "field",
			"label": "Occurrence Code",
			"order_number": 6,
			"plotInfoField": "occurrenceCode"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number":7
		},
		{
			"type": "field",
			"label": "Source Entry Number",
			"order_number": 8,
			"plotInfoField": "entryNumber"
		}
	],
	"individual_ear": [{
			"type": "field",
			"label": "Source Nursery Site Code",
			"order_number": 0,
			"plotInfoField": "nurserySiteCode"
		},
		{
			"type": "field",
			"label": "Experiment Year - YY",
			"order_number": 1,
			"plotInfoField": "experimentYearYY"
		},
		{
			"type": "field",
			"label": "Source Season Code",
			"order_number": 2,
			"plotInfoField": "seasonCode"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number": 3
		},
		{
			"type": "field",
			"label": "Experiment Name",
			"order_number": 4,
			"plotInfoField": "experimentName"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number": 5
		},
		{
			"type": "field",
			"label": "Occurrence Code",
			"order_number": 6,
			"plotInfoField": "occurrenceCode"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number":7
		},
		{
			"type": "field",
			"label": "Source Entry Number",
			"order_number": 8,
			"plotInfoField": "entryNumber"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number": 9
		},
		{
			"type": "counter",
			"order_number": 10
		}
	],
	"default": [{
			"type": "field",
			"label": "Source Nursery Site Code",
			"order_number": 0,
			"plotInfoField": "nurserySiteCode"
		},
		{
			"type": "field",
			"label": "Experiment Year - YY",
			"order_number": 1,
			"plotInfoField": "experimentYearYY"
		},
		{
			"type": "field",
			"label": "Source Season Code",
			"order_number": 2,
			"plotInfoField": "seasonCode"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number": 3
		},
		{
			"type": "field",
			"label": "Experiment Name",
			"order_number": 4,
			"plotInfoField": "experimentName"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number": 5
		},
		{
			"type": "field",
			"label": "Occurrence Code",
			"order_number": 6,
			"plotInfoField": "occurrenceCode"
		},
		{
			"type": "delimeter",
			"value": "-",
			"order_number":7
		},
		{
			"type": "field",
			"label": "Source Entry Number",
			"order_number": 8,
			"plotInfoField": "entryNumber"
		}
	]
}
$$,
        1,
        'harvest_manager',
        1,
        'B4R 6725 update config - a.caneda 2020-09-07')


