CREATE OR REPLACE FUNCTION data_terminal.create_transaction_dataset(var_transaction_id integer, var_user_id integer, var_dataset json, var_variables text[])
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$

DECLARE
    tableName text; 
    columnString text; 
       items RECORD; 
    locationId integer;	
    insertedCount integer;	
    updatedCount integer; 
begin
    updatedCount:=0;
    insertedCount:=0;
    tableName := 'dataset_'||var_transaction_id||'_'||var_user_id;
    columnString := 'row_id, '|| array_to_string(var_variables, ',');

    SELECT location_id into locationId FROM data_terminal."transaction" t where id=var_transaction_id;
    
    
    execute '
    CREATE TEMP TABLE dataset_'||var_transaction_id||'_'||var_user_id||'(
        id serial NOT NULL,
        row_id text, 
        '||(SELECT string_agg(a.concat,',') FROM (SELECT concat(unnest(var_variables),' text'))a)||'
    ) ON COMMIT DROP
    ';
    
    execute 'INSERT into '||tableName ||'('|| columnString || ') 
        SELECT 
            '|| columnString ||' 
        FROM json_populate_recordset(null::'||tableName||','''||var_dataset||''')';
    
    FOR items IN EXECUTE format('SELECT * FROM %I order by id asc', tableName)
    LOOP
        if  (items.row_id is null or items.row_id !~ E'^\\d+$') then 
            return 'Record '|| items.id || ' with cannot have an empty ROW_ID and must be an integer';

        elsif NOT EXISTS(SELECT 1 FROM experiment.plot WHERE location_id=locationId AND id=items.row_id::integer AND is_void=false) then 
            return 'Record '|| items.id || ' with ROW_ID of '|| items.row_id || ' is not a plot of location.';

        end if;
    END LOOP;
    
    
    EXECUTE (
    SELECT '
        WITH rows as (
            UPDATE data_terminal.transaction_dataset td
            SET 
                is_suppressed = false,
                value = a.value,
                modification_timestamp = now(),
                modifier_id='|| var_user_id ||'
                
            FROM
            (
                SELECT 
                    DISTINCT ON (u.variable,u.val,t.row_id)
                    '|| var_transaction_id ||' as transaction_id,
                    (SELECT id FROM master.variable where abbrev=upper(u.variable)) as variable_id, 
                    u.val as value,
                    '||var_user_id||' as creator_id,
                    ''plot_data'' as entity,
                    t.row_id::integer as entity_id
                FROM  
                    ' || tableName || ' t
                        LEFT JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', tempVar, tempVar), ', ') || ') u(variable, val) ON true,
                    master.variable v
                where 
                    lower(v.abbrev)=u.variable
                    AND trim(u.val)<>''''
                    AND NOT EXISTS(
                      SELECT 1 
                      FROM
                          experiment.plot_data pd
                      where 
                          t.row_id::int=pd.plot_id
                          AND pd.data_value=u.val
                          AND pd.variable_id = v.id
                          AND pd.is_void=false
                    )
            )a
            WHERE 
                td.entity_id = a.entity_id
                AND td.transaction_id ='||var_transaction_id||'
                AND td.variable_id = a.variable_id
                AND td.entity_id = a.entity_id
                AND td.value is not null 
                AND td.value <> a.value
                AND td.is_void = false
            returning id 
        ) SELECT count(1) FROM rows' 
    FROM   
          (SELECT unnest(var_variables) except SELECT unnest(array['row_id']) )t(tempVar)
      ) into updatedCount;
         
    
    EXECUTE (
    SELECT '
        WITH rows as (INSERT INTO data_terminal.transaction_dataset 
            (transaction_id, data_unit, variable_id, value , creator_id, entity, entity_id , 
      is_void, is_generated, is_suppressed)
            SELECT 
                DISTINCT ON (u.variable,value,t.row_id)
                '|| var_transaction_id ||' as transaction_id,
                ''plot'' as data_unit,
                (SELECT id FROM master.variable where abbrev=upper(u.variable)) as variable_id, 
                u.val as value,
                '||var_user_id||' as creator_id,
                ''plot_data'' as entity,
                t.row_id::integer as entity_id,
                false as is_void,
                false as is_generated,
                false as is_suppressed
            FROM  
                ' || tableName || ' t
                    LEFT   JOIN LATERAL ( VALUES ' || string_agg(format('(''%s'', t.%I)', y, y), ', ') || ') u(variable, val) ON true,
                master.variable v
            where 
                lower(v.abbrev)=u.variable
                AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        data_terminal.transaction_dataset td
                    where 
                        td.transaction_id='||var_transaction_id||'
                        AND t.row_id::int=td.entity_id
                        AND td.value is not null 
                        AND td.value=u.val
                        AND td.variable_id = v.id
                        AND td.is_void=false
                ) AND NOT EXISTS(
                    SELECT 1 
                    FROM
                        experiment.plot_data pd
                    where 
                        t.row_id::int=pd.plot_id
                        AND pd.data_value=u.val
                        AND pd.variable_id = v.id
                        AND pd.is_void=false
                )
                AND trim(u.val)<>''''
            RETURNING id 
        ) SELECT count(1) FROM rows' 
    FROM   
        (SELECT unnest(var_variables) except SELECT unnest(array['row_id']) )t(y)
    )into insertedCount;
          
    return insertedCount + updatedCount;
END;
$function$
;
