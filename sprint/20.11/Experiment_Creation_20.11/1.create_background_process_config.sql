INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD', 'Background processing threshold values for Experiment Creation tool features', 
    '{
        "deleteEntries": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "updateEntries": {
            "size": "500",
            "description": "Update entries of an Experiment"
        },
        "createEntries": {
            "size": "500",
            "description": "Create entries of an Experiment"
        }
  }'::json, 1, 'experiment_creation_bg_process', 1,'added by j.antonio');