update platform.config set config_value = '{
	"createEntries": {
		"size": "500",
		"description": "Create entries of an Experiment"
	},
	"deleteEntries": {
		"size": "500",
		"description": "Delete entries of an Experiment"
	},
	"updateEntries": {
		"size": "500",
		"description": "Update entries of an Experiment"
	},
	"deleteOccurrences": {
	    "size": "500",
		"description": "Delete occurrence plot and/or planting instruction records of an Experiment"
	},
	"createOccurrences": {
	  	 "size": "500",
		 "description": "Create occurrence plot and/or planting instruction records of an Experiment"
	}
}' where abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';