--Update table ownership to postgres
select 
	nsp.nspname as object_schema,
	cls.relname as object_name, 
	rol.rolname as owner, 
	case cls.relkind
		when 'r' then 'TABLE'
		when 'm' then 'MATERIALIZED_VIEW'
		when 'i' then 'INDEX'
		when 'S' then 'SEQUENCE'
		when 'v' then 'VIEW'
		when 'c' then 'TYPE'
		else cls.relkind::text
	end as object_type,
		z_admin.execute(format(
						'ALTER TABLE %I.%I OWNER TO %I',
						nsp.nspname,
						cls.relname,
						'postgres'))
from pg_class cls
	join pg_roles rol on rol.oid = cls.relowner
	join pg_namespace nsp on nsp.oid = cls.relnamespace
where 
	nsp.nspname not in ('pg_catalog')
and
	nsp.nspname 
not like 
	'pg_toast%'
and 
	rol.rolname <> 'postgres'
and 
	cls.relkind = 'r'
order by 
	nsp.nspname, cls.relname;
	
--Update foreign table ownership to postgres
select 
	nsp.nspname as object_schema,
	cls.relname as object_name, 
	rol.rolname as owner, 
	case cls.relkind
		when 'r' then 'TABLE'
		when 'm' then 'MATERIALIZED_VIEW'
		when 'i' then 'INDEX'
		when 'S' then 'SEQUENCE'
		when 'v' then 'VIEW'
		when 'c' then 'TYPE'
		when 't' then 'FOREIGN_TABLE'
		else cls.relkind::text
	end as object_type,
	z_admin.execute(format(
						'ALTER FOREIGN TABLE %I.%I OWNER TO %I',
						nsp.nspname,
						cls.relname,
						'postgres'))
from pg_class cls
	join pg_roles rol on rol.oid = cls.relowner
	join pg_namespace nsp on nsp.oid = cls.relnamespace
where 
	nsp.nspname not in ('pg_catalog')
and
	nsp.nspname 
not like 
	'pg_toast%'
and 
	rol.rolname <> 'postgres'
and 
	cls.relkind = 't'
order by 
	nsp.nspname, cls.relname;
		
--Update materialize view ownership to postgres
select 
	nsp.nspname as object_schema,
	cls.relname as object_name, 
	rol.rolname as owner, 
	case cls.relkind
		when 'r' then 'TABLE'
		when 'm' then 'MATERIALIZED_VIEW'
		when 'i' then 'INDEX'
		when 'S' then 'SEQUENCE'
		when 'v' then 'VIEW'
		when 'c' then 'TYPE'
		else cls.relkind::text
	end as object_type,
	z_admin.execute(format(
						'ALTER MATERIALIZED VIEW %I.%I OWNER TO %I',
						nsp.nspname,
						cls.relname,
						'postgres'))
from pg_class cls
	join pg_roles rol on rol.oid = cls.relowner
	join pg_namespace nsp on nsp.oid = cls.relnamespace
where 
	nsp.nspname not in ('pg_catalog')
and
	nsp.nspname 
not like 
	'pg_toast%'
and 
	rol.rolname <> 'postgres'
and 
	cls.relkind = 'm'
order by 
	nsp.nspname, cls.relname;
	
--Update index ownership to postgres
select 
	nsp.nspname as object_schema,
	cls.relname as object_name, 
	rol.rolname as owner, 
	case cls.relkind
		when 'r' then 'TABLE'
		when 'm' then 'MATERIALIZED_VIEW'
		when 'i' then 'INDEX'
		when 'S' then 'SEQUENCE'
		when 'v' then 'VIEW'
		when 'c' then 'TYPE'
		else cls.relkind::text
	end as object_type,
	z_admin.execute(format(
						'ALTER INDEX %I.%I OWNER TO %I',
						nsp.nspname,
						cls.relname,
						'postgres'))
from pg_class cls
	join pg_roles rol on rol.oid = cls.relowner
	join pg_namespace nsp on nsp.oid = cls.relnamespace
	--where nsp.nspname not in ('information_schema', 'pg_catalog')
where 
	nsp.nspname not in ('pg_catalog')
and
	nsp.nspname 
not like 
	'pg_toast%'
and 
	rol.rolname <> 'postgres'
and 
	cls.relkind = 'i'
order by 
	nsp.nspname, cls.relname;

--Update sequence ownership to postgres
select 
	nsp.nspname as object_schema,
	cls.relname as object_name, 
	rol.rolname as owner, 
	case cls.relkind
		when 'r' then 'TABLE'
		when 'm' then 'MATERIALIZED_VIEW'
		when 'i' then 'INDEX'
		when 'S' then 'SEQUENCE'
		when 'v' then 'VIEW'
		when 'c' then 'TYPE'
		else cls.relkind::text
	end as object_type,
	z_admin.execute(format(
						'ALTER SEQUENCE %I.%I OWNER TO %I',
						nsp.nspname,
						cls.relname,
						'postgres'))
from pg_class cls
	join pg_roles rol on rol.oid = cls.relowner
	join pg_namespace nsp on nsp.oid = cls.relnamespace
where 
	nsp.nspname not in ('pg_catalog')
and
	nsp.nspname 
not like 
	'pg_toast%'
and 
	rol.rolname <> 'postgres'
and 
	cls.relkind = 'S'
order by 
	nsp.nspname, cls.relname;
	
--Update view ownership to postgres
select 
	nsp.nspname as object_schema,
	cls.relname as object_name, 
	rol.rolname as owner, 
	case cls.relkind
		when 'r' then 'TABLE'
		when 'm' then 'MATERIALIZED_VIEW'
		when 'i' then 'INDEX'
		when 'S' then 'SEQUENCE'
		when 'v' then 'VIEW'
		when 'c' then 'TYPE'
		else cls.relkind::text
	end as object_type,
	z_admin.execute(format(
						'ALTER VIEW %I.%I OWNER TO %I',
						nsp.nspname,
						cls.relname,
						'postgres'))
from pg_class cls
	join pg_roles rol on rol.oid = cls.relowner
	join pg_namespace nsp on nsp.oid = cls.relnamespace
	--where nsp.nspname not in ('information_schema', 'pg_catalog')
where 
	nsp.nspname not in ('pg_catalog')
and
	nsp.nspname 
not like 
	'pg_toast%'
and 
	rol.rolname <> 'postgres'
and 
	cls.relkind = 'v'
order by 
	nsp.nspname, cls.relname;

--Update type ownership to postgres
select 
	nsp.nspname as object_schema,
	cls.relname as object_name, 
	rol.rolname as owner, 
	case cls.relkind
		when 'r' then 'TABLE'
		when 'm' then 'MATERIALIZED_VIEW'
		when 'i' then 'INDEX'
		when 'S' then 'SEQUENCE'
		when 'v' then 'VIEW'
		when 'c' then 'TYPE'
		else cls.relkind::text
	end as object_type,
	z_admin.execute(format(
						'ALTER TYPE %I.%I OWNER TO %I',
						nsp.nspname,
						cls.relname,
						'postgres'))
from pg_class cls
	join pg_roles rol on rol.oid = cls.relowner
	join pg_namespace nsp on nsp.oid = cls.relnamespace
	--where nsp.nspname not in ('information_schema', 'pg_catalog')
where 
	nsp.nspname not in ('pg_catalog')
and
	nsp.nspname 
not like 
	'pg_toast%'
and 
	rol.rolname <> 'postgres'
and 
	cls.relkind = 'c'
order by 
	nsp.nspname, cls.relname;

--Update trigger ownership to postgres
select 
	n.nspname,
	p.proname,
	p.proargtypes,
	z_admin.execute(format(
		'alter function '||n.nspname||'."'||p.proname||'"('||pg_get_function_identity_arguments(p.oid)||') owner to postgres;'
	))
from 
	pg_proc p  
join 
	pg_namespace n 
on
	n.oid = p.pronamespace
where 
	n.nspname 
not 
	in ('pg_catalog')
and
	n.nspname 
not like 
	'pg_toast%'
order by
	n.nspname;
	