-- SEQUENCE: master.institute_id_seq

-- DROP SEQUENCE master.institute_id_seq;

CREATE SEQUENCE master.institute_id_seq
    INCREMENT 1
    START 14
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE master.institute_id_seq
    OWNER TO postgres;

GRANT SELECT, USAGE ON SEQUENCE master.institute_id_seq TO postgres;

GRANT ALL ON SEQUENCE master.institute_id_seq TO postgres;

GRANT ALL ON SEQUENCE master.institute_id_seq TO postgres;