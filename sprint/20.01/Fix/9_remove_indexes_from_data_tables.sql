select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'study_metadata');
DROP INDEX operational.study_metadata_event_log_idx;

select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'study_data');
DROP INDEX operational.study_data_event_log_idx;

select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'service_request_metadata');
DROP INDEX operational.service_request_metadata_event_log_idx;

select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'service_request');
DROP INDEX operational.service_request_event_log_idx;

select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'seed_storage_data');
DROP INDEX operational.seed_storage_data_event_log_idx;

select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'experiment_design_data');
DROP INDEX operational.experiment_design_data_event_log_idx;

select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'experiment_data');
DROP INDEX operational.experiment_data_event_log_idx;

select * from z_admin.audit_table(in_schema_name := 'operational', in_table_name := 'entry_list_data');
DROP INDEX operational.entry_list_data_event_log_idx;

DROP INDEX operational.entry_data_event_log_idx;
DROP INDEX operational.entry_metadata_event_log_idx;
DROP INDEX operational.plot_data_event_log_idx;
DROP INDEX operational.plot_metadata_event_log_idx;
DROP INDEX operational.cross_data_event_log_idx;
DROP INDEX operational.cross_metadata_event_log_idx;
DROP INDEX operational.experiment_design_data_event_log_idx;
DROP INDEX operational.subplot_data_event_log_idx;
DROP INDEX operational.subplot_metadata_event_log_idx;
