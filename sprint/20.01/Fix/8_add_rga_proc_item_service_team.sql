/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>.
*/

/**
* @author Maria Relliza A. Pasang <m.pasang@irri.org>
* @date 2020-02-18 10:00 am
*/

-- B4R-4510 Add RGA process path in item service team
insert into master.item_service_team(item_id,team_id,program_id, creator_id) values (
    (select id from master.item where abbrev = 'RGA_PROC'), -- item_id
    (select id from master.team where abbrev = 'RGA'), -- team_id
    (select id from master.program where abbrev='RGA'), -- program_id
    1); -- creator_id