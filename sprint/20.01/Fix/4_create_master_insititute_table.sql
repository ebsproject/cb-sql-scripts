-- Table: master.institute

-- DROP TABLE master.institute;

CREATE TABLE master.institute
(
    id integer NOT NULL DEFAULT nextval('master.institute_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    abbrev character varying COLLATE pg_catalog."default" NOT NULL,
    institute_type character varying COLLATE pg_catalog."default",
    institute_department character varying COLLATE pg_catalog."default",
    remarks text COLLATE pg_catalog."default",
    creation_timestamp timestamp without time zone NOT NULL DEFAULT now(),
    creator_id integer NOT NULL DEFAULT 1,
    modification_timestamp timestamp without time zone,
    modifier_id integer,
    notes text COLLATE pg_catalog."default",
    is_void boolean NOT NULL DEFAULT false,
    is_active boolean NOT NULL DEFAULT true,
    event_log jsonb,
    CONSTRAINT institute_id_pkey PRIMARY KEY (id),
    CONSTRAINT institute_creator_id_fkey FOREIGN KEY (creator_id)
        REFERENCES master."user" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT institute_modifier_id_fkey FOREIGN KEY (modifier_id)
        REFERENCES master."user" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE master.institute
    OWNER to postgres;

GRANT ALL ON TABLE master.institute TO postgres;

GRANT SELECT ON TABLE master.institute TO postgres;

GRANT ALL ON TABLE master.institute TO postgres;

GRANT ALL ON TABLE master.institute TO postgres;

GRANT ALL ON TABLE master.institute TO postgres;

COMMENT ON TABLE master.institute
    IS 'Stores records of institutes/organizations';

COMMENT ON COLUMN master.institute.id
    IS 'Primary key';

COMMENT ON COLUMN master.institute.name
    IS 'The full name of the institute';

COMMENT ON COLUMN master.institute.abbrev
    IS 'The abbreviation/acronym of the institute';

COMMENT ON COLUMN master.institute.institute_type
    IS 'Classification of the institute';

COMMENT ON COLUMN master.institute.institute_department
    IS 'Sub division of an institute. i.e. PBGB of IRRI.';

-- Trigger: institute_event_log_tgr

-- DROP TRIGGER institute_event_log_tgr ON master.institute;

CREATE TRIGGER institute_event_log_tgr
    BEFORE INSERT OR UPDATE 
    ON master.institute
    FOR EACH ROW
    EXECUTE PROCEDURE z_admin.log_record_event();