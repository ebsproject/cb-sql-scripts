/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-02-24
 *
 * Hide IRRI experiment templates in Experiment creation
 */

update 
	master.item 
set 
	is_void = true,
	notes = 'hide in CIMMYT database' 
where 
	abbrev ilike '%IRRI%' 
	and process_type = 'experiment_creation_data_process';

-- void experiments with voided experiment templates

update 
	operational.experiment 
set 
	is_void = true 
where 
	data_process_id in 
	(select id from master.item where abbrev ilike '%IRRI%' and process_type = 'experiment_creation_data_process');