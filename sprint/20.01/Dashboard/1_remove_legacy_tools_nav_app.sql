/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-02-06
 *
 * Remove legacy tools in navigation and list of applications
 */

-- void previous spaces
update platform.space set is_void = true, abbrev = abbrev || '_VOIDED_' || id, order_number = (order_number + 100);

-- insert new navigation config for default user
insert into platform.space (abbrev,name,menu_data,order_number,creator_id)
values 
(
	'DEFAULT',
	'default',
	'{
		"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiments",
			"appAbbrev": "EXPERIMENT_CREATION"
		}, {
			"name": "operational-studies",
			"label": "Operational studies",
			"appAbbrev": "OPERATIONAL_STUDIES"
		}, {
			"name": "tasks",
			"label": "Tasks",
			"appAbbrev": "TASKS"
		}, {
			"name": "data-collection-qc",
			"items": [{
				"name": "data-collection-qc-quality-control",
				"label": "Quality control",
				"appAbbrev": "QUALITY_CONTROL"
			}],
			"label": "Data collection & QC"
		}, {
			"name": "seeds",
			"label": "Seeds",
			"items": [{
				"name": "seeds-find-seeds",
				"label": "Find seeds",
				"appAbbrev": "FIND_SEEDS"
			}, {
				"name": "seeds-harvest-manager",
				"label": "Harvest manager",
				"appAbbrev": "HARVEST_MANAGER"
			}]
		}]
	}',
	100,
	1
);

-- insert new navigation for admin user
insert into platform.space (abbrev,name,menu_data,order_number,creator_id)
values 
(
	'ADMIN',
	'admin',
	'{
		"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiments",
			"appAbbrev": "EXPERIMENT_CREATION"
		}, {
			"name": "operational-studies",
			"label": "Operational studies",
			"appAbbrev": "OPERATIONAL_STUDIES"
		}, {
			"name": "tasks",
			"label": "Tasks",
			"appAbbrev": "TASKS"
		}, {
			"name": "data-collection-qc",
			"items": [{
				"name": "data-collection-qc-quality-control",
				"label": "Quality control",
				"appAbbrev": "QUALITY_CONTROL"
			}],
			"label": "Data collection & QC"
		}, {
			"name": "seeds",
			"label": "Seeds",
			"items": [{
				"name": "seeds-find-seeds",
				"label": "Find seeds",
				"appAbbrev": "FIND_SEEDS"
			}, {
				"name": "seeds-harvest-manager",
				"label": "Harvest manager",
				"appAbbrev": "HARVEST_MANAGER"
			}]
		}],
		"main_menu_items": [{
			"icon": "folder_special",
			"name": "data-management",
			"items": [{
				"name": "data-management_seasons",
				"label": "Seasons",
				"appAbbrev": "MANAGE_SEASONS"
			}],
			"label": "Data management"
		}]
	}',
	1,
	1
);

-- void legacy tools
update 
	platform.application 
set 
	is_void = true, 
	notes = 'remove legacy tools' 
where 
	abbrev not in (
		'PROFILE',
		'SEARCH',
		'MANAGE_SEASONS',
		'CONFIGURE_DASHBOARD',
		'OPERATIONAL_STUDIES',
		'TASKS',
		'QUALITY_CONTROL',
		'DATA_EXPORT',
		'EXPERIMENT_CREATION',
		'PRINTOUTS',
		'LISTS',
		'FIND_SEEDS',
		'SEED_INVENTORY_CONFIG',
		'PROJECTS',
		'HARVEST_MANAGER',
		'STATUS'
	);

-- hide service requests and shipments widgets
update platform.dashboard_widget set is_void = true, notes = 'hide in b4r-ebs project' where abbrev in ('RECEIVED_SERVICE_REQUESTS','SERVICE_REQUESTS','SHIPMENTS');