-- 
-- B4R-3997
-- 

-- remove crosses tab in seed increase template

delete from platform.item_module 
where item_id IN (select id from master.item where abbrev IN ('EXPT_SEED_INCREASE_MATRIX_ACT','EXPT_SEED_INCREASE_MANAGE_ACT'))
or module_id IN (select id from platform.module where abbrev IN('EXPT_SEED_INCREASE_CROSSES_ACT_MOD','EXPT_SEED_INCREASE_MANAGE_ACT_MOD','EXPT_SEED_INCREASE_MATRIX_ACT_MOD') );

delete from platform.module where abbrev IN ('EXPT_SEED_INCREASE_CROSSES_ACT_MOD','EXPT_SEED_INCREASE_MANAGE_ACT_MOD','EXPT_SEED_INCREASE_MATRIX_ACT_MOD');

delete from master.item_relation where root_id IN (select id from master.item where abbrev = 'EXPT_SEED_INCREASE_DATA_PROCESS') and parent_id = (select id from master.item where abbrev = 'EXPT_SEED_INCREASE_CROSSES_ACT');

delete from master.item where abbrev IN ('EXPT_SEED_INCREASE_CROSSES_ACT','EXPT_SEED_INCREASE_MATRIX_ACT','EXPT_SEED_INCREASE_MANAGE_ACT');


-- remove crosses tab in selection and advancement template

delete from platform.item_module 
where item_id IN (select id from master.item where abbrev IN ('EXPT_SELECTION_ADVANCEMENT_MATRIX_ACT','EXPT_SELECTION_ADVANCEMENT_MANAGE_ACT'))
or module_id IN (select id from platform.module where abbrev IN('EXPT_SELECTION_ADVANCEMENT_CROSSES_ACT_MOD','EXPT_SELECTION_ADVANCEMENT_MANAGE_ACT_MOD','EXPT_SELECTION_ADVANCEMENT_MATRIX_ACT_MOD') );

delete from platform.module where abbrev IN ('EXPT_SELECTION_ADVANCEMENT_CROSSES_ACT_MOD','EXPT_SELECTION_ADVANCEMENT_MANAGE_ACT_MOD','EXPT_SELECTION_ADVANCEMENT_MATRIX_ACT_MOD');

delete from master.item_relation where root_id IN (select id from master.item where abbrev = 'EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS') and parent_id = (select id from master.item where abbrev = 'EXPT_SELECTION_ADVANCEMENT_CROSSES_ACT');

delete from master.item where abbrev IN ('EXPT_SELECTION_ADVANCEMENT_CROSSES_ACT','EXPT_SELECTION_ADVANCEMENT_MATRIX_ACT','EXPT_SELECTION_ADVANCEMENT_MANAGE_ACT');
