/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Adds contact person to place configurations
 * in all experiment templates
 *
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-01-21
 */

with t as (
	select (ov::jsonb || av::jsonb) as nv, abbrev, config_value
	from (
		select 
			config_value->>'Values', '{"field_label":"Contact person","variable_abbrev":"CONTCT_PERSON_CONT","allow_new_val":true}'::jsonb, abbrev, config_value
		from 
			platform.config 
		where 
			abbrev ilike '%_PLACE_ACT_VAL'
	) as t(ov, av)
) 
update 
	platform.config c
set 
	config_value = jsonb_set(t.config_value, '{Values}', nv, true),
	notes = notes || '; added contact person by jp.ramos'
from 
	t
where 
	c.abbrev = t.abbrev

/* Set target_table to master.user for contact person variable */
update master.variable set field_prop = '{"target_column":"display_name"}', target_table = 'master.user' where abbrev = 'CONTCT_PERSON_CONT';

/* Update unit of plot length */
update 
	master.scale 
set 
	unit = 'm' 
where 
	id in 
	( 
		select 
			id 
		from 
			master.scale 
		where 
			id in (select scale_id from master.variable where name ilike '%plot length%')
	);
