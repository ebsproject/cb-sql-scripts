
insert into master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage,subject_entity_id) 
values('EXPT_SEED_INCREASE_IRRI_DATA_PROCESS','Seed Increase for IRRI Experiment',40,'Applies to experiments Seed Increase (or Seed Multiplication or Seed Regeneration).','Seed Increase for IRRI Experiment',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation',(select id from master.entity where abbrev='EXPERIMENT'));

--create specify basic information
--Basic, Entry List, Design, Protocols, Place, Review.
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

--create Specify Entry List
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol');


--create design tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_DESIGN_ACT','Planting Arrangement',30,'Planting Arrangement','Planting Arrangement',1,'active','fa fa-files-o');

--Additions in design tab
--Create design tabs
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT','Add blocks',20,'Add blocks','Add blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT','Assign entries',20,'Assign entries','Assign entries',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT','Manage blocks',20,'Manage blocks','Manage blocks',1,'active','fa fa-random');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT','Overview',20,'Overview','Overview',1,'active','fa fa-random');



--create protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT','Protocols',30,'Protocols','Protocols',1,'active','fa fa-files-o');

--Additions for Protocols tab
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT','Planting',20,'Planting Protocol','Planting',1,'active','fa fa-pagelines');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT','Traits',20,'Traits Protocol','Traits',1,'active','fa fa-braille');

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT','Process Path',20,'Process Path Protocol','Process Path',1,'active','fa fa-list-ol');




--create Specify locations
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_PLACE_ACT','Specify Place',30,'Specify Place','Place',1,'active','fa fa-map-marker');

--create Preview and confirm
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('EXPT_SEED_INCREASE_IRRI_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');


--insert the sequence of activities for the cross pre-planning the data process
insert into master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select 
  (select id from master.item where abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as root_id,
  (select id from master.item where abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as parent_id,
   id as child_id,
   case 
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT' then 1
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT' then 2
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT' then 3
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT' then 4
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_PLACE_ACT' then 5
     when abbrev = 'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT' then 6
     else 7 end as order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
from 
   master.item
where 
   abbrev in ('EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT','EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT','EXPT_SEED_INCREASE_IRRI_DESIGN_ACT','EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_PLACE_ACT','EXPT_SEED_INCREASE_IRRI_REVIEW_ACT');



--Additions for Design tab
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT' then 1
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT'then 2
      when abbrev = 'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT'then 3
      when abbrev = 'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT'then 4
		else 5 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT','EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT','EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT','EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT');
;

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_DATA_PROCESS') as root_id,
	(select id from master.item where abbrev = 'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT') as parent_id,
	id as child_id,
  case
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT' then 1
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT'then 2
    when abbrev = 'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT'then 3
		else 4 end as order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT');
;





--insert the activities in the platform module
insert into platform.module(abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
values 
  ('EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
  ('EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
  ('EXPT_SEED_INCREASE_IRRI_DESIGN_ACT_MOD','Planting Arrangement','Planting Arrangement','experimentCreation','create','specify-cross-design',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT_MOD','Add Blocks','Add Blocks','experimentCreation','create','add-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT_MOD','Assign Entries','Assign Entries','experimentCreation','create','assign-entries',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT_MOD','Manage Blocks','Manage Blocks','experimentCreation','create','manage-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT_MOD','Overview','Overview','experimentCreation','create','overview',1,'added by j.antonio ' || now(), 'design generated'),
  ('EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT_MOD','Protocols','Protocols','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT_MOD','Planting Protocols','Planting Protocols','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT_MOD','Traits Protocols','Traits Protocols','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD','Process Path Protocols','Process Path Protocols','experimentCreation','protocol','process-path-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD','Specify Locations','Specify Locations','experimentCreation','create','specify-locations',1,'added by j.antonio ' || now(), 'location rep studies created'),
  ('EXPT_SEED_INCREASE_IRRI_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'finalized');


--insert in platform.item_module
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
	case
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT' then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_MOD')
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_MOD')
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT_MOD')
      when abbrev = 'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT_MOD')
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_PLACE_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD')
		when abbrev = 'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT_MOD')
		else (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
	abbrev in ('EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT','EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT', 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT','EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT', 'EXPT_SEED_INCREASE_IRRI_PLACE_ACT','EXPT_SEED_INCREASE_IRRI_REVIEW_ACT');


--Additions for Design tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT_MOD')
  when abbrev = 'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT_MOD')
  when abbrev = 'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT_MOD')
  when abbrev = 'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT','EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT','EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT','EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT');


--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
	id as item_id,
case
  when abbrev = 'EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT_MOD')
  when abbrev = 'EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT_MOD')
   when abbrev = 'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT'then (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD')
  else (select id from platform.module where abbrev = 'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD') end as module_id,
	1,
	'added by j.antonio ' || now()
from
	master.item
where
abbrev in ('EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT','EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT');
