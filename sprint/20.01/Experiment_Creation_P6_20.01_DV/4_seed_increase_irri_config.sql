INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL', 'Seed Increase for IRRI Experiment Basic Information',  '{
  	"Values": [{
  			"default": false,
  			"disabled": true,
  			"required": "required",
  			"field_label": "Experiment Template",
  			"order_number": 1,
  			"variable_abbrev": "EXPERIMENT_TEMPLATE",
  			"field_description": "Experiment Template"
  		},{
  			"default": false,
  			"variable_abbrev": "EXPERIMENT_TYPE",
  			"order_number": 2,
  			"field_label": "Experiment Type",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Experiment Type"
  		},{
  			"field_label": "Crop",
  			"order_number": 3,
  			"variable_abbrev": "CROP",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Program Crop"
  		},
  		{
  			"field_label": "Program",
  			"order_number": 4,
  			"variable_abbrev": "PROGRAM",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Program"
  		},
  		{
  			"field_label": "Project",
  			"order_number": 5,
  			"variable_abbrev": "PROJECT",
  			"allow_new_val": true,
  			"disabled": false,
  			"field_description": "Project where the experiment is under"
  		},
  		{
  			"field_label": "Experiment Code",
  			"order_number": 6,
  			"variable_abbrev": "EXPERIMENT_CODE",
  			"required": "required",
  			"disabled": true,
  			"field_description": "Auto-generated code for the experiment"
  		},
  		{
  			"field_label": "Experiment Name",
  			"order_number": 7,
  			"variable_abbrev": "EXPERIMENT_NAME",
  			"required": "required",
  			"disabled": false,
  			"field_description": "User inputted name for the experiment"
  		},
  		{
  			"default": "BRE",
  			"variable_abbrev": "PHASE",
  			"order_number": 8,
  			"field_label": "Experiment sub-type",
  			"required": "required",
  			"disabled": false,
  			"allowed_values": [
				"BRE",
				"SEM",
                "PN"
  			],
  			"field_description": "Experiment Sub-type/Phase of the experiment"
  		},
  		{
  			"variable_abbrev": "EXPERIMENT_SUB_SUBTYPE",
  			"order_number": 9,
  			"field_label": "Experiment Sub Sub-type",
  			"disabled": false,
  			"field_description": "Experiment Sub Sub-type/Objective of the experiment"
  		},
  		{
  			"field_label": "Evaluation Year",
  			"order_number": 10,
  			"variable_abbrev": "YEAR",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Evaluation Year"
  		},
  		{
  			"field_label": "Evaluation Season",
  			"order_number": 11,
  			"variable_abbrev": "SEASON",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Evaluation Season"
  		},
  		{
  			"field_label": "Experiment Steward",
  			"order_number": 12,
  			"variable_abbrev": "EXPERIMENT_STEWARD",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Steward of the experiment"
  		},
  		{
  			"field_label": "Remarks",
  			"disabled": false,
  			"field_description": "Experiment Comment",
  			"order_number": 13,
  			"variable_abbrev": "REMARKS"
  		}
  	],
  	"Name": "Required experiment level metadata variables for seed increase data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio'),


('EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_VAL','Seed Increase for IRRI Experiment Entry List',
 '{
   "Name": "Required and default entry level metadata variables for seed increase data process",
   "Values": [
     {
       "disabled": false,
       "field_label": "Remarks",
       "field_description": "Entry Remarks",
       "order_number": 2,
       "variable_abbrev": "REMARKS"
     }
   ]
 }', 1, 'experiment_creation', 1,'added by j.antonio'),

('EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT_VAL', 'Experiment IRRI Seed Increase Protocols',   ---EXPT_TRIAL_IRRI_PROTOCOLS_ACT_VAL
 '{
   "Name": "Required and default protocols metadata variables for IRRI Seed Increase data process",
   "Values": [
      {
        "required": "required",
        "disabled": false,
        "field_label": "Process Paths",
        "field_description": "Process Paths",
        "order_number": 2,
        "variable_abbrev": "PROCESS_PATH",
        "process_path_abbrevs": [
			"AG_SCREENING_PROC",
			"BREEDER_SEED_PRODUCTION_PROC",
			"BSREC_BACTERIAL_BLIGHT_PROC",
			"BSREC_BLAST_PROC",
			"BSREC_GLH_BPH_PROC",
			"BSREC_RTV_GH_PROC",
			"COLD_TOLERANCE_SCREENING_PROC",
			"COLD_TOLERANT_SCREENING_PGF_RIC_CONTROL_PROC",
			"COLD_TOLERANT_SCREENING_PGF_RIC_PROC",
			"CROSS_LIST_PROC",
			"DISEASE_RESISTANCE_EVALUATION",
			"F1_NURSERY_PROC",
			"FIELD_DSR_PROC",
			"FLD_PROC",
			"GSL_PROCESS_PATH",
			"HB_CROSS_LIST_PROC",
			"HB_F1_NURSERY_PROC",
			"HB_PARENT_LIST_PROC",
			"HB_PROC",
			"HERBICIDE_TOLERANCE_SCREENING_PROC",
			"HYBRID_DOUBLE_HAPLOID_DEVELOPMENT_PROC",
			"HYBRID_STERILITY_SCREENING_PROC",
			"PARENT_LIST_PROC",
			"RGA_PROC",
			"SEED_INCREASE_SERVICE_PROC",
			"YIELD_TRIAL_SERVICE_PROC"
        ]
      }
   ]
 }
'::json, 1, 'experiment_creation', 1,'added by j.antonio'),

('EXPT_SEED_INCREASE_IRRI_PLACE_ACT_VAL', 'Seed Increase for IRRI Experiment Place',
 '{
   "Name": "Required and default place metadata variables for seed increase data process",
   "Values": [
     {
       "disabled": false,
       "field_label": "Remarks",
       "field_description": "Place Remarks",
       "order_number": 1,
       "variable_abbrev": "REMARKS"
     }
   ]
 }
', 1, 'experiment_creation', 1,'added by j.antonio');


INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT_VAL', 'Experiment Seed Increase for IRRIHQ Protocol variables',  '{
  	"Values": [{
  			"default": false,
  			"disabled": false,
  			"field_label": "Crop Establishment",
  			"order_number": 1,
  			"variable_abbrev": "ESTABLISHMENT",
  			"field_description": "Crop Establishment"
  		},{
  			"default": false,
  			"variable_abbrev": "PLANTING_TYPE",
  			"order_number": 2,
  			"field_label": "Planting Type",
  			"disabled": false,
  			"field_description": "Planting Type"
  		},{
  			"field_label": "Plot Type",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_TYPE",
  			"required": "required",
  			"disabled": false,
  			"field_description": "Plot Type"
  		}
  	],
  	"Name": "Required experiment level protocol variables for Seed Increase IRRIHQ data process"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


--Update the item_class column of the new data process 
update master.item set item_class = 'nursery' where abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS';
