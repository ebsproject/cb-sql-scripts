--UPDATE: EXPERIMENT_NAME
UPDATE master.variable set description='Desired name for this experiment',abbrev='EXPERIMENT_NAME' where abbrev = 'EXPERIMENT_NAME';
--UPDATE: HAS_EXPERIMENT_GROUP
UPDATE master.variable set description='Whether experiment has experiment group or not ',abbrev='HAS_EXPERIMENT_GROUP' where abbrev = 'HAS_EXPERIMENT_GROUP';
--UPDATE: YEAR
UPDATE master.variable set name='Evaluation Year',label='Evaluation Year',abbrev='YEAR' where abbrev = 'YEAR';
--UPDATE: SEASON
UPDATE master.variable set name='Evaluation Season',label='Evaluation Season',abbrev='SEASON' where abbrev = 'SEASON';
--UPDATE: GID
UPDATE master.variable set name='Germplasm ID (GID)',label='Germplasm ID (GID)',abbrev='GID' where abbrev = 'GID';
--UPDATE: PARENT_TYPE
UPDATE master.variable set description='Indicates if the parent is femal or male or female and male',name='Parent Role',label='Parent Role',abbrev='PARENT_TYPE' where abbrev = 'PARENT_TYPE';
--UPDATE: HYBRID_ROLE
UPDATE master.variable set description='Indicates hybrid cross methods',abbrev='HYBRID_ROLE' where abbrev = 'HYBRID_ROLE';
--UPDATE: CROSS_METHOD
UPDATE master.variable set description='Crossing method used',abbrev='CROSS_METHOD' where abbrev = 'CROSS_METHOD';
--UPDATE: PLOT_NUMBERING_ORDER
UPDATE master.variable set description='Plot ordering pattern or direction',abbrev='PLOT_NUMBERING_ORDER' where abbrev = 'PLOT_NUMBERING_ORDER';
--UPDATE: STARTING_CORNER
UPDATE master.variable set description='Starting corner of plot number',abbrev='STARTING_CORNER' where abbrev = 'STARTING_CORNER';
--UPDATE: NO_OF_BLOCKS
UPDATE master.variable set description='Number of blocks',abbrev='NO_OF_BLOCKS' where abbrev = 'NO_OF_BLOCKS';
--UPDATE: NO_OF_COLS_IN_BLOCK
UPDATE master.variable set description='Number of columns in the block',abbrev='NO_OF_COLS_IN_BLOCK' where abbrev = 'NO_OF_COLS_IN_BLOCK';
--UPDATE: NO_OF_ROWS_IN_BLOCK
UPDATE master.variable set description='Number of rows in the block',abbrev='NO_OF_ROWS_IN_BLOCK' where abbrev = 'NO_OF_ROWS_IN_BLOCK';
--UPDATE: NO_OF_REPS_IN_BLOCK
UPDATE master.variable set description='Number of reps in the block',abbrev='NO_OF_REPS_IN_BLOCK' where abbrev = 'NO_OF_REPS_IN_BLOCK';
--UPDATE: ESTABLISHMENT
UPDATE master.variable set description='Crop establishment method',abbrev='ESTABLISHMENT' where abbrev = 'ESTABLISHMENT';
--UPDATE: PLANTING_TYPE
UPDATE master.variable set description='Planting type',abbrev='PLANTING_TYPE' where abbrev = 'PLANTING_TYPE';
--UPDATE: PLOT_TYPE
UPDATE master.variable set description='Various set of plots with defined plot attribute values',abbrev='PLOT_TYPE' where abbrev = 'PLOT_TYPE';
--UPDATE: PLANTING_INSTRUCTIONS
UPDATE master.variable set description='Brief planting instructions or guidelines for field staff ',abbrev='PLANTING_INSTRUCTIONS' where abbrev = 'PLANTING_INSTRUCTIONS';
--UPDATE: DIST_BET_ROWS_IN_M
UPDATE master.variable set description='Space between rows',name='Distance between rows',abbrev='DIST_BET_ROWS_IN_M' where abbrev = 'DIST_BET_ROWS_IN_M';
--UPDATE: DISTANCE_BET_PLOTS_IN_M
UPDATE master.variable set description='Space between plots',name='Distance between plots',abbrev='DISTANCE_BET_PLOTS_IN_M' where abbrev = 'DISTANCE_BET_PLOTS_IN_M';
--UPDATE: PLOT_WIDTH
UPDATE master.variable set description='The width of plot',abbrev='PLOT_WIDTH' where abbrev = 'PLOT_WIDTH';
--UPDATE: PLOT_AREA_SQM_CONT
UPDATE master.variable set description='Plot size in square meters',abbrev='PLOT_AREA_SQM_CONT' where abbrev = 'PLOT_AREA_SQM_CONT';
--UPDATE: SEEDING_RATE
UPDATE master.variable set description='The seed showing rate like sparse, normal, dense',abbrev='SEEDING_RATE' where abbrev = 'SEEDING_RATE';
--UPDATE: ALLEY_LENGTH
UPDATE master.variable set description='The walking space between two ranges or/and columns',abbrev='ALLEY_LENGTH' where abbrev = 'ALLEY_LENGTH';
--UPDATE: NO_OF_BEDS_PER_PLOT
UPDATE master.variable set description='Total number of beds per plot',abbrev='NO_OF_BEDS_PER_PLOT' where abbrev = 'NO_OF_BEDS_PER_PLOT';
--UPDATE: NO_OF_ROWS_PER_BED
UPDATE master.variable set description='Total number of rows per bed',abbrev='NO_OF_ROWS_PER_BED' where abbrev = 'NO_OF_ROWS_PER_BED';
--UPDATE: PLOT_WIDTH_RICE
UPDATE master.variable set description='The width of plot',abbrev='PLOT_WIDTH_RICE' where abbrev = 'PLOT_WIDTH_RICE';
--UPDATE: PLOT_WIDTH_MAIZE_BED
UPDATE master.variable set description='The width of plot',abbrev='PLOT_WIDTH_MAIZE_BED' where abbrev = 'PLOT_WIDTH_MAIZE_BED';
--UPDATE: PLOT_WIDTH_WHEAT_FLAT
UPDATE master.variable set description='The width of plot',abbrev='PLOT_WIDTH_WHEAT_FLAT' where abbrev = 'PLOT_WIDTH_WHEAT_FLAT';
--UPDATE: BED_WIDTH
UPDATE master.variable set description='The width of bed',abbrev='BED_WIDTH' where abbrev = 'BED_WIDTH';
--UPDATE: PLOT_AREA_1
UPDATE master.variable set description='Plot size in square meters',name='Plot area',label='Plot area',abbrev='PLOT_AREA_1' where abbrev = 'PLOT_AREA_1';
--UPDATE: PLOT_AREA_2
UPDATE master.variable set description='Plot size in square meters',name='Plot area',label='Plot area',abbrev='PLOT_AREA_2' where abbrev = 'PLOT_AREA_2';
--UPDATE: PLOT_AREA_3
UPDATE master.variable set description='Plot size in square meters',name='Plot area',label='Plot area',abbrev='PLOT_AREA_3' where abbrev = 'PLOT_AREA_3';
--UPDATE: PLOT_AREA_4
UPDATE master.variable set description='Plot size in square meters',name='Plot area',label='Plot area',abbrev='PLOT_AREA_4' where abbrev = 'PLOT_AREA_4';
--UPDATE: HILLS_BY_PLOT
UPDATE master.variable set description='Hills by plot',abbrev='HILLS_BY_PLOT' where abbrev = 'HILLS_BY_PLOT';
--UPDATE: PLOT_DENSITY
UPDATE master.variable set description='Density of the plot',abbrev='PLOT_DENSITY' where abbrev = 'PLOT_DENSITY';
--UPDATE: PLOT_UNIT_MEASUREMENT
UPDATE master.variable set description='Plot unit measurement',abbrev='PLOT_UNIT_MEASUREMENT' where abbrev = 'PLOT_UNIT_MEASUREMENT';
--UPDATE: PLOT_LN_1
UPDATE master.variable set description='The length of the plot',name='Plot length',label='Plot length',abbrev='PLOT_LN_1' where abbrev = 'PLOT_LN_1';
--UPDATE: PLOT_WIDTH_WHEAT_BED
UPDATE master.variable set description='The width of the plot',name='Plot width',label='Plot width',abbrev='PLOT_WIDTH_WHEAT_BED' where abbrev = 'PLOT_WIDTH_WHEAT_BED';
--UPDATE: CONTCT_PERSON_CONT
UPDATE master.variable set description='The point person to contact about the study (usually the actual person who oversee the study in the field).',abbrev='CONTCT_PERSON_CONT' where abbrev = 'CONTCT_PERSON_CONT';
--UPDATE: REMARKS
UPDATE master.variable set description='Additional information about the record',abbrev='REMARKS' where abbrev = 'REMARKS';