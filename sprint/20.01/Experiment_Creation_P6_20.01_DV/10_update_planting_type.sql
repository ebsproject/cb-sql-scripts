update platform.config set config_value = '{
  	"Values": [
      {
  			"default": "Flat",
  			"disabled": false,
            "required": "required",
  			"field_label": "Planting Type",
  			"order_number": 1,
  			"variable_abbrev": "PLANTING_TYPE",
  			"field_description": "Planting Type",
            "planting_type_abbrevs" : [
                "PLANTING_TYPE_FLAT"
            ]
  		}
  	],
  	"Name": "Required experiment level planting protocol variables"
  }' , modifier_id = '25', notes='Remove bed as planting type option for Maize' where abbrev = 'MAIZE_PLANTING_TYPE';

  --Additions when no planting type is selected for maize
  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('MAIZE_PLOT_TYPE', 'Experiment Protocol Plot types for Maize with no selected planting type',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_2R5M",
                "PLOT_TYPE_MAIZE_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');


  update platform.config set config_value = '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
			"computed": "computed",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_2",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', modifier_id = 25, notes = 'added attribute computed for variable plot area' where abbrev = 'PLOT_TYPE_MAIZE_UNKNOWN';


  --Additions for wheat when no planting type is specified
  --List down all plot types
  INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage, creator_id, notes)
	VALUES ('WHEAT_PLOT_TYPE', 'Experiment Protocol Plot types for Wheat with no selected planting type ',  '{
  	"Values": [
        {
  			"default": false,
  			"disabled": false,
            "required": "required",
  			"field_label": "Plot Type",
  			"order_number": 1,
  			"variable_abbrev": "PLOT_TYPE",
  			"field_description": "Plot Type",
            "plot_type_abbrevs" : [
                "PLOT_TYPE_1SD",
                "PLOT_TYPE_1SD_0_7M",
                "PLOT_TYPE_1SD_1_5M",
                "PLOT_TYPE_1SD_3M",
                "PLOT_TYPE_1SD_5M",
                "PLOT_TYPE_1ST",
                "PLOT_TYPE_1ST_2_8M",
                "PLOT_TYPE_2SD",
                "PLOT_TYPE_2SD_3M",
                "PLOT_TYPE_2SD_5M",
                "PLOT_TYPE_2ST",
                "PLOT_TYPE_2ST_2_8M",
                "PLOT_TYPE_4SD",
                "PLOT_TYPE_4ST",
                "PLOT_TYPE_6SS_4M",
                "PLOT_TYPE_6SS_5M",
                "PLOT_TYPE_8SD",
                "PLOT_TYPE_FLAT_UNKNOWN",
                "PLOT_TYPE_BED_UNKNOWN"
            ]
  		}
  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', 1, 'experiment_creation', 1,'added by j.antonio');

update platform.config set config_value = '{
  	"Values": [
       {
  			"default": false,
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Rows per plot",
  			"order_number": 1,
  			"variable_abbrev": "ROWS_PER_PLOT_CONT",
  			"field_description": "Number of rows per plot"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Dist. bet. rows",
  			"order_number": 2,
  			"variable_abbrev": "DIST_BET_ROWS",
  			"field_description": "Distance between rows"
  		},
	    {
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Dist. bet. plots",
  			"order_number": 3,
  			"variable_abbrev": "DISTANCE_BET_PLOTS_IN_M",
  			"field_description": "Distance betweem plots"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Plot width",
  			"order_number": 3,
  			"variable_abbrev": "PLOT_WIDTH_WHEAT_FLAT",
  			"field_description": "Plot width"
  		},    
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
            "computed": "computed",
  			"field_label": "Plot length",
  			"order_number": 4,
  			"variable_abbrev": "PLOT_LN",
  			"field_description": "Plot length"
  		},
		{
  			"default": false,
            "unit": "sqm",  
  			"disabled": true,
            "required": "required",
            "computed": "computed",
  			"field_label": "Plot area",
  			"order_number": 5,
  			"variable_abbrev": "PLOT_AREA_3",
  			"field_description": "Plot area"
  		},
		{
  			"default": false,
            "unit": "m",  
  			"disabled": false,
            "required": "required",
  			"field_label": "Alley length",
  			"order_number": 6,
  			"variable_abbrev": "ALLEY_LENGTH",
  			"field_description": "Alley length"
  		}

  	],
  	"Name": "Required experiment level protocol plot type variables"
  }', modifier_id = 25, notes='updated the variables' where abbrev = 'PLOT_TYPE_FLAT_UNKNOWN';