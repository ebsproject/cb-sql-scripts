-- CORB-2617 EC - EC: Update ExperimentCreationWorker to process update of entry records for new sorting
--------------------------------------------------------------------------------------
UPDATE platform.config
    SET config_value=$${
        "createEntries": {
            "size": "500",
            "description": "Create entries of an Experiment"
        },
        "deleteEntries": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "updateEntries": {
            "size": "500",
            "description": "Update entries of an Experiment"
        },
        "createOccurrences": {
            "size": "500",
            "description": "Create occurrence plot and/or planting instruction records of an Experiment"
        },
        "deleteOccurrences": {
            "size": "500",
            "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
        },
        "reorderAllEntries": {
            "size": "200",
            "description": "Reorder all entries of an Experiment"
        }
    }
    $$
WHERE abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';