--ADD MANAGEMENT PROTOCOL

--1.Breeding Trial

insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT','Management Protocol',20,'Management Protocol','Management',1,'active','fa fa-tags');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT' then 4
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT_MOD','Management Protocol','Management Protocol','experimentCreation','protocol','management-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT');


--2.INTENTIONAL CROSSING NURSERY
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT','Management Protocol',20,'Management Protocol','Management',1,'active','fa fa-tags');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT' then 4
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT_MOD','Management Protocol','Management Protocol','experimentCreation','protocol','management-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT');

--3.GENERATION NURSERY
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT','Management Protocol',20,'Management Protocol','Management',1,'active','fa fa-tags');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT' then 4
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT_MOD','Management Protocol','Management Protocol','experimentCreation','protocol','management-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT');

--4.CROSS PARENT NURSERY (PHASE I)
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT','Management Protocol',20,'Management Protocol','Management',1,'active','fa fa-tags');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT' then 4
        else 4 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT_MOD','Management Protocol','Management Protocol','experimentCreation','protocol','management-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT');


--5.CROSS PARENT NURSERY (PHASE II)
insert into master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
values('CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT','Management Protocol',20,'Management Protocol','Management',1,'active','fa fa-tags');

--Additions for Protocols tab
--insert  in master.item_relation
insert into master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
select
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') as root_id,
    (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT') as parent_id,
    id as child_id,
  case
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT' then 4
        else 3 end as order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT');
;

--
insert into platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
('CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT_MOD','Management Protocol','Maangement Protocol','experimentCreation','protocol','management-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--Additions for Protocols tabs
insert into platform.item_module(item_id,module_id,creator_id,notes)
select
    id as item_id,
    (select id from platform.module where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT_MOD') as module_id,
    1,
    'added by j.antonio ' || now()
from
    master.item
where
abbrev in ('CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT');
