
--PLOT_TYPE_1R
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "m",
            "default": 0.2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_2"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1R';

--PLOT_TYPE_STAGE_I_II
update platform.config set config_value = '
 {
	"Name": "Required experiment level protocol plot type variables for Plot type Stage I-II",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "m",
			"default": 0.75,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": 0.75,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 3.75,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": 1,
			"disabled": true,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_STAGE_I_II';

--PLOT_TYPE_STAGE_III_IV
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type Stage III-IV",
	"Values": [{
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "m",
			"default": 0.75,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 7.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": 1,
			"disabled": true,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_STAGE_III_IV';

--PLOT_TYPE_MLN_FAW_SCREENING
update platform.config set config_value = '
    {
	"Name": "Required experiment level protocol plot type variables for Plot type MLN/FAW screening",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "m",
			"default": 0.75,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 3,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": 0.75,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 2.25,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": 1,
			"disabled": true,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_MLN_FAW_SCREENING';

--PLOT_TYPE_FOLIAR_DISEASE_SCREENING
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type Foliar disease screening",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "m",
			"default": 0.75,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 2.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": 0.75,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 1.875,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": 1,
			"disabled": true,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_FOLIAR_DISEASE_SCREENING';

--PLOT_TYPE_6R_X_25H
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"default": 25,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "HILLS_PER_ROW_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_HILLS"
		},
		{
			"unit": "m",
			"default": 5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN_1"
		},
		{
			"unit": "m",
			"default": 1.2,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_RICE"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_1"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_6R_X_25H';

--PLOT_TYPE_1C_6H_PGI_RIEGO
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PGI_Riego",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 8,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_1C_6H_PGI_RIEGO';

--PLOT_TYPE_1C_6H_PGII_RIEGO
update platform.config set config_value='
{
	"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PGII_Riego",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 6.4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_1C_6H_PGII_RIEGO';

--PLOT_TYPE_1C_6H_PC_RIEGO
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PC_Riego",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 1.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 2.4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }    
	]
}
' where abbrev = 'PLOT_TYPE_1C_6H_PC_RIEGO';

--PLOT_TYPE_1C_2H_HP_RIEGO
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 1C/2H_HP_Riego",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 0.8,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 0.2,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 0.16,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_1C_2H_HP_RIEGO';

--PLOT_TYPE_2C_2H_PG_SEQUIA
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 2C/2H_PG_Sequia",
	"Values": [{
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 3.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 5.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_2C_2H_PG_SEQUIA';

--PLOT_TYPE_2C_2H_PC_SEQUIA
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 2C/2H_PC_Sequia",
	"Values": [{
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 1.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 2.4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_2C_2H_PC_SEQUIA';

--PLOT_TYPE_2C_6H_PG_CALOR
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 2C/6H_PG_Calor",
	"Values": [{
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 3.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 5.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_2C_6H_PG_CALOR';

--PLOT_TYPE_2R
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 0.4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_2R';

--PLOT_TYPE_1C_6H_PC_CALOR
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 1C/6H_PC_Calor",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "BED_WIDTH"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_WHEAT_BED"
		},
		{
			"unit": "m",
			"default": 1.5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 2.4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_1C_6H_PC_CALOR';

--PLOT_TYPE_6SS_4M
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "m",
			"default": 0.18,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 0.4,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DISTANCE_BET_PLOTS_IN_M"
		},
		{
			"unit": "m",
			"default": 1.3,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": 4,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN_1"
		},
		{
			"unit": "sqm",
			"default": 5.2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_6SS_4M';

--PLOT_TYPE_1MELGA_8H
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type 1Melga/8H",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": 8,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": 1.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": 5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": 8,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_1MELGA_8H';

--PLOT_TYPE_MELGA_UNKNOWN
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables for Plot type Melga_unknown",
	"Values": [{
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "NO_OF_BEDS_PER_PLOT"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "NO_OF_ROWS_PER_BED"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"allow_new_val": false,
			"variable_abbrev": "PLOT_AREA_4"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_MELGA_UNKNOWN';

--PLOT_TYPE_5R
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 5,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 1,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_5R';

--PLOT_TYPE_2R_X_12H
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 2,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"default": 12,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "HILLS_PER_ROW_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_HILLS"
		},
		{
			"unit": "m",
			"default": 5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN_1"
		},
		{
			"unit": "m",
			"default": 0.4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_RICE"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_1"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_2R_X_12H';

--PLOT_TYPE_3R
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 3,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 0.6,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_3R';

--PLOT_TYPE_4R
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 4,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 0.8,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_4R';

--PLOT_TYPE_6R
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 6,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 1.2,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_6R';

--PLOT_TYPE_10R
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 10,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 2,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}

' where abbrev = 'PLOT_TYPE_10R';

--PLOT_TYPE_20R
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "m",
			"default": 4,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH"
		},
		{
			"unit": "m",
			"default": false,
			"computed": "computed",
			"disabled": false,
			"required": "required",
			"variable_abbrev": "PLOT_LN"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_2"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_20R';

--PLOT_TYPE_50R
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 50,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "m",
            "default": 10,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_2"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_50R';

--PLOT_TYPE_DIRECT_SEEDED_UNKNOWN
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "unit": "cm",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_2"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_DIRECT_SEEDED_UNKNOWN';

--PLOT_TYPE_10R_X_25H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 10,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 25,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_10R_X_25H';

--PLOT_TYPE_10R_X_26H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 10,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 26,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_10R_X_26H';

--PLOT_TYPE_1R_X_12H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 12,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 0.2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1R_X_12H';

--PLOT_TYPE_1R_X_25H"
update platform.config set config_value = '
{
	"Name": "Required experiment level protocol plot type variables",
	"Values": [{
			"default": 1,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "ROWS_PER_PLOT_CONT"
		},
		{
			"default": 25,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "HILLS_PER_ROW_CONT"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_ROWS"
		},
		{
			"unit": "cm",
			"default": 20,
			"disabled": true,
			"required": "required",
			"variable_abbrev": "DIST_BET_HILLS"
		},
		{
			"unit": "m",
			"default": 5,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_LN_1"
		},
		{
			"unit": "m",
			"default": 0.2,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_WIDTH_RICE"
		},
		{
			"unit": "sqm",
			"default": false,
			"computed": "computed",
			"disabled": true,
			"required": "required",
			"variable_abbrev": "PLOT_AREA_1"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": "required",
			"variable_abbrev": "ALLEY_LENGTH"
		},
		{
			"unit": "m",
			"default": false,
			"disabled": false,
			"required": false,
			"allow_new_val": true,
			"variable_abbrev": "SEEDING_RATE"
		},
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
	]
}
' where abbrev = 'PLOT_TYPE_1R_X_25H';

--PLOT_TYPE_1SD
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1SD';

--PLOT_TYPE_1SD_0_7M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 0.7,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 0.28,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1SD_0_7M';

--PLOT_TYPE_1SD_1_5M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 1.5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 0.6,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1SD_1_5M';

--PLOT_TYPE_7R_X_25H"
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 7,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 25,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 1.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_7R_X_25H';

--PLOT_TYPE_1SD_3M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 3,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 1.2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1SD_3M';

--PLOT_TYPE_1SD_5M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1SD_5M';

--PLOT_TYPE_1ST
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 3,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1ST';

--PLOT_TYPE_1ST_2_8M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 1,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 3,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 2.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 1.12,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_1ST_2_8M';

--PLOT_TYPE_2R5M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "unit": "m",
            "default": 1.5,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "m",
            "default": 3,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH"
        },
        {
            "unit": "m",
            "default": 5,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "sqm",
            "default": 15,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_2"
        },
        {
            "unit": "m",
            "default": 0.8,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_2R5M';

--PLOT_TYPE_2R_X_25H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 25,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_2R_X_25H';

--PLOT_TYPE_2SD
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_2SD';

--PLOT_TYPE_2SD_3M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 3,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 2.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_2SD_3M';

--PLOT_TYPE_7R_X_26H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 7,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 26,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 1.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },{
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }

    ]
}
' where abbrev = 'PLOT_TYPE_7R_X_26H';

--PLOT_TYPE_7R_X_32H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 7,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 32,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 1.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_7R_X_32H';

--PLOT_TYPE_2SD_5M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_2SD_5M';

--PLOT_TYPE_2ST
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 3,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_2ST';

--PLOT_TYPE_2ST_2_8M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 2,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
        },
        {
            "unit": "m",
            "default": 3,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
        },
        {
            "unit": "m",
            "default": 0.4,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "BED_WIDTH"
        },
        {
            "unit": "m",
            "default": 0.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
        },
        {
            "unit": "m",
            "default": 2.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN"
        },
        {
            "unit": "sqm",
            "default": 2.24,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_4"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_2ST_2_8M';

--PLOT_TYPE_4R_X_25H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 4,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
            "default": 25,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "HILLS_PER_ROW_CONT"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_ROWS"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "variable_abbrev": "DIST_BET_HILLS"
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_LN_1"
        },
        {
            "unit": "m",
            "default": 0.8,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_WIDTH_RICE"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "variable_abbrev": "PLOT_AREA_1"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ALLEY_LENGTH"
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE"
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_4R_X_25H';

--PLOT_TYPE_TRANSPLANTED_UNKNOWN
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
            
        },
        {
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "HILLS_PER_ROW_CONT"
            
        },
        {
            "unit": "cm",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_ROWS"
            
        },
        {
            "unit": "cm",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_HILLS"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN_1"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_RICE"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_1"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_TRANSPLANTED_UNKNOWN';

--PLOT_TYPE_4SD"
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 4,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
            
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "BED_WIDTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_4"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_4SD';

--PLOT_TYPE_4ST
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 4,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
            
        },
        {
            "unit": "m",
            "default": 3,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "BED_WIDTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_4"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_4ST';

--PLOT_TYPE_5R_X_12H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 5,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
            
        },
        {
            "default": 12,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "HILLS_PER_ROW_CONT"
            
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_ROWS"
            
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_HILLS"
            
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN_1"
            
        },
        {
            "unit": "m",
            "default": 1,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_RICE"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_1"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_5R_X_12H';

--PLOT_TYPE_5R_X_25H
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 5,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
            
        },
        {
            "default": 25,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "HILLS_PER_ROW_CONT"
            
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_ROWS"
            
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_HILLS"
            
        },
        {
            "unit": "m",
            "default": 5,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN_1"
            
        },
        {
            "unit": "m",
            "default": 1,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_RICE"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_1"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
          {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_5R_X_25H';

--PLOT_TYPE_6SS_5M
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 6,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
            
        },
        {
            "unit": "m",
            "default": 0.18,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_ROWS"
            
        },
        {
            "unit": "m",
            "default": 0.4,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "DISTANCE_BET_PLOTS_IN_M"
            
        },
        {
            "unit": "m",
            "default": 1.3,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH"
            
        },
        {
            "unit": "m",
            "default": 5,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN_1"
            
        },
        {
            "unit": "sqm",
            "default": 6.5,
            "disabled": true,
            "required": "required",
            
             "variable_abbrev": "PLOT_AREA_2"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
           {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_6SS_5M';

--PLOT_TYPE_8SD
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": 8,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
            
        },
        {
            "unit": "m",
            "default": 2,
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "BED_WIDTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_4"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
        {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_8SD';

--PLOT_TYPE_BED_UNKNOWN
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "NO_OF_BEDS_PER_PLOT"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "NO_OF_ROWS_PER_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "BED_WIDTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_MAIZE_BED"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_4"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
          {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_BED_UNKNOWN';

--PLOT_TYPE_FLAT_UNKNOWN
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ROWS_PER_PLOT_CONT"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_ROWS"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "DISTANCE_BET_PLOTS_IN_M"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH_WHEAT_FLAT"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
            "variable_abbrev": "PLOT_AREA_3"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
           {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_FLAT_UNKNOWN';

--PLOT_TYPE_MAIZE_UNKNOWN
update platform.config set config_value = '
{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [
        {
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ROWS_PER_PLOT_CONT"

        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "DIST_BET_ROWS"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_WIDTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "PLOT_LN"
            
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            
             "variable_abbrev": "PLOT_AREA_2"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": "required",
            
            "variable_abbrev": "ALLEY_LENGTH"
            
        },
        {
            "unit": "m",
            "default": false,
            "disabled": false,
            "required": false,
            
            "allow_new_val": true,
             "variable_abbrev": "SEEDING_RATE"
            
        },
         {
            "default": false,
            "disabled": false,
            "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
    ]
}
' where abbrev = 'PLOT_TYPE_MAIZE_UNKNOWN';
