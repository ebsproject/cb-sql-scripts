/** 
  Protocols order:
       1. Planting
       2. Pollination
       3. Traits
       4. Management
       5. Harvest
*/
--1. Breeding Trial
--Pollination
update
    master.item_relation set order_number = 2 
where  
   root_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT');

--Traits
update
    master.item_relation set order_number = 3 
where  
   root_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT');


--Management
update
    master.item_relation set order_number = 4
where  
   root_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_MANAGEMENT_PROTOCOL_ACT');

--Harvest
update
    master.item_relation set order_number = 5
where  
   root_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'BREEDING_TRIAL_HARVEST_PROTOCOL_ACT');

--2. INTENTIONAL CROSSING NURSERY
--Pollination
update
    master.item_relation set order_number = 2 
where  
   root_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_POLLINATION_PROTOCOL_ACT');

--Traits
update
    master.item_relation set order_number = 3 
where  
   root_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT');


--Management
update
    master.item_relation set order_number = 4
where  
   root_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGEMENT_PROTOCOL_ACT');

--Harvest
update
    master.item_relation set order_number = 5
where  
   root_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT');


--3. GENERATION NURSERY
--Pollination
update
    master.item_relation set order_number = 2 
where  
   root_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT');

--Traits
update
    master.item_relation set order_number = 3 
where  
   root_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT');


--Management
update
    master.item_relation set order_number = 4
where  
   root_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_MANAGEMENT_PROTOCOL_ACT');

--Harvest
update
    master.item_relation set order_number = 5
where  
   root_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'GENERATION_NURSERY_HARVEST_PROTOCOL_ACT');


--4. CROSS PARENT NURSERY PHASE I
--Pollination
update
    master.item_relation set order_number = 2 
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_POLLINATION_PROTOCOL_ACT');

--Traits
update
    master.item_relation set order_number = 3 
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT');


--Management
update
    master.item_relation set order_number = 4
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_MANAGEMENT_PROTOCOL_ACT');

--Harvest
update
    master.item_relation set order_number = 5
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT');



--5. CROSS PARENT NURSERY PHASE II
--Pollination
update
    master.item_relation set order_number = 2 
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_POLLINATION_PROTOCOL_ACT');

--Traits
update
    master.item_relation set order_number = 3 
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT');


--Management
update
    master.item_relation set order_number = 4
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_MANAGEMENT_PROTOCOL_ACT');

--Harvest
update
    master.item_relation set order_number = 5
where  
   root_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
   and parent_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT')
   and child_id = (select id from master.item where abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT');
