--UPDATE HARVEST REMARKS FROM THE EXPERIMENT TYPE PROTOCOLS

--CONFIGS
 
 --1.BREEDING TRIAL
  UPDATE platform.config set config_value = '{
      "Name": "Required experiment level harvest protocol variables for Breeding Trial data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HARVEST_INSTRUCTIONS"
          }
      ]
  }' WHERE abbrev = 'BREEDING_TRIAL_HARVEST_PROTOCOL_ACT_VAL';

--2.INTENTIONAL CROSSING NURSERY
  UPDATE platform.config set config_value = '{
      "Name": "Required experiment level harvest protocol variables for Intentional Crossing Nursery data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HARVEST_INSTRUCTIONS"
          }
      ]
  }' WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL';

--3.GENERATION NURSERY
  UPDATE platform.config set config_value = '{
      "Name": "Required experiment level harvest protocol variables for Generation Nursery data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HARVEST_INSTRUCTIONS"
          }
      ]
  }' WHERE abbrev = 'GENERATION_NURSERY_HARVEST_PROTOCOL_ACT_VAL';

--4.CROSS PARENT NURSERY PHASE I
  UPDATE platform.config set config_value = '{
      "Name": "Required experiment level harvest protocol variables for Cross Parent Nursery Phase I data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HARVEST_INSTRUCTIONS"
          }
      ]
  }' WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT_VAL';

--5.CROSS PARENT NURSERY PHASE II
UPDATE platform.config set config_value = '{
      "Name": "Required experiment level harvest protocol variables for Cross Parent Nursery Phase II data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HARVEST_INSTRUCTIONS"
          }
      ]
  }' WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_HARVEST_PROTOCOL_ACT_VAL';