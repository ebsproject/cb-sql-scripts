--Updates are based on the new data model implemented
--@author j.antonio

--Experiment Templates
/**
"EXPT_NURSERY_CB_BASIC_INFO_ACT_VAL"
"EXPT_NURSERY_CROSS_LIST_BASIC_INFO_ACT_VAL"
"EXPT_CROSS_PRE_PLANNING_BASIC_INFO_ACT_VAL"
"EXPT_TRIAL_IRRI_BASIC_INFO_ACT_VAL"
"EXPT_SEED_INCREASE_BASIC_INFO_ACT_VAL"
"EXPT_TRIAL_BASIC_INFO_ACT_VAL"
"EXPT_SELECTION_ADVANCEMENT_BASIC_INFO_ACT_VAL"
"EXPT_NURSERY_CROSS_LIST_IRRIHQ_BASIC_INFO_ACT_VAL"
"EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_VAL"
"EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL"
"EXPT_NURSERY_PARENT_LIST_BASIC_INFO_ACT_VAL"
*/

--EXPT_TRIAL_BASIC_INFO_ACT_VAL
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for trial data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
			"secondary_target_column":"itemDbId",
			"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
			"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
      "target_value":"programCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
			"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
      "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
			"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
      "target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
			"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"IYT",
				"OYT",
				"PYT",
				"AYT",
				"MET0",
				"MET1",
				"MET2",
				"RYT"
			],
			"target_column": "stageDbId",
      "target_value":"stageCode",
			"api_resource_method": "POST",
			"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
			"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
   	{
			"disabled": false,
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
			"allowed_values": [
				"First Year",
				"Second Year",
				"Re Test",
				"Germplasm Viability"
			],
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
    {
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
			"target_value":"seasonCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
			"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
			"disabled": false,
			"required": "required",
			"variable_type" : "identification",
			"variable_abbrev": "PLANTING_SEASON"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
			"secondary_target_column": "personDbId",
			"target_value":"personName",
			"api_resource_method": "GET",
			"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
			"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
			"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		},
		{
			"default": false,
			"disabled": false,
			"required": "required",
			"variable_type" : "metadata",
			"variable_abbrev" : "HAS_EXPERIMENT_GROUP"
		}
	]
}
'  where abbrev='EXPT_TRIAL_BASIC_INFO_ACT_VAL';


---EXPT_TRIAL_IRRI_BASIC_INFO_ACT_VAL
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for IRRI trial data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
      "target_value":"programCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
      "target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
		{
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"IYT",
				"OYT",
				"PYT",
				"AYT",
				"MET0",
				"MET1",
				"MET2",
				"RYT"
			],
			"target_column": "stageDbId",
      "target_value":"stageCode",
			"api_resource_method": "POST",
		  "api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
   	{
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
			"allowed_values": [
				"First Year",
				"Second Year",
				"Re Test",
				"Germplasm Viability"
			],
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
    {
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
      "target_value":"seasonCode",
		  "api_resource_method": "GET",
			"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "PLANTING_SEASON"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
			"secondary_target_column": "personDbId",
			"target_value":"personName",
			"api_resource_method": "GET",
			"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		},
		{
			"default": false,
			"disabled": false,
			"required": "required",
			"variable_type" : "metadata",
			"variable_abbrev": "HAS_EXPERIMENT_GROUP"
		}
	]
}
'  where abbrev='EXPT_TRIAL_IRRI_BASIC_INFO_ACT_VAL';



--EXPT_NURSERY_CB_BASIC_INFO_ACT_VAL

update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for nursery crossing block data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": "Nursery",
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
      "target_value":"programCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
      "target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "HB",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"HB",
				"F1"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
			"api_resource_method": "POST",
		  "api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"default": "Crossing Block",
			"disabled": false,
			"allowed_values": [
				"Crossing Block"
			],
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
"target_value":"seasonCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "PLANTING_SEASON"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
			"secondary_target_column": "personDbId",
			"target_value":"personName",
			"api_resource_method": "GET",
			"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},	
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_NURSERY_CB_BASIC_INFO_ACT_VAL';

--EXPT_NURSERY_CROSS_LIST_BASIC_INFO_ACT_VAL

update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for nursery-cross list data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": "Nursery",
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   "target_value":"programCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
  "target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "HB",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"HB",
				"F1"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
			"api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"default": "Crossing Block",
			"disabled": false,
			"allowed_values": [
				"Crossing Block"
			],
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
"target_value":"seasonCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "PLANTING_SEASON"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method": "GET",
				"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_NURSERY_CROSS_LIST_BASIC_INFO_ACT_VAL';

--EXPT_CROSS_PRE_PLANNING_BASIC_INFO_ACT_VAL
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for cross pre-planning data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
			"secondary_target_column":"itemDbId",
			"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   "target_value":"programCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
      "target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "HB",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"HB",
				"F1"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
			"api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"default": "Crossing Block",
			"disabled": false,
			"allowed_values": [
				"Crossing Block"
			],
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
"target_value":"seasonCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "PLANTING_SEASON"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method": "GET",
				"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev= 'EXPT_NURSERY_CROSS_LIST_BASIC_INFO_ACT_VAL';

--EXPT_SEED_INCREASE_BASIC_INFO_ACT_VAL
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for seed increase data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   "target_value":"programCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
  "target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "BRE",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"BRE",
				"SEM"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
			"api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
"target_value":"seasonCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "PLANTING_SEASON"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method": "GET",
				"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_SEED_INCREASE_BASIC_INFO_ACT_VAL';



--"EXPT_SELECTION_ADVANCEMENT_BASIC_INFO_ACT_VAL"
update platform.config set config_value='
{
	"Name": "Required experiment level metadata variables for Selection and Advancement data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   "target_value":"programCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
  		"target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"F2",
				"F3",
				"F4",
				"F5",
				"F6",
				"F7",
				"F8",
				"F9"
			],
			"target_column": "stageDbId",
      "target_value":"stageCode",
			"api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
"target_value":"seasonCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
      "disabled": false,
      "required": "required",
      "variable_abbrev": "PLANTING_SEASON"
    },
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method": "GET",
				"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_SELECTION_ADVANCEMENT_BASIC_INFO_ACT_VAL';

 
--"EXPT_NURSERY_CROSS_LIST_IRRIHQ_BASIC_INFO_ACT_VAL"
update platform.config set config_value='
{
	"Name": "Required experiment level metadata variables for seed increase data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   		"target_value":"programCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},		
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
  		"target_value": "projectCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "HB",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"F1",
				"HB"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
		  "api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
			"target_value":"seasonCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
      "disabled": false,
      "required": "required",
      "variable_abbrev": "PLANTING_SEASON"
    },
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method" : "GET",
			"api_resource_endpoint" : "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_BASIC_INFO_ACT_VAL';


--"EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_VAL"
update platform.config set config_value='
{
	"Name": "Required experiment level metadata variables for Selection and Advancement data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   		"target_value":"programCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "programDbId",
   		"target_value":"projectCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"F2",
				"F3",
				"F4",
				"F5",
				"F6",
				"F7",
				"F8",
				"F9",
				"PN"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
		  "api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
			"target_value":"seasonCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
      "disabled": false,
      "required": "required",
      "variable_abbrev": "PLANTING_SEASON"
    },
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method" : "GET",
			"api_resource_endpoint" : "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_VAL';


--"EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL"
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for seed increase data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": false,
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   		"target_value":"programCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
			"api_resource_filter": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
      "target_value": "projectCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "BRE",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"BRE",
				"SEM"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
		 "api_resource_method": "POST",
		 "api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"disabled": false,
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
"target_value":"seasonCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
      "disabled": false,
      "required": "required",
      "variable_abbrev": "PLANTING_SEASON"
    },
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method" : "GET",
			"api_resource_endpoint" : "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL';


--"EXPT_NURSERY_PARENT_LIST_BASIC_INFO_ACT_VAL"
update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for nursery-parent list data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": "Nursery",
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   		"target_value":"programCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},		
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  	  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
  		"target_value": "projectCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "HB",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"HB"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
		"api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"default": "Crossing Block",
			"disabled": false,
			"allowed_values": [
				"Crossing Block"
			],
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
			"target_value":"seasonCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
      "disabled": false,
      "required": "required",
      "variable_abbrev": "PLANTING_SEASON"
    },
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method" : "GET",
			"api_resource_endpoint" : "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_NURSERY_PARENT_LIST_BASIC_INFO_ACT_VAL';


--EXPT_CROSS_POST_PLANNING_BASIC_INFO_ACT_VAL

update platform.config set config_value = '
{
	"Name": "Required experiment level metadata variables for nursery-cross list data process",
	"Values": [{
			"default": false,
			"disabled": true,
			"required": "required",
			"target_column": "dataProcessDbId",
"secondary_target_column":"itemDbId",
"target_value":"name",
			"api_resource_method" : "POST",
			"api_resource_endpoint" : "items-search",
      "api_resource_filter" : {"processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
			"api_resource_sort": "sort=name", 
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TEMPLATE"
		},
		{
			"default": "Nursery",
			"disabled": true,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_TYPE"
		},
		{
			"default": "RICE",
			"disabled": true,
			"required": "required",
			"target_column": "cropDbId",
			"target_value" : "cropCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "crops",
			"api_resource_sort": "sort=cropCode",
		"variable_type" : "identification",
			"variable_abbrev": "CROP"
		},
		{
			"disabled": true,
			"required": "required",
			"target_column": "programDbId",
   "target_value":"programCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "programs",
			"api_resource_sort": "sort=programCode",
				"variable_type" : "identification",
			"variable_abbrev": "PROGRAM"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "pipelineDbId",
  "target_value":"pipelineCode",
			"api_resource_method" : "GET",
			"api_resource_endpoint" : "pipelines",
			"api_resource_sort": "sort=pipelineCode",
						"variable_type" : "identification",
			"variable_abbrev": "PIPELINE"
		},
		{
			"disabled": false,
			"allow_new_val": true,
			"target_column": "projectDbId",
  "target_value": "projectCode",
			"api_resource_method": "GET",
			"api_resource_endpoint": "projects",
			"api_resource_sort": "sort=projectCode",
					"variable_type" : "identification",
			"variable_abbrev": "PROJECT"
		},
		{
			"default" : "EXPT-XXXX",
			"disabled": true,
			"required": "required",
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_CODE"
		},
		{
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_NAME"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_OBJECTIVE"
		},
		{
			"default": "HB",
			"disabled": false,
			"required": "required",
			"allowed_values": [
				"HB",
				"F1"
			],
			"target_column": "stageDbId",
"target_value":"stageCode",
			"api_resource_method": "POST",
		"api_resource_endpoint": "stages-search",
			"api_resource_sort": "sort=stageCode",
					"variable_type" : "identification",
			"variable_abbrev": "STAGE"
		},
    {
			"disabled": false,
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_TYPE"
		},
		{
			"default": "Crossing Block",
			"disabled": false,
			"allowed_values": [
				"Crossing Block"
			],
					"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
		},
		{
			"disabled": false,
			"required": "required",
			
				"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_YEAR"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "seasonDbId",
"target_value":"seasonCode",
			"api_resource_method": "GET",
				"api_resource_endpoint": "seasons",
			"api_resource_sort": "sort=seasonCode",
		"variable_type" : "identification",
			"variable_abbrev": "SEASON"
		},
    {
			"disabled": false,
			"required": "required",
				"variable_type" : "identification",
			"variable_abbrev": "PLANTING_SEASON"
		},
		{
			"disabled": false,
			"required": "required",
			"target_column": "stewardDbId",
"secondary_target_column": "personDbId",
"target_value":"personName",

			"api_resource_method": "GET",
				"api_resource_endpoint": "persons",
			"api_resource_sort": "sort=personName",
		"variable_type" : "identification",
			"variable_abbrev": "EXPERIMENT_STEWARD"
		},
		{
			"disabled": false,
				"variable_type": "identification",
			"variable_abbrev": "DESCRIPTION"
		}
	]
}
' where abbrev = 'EXPT_CROSS_POST_PLANNING_BASIC_INFO_ACT_VAL';
