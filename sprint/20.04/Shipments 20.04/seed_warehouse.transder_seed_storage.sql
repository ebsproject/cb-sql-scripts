-- Function: seed_warehouse.transfer_seed_storage(integer, integer)

-- DROP FUNCTION seed_warehouse.transfer_seed_storage(integer, integer);

CREATE OR REPLACE FUNCTION seed_warehouse.transfer_seed_storage(
    var_shipment_id integer,
    var_user_id integer)
  RETURNS character varying AS
$BODY$
DECLARE
    var_program_name varchar;
    r_shipment RECORD;
    r_seed_storage RECORD;
    r_seed_storage_metadata RECORD;
    var_volume double precision;
    var_unit character varying;
    var_gdate date;
    var_receiver_id integer;
    var_shipper_id integer;
    germuid integer;
    var_glocn integer;
    var_method_id integer;
    var_gpid1 integer;
    gdate integer;
    var_count integer;
    var_arr VARCHAR[];
    var_max NUMERIC;
    var_new_gid numeric;
    lgid numeric;
    var_new_nid numeric;
    var_name varchar;
    var_date integer;
    var_new_seed_storage_id INTEGER;
    var_new_seed_storage_log_id INTEGER;
    var_program_place_id INTEGER;
    var_new_seed_lot_id NUMERIC;
    var_method_type CHARACTER VARYING;
    var_gid_type CHARACTER VARYING;
    var_new_product_gid_id INTEGER;
    var_name_type CHARACTER VARYING;
    designation_val CHARACTER VARYING;
    var_facility_id INTEGER;
    var_container_id INTEGER;
    var_old_ntype INTEGER;
    var_ndate integer;
    var_nlocn integer;
    var_nuid bigint;
    var_seed_storage_qr_code text;
BEGIN
    FOR r_shipment in
    select * from seed_warehouse.shipment_item where shipment_id = var_shipment_id and volume is not null and is_void = false and status != 'RECEIVED'
    LOOP

        FOR r_seed_storage in
        select * from operational.seed_storage where id = r_shipment.seed_storage_id
        LOOP
            -- get values
            var_volume := r_shipment.volume;
            RAISE NOTICE 'label % volume %', r_seed_storage.label, var_volume;
            var_unit := r_shipment.unit;

            var_receiver_id := null;
            /*select receiver_program_id 
            into var_receiver_id
            from seed_warehouse.shipment
            where id = var_shipment_id;*/

            var_shipper_id := null;
            /*select shipper_id
            into var_shipper_id
            from seed_warehouse.shipment
            where id = var_shipment_id;*/

            select receiver_program_id, shipper_id
            into var_receiver_id, var_shipper_id
            from seed_warehouse.shipment
            where id = var_shipment_id;

            var_gdate := null;
            select value into var_gdate
            from seed_warehouse.shipment_metadata
            where shipment_id = var_shipment_id
            and is_void = false
            and variable_id = 1948; --1898; -- 

            --get program
            var_program_name := null;

            select abbrev into var_program_name
            from master.program where id = var_receiver_id;

            ---for gms.germplsm
            --getting the user id
            germuid := NULL;
            select value into germuid
            from master.user_metadata 
            where user_id = var_shipper_id and variable_id = 582;

            IF germuid IS NULL THEN germuid = 2; END IF;

            --getting the location id
            var_glocn := NULL;
            var_method_id := 62; --default for import (MAN)
            var_gpid1 := NULL;

            select gpid1 into var_gpid1
            from gms.germplsm
            where gid = r_seed_storage.gid;

            select mppm.value into var_glocn
            from master.program_place mpp, master.program_place_metadata mppm
            where mpp.program_id = var_receiver_id and mpp.place_id = 10001
            and mpp.id = mppm.program_place_id and mppm.variable_id = 583;

            --add default for glocn
            IF var_glocn IS NULL
            THEN
                var_glocn := 9000;
            END IF;
    

            --getting the harvest date
            gdate := NULL;
            gdate = to_char(var_gdate, 'YYYYMMDD');

            --getting the next available gid
            SELECT COUNT(*) INTO var_count FROM gms.germplsm;

            var_max := NULL;
            var_count := NULL;

            IF var_count <= 0
            THEN
                SELECT ugid INTO var_new_gid FROM gms.instln;
            ELSE
                SELECT MAX(gid) INTO var_max FROM gms.germplsm WHERE gid >= 300000001 AND gid < 400000000;

                IF var_max Is NULL
                THEN
                    var_new_gid := 300000001;
                ELSE
                    var_new_gid := var_max + 1;
                END IF;
    
            END IF;

            --getting the lgid
            lgid := -(var_new_gid);


            --creating new record in germplasm
            INSERT INTO gms.germplsm (gid,methn,gnpgs,gpid1,gpid2,germuid,lgid,glocn,gdate,gref,grplce,mgid)
            VALUES (
                var_new_gid,var_method_id,-1,var_gpid1,r_seed_storage.gid,germuid,lgid,var_glocn,gdate,0,0,0
            );


            ---for gms.names

            --getting the next available nid
            var_max := NULL;
            var_new_nid := NULL;

            SELECT MAX(nid) INTO var_max FROM gms.names WHERE nid >= 300000001 AND nid < 400000000;

            IF var_max IS NULL
            THEN 
                var_new_nid := 300000001;
            ELSE
                var_new_nid := var_max + 1;
            END IF;

            -- set the default
            var_name := NULL;
            var_old_ntype := 2;
            var_ndate := 0;
            var_nlocn := 0;
            
            -- get the values of source
            select gn.nval,gn.ntype into var_name,var_old_ntype
            from gms.names gn
            where gn.gid = r_seed_storage.gid
            and gn.nstat = 1;
            
            IF var_name IS NULL
            THEN
                select mp.designation into var_name 
                from master.product mp, master.product_gid mpg
                where mpg.product_id = mp.id
                and mpg.gid = r_seed_storage.gid;
                
                select gms.fldno into var_old_ntype 
                from 
                    master.product_name mpn
                        left join (
                            select 
                                regexp_replace(regexp_replace(lower(fname), '\''', '','g'), '(\s)+', '_','g')as fname,
                                fname as name_type,
                                fldno 
                            from 
                                gms.udflds 
                            where 
                                ftable = 'NAMES' and ftype = 'NAME'
                        )gms on gms.fname = mpn.name_type,
                    master.product_gid mpg
                where 
                    mpn.value=var_name
                    and mpn.product_gid_id=mpg.id
                    and mpg.gid = r_seed_storage.gid;
            END IF;
            --getting the current date
            var_date := to_char(current_timestamp, 'YYYYMMDD');

            raise notice 'ntype: %',var_old_ntype;

            --creating new record in names
            INSERT INTO gms.names (nid,gid,ntype,nstat,nuid,nval,nlocn,ndate,nref)
            VALUES (
                var_new_nid, var_new_gid, var_old_ntype, 1, germuid, var_name, var_nlocn, var_ndate, 0
            );

            --change the value of the seed_lot_id by etenorio (RBIMS-3480)
            var_new_seed_lot_id = r_seed_storage.seed_lot_id;
            
            -- get the value of var_facility_id
            var_facility_id := null;
            SELECT value into var_facility_id
            FROM seed_warehouse.shipment_metadata
            WHERE shipment_id = r_shipment.shipment_id
            AND variable_id = 852;

            -- get the value of var_container_id
            var_container_id := null;
            SELECT value into var_container_id
            FROM seed_warehouse.shipment_metadata
            WHERE shipment_id = r_shipment.shipment_id
            AND variable_id = 1949;

            --get an nuid 
             select nuid into var_nuid from z_admin.generate_nuid();

             var_seed_storage_qr_code = var_new_seed_lot_id || '.' || var_nuid;
             raise notice 'seed storage qr code: %',var_seed_storage_qr_code;
             
            --insert new seed storage
            --- add facility_id, container_id and shipment_id
            INSERT INTO operational.seed_storage (
                product_id, seed_lot_id, key_type, seed_manager, gid, volume, unit, seeds_acquired_date, label, 
                original_storage_id, creation_timestamp, creator_id, notes, is_void, product_gid_id, 
                facility_id, container_id, shipment_id, seed_storage_qr_code
            ) VALUES(
                r_seed_storage.product_id, var_new_seed_lot_id, r_seed_storage.key_type, var_program_name, var_new_gid, 
                var_volume, var_unit, var_gdate, r_seed_storage.label,
                NULL, CURRENT_TIMESTAMP, var_shipper_id, 'created via transfer seed storage (shipment tool)', FALSE, r_seed_storage.product_gid_id,
                var_facility_id, var_container_id, r_shipment.shipment_id, var_seed_storage_qr_code
            ) returning id into var_new_seed_storage_id;


            RAISE NOTICE 'var_new_seed_storage_id %', var_new_seed_storage_id;

            --creating seed storage log

            --deposit
            INSERT INTO operational.seed_storage_log (
                seed_storage_id, encoder_id, encode_timestamp, transaction_type, volume, unit, event_timestamp,
            sender, creation_timestamp, creator_id, notes, is_void
            ) VALUES(
                var_new_seed_storage_id, var_shipper_id, CURRENT_TIMESTAMP, 'deposit', var_volume, var_unit, CURRENT_TIMESTAMP,
                r_seed_storage.label, CURRENT_TIMESTAMP, var_shipper_id,'created via transfer seed storage (shipment tool)', FALSE
            ) returning id into var_new_seed_storage_log_id;

            --withdraw
            INSERT INTO operational.seed_storage_log (
                seed_storage_id, encoder_id, encode_timestamp, transaction_type, volume, unit, event_timestamp,
            receiver, creation_timestamp, creator_id, notes, is_void
            ) VALUES(
                r_seed_storage.id, var_shipper_id, CURRENT_TIMESTAMP, 'withdraw', var_volume, var_unit, CURRENT_TIMESTAMP,
                r_seed_storage.label, CURRENT_TIMESTAMP, var_shipper_id,'created via transfer seed storage (shipment tool)', FALSE
            ) returning id into var_new_seed_storage_log_id;

            -- get gid_type --added by jpr
            var_method_type := null;
            select m.mtype into var_method_type from gms.germplsm g
            join gms.methods m on m.mid = g.methn
            where g.gid = CAST(r_seed_storage.gid as INTEGER) and g.grplce = 0;

            if var_method_type = 'GEN' THEN
                var_gid_type = 'cross';
            elsif var_method_type = 'DER' THEN
                var_gid_type = 'derivative';
            else
                var_gid_type = 'fixed';
            end if;

            --insert new product_gid --added by jpr
            INSERT INTO master.product_gid(product_id, gid, gid_type, creator_id, notes)
            values(r_seed_storage.product_id,var_new_gid, var_gid_type,var_shipper_id,'created via transfer seed storage (shipment tool)')
            returning id into var_new_product_gid_id;

            --update product_gid_id of new seed storage record --added by jpr
            UPDATE operational.seed_storage SET product_gid_id = var_new_product_gid_id where id = var_new_seed_storage_id;

            -- get name_type --added by jpr
            var_name_type := null;
            select lower(name_type) into var_name_type from master.product_name
            where product_id = CAST(r_seed_storage.product_id as INTEGER)
            and value = var_name;

            if var_name_type IS NOT NULL
            then
                var_name_type = replace(replace(lower(trim(var_name_type)), ' ', '_'), E'''','');
            else
                -- get the corresponding name in gms 20170907 - fix in RBIMS-5946
                select lower(replace(fname,' ','_')) into var_name_type
                from gms.udflds
                where fldno = var_old_ntype;
            end if;

            --select designation
            select designation into designation_val from master.product where id = r_seed_storage.product_id;

            -- insert to master.product_name --added by jpr
            insert into master.product_name(product_id, name_type, value, notes, product_gid_id, creator_id)
            values(r_seed_storage.product_id, var_name_type, designation_val, 'created via transfer seed storage (shipment tool)', var_new_product_gid_id, var_shipper_id);

            --insert in operational.seed_storage_metadata
            FOR r_seed_storage_metadata in
            select * from operational.seed_storage_metadata where seed_storage_id = r_seed_storage.id
            LOOP

                INSERT INTO operational.seed_storage_metadata (
                    seed_storage_id, variable_id, value, notes
                ) VALUES (
                    var_new_seed_storage_id, r_seed_storage_metadata.variable_id, r_seed_storage_metadata.value, 'created via transfer seed storage (shipment tool)'
                );

            END LOOP;

            --update the current volume
            UPDATE operational.seed_storage
            SET volume = (r_seed_storage.volume - var_volume), notes = notes || '; updated the volume via transfer seed storage (shipment tool)'
            WHERE id = r_seed_storage.id;


        END LOOP;

        -- update status of shipment_item 20171002 -- fix in RBIMS-6046
        UPDATE seed_warehouse.shipment_item
        SET status = 'RECEIVED'
        WHERE id = r_shipment.id;

    END LOOP;

RETURN 'SUCCESS';	

END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
