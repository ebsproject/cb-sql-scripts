-- FUNCTION: operational.get_sampling_tag_gsl_values(integer, integer, integer, character varying)

-- DROP FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION operational.get_sampling_tag_gsl_values(
	studyid integer,
	from_page integer,
	to_page integer,
	ref_table character varying,
	OUT page_number bigint,
	OUT sample_name character varying,
	OUT sample_no integer)
    RETURNS SETOF record 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE

  plot_rec varchar;
  result varchar; 
  start_query varchar;
  end_query varchar;
BEGIN
  /**
    * Get the plot values of the given study id for the Sampling Tags - GSL
    * @param study id
    * @param from_page
    * @param to_page
    * @return query study and sow order 
    * @author Maria Relliza Pasang <m.pasang@irri.org>
    * @date 2020-04-18 06:44 pm
  **/

  if(from_page is not NULL)
  then
    start_query = 'select * from ( ';
    end_query = ' ) as t where t.page_number between ' || from_page || ' and ' || to_page;
  else
    start_query = '';
    end_query = '';
  end if;

  plot_rec = start_query || 
    'select
      row_number () over (order by sample_no) as page_number,
      sample_name,
      sample_no
    from seed_warehouse.sample
    where id in (select sample_id from operational.' || ref_table || ' where study_id = ''' || studyid || ''' and is_void = false)
    order by sample_no'
	|| end_query;
  
  return query execute plot_rec;
END;$BODY$;

ALTER FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer, character varying)
    OWNER TO testb4radmin;

GRANT EXECUTE ON FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer, character varying) TO dev_readwrite;

GRANT EXECUTE ON FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer, character varying) TO testb4radmin;

COMMENT ON FUNCTION operational.get_sampling_tag_gsl_values(integer, integer, integer, character varying)
    IS 'Function for the Sampling Tags - GSL values';
