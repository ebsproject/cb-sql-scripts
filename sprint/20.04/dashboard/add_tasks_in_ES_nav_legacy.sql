/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-04-06
 *
 * Add Tasks in ES left navigation
 */

update
	platform.space
set
	menu_data = 
		'{
			"left_menu_items": [{
					"name": "tasks",
					"label": "Tasks",
					"appAbbrev": "TASKS"
				},
				{
					"name": "service-requests",
					"label": "Service requests",
					"appAbbrev": "SERVICE_REQUESTS"
				}, {
					"name": "data-collection-qc",
					"items": [{
						"name": "data-collection-qc-data-collection",
						"label": "Data collection",
						"appAbbrev": "DATA_COLLECTION"
					}, {
						"name": "data-collection-qc-quality-control",
						"label": "Quality control",
						"appAbbrev": "QUALITY_CONTROL"
					}],
					"label": "Data collection & QC"
				}, {
					"name": "seeds",
					"items": [{
						"name": "seeds-shipments",
						"label": "Shipments",
						"appAbbrev": "SHIPMENTS"
					}, {
						"name": "seeds-containers",
						"label": "Containers",
						"appAbbrev": "CONTAINERS"
					}, {
						"name": "seeds-facilities",
						"label": "Facilities",
						"appAbbrev": "FACILITIES"
					}, {
						"name": "seeds-receive-seeds",
						"label": "Receive seeds",
						"appAbbrev": "RECEIVE_SEEDS"
					}, {
						"name": "seeds-repack",
						"label": "Repack seeds",
						"appAbbrev": "REPACK_SEEDS"
					}, {
						"name": "seeds-deposit",
						"label": "Deposit seeds",
						"appAbbrev": "DEPOSIT_SEEDS"
					}],
					"label": "Seeds"
				}
			]
		}'
where
	abbrev = 'ES'
;