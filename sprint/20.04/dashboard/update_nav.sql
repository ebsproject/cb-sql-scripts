/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-04-24
 *
 * Update navigation for 20.04 release
 * And add 'Occurrences' in list of applications
 */

-- Update Users application to Persons
update platform.application set abbrev = 'PERSONS' where abbrev = 'USERS';

-- update navigation for admin users
update platform.space 
	set 
	menu_data = '
	{
		"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiments",
			"items": [{
				"name": "experiment-creation-experiments",
				"label": "Experiments",
				"appAbbrev": "EXPERIMENT_CREATION"
			}, {
				"name": "experiment-creation-occurrences",
				"label": "Occurrences",
				"appAbbrev": "OCCURRENCES"
			}]
		}, {
			"name": "data-collection-qc",
			"items": [{
				"name": "data-collection-qc-quality-control",
				"label": "Quality control",
				"appAbbrev": "QUALITY_CONTROL"
			}],
			"label": "Data collection & QC"
		}, {
			"name": "seeds",
			"items": [{
				"name": "seeds-find-seeds",
				"label": "Find seeds",
				"appAbbrev": "FIND_SEEDS"
			}, {
				"name": "seeds-harvest-manager",
				"label": "Harvest manager",
				"appAbbrev": "HARVEST_MANAGER"
			}],
			"label": "Seeds"
		}],
		"main_menu_items": [{
			"icon": "folder_special",
			"name": "data-management",
			"items": [{
				"name": "data-management_seasons",
				"label": "Seasons",
				"appAbbrev": "MANAGE_SEASONS"
			}, {
				"name": "data-management_germplasm",
				"label": "Germplasm",
				"appAbbrev": "GERMPLASM_CATALOG"
			}],
			"label": "Data management"
		}, {
			"icon": "settings",
			"name": "administration",
			"items": [{
				"name": "administration_users",
				"label": "Persons",
				"appAbbrev": "PERSONS"
			}],
			"label": "Administration"
		}]
	}
	'
where 
	abbrev = 'ADMIN';

-- update navigation for default users
update platform.space 
	set 
	menu_data = '
	{
		"left_menu_items": [{
			"name": "experiment-creation",
			"label": "Experiments",
			"items": [{
				"name": "experiment-creation-experiments",
				"label": "Experiments",
				"appAbbrev": "EXPERIMENT_CREATION"
			}, {
				"name": "experiment-creation-occurrences",
				"label": "Occurrences",
				"appAbbrev": "OCCURRENCES"
			}]
		}, {
			"name": "data-collection-qc",
			"items": [{
				"name": "data-collection-qc-quality-control",
				"label": "Quality control",
				"appAbbrev": "QUALITY_CONTROL"
			}],
			"label": "Data collection & QC"
		}, {
			"name": "seeds",
			"items": [{
				"name": "seeds-find-seeds",
				"label": "Find seeds",
				"appAbbrev": "FIND_SEEDS"
			}, {
				"name": "seeds-harvest-manager",
				"label": "Harvest manager",
				"appAbbrev": "HARVEST_MANAGER"
			}],
			"label": "Seeds"
		}]
	}
	'
where 
	abbrev = 'DEFAULT';

-- insert in list of applications
insert into platform.application (abbrev, label, action_label, icon, creator_id)
values
('OCCURRENCES','Occurrences','Occurrences','settings_applications',1);

insert into platform.application_action (application_id, module, creator_id)
select 
	id,
	'occurrence',
	1
from 
	platform.application
where 
	abbrev = 'OCCURRENCES';