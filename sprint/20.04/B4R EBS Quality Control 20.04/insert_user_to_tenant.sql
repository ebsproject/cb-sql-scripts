﻿--select * from tenant.person

insert into tenant.person (id, email, username, first_name, last_name, person_name, person_type, person_role_id, person_status, is_active, creator_id, creation_timestamp,is_void)
select
id,
email,
username,
first_name,
last_name,
display_name,
'admin' person_type,
1 as person_role_id,
'active' as person_status,
true as is_active,
1 as creator_id,
now(),
false
from 
master.user where id=29