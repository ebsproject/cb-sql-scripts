
select api.insert_messages(
400,
'ERR',
'The background process ID does not exist.',
'The background process status is invalid. Status should not be empty.',
'The background process status is invalid. Status should be one of the following: in progress, success, error' ,
'Invalid format, isSeen must be boolean.',
'Required parameters are missing. Ensure that the field, message, is not empty.',
'The background process message is invalid. Message should not be empty.',
'Invalid format, processId must be an integer.'
)