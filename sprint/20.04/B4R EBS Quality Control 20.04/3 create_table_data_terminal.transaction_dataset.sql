-- Table: data_terminal.transaction_dataset

-- DROP TABLE data_terminal.transaction_dataset;

CREATE TABLE data_terminal.transaction_dataset
(
  id serial NOT NULL, --primary key
  transaction_id integer NOT NULL,
  variable_id integer NOT NULL,
  value character varying,
  status character varying, -- {new,updated,invalid}
  is_suppressed boolean, 
  suppress_remarks text,
  is_generated boolean, -- if record is a result from formula computation
  data_unit character varying, -- data unit (plot)
  entity character varying, -- refers to the table that this record will be inserted (e.g plot_data)
  entity_id integer, -- Identifier of the entity (e.g plot_id) 
  creator_id integer NOT NULL, -- Id of the user who created the record
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record is created.
  modifier_id integer, -- Id of the user who last modified the transaction record.
  modification_timestamp timestamp without time zone, -- Timestamp when the last modification made.
  is_void boolean, -- Indicates whether the record deleted or not.
  remarks character varying, -- Additional details about the transaction.
  notes text,
  CONSTRAINT dataset_id_pkey PRIMARY KEY (id),
CONSTRAINT transaction_dataset_transaction_id_fkey FOREIGN KEY (transaction_id)
      REFERENCES data_terminal.transaction (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the transaction_id column, which refers to the id column of data_terminal.transaction table
  CONSTRAINT transaction_dataset_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of master.user table
  CONSTRAINT transaction_dataset_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the modifier_id column, which refers to the id column of the master.user table
  CONSTRAINT transaction_dataset_status CHECK (status::text = ANY (ARRAY[ 'new'::character varying::text, 'invalid'::character varying::text, 'updated'::character varying::text]))
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE data_terminal.transaction_dataset
  IS 'Terminal records of transaction';

CREATE INDEX transaction_dataset_data_unit_idx
  ON data_terminal.transaction_dataset
  USING btree
  (data_unit COLLATE pg_catalog."default");

CREATE INDEX transaction_dataset_status_idx
  ON data_terminal.transaction_dataset
  USING btree
  (status COLLATE pg_catalog."default");

CREATE INDEX transaction_dataset_entity_id_idx
  ON data_terminal.transaction_dataset
  USING btree
  (entity_id);

CREATE INDEX transaction_dataset_transaction_id_idx
  ON data_terminal.transaction_dataset
  USING btree
  (transaction_id);

CREATE INDEX terminal_variable_id_idx
  ON data_terminal.transaction_dataset
  USING btree
  (variable_id);

