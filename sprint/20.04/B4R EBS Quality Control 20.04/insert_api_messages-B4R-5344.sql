﻿select api.insert_messages(
400,
'ERR',
'Required parameters are missing. Ensure that locationDbId and action fields are not empty.',
'Invalid format, locationDbId must be an integer.',
'The provided locationDbId does not exist.',
'The provided action is invalid. Action should be data_collection or remove_operational_data.',
'User still has an open transaction on locationDbId.'
)