CREATE OR REPLACE FUNCTION data_terminal.validate_transaction(
    var_transaction_id integer
)
RETURNS character varying
LANGUAGE plpgsql
AS $function$
DECLARE
    r_variable RECORD;
    r_data_unit RECORD;	
BEGIN
    DELETE FROM data_terminal.transaction_dataset 
    WHERE 
        transaction_id = var_transaction_id
        AND (value is null or trim(value) = '')
        AND is_generated = false;

    --start get all variables in the transaction
    for r_variable in
        SELECT 
            v.id as variable_id,
            v.scale_id,
            v.data_type 
        FROM 
            master.variable v  
        WHERE 
            v.is_void = false 
            and v.id in (
                SELECT
                    distinct variable_id 
                FROM 
                    data_terminal.transaction_dataset 
                WHERE 
                    transaction_id = var_transaction_id
            )
    LOOP

    --start validates the data type
    IF r_variable.data_type = 'integer'
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^\d+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid'  
        WHERE 
            value !~ '^\d+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;
    

    ELSIF (r_variable.data_type = 'float' or r_variable.data_type = 'double precision') 
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^[-+]?[0-9]*\.?[0-9]+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid' 
        WHERE 
            value !~ '^[-+]?[0-9]*\.?[0-9]+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;
    
    ELSIF (r_variable.data_type = 'character varying' or r_variable.data_type = 'text' or r_variable.data_type = 'varchar' ) 
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id;
    
    ELSIF r_variable.data_type = 'date' THEN	
        /* valid date format is YYYY-mm-dd */
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        /*convert format from dd-mm-YYYY into YYYY-mm-dd */
        UPDATE data_terminal.transaction_dataset 
        SET value=
            substring( value from '^\d+\/\d+\/((19|20)\d\d)$') /*year*/ || '-' ||
            substring( value from '^\d+\/(0[1-9]|1[012])\/\d+$') /*month*/ || '-' ||
            substring( value from '^((0[1-9]|[12][0-9]|3[01]))\/(.)+$') /*day*/
        WHERE 
            value ~ '^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(19|20)\d\d$'
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid'  
        WHERE 
            value !~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

    ELSIF r_variable.data_type = 'time'
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid'  
        WHERE 
            value !~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id;
                
    END IF;

--end validation of the data type

    --for scale values, if variable has scale values
    IF r_variable.scale_id is not NULL THEN
        --start validates the value
        IF EXISTS (SELECT 1 FROM master.scale_value where scale_id = r_variable.scale_id) THEN
            UPDATE data_terminal.transaction_dataset 
                SET status = 'invalid' 
            WHERE 
                value NOT IN (
                    SELECT value 
                    from 
                        master.scale_value 
                    WHERE 
                    scale_id = r_variable.scale_id
                    AND is_void = false
                )
                AND transaction_id = var_transaction_id
                AND variable_id = r_variable.variable_id;

            UPDATE data_terminal.transaction_dataset 
                SET status = 'new'
            WHERE 
                value IN (
                    SELECT value 
                    from 
                        master.scale_value 
                    WHERE 
                    scale_id = r_variable.scale_id
                    AND is_void = false
                )
                AND transaction_id = var_transaction_id
                AND variable_id = r_variable.variable_id;
        ELSE	-- no scale value, hence, value is valid
             UPDATE data_terminal.transaction_dataset 
                SET status = 'new' 
            WHERE 
                transaction_id = var_transaction_id
                AND variable_id = r_variable.variable_id
                AND status <> 'invalid';

        END IF;
        --end validates the value
    ELSE
    -- no scale value, hence, value is valid
        UPDATE data_terminal.transaction_dataset 
        SET is_data_value_valid = 'new' 
        WHERE 
            transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'invalid';
    END IF;
    
    END LOOP;

    -- start looping through the data unit in the transaction
    for r_data_unit in
        SELECT 
            t.data_unit
        FROM 
            data_terminal.transaction_dataset t
        WHERE 
            t.transaction_id = var_transaction_id
        GROUP by t.data_unit
    LOOP
        IF r_data_unit.data_unit = 'plot_data' THEN
            UPDATE data_terminal.transaction_dataset t
            SET status = 'updated'
            FROM 
                operational.plot_data pd
            WHERE 
                pd.variable_id = t.variable_id
                and t.transaction_id = var_transaction_id
                and pd.plot_id = t.entity_id
                and pd.is_void = false
                and t.entity = 'plot_data'
                and t.value <> pd.value;
    -- end check if the record is already in the operational
        END IF;
-- end looping through the data unit in the transaction	
    END LOOP;

RETURN 'success';
    
END;	
$function$;
