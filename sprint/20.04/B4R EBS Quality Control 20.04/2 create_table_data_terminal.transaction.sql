-- Table: data_terminal.transaction

-- DROP TABLE data_terminal.transaction CASCADE;

CREATE TABLE data_terminal.transaction
(
  id serial NOT NULL, -- primary key
  status character varying NOT NULL, -- {uploaded,validated,committed, uploading in progress, }
  location_id integer,
  occurrence jsonb, -- array of occurrence_id in the dataset including the data unit uploaded and total number of data units in the dataset
  checksum text, -- Checksum of the dataset inserted.
  action character varying NOT NULL DEFAULT 'data_collection'::character varying, -- {data collection, remove operational data}
  creator_id integer NOT NULL, -- Id of the user wh created the transaction
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the transaction is created.
  committed_timestamp timestamp without time zone, -- Timestamp when the transaction is committed to operational data.
  committer_id integer, -- User identifier of the one who committed the transaction
  modifier_id integer, -- Id of the user who last modified the transaction record.
  modification_timestamp timestamp without time zone, -- Timestamp when the last modification made.
  is_void boolean, -- Indicates whether the transaction is deleted or not.
  remarks character varying, -- Additional details about the transaction.
  notes text,
  CONSTRAINT transaction_location_id_fkey FOREIGN KEY (location_id)
      REFERENCES experiment.location (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the location column, which refers to the id column of experiment.location table
  CONSTRAINT transaction_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of master.user table
  CONSTRAINT transaction_committer_id_fkey FOREIGN KEY (committer_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the committer_id column, which refers to the id column of master.user table
  CONSTRAINT transaction_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the modifier_id column, which refers to the id column of the master.user table
  CONSTRAINT transaction_id_pkey PRIMARY KEY (id),
  CONSTRAINT terminal_status CHECK (status::text = ANY (ARRAY[ 'uploading in progress'::character varying::text, 'error in background process'::character varying::text, 'in queue'::character varying::text, 'committing in progress'::character varying::text, 'suppression in progress'::character varying::text, 'undo suppression in progress'::character varying::text, 'removing data in progress'::character varying::text, 'undo removing data in progress'::character varying::text, 'uploaded'::character varying::text, 'validation in progress'::character varying::text, 'committed'::character varying::text, 'validated'::character varying::text]))
)
WITH (
  OIDS=FALSE
);
-- Index: data_terminal.transaction_is_void_idx

-- DROP INDEX data_terminal.transaction_is_void_idx;

CREATE INDEX transaction_location_id_idx
  ON data_terminal.transaction
  USING btree
  (location_id);

-- Index: data_terminal.transaction_is_void_idx

-- DROP INDEX data_terminal.transaction_is_void_idx;

CREATE INDEX transaction_is_void_idx
  ON data_terminal.transaction
  USING btree
  (is_void);

-- Index: data_terminal.transaction_creator_id_idx

-- DROP INDEX data_terminal.transaction_creator_id_idx;

CREATE INDEX transaction_creator_id_idx
  ON data_terminal.transaction
  USING btree
  (creator_id);

-- Index: data_terminal.transaction_status_idx

-- DROP INDEX data_terminal.transaction_status_idx;

CREATE INDEX transaction_status_idx
  ON data_terminal.transaction
  USING btree
  (status COLLATE pg_catalog."default");


