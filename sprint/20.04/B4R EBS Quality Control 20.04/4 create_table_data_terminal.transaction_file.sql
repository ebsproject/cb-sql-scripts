-- Table: data_terminal.transaction_file

-- DROP TABLE data_terminal.transaction_file;

CREATE TABLE data_terminal.transaction_file
(
  id serial NOT NULL, -- primary key
  transaction_id integer, -- Id of the transaction,
  status character varying NOT NULL, -- Status of the record ("uploaded", "committed", "invalid")
  dataset jsonb NOT NULL, -- Object of the dataset of the transaction.
  creator_id integer NOT NULL, -- Id of the user who added the record.
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record is added
  modifier_id integer, -- Id of the user who last modified the transaction record.
  modification_timestamp timestamp without time zone, -- Timestamp when the last modification made.
  is_void boolean, -- Indicates whether the record is deleted or not
  remarks character varying, -- Additional details about the record
  notes text,
  CONSTRAINT transaction_file_id_pkey PRIMARY KEY (id),
  CONSTRAINT transaction_file_transaction_id_fkey FOREIGN KEY (transaction_id)
      REFERENCES data_terminal.transaction (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the transaction_id column, which refers to the id column of data_terminal.transaction table
  CONSTRAINT transaction_file_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of master.user table
  CONSTRAINT transaction_file_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE -- Foreign key constraint for the modifier_id column, which refers to the id column of the master.user table
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE data_terminal.transaction_file
  IS 'Contains the transaction dataset of uploaded, committed, or are invalid.';

CREATE INDEX transaction_file_transaction_id_idx
  ON data_terminal.transaction_file
  USING btree
  (transaction_id);


