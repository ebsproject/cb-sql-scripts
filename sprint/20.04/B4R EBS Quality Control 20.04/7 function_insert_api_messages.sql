CREATE OR REPLACE FUNCTION api.insert_messages(
    var_http_status_code integer,
    var_msg_type character varying,
    variadic var_msg text[]
)
RETURNS character varying
LANGUAGE plpgsql
AS $function$
DECLARE
    currentMsg text;
BEGIN
    FOREACH currentMsg IN ARRAY var_msg
    LOOP
        IF not exists(select 1 from api.messages where http_status_code=var_http_status_code and message=currentMsg)
        THEN
        INSERT INTO api.messages(http_status_code, code, type, message,url)
        SELECT
            var_http_status_code,
            (select code+1 from  api.messages where http_status_code=var_http_status_code order by code desc limit 1),
            var_msg_type,
            currentMsg,
            'responses.html#'|| (select code+1 from  api.messages where http_status_code=var_http_status_code order by code desc limit 1);

        END IF;
    END LOOP;
    
RETURN 'success';
    
END;	
$function$;
