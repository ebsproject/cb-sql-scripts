select api.insert_messages(
400,
'ERR',
'The transaction status is invalid. Status should be one of the following: uploaded, invalid, committed, suppressed',
'The terminal transaction ID does not exist.',
'Invalid format, transactionDbId must be an integer.',
'Required parameters are missing. Ensure that transactionDbId, status, and dataset fields are not empty.',
'The transaction file status is invalid. Status is empty.'
)