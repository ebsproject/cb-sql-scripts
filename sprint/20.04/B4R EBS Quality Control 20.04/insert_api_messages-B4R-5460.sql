
select api.insert_messages(
400,
'ERR',
'The terminal transaction ID does not exist.',
'Invalid format, transactionDbId must be an integer.'
);

select api.insert_messages(
404,
'ERR',
'The dataset records you have requested do not exist.'
);
