-- Table: data_terminal.background_process

-- DROP TABLE data_terminal.background_process;

CREATE TABLE data_terminal.background_process
(
  id serial NOT NULL,
  transaction_id integer NOT NULL,
  process_id character varying,
  process_name character varying,
  status character varying(20),
  message text,
  start_time timestamp without time zone,
  end_time timestamp without time zone,
  is_seen boolean NOT NULL DEFAULT false, -- Indicates whether the record is seen by the user
  creator_id integer, -- Id of the user who added the record.
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record is added
  modifier_id integer,
  modification_timestamp timestamp without time zone,
  is_void boolean NOT NULL DEFAULT false, -- Indicates whether the record is deleted or not
  remarks character varying, -- Additional details about the record
  notes text,
  CONSTRAINT background_process_transaction_id_fkey FOREIGN KEY (transaction_id)
      REFERENCES data_terminal.transaction (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the transaction_id column, which refers to the id column of data_terminal.transaction table
  CONSTRAINT background_process_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of master.user table
  CONSTRAINT background_process_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the modifier_id column, which refers to the id column of the master.user table
  CONSTRAINT background_process_id_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE data_terminal.background_process
  IS 'Stores the transactions of background processes';
