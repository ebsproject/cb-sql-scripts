-- Schema: data_terminal

-- DROP SCHEMA data_terminal;

CREATE SCHEMA data_terminal;

COMMENT ON SCHEMA data_terminal
  IS 'Operational data terminal for Quality_Control_P13.20.04';

