
select api.insert_messages(
400,
'ERR',
'The provided processName is invalid. ProcessName should be one of the following: DataCollectionUpload',
'The terminal transaction ID does not exist.',
'Invalid format, transactionDbId must be an integer.',
'Required parameters are missing. Ensure that transactionDbId and processName fields are not empty.'
)