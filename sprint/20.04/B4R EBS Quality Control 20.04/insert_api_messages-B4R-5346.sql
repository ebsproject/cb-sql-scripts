select api.insert_messages(
400,
'ERR',
'Required parameters are missing. Ensure that measurementVariables fields are not empty.'
'Invalid format, measurementVariables must be an array.',
'The terminal transaction ID does not exist.',
'There is at least one(1) invalid variable in measurementVariables field.',
'The dataset cannot be created. Ensure that the status of the terminal transaction ID is uploaded, uploading in progress, or validated.',
'There is at least one(1) plot that do not exist in the location.'
)
