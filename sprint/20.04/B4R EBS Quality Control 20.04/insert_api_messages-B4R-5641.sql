
select api.insert_messages(
400,
'ERR',
'The transaction cannot be validated. Ensure that the status of the terminal transaction ID is uploaded.',
'The transaction status cannot be updated when it is already committed.',
'The transaction status is invalid. Status is empty.',
'The transaction status is invalid. Status should be one of the following: uploading in progress,error in background process,in queue,committing in progress,suppression in progress,undo suppression in progress,removing data in progress,undo removing data in progress,uploaded,validation in progress,committed,validated',
'The transaction cannot be committed. The value of the current status should validated to be able to update the status into committed.',
'The transaction checksum is invalid. Checksum is empty.',
'Invalid format, occurrences must be an array.',
'There is at least one(1) invalid record in occurrences. Ensure that the fields; occurrenceDbId, dataUnitCount, dataUnit, and variableDbIds are not empty.',
'There is at least one(1) invalid record in occurrences. Ensure that the variableDbIds is an array.',
'There is at least one(1) invalid record in occurrences. Ensure that the variableDbIds is not empty.',
'There is at least one(1) invalid record in occurrences. Ensure that the variableDbIds exist in database and must be of type metadata or observation.',
'There is at least one(1) invalid record in occurrences. Ensure that the variableDbIds is not empty.',
'There is at least one(1) invalid record in occurrences. The field occurrenceDbId must be an integer.',
'There is at least one(1) invalid record in occurrences. The occurrenceDbId does not exist in the location.',
'There is at least one(1) invalid record in occurrences. The field dataUnitCount must be an integer.',
'There is at least one(1) invalid record in occurrences. The dataUnit should be one of the following: plot',
'Ensure that occurrences is not empty.'
)