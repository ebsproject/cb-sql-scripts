/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Karen de la Rosa <k.delarosa@irri.org>
 * @date 2020-04-13 16:00:00
 */


--/*
BEGIN;

-- SAVEPOINT sp1;
--*/


/**
 * B4R-5186 Update Find Seeds query parameters config
 */


INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'FIND_SEEDS_PARAMS',
        'Find Seeds Parameters',
        '{
           "name": "Find Seeds Query Parameters",
           "main_table": "germplasm.package",
		   "main_table_alias": "ss",
		   "main_table_primary_key": "id",
		   "search_method": "POST",
		   "search_call": "packages-search",
		   "search_sort_column": "id",
		   "search_sort_order": "ASC",
           "values": [
               {
					"disabled": "false",
					"required": "true",
					"input_type": "single",
					"variable_abbrev": "PROGRAM",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "true",
					"output_id_value" : "id",
					"output_display_value" : "program_name",
					"api_resource_method": "GET",
					"api_resource_endpoint": "persons/{id}/programs",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "YEAR",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "true",
					"output_id_value" : "experiment_year",
					"output_display_value" : "experiment_year",
					"api_resource_method": "POST",
					"api_resource_endpoint": "experiments-search",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "DESC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "EXPERIMENT_NAME",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "true",
					"output_id_value" : "id",
					"output_display_value" : "experiment_name",
					"api_resource_method": "POST",
					"api_resource_endpoint": "experiments-search",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "id",
					"search_sort_order": "DESC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "LOCATION",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "true",
					"output_id_value" : "id",
					"output_display_value" : "location_name",
					"api_resource_method": "POST",
					"api_resource_endpoint": "locations-search",
					"secondary_resource_endpoint" : "location-occurrence-groups-search",
					"secondary_resource_endpoint_method" : "POST",
					"reference_column": "locationDbId",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "EVALUATION_STAGE",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "true",
					"output_id_value" : "id",
					"output_display_value" : "stage_name",
					"api_resource_method": "POST",
					"api_resource_endpoint": "stages-search",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "SEASON",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "true",
					"output_id_value" : "id",
					"output_display_value" : "season_name",
					"api_resource_method": "POST",
					"api_resource_endpoint": "seasons-search",
					"secondary_resource_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "EXPERIMENT_TYPE",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "true",
					"output_id_value" : "experiment_type",
					"output_display_value" : "experiment_type",
					"api_resource_method": "POST",
					"api_resource_endpoint": "experiments-search",
					"secondary_resource_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "DESC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "FACILITY",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "facility_type: EQUALS structure | facility_subtype: EQUALS building, seed warehouse, farm house",
					"basic_parameter": "false",
					"output_id_value" : "id",
					"output_display_value" : "facility_name",
					"api_resource_method": "POST",
					"api_resource_endpoint": "facilities-search",
					"secondary_resource_endpoint_method" : "POST",
					"secondary_resource_endpoint" : "facilities-search",
					"reference_column": "root_facility_id",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "SUB_FACILITY",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "facility_type: NOT EQUALS structure | facility_subtype: NOT EQUALS building, seed warehouse, farm house",
					"basic_parameter": "false",
					"output_id_value" : "id",
					"output_display_value" : "facility_name",
					"api_resource_method": "POST",
					"api_resource_endpoint": "facilities-search",
					"secondary_resource_endpoint_method" : "POST",
					"secondary_resource_endpoint" : "facilities-search",
					"reference_column": "root_facility_id",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "SOURCE_HARV_YEAR",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "location_harvest_date",
					"output_display_value" : "location_harvest_date",
					"api_resource_method": "POST",
					"api_resource_endpoint": "locations-search",
					"secondary_resource_endpoint_method" : "POST",
					"secondary_resource_endpoint" : "location-occurrence-groups-search",
					"reference_column": "locationDbId",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "PLOTNO",
					"input_field": "input field",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "id",
					"output_display_value" : "plot_number",
					"api_resource_method": "POST",
					"api_resource_endpoint": "locations/{id}/plots-search",
					"secondary_resource_endpoint_method" : "POST",
					"secondary_resource_endpoint" : "seeds-search",
					"reference_column": "sourcePlotDbId",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "ENTNO",
					"input_field": "input field",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "id",
					"output_display_value" : "entry_number",
					"api_resource_method": "POST",
					"api_resource_endpoint": "locations/{id}/entries-search",
					"secondary_resource_endpoint_method" : "POST",
					"secondary_resource_endpoint" : "seeds-search",
					"reference_column": "sourceEntryDbId",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "ENTCODE",
					"input_field": "input text",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "id",
					"output_display_value" : "entry_code",
					"api_resource_method": "POST",
					"api_resource_endpoint": "locations/{id}/entries-search",
					"secondary_resource_endpoint_method" : "POST",
					"secondary_resource_endpoint" : "seeds-search",
					"reference_column": "sourceEntryDbId",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "single",
					"variable_abbrev": "VOLUME",
					"input_field": "selection",
					"default_value": "et:equal to (=), gt:greater than (>), lt:less than (<), gte:greater than or equal to (>=), lte:less than or equal to (<=)",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "package_volume",
					"output_display_value" : "package_volume",
					"api_resource_method": "POST",
					"api_resource_endpoint": "packages-search",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "single",
					"variable_abbrev": "UNIT",
					"input_field": "selection",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "package_unit",
					"output_display_value" : "package_unit",
					"api_resource_method": "POST",
					"api_resource_endpoint": "packages-search",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "PLOT_CODE",
					"input_field": "input text",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "id",
					"output_display_value" : "plot_code",
					"api_resource_method": "POST",
					"api_resource_endpoint": "locations/{id}/plots-search",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				},
				{
					"disabled": "false",
					"required": "false",
					"input_type": "multiple",
					"variable_abbrev": "REP",
					"input_field": "input text",
					"default_value": "",
					"allowed_values": "",
					"basic_parameter": "false",
					"output_id_value" : "id",
					"output_display_value" : "rep",
					"api_resource_method": "POST",
					"api_resource_endpoint": "locations/{id}/plots-search",
					"secondary_resource_endpoint_method" : "",
					"secondary_resource_endpoint" : "",
					"reference_column": "",
					"search_sort_column": "display_value",
					"search_sort_order": "ASC"
				}
           ]
        }',
        1,
        'find_seed',
        1,
        'B4R-5186 update find seeds config - k.delarosa 2020-04-13'
    )
;


--/*
-- ROLLBACK TO sp1;

END;
--*/