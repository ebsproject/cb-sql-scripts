/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @author Erica Banasihan <e,banasihan@irri.org>
 * @date 2020-04-16 16:00:00
 */


--/*
BEGIN;

-- SAVEPOINT sp1;
--*/


/**
 * B4R-5207 Update Find Seeds query parameters config
 */


INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'FIND_SEEDS_QUERY_RESULTS',
        'Find Seeds Query Results',
        '{
        "name": "Find Seeds Query Results",
        "main_table": "germplasm.package",
        "main_table_alias": "gp",
        "main_table_primary_key": "id",
		"search_method": "POST",
		"search_call": "packages-search",
		"search_sort_column": "id",
		"search_sort_order": "ASC",
        "values": [
            {
                "hidden": "false",
                "field_label": "SEED MANAGER",
                "order_number": "1",
                "variable_abbrev": "SEED_MANAGER",
                "target_table": "tenant.program",
                "target_table_alias": "tp",
                "target_column": "program_code",
                "secondary_target_table": "",
                "secondary_target_table_alias": "",
                "secondary_target_column": "",
                "reference_column": "program_id",
                "additional_columns": ""
            },


            {
                "hidden": "false",
                "field_label": "LABEL",
                "order_number": "1",
                "variable_abbrev": "LABEL",
                "target_table": "germplasm.package",
                "target_table_alias": "gp",
                "target_column": "label",
                "secondary_target_table": "",
                "secondary_target_table_alias": "",
                "secondary_target_column": "",
                "reference_column": "",
                "additional_columns": ""
            },
            
            {
                "hidden": "false",
                "field_label": "GID",
                "order_number": "3",
                "variable_abbrev": "GID",
                "target_table": "germplasm.seed",
                "target_table_alias": "gs",
                "target_column": "seed_code",
                "secondary_target_table": "",
                "secondary_target_table_alias": "",
                "secondary_target_column": "",
                "reference_column": "",
                "additional_columns":""
            },


            {
                "hidden": "false",
                "field_label": "VOLUME",
                "order_number": "3",
                "variable_abbrev": "VOLUME",
                "target_table": "germplasm.package",
                "target_table_alias": "gp",
                "target_column": "package_volume",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "package_volume",
                "additional_columns": ""
            }, 
                   
            
            {
                "hidden": "false",
                "field_label": "UNIT",
                "order_number": "4",
                "variable_abbrev": "UNIT",
                "target_table": "germplasm.package",
                "target_table_alias": "gp",
                "target_column": "package_unit",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "package_unit",
                "additional_columns": ""
            },
            
            
            {
                "hidden": "false",
                "field_label": "DESIGNATION",
                "order_number": "5",
                "variable_abbrev": "DESIGNATION",
                "target_table": "germplasm.germplasm",
                "target_table_alias": "gg",
                "target_column": "germplasm_name",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "germplasm_id",
                "additional_columns": "pr.id as product_id"
            },


            {
                "hidden": "false",
                "field_label": "SOURCE HARVEST YEAR",
                "order_number": "6",
                "variable_abbrev": "SOURCE_HARV_YEAR",
                "target_table": "experiment.location",
                "target_table_alias": "el",
                "target_column": "location_harvest_date",
                "secondary_target_table": "germplasm.seed",
                "secondary_target_column": "source_location_id",
                "secondary_target_table_alias": "gs",
                "reference_column": "source_location_id",
                "additional_columns": ""
            }, 
            

            {
                "hidden": "false",
                "field_label": "SOURCE STUDY",
                "order_number": "7",
                "variable_abbrev": "SOURCE_STUDY",
                "target_table": "experiment.location",
                "target_table_alias": "el",
                "target_column": "location_code",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "source_location_id",
                "additional_columns": "s.id as source_study_id"
            },
            
            
            {
                "hidden": "false",
                "field_label": "SOURCE STUDY NAME",
                "order_number": "8",
                "variable_abbrev": "SOURCE_STUDY_NAME",
                "target_table": "experiment.location",
                "target_table_alias": "el",
                "target_column": "location_name",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "source_location_id",
                "additional_columns": "s.id as source_study_id"
            },
                       
            
            {
                "hidden": "false",
                "field_label": "SOURCE ENTRY CODE",
                "order_number": "9",
                "variable_abbrev": "SOURCE_ENTRY",
                "target_table": "experiment.entry",
                "target_table_alias": "ee",
                "target_column": "entry_code",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "source_entry_id",
                "additional_columns": ""
            },
            
            
            {
                "hidden": "false",
                "field_label": "SEED SOURCES ENTRY NUMBER",
                "order_number": "10",
                "variable_abbrev": "SRC_ENTRY",
                "target_table": "experiment.entry",
                "target_table_alias": "en",
                "target_column": "entry_code",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "source_entry_id",
                "additional_columns": ""
            },
            

            {
                "hidden": "false",
                "field_label": "SEED SOURCE PLOT CODE",
                "order_number": "11",
                "variable_abbrev": "PLOT_CODE",
                "target_table": "experiment.plot",
                "target_table_alias": "ep",
                "target_column": "plot_code",
                "secondary_target_table": "",
                "secondary_target_column": "source_plot_id",
                "secondary_target_table_alias": "",
                "reference_column": "source_plot_id",
                "additional_columns": ""
            },
            
            
            {
                "hidden": "false",
                "field_label": "SEED SOURCE PLOT NUMBER",
                "order_number": "12",
                "variable_abbrev": "SRC_PLOT",
                "target_table": "experiment.plot",
                "target_table_alias": "ep",
                "target_column": "plot_number",
                "secondary_target_table": "",
                "secondary_target_column": "source_plot_id",
                "secondary_target_table_alias": "",
                "reference_column": "source_plot_id",
                "additional_columns": ""
            },
                          

            {
                "hidden": "false",
                "field_label": "REPLICATION",
                "order_number": "13",
                "variable_abbrev": "REP",
                "target_table": "experiment.plot",
                "target_table_alias": "ep",
                "target_column": "rep",
                "secondary_target_table": "germplasm.seed",
                "secondary_target_column": "source_plot_id",
                "secondary_target_table_alias": "",
                "reference_column": "source_plot_id",
                "additional_columns": ""
            },
            

            {
                "hidden": "false",
                "field_label": "SOURCE STUDY SEASON",
                "order_number": "14",
                "variable_abbrev": "SRC_SEASON",
                "target_table": "tenant.season",
                "target_table_alias": "ts",
                "target_column": "season_code",
                "secondary_target_table": "tenant.season",
                "secondary_target_column": "abbrev",
                "secondary_target_table_alias": "se2",
                "reference_column": "source_experiment_id",
                "additional_columns": ""
            },
            
            
            {
                "hidden": "false",
                "field_label": "HARVEST_DATE",
                "order_number": "15",
                "variable_abbrev": "HARVEST_DATE",
                "target_table": "germplasm.seed",
                "target_table_alias": "gs",
                "target_column": "harvest_date",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "harvest_date",
                "additional_columns": ""
            },


            {
                "hidden": "false",
                "field_label": "FACILITY",
                "order_number": "16",
                "variable_abbrev": "FACILITY",
                "target_table": "place.facility",
                "target_table_alias": "pf",
                "target_column": "facility_name",
                "secondary_target_table": "germplasm.package",
                "secondary_target_column": "id",
                "secondary_target_table_alias": "gp",
                "reference_column": "facility_id",
                "additional_columns": ""
            },


            {
                "hidden": "false",
                "field_label": "SUB_FACILITY",
                "order_number": "17",
                "variable_abbrev": "SUB_FACILITY",
                "target_table": "place.facility",
                "target_table_alias": "pf",
                "target_column": "facility_name",
                "secondary_target_table": "germplasm.package",
                "secondary_target_column": "id",
                "secondary_target_table_alias": "gp",
                "reference_column": "facility_id",
                "additional_columns": ""
            },
            

            {
                "hidden": "false",
                "field_label": "CONTAINER",
                "order_number": "18",
                "variable_abbrev": "CONTAINER",
                "target_table": "place.facility",
                "target_table_alias": "pc",
                "target_column": "facility_name",
                "secondary_target_table": "",
                "secondary_target_column": "",
                "secondary_target_table_alias": "",
                "reference_column": "facility_id",
                "additional_columns": ""
            }
        ]
        }',
        1,
        'find_seed',
        1,
        'B4R-5207 update find seeds config - e.banasihan 2020-04-16'
    )
;


--/*
-- ROLLBACK TO sp1;

END;
--*/