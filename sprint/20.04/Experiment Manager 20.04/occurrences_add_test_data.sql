/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Jahzeel Ramos <jp.ramos@irri.org>
 * @date 2020-04-24
 *
 * Add sample data for browse Occurrences
 */

-- update information of occurrences for testing purposes
update experiment.occurrence 
set 
	modifier_id = 7,
	modification_timestamp = '2020-04-24 6:29:31.726979'
where 
	id in (3376);

update experiment.occurrence 
set 
	occurrence_status = 'layout created', 
	description = 'Some information of this occurrence has been updated for testing purposes.',
	creator_id = 7,
	modifier_id = 18,
	modification_timestamp = '2020-04-23 02:29:31.726979'
where 
	id in (3375, 3374, 3370);

update experiment.occurrence 
set 
	occurrence_status = 'draft', 
	description = 'The status of this occurrence has been updated for testing purposes.',
	creator_id = 6
where 
	id in (3373, 3043, 3042);

update experiment.occurrence 
set 
	description = 'The creator of this occurrence has been updated for testing purposes.',
	creator_id = 6
where 
	id in (3044, 3030, 3040);

update experiment.occurrence 
set
	description = 'This a sample fo displaying a long description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique tempus erat sed lobortis. 
		Pellentesque volutpat risus vel elit luctus placerat.',
	modifier_id = 7,
	modification_timestamp = '2020-04-23 02:29:31.726979'
where id in (3041, 3028);

-- map multiple occurrences to an Experiment (IRSEA-F2-2016-WS)
update 
	experiment.occurrence 
set 
	experiment_id = 201,
	notes = 'update experiment id from ' || experiment_id || ' to 201'
where id in (
	select 
		id 
	from 
		experiment.occurrence 
	where 
		experiment_id in (
			select 
				id 
			from 
				experiment.experiment 
			where 
				experiment_name = 'IRSEA-F2-2016-WS'
		)
	);

-- void previous mapping of location and occurrence
update 
	experiment.location_occurrence_group 
set 
	is_void = true, 
	notes = 'temporarily void for testing' 
where 
	occurrence_id in (
		select 
			id 
		from 
			experiment.occurrence 
		where 
			experiment_id in (
				select 
					id 
				from 
					experiment.experiment 
				where 
					experiment_name = 'IRSEA-F2-2016-WS'
			)
	) 
	and location_id != 2441;

-- map multiple occurrences to a Location (Location #231)
update
	experiment.location_occurrence_group
set
	location_id = 2441,
	notes = 'update location id from ' || location_id || ' to 2441'
where
	occurrence_id in (
		select 
			id 
		from 
			experiment.occurrence 
		where 
			experiment_id in (
				select 
					id 
				from 
					experiment.experiment 
				where 
					experiment_name = 'IRSEA-F2-2016-WS'
		)
	);

-- IRSEA-F2-2016-WS
update 
	experiment.experiment 
set 
	is_void = true, 
	notes = 'temporarily void for testing' 
where 
	id in (
		select 
			id 
		from 
			experiment.experiment 
		where 
			experiment_name = 'IRSEA-F2-2016-WS' 
			and id != 201
	);
