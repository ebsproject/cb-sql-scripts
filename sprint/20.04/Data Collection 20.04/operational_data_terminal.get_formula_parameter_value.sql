-- Function: operational_data_terminal.get_formula_parameter_value(integer, integer, integer, integer, character varying, integer, character varying, character varying, integer, text)

-- DROP FUNCTION operational_data_terminal.get_formula_parameter_value(integer, integer, integer, integer, character varying, integer, character varying, character varying, integer, text);

CREATE OR REPLACE FUNCTION operational_data_terminal.get_formula_parameter_value(
    study_id integer,
    entry_id integer,
    plot_id integer,
    result_variable_id integer,
    result_data_level character varying,
    param_variable_id integer,
    param_data_level character varying,
    param_variable_type character varying,
    transaction_id integer,
    condition text)
  RETURNS character varying AS
$BODY$
declare
    schema_name varchar = 'operational';
    table_name varchar;
    table_name_op varchar;
    column_name varchar;
    row_id varchar;
    query_string varchar;
    data_level_array varchar[];
    data_level varchar;
    variable_type_suffix varchar;
    param_value varchar;
    param_variable record;
    is_multiple boolean = false;
    orig_param_variable_id integer;
    member_variable_data_level varchar;
    op_level_cond text;
    op_cond text;
    
begin
    /**
     * Get formula parameter value from terminal and operational
     * 
     * Retrieves variable value to be used as parameter in formula
     * 
     * @param study_id integer
     * @param entry_id integer
     * @param plot_id integer
     * @param result_variable_id integer
     * @param result_data_level varchar
     * @param param_variable_id integer
     * @param param_data_level varchar
     * @param param_variable_type varchar
     * @return text Response
     * 
     * @author Nikki Carumba
     */
    orig_param_variable_id = param_variable_id;
    select * into param_variable from z_admin.get_variable(param_variable_id::text);
    
    if (param_variable.parent_variable_id is not null) then
        is_multiple = true;
        member_variable_data_level = param_variable.json_data_level;
        if (array_length(param_variable.parent_variable_id, 1) = 1) then
            param_variable_id = param_variable.parent_variable_id[1];
        else
            param_variable_id = param_variable.parent_variable_id[1];
        end if;
        
        select * into param_variable from z_admin.get_variable(param_variable_id::text);
    end if;

    if (param_variable.data_type is not null and param_variable.data_type = 'json') then
    
        is_multiple = true;
        member_variable_data_level = param_variable.json_data_level;
    end if;
    
    if (param_variable_type = 'observation') then
         variable_type_suffix = '_data';
    elsif (param_variable_type = 'metadata') then
         variable_type_suffix = '_metadata';
    end if;
    
    -- multiple data levels
    if (position(';' in param_data_level) > 0) then
        data_level_array = string_to_array(param_data_level, ';');
    else -- single level
        data_level_array = array[param_data_level];
    end if;
    
    -- loop each data level
    foreach data_level in array data_level_array 
    loop
        -- table name where the value will be taken from
        table_name = quote_ident(schema_name) || '.' || quote_ident(data_level || variable_type_suffix);
        table_name_op = quote_ident(schema_name) || '.' || quote_ident(data_level);
        -- colum name of the record in the table
        column_name = data_level || '_id';

        -- decide which value to search in what level
        if (data_level = 'plot') then
            row_id = plot_id;
            
        elsif (data_level = 'entry') then
            row_id = entry_id;
            
        elseif (data_level = 'study') then
            row_id = study_id;
            
        end if;

        -- construct query string that will search the value in the appropriate table
        if(member_variable_data_level != data_level) then
        
            param_variable_id = orig_param_variable_id; 
        end if;

        -- decide which value to search in what level
        op_cond:='';
        op_level_cond:='';

        if(condition<>'')then
            op_cond:='and op.'||condition;
            op_level_cond:='and tx.'||condition;
        end if;
        
        column_name = data_level || '_id';

        -- decide which value to search in what level
        if (data_level = 'plot') then
            row_id = plot_id;
        elsif (data_level = 'entry') then
            row_id = entry_id;
        elsif (data_level = 'study') then
            row_id = study_id;
        end if;

        query_string = ('            
        select
            --COALESCE(terminal.value, operational.value) as value,
            (CASE
                when terminal.is_suppress=true then null
                when terminal.is_suppress=false and terminal.value is null then null
                when terminal.value is not null then terminal.value
                else operational.value
            END)as value,
            id
        from 
            (select
                t.value,
                op.id,
                t.is_suppress
            from
                operational_data_terminal.terminal t 
                    left join ' || table_name_op || ' op on op.key=t.key
            where
                t.transaction_id = ' || transaction_id || '
                and t.is_data_type_valid = true
                and t.is_data_value_valid = true
                and t.variable_id = ' || param_variable_id || '
                and t.is_void = false 
                --and (t.is_suppress = false or t.is_suppress is null)
                and op.id='||row_id||'

            )terminal FULL OUTER JOIN(
                        
            select 
                op.value,
                op.'||column_name||' as id,
                op.is_suppress
            from 
                '||table_name||' op
                
            where 
                op.variable_id = ' || param_variable_id || ' 
                and op.is_void=false 
                and (op.is_suppress = false or op.is_suppress is null) 
                and op.'||column_name||'='||row_id||'
            )
            operational USING(id)
        ');
        
        -- find value
        --raise notice '1: %',query_string;
        execute query_string into param_value;
        --RAISE NOTICE 'q: % ', query_string;
        -- value is null, then go to other data levels
        if (param_value is null) then
            continue;
        end if;

        -- check if value is json
        if (is_multiple = true and z_admin.is_json(param_value) and ((param_value::json)->'0') is not null) then 
        
            if( ((param_value::json)->'0')::text !='""' )   then
                param_value = (param_value::json)->'0';
                return param_value;
            else
                continue;
            end if;
    
        else 
            return param_value;
        end if;
            
        RAISE NOTICE 'result: % ', param_value;      
        
        -- value found
    end loop;

    -- if not found in variable for mutiple records, search again using the original param variable
    param_variable_id = orig_param_variable_id; 
    foreach data_level in array data_level_array 
    loop
        -- table name where the value will be taken from
        table_name = quote_ident(schema_name) || '.' || quote_ident(data_level || variable_type_suffix);
        table_name_op = quote_ident(schema_name) || '.' || quote_ident(data_level);
        -- colum name of the record in the table
        column_name = data_level || '_id';

        -- decide which value to search in what level
        op_cond:='';
        op_level_cond:='';
        if(condition<>'')then
            op_cond:='and op.'||condition;
            op_level_cond:='and tx.'||condition;
        end if;
      
        column_name = data_level || '_id';

        -- decide which value to search in what level
        if (data_level = 'plot') then
            row_id = plot_id;
        elsif (data_level = 'entry') then
            row_id = entry_id;
        elsif (data_level = 'study') then
            row_id = study_id;
        end if;

        query_string = ('
            
        select
            (CASE
                when terminal.is_suppress=true then null
                when terminal.is_suppress=false and terminal.value is null then null
                when terminal.value is not null then terminal.value
                else operational.value
            END)as value,
            id
        from 
            (select
                t.value,
                op.id,
                t.is_suppress
            from
                operational_data_terminal.terminal t 
                    left join ' || table_name_op || ' op on op.key=t.key
            where
                t.transaction_id = ' || transaction_id || '
                and t.is_data_type_valid = true
                and t.is_data_value_valid = true
                and t.variable_id = ' || param_variable_id || '
                and t.is_void = false 
                --and (t.is_suppress = false or t.is_suppress is null)
                and op.id='||row_id||'

            )terminal FULL OUTER JOIN(
                        
            select 
                op.value,
                op.'||column_name||' as id,
                op.is_suppress
            from 
                '||table_name||' op    
            where 
                op.variable_id = ' || param_variable_id || ' 
                and op.is_void=false 
                and (op.is_suppress = false or op.is_suppress is null) 
                and op.'||column_name||'='||row_id||'
            )
            operational USING(id)
        ');

        -- find value
        execute query_string into param_value;
        
        -- value is null, then go to other data levels
        if (param_value is null) then
            continue;
        end if;

        -- check if value is json
        if (is_multiple = true and z_admin.is_json(param_value) and ((param_value::json)->'0') is not null and ((param_value::json)->'0')::text !='""' ) then 
            --RAISE NOTICE 'here: %', (param_value::json)->'0';
            param_value = (param_value::json)->'0';
            --RAISE NOTICE 'value(%)', param_value;
        end if;
              
        return param_value;
        
        -- value found
    end loop;
    
return null;

end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
