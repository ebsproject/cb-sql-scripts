﻿-- Function: operational_data_terminal.get_suppress_formula(integer, text, boolean, text)

-- DROP FUNCTION operational_data_terminal.get_suppress_formula(integer, text, boolean, text);

CREATE OR REPLACE FUNCTION operational_data_terminal.get_suppress_formula(
    var_transaction_id integer,
    var_abbrev text,
    var_is_suppress boolean,
    var_data_level text)
  RETURNS SETOF operational_data_terminal.suppress_summary_count AS
$BODY$

DECLARE
    
    r_variable RECORD;
    r_condition RECORD;
    r_terminal_rec RECORD;
    r_result RECORD;
    iterator integer;
    var_cond text;
    var_sql text;
    var_query_val text;
    var_transaction_status text[];
    var_study_id integer;
    var_variable_id integer;
    var_keys text;
    var_cond_arr text[];
    var_op_id text;
    var_condition text;
    formula_rec RECORD;
    var_suppress_cond boolean;
BEGIN

iterator:=0;

var_cond :='';

if var_is_suppress then var_suppress_cond:=false; else var_suppress_cond:=true; end if;

if var_is_suppress then var_transaction_status:= ARRAY['complete']; else var_transaction_status:=ARRAY['complete','new']; end if;

select s.id into var_study_id from operational_data_terminal.transaction2 t left join operational.study s on s.name=t.study_name where t.id=var_transaction_id;

select id into var_variable_id from master.variable where abbrev=var_abbrev;

select * into r_variable from master.variable where abbrev=var_abbrev;

for r_condition in 
    select transaction_filter.*,v.data_type, v.type from operational_data_terminal.transaction_filter left join master.variable v on v.id=factor_variable_id 
        where transaction_id=var_transaction_id and transaction_filter.variable_id=var_variable_id and transaction_filter.status in ('complete','new') order by id asc
LOOP
    
    var_query_val:=' and d.value';
                
    if r_condition.data_type='text' or r_condition.data_type='string' or r_condition.data_type='character varying' then

        if r_condition.operator='has' then
            select 'ilike' ,'$$%'||r_condition.value||'%$$' into r_condition.operator,r_condition.value ;
            
        elsif r_condition.operator='does not have' then
        
            select 'not ilike','$$%'||r_condition.value||'%$$' into r_condition.operator, r_condition.value ;
        else
        
            select r_condition.operator,'$$'||r_condition.value||'$$' into r_condition.operator, r_condition.value ;
        end if;
        
    elsif r_condition.data_type='integer' or r_condition.data_type='float' or r_condition.data_type='double precision' then

        var_query_val:=var_query_val||'::'||r_condition.data_type;
        
    elsif r_condition.data_type='date'  then
    
        select TO_DATE(r_condition.value, 'YYYY-MM-DD') into r_condition.value ;
        r_condition.value:=''''||r_condition.value||'''';
        var_query_val:=var_query_val||'::'||r_condition.data_type;
        
    end if;
    
    var_cond:=  ' d.variable_id='||r_condition.factor_variable_id|| var_query_val||' '||r_condition.operator||' '||r_condition.value||'';
        
raise notice 'suppress var ID: %',r_condition.variable_id;
raise notice '         factor var ID: %',r_condition.factor_variable_id;
    if iterator=0 then 
        r_condition.conjunction:='';
    end if;
    
    iterator := iterator + 1;
    
    IF r_condition.variable_id=r_condition.factor_variable_id then 
        execute ' 
            select 
                ''o.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')''
            from 
                operational_data_terminal.terminal d
            where
                transaction_id='|| var_transaction_id ||' and  
                ('||var_cond||') and 
                is_void=false
            ' into var_keys;
            
        var_cond_arr:=array_append(var_cond_arr,r_condition.conjunction||' '||var_keys);
    else

        if r_condition.data_level in ('plot', 'entry') then 
            
            if (var_data_level='plot' and r_condition.data_level='plot') or (var_data_level='entry' and r_condition.data_level='entry') then -- extract plot/entry keys
                var_op_id= ' p.id=d.'||r_condition.data_level||'_id ';
            
            if var_data_level='plot' and r_condition.data_level='entry' then -- extract plot keys
                var_op_id= ' p.entry_id=d.'||r_condition.data_level||'_id ';
                
            elsif var_data_level='entry' and r_condition.data_level='plot' then -- extract entry keys
                var_op_id= ' p.id=d.'||var_data_level||'_id ';
            end if;

                
                IF(r_condition.type='metadata') then
                    execute ' 
                    select 
                        COALESCE(terminal.key, operational.key) as key
                    FROM
                    (
                        select 
                            ''o.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')'' as key
                        from 
                            operational_data_terminal.terminal d
                        where
                            transaction_id='|| var_transaction_id ||' and  
                            ('||var_cond||') and is_void=false 
                    )terminal FULL OUTER JOIN(
                    
                        select 
                            ''o.key in(''|| coalesce(string_agg(p.key::text,'',''), ''00'') || '')'' as key
                        from 
                            operational.'||r_condition.data_level||'_metadata d 
                                left join operational.'||var_data_level||' p on '||var_op_id||' 
                        where 
                            ('||var_cond||') and 
                            d.is_void=false and 
                            d.study_id='||var_study_id||'
                    ) operational USING (key)
                    ' into var_keys;
                    
                
                ELSIF(r_condition.type='observation') then
                    execute ' 
                    select 
                        COALESCE(terminal.key, operational.key) as key
                    FROM
                    (
                        select 
                            ''o.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')'' as key
                        from 
                            operational_data_terminal.terminal d
                        where
                            transaction_id='|| var_transaction_id ||' and  
                            ('||var_cond||') and is_void=false 
                    )terminal FULL OUTER JOIN(
                    
                        select 
                            ''o.key in(''|| coalesce(string_agg(p.key::text,'',''), ''00'') || '')'' as key
                        from 
                            operational.'||r_condition.data_level||'_data d 
                                left join operational.'||var_data_level||' p on '||var_op_id||' 
                        where 
                            ('||var_cond||') and 
                            d.is_void=false and 
                            d.study_id='||var_study_id||' 
                    ) operational USING (key)
                    ' into var_keys;

                end if;

            END IF;
            var_cond_arr:=array_append(var_cond_arr,r_condition.conjunction||' '||var_keys);

        ELSE
            if var_data_level in ('plot', 'entry')  then
                execute ' 
                    select 
                        COALESCE(terminal.key, operational.key) as key
                    FROM
                    (
                        select 
                            ''o.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')'' as key
                        from 
                            operational_data_terminal.terminal d
                        where
                            transaction_id='|| var_transaction_id ||' and  
                            ('||var_cond||') and is_void=false 
                    
                    )terminal FULL OUTER JOIN(
                    select 
                        ''o.key in(''|| coalesce(string_agg(p.key::text,'',''), ''00'') || '')'' as key
                    from 
                        operational.'||var_data_level||' p
                        operational.study_metadata d 
                    where 
                        d.study_id=p.study_id and
                        ('||var_cond||') and 
                        d.is_void=false and 
                        d.study_id='||var_study_id||'

                    ' into var_keys;

                var_cond_arr:= array_append(var_cond_arr,r_condition.conjunction||' '||var_keys);
            ELSE
                execute ' 
                    select 
                        COALESCE(terminal.variable_id, operational.variable_id) 
                    FROM
                    (
                        select 
                            variable_id
                        from 
                            operational_data_terminal.terminal d
                        where
                            transaction_id='|| var_transaction_id ||' and  
                            ('||var_cond||') and 
                            is_void=false 
                    
                    )terminal FULL OUTER JOIN(
                        select 
                            variable_id
                        from 
                            operational.study_metadata d 
                        where 

                            ('||var_cond||') and 
                            d.is_void=false and 
                            d.study_id='||var_study_id||' 
                        
                    )operational USING (variable_id)

                    ' into var_keys;
                if var_keys is not null then 
                    var_cond_arr:= array_append(var_cond_arr,r_condition.conjunction||' variable_id='||r_condition.factor_variable_id);
                else
                    var_cond_arr:= array_append(var_cond_arr,r_condition.conjunction||' variable_id=00');
                end if;

            END IF;
        END IF;
    END IF;

END LOOP;

var_condition:= '(' || array_to_string(var_cond_arr,' ') || ')';
raise notice 'cond: %',var_condition;
raise notice '%','select f.*,v.abbrev from operational_data_terminal.get_related_formula_by_variable('||var_transaction_id||','||var_variable_id||','|| var_data_level||','||') f(id integer,data_levelx text, variable_id integer';

    for formula_rec in select f.*,v.abbrev from operational_data_terminal.get_related_formula_by_variable(var_transaction_id,var_variable_id, var_data_level) 
        f(id integer,data_levelx text, variable_id integer) 
            left join master.variable v on v.id=f.variable_id

    LOOP
    --raise notice '%',formula_rec.variable_id;
        if(var_condition is not null) then
             raise notice '%','select '||formula_rec.variable_id||' as variable_id, '''|| formula_rec.abbrev||'''::character varying as abbrev, count(1)::int 
            from(
            
            select 
                variable_id,
                id 
            from 
                operational_data_terminal.terminal o 
            where 
                o.transaction_id='||var_transaction_id ||' 
                and ('||var_condition||') 
                and o.is_data_value_valid=true 
                and o.is_data_type_valid=true 
                
                and variable_id='||formula_rec.variable_id||'
                
                )a group by variable_id';
            execute' select '||formula_rec.variable_id||' as variable_id, '''|| formula_rec.abbrev||'''::character varying as abbrev, count(1)::int 
            from(
            
            select 
                variable_id,
                id 
            from 
                operational_data_terminal.terminal o 
            where 
                o.transaction_id='||var_transaction_id ||' 
                and ('||var_condition||') 
                and o.is_data_value_valid=true 
                and o.is_data_type_valid=true 
                
                and variable_id='||formula_rec.variable_id||'
                
                )a group by variable_id
            ' into r_result ;
            
            if r_result.variable_id is null
                then select formula_rec.variable_id , formula_rec.abbrev, 0 into r_result.variable_id,r_result.abbrev, r_result.count;
            end if;

            return next r_result;
        else
            raise notice 'END';
            return query select r_variable.id , var_abbrev::character varying as abbrev,  0 as count;
        end if;
                
    END LOOP;

return query select r_variable.id , var_abbrev::character varying as abbrev,  0 as count;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

