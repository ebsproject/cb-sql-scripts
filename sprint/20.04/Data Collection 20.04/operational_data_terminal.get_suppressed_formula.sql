﻿-- Function: operational_data_terminal.get_suppress_formula(integer, text, boolean)

-- DROP FUNCTION operational_data_terminal.get_suppress_formula(integer, text, boolean);

CREATE OR REPLACE FUNCTION operational_data_terminal.get_suppress_formula(
    IN var_transaction_id integer,
    IN var_abbrev text,
    IN var_is_suppress boolean)
  RETURNS TABLE(count integer, data_levelx text, variable_id integer, condition text) AS
$BODY$

DECLARE
    r_rec RECORD;
    r_terminal_rec RECORD;
    r_op_rec RECORD;
    var_unit text;
    var_iden_key text;
    var_entry_sql text;
    r_result RECORD;
    iterator integer;
    var_cond text;
    var_sql text;
    var_query_val text;
    r_variable RECORD;
    r_condition RECORD;
    var_data_level text;
    var_status text;
    var_status_condition text;
    var_transaction_status text;
    variable_data_unit text;
    var_has_filter boolean;
begin

var_has_filter:=false;
if var_is_suppress then var_transaction_status:='new'; else var_transaction_status:='complete'; end if;

for r_variable in 
    select 
        distinct (tf.variable_id),
        v.abbrev,
        v.type 
    from 
        operational_data_terminal.transaction_filter tf 
            left join master.variable v on v.id=tf.variable_id 
    where 
        tf.transaction_id=var_transaction_id  
        and v.abbrev=var_abbrev 
        --and tf.status=var_transaction_status
LOOP
raise notice 'here';
var_has_filter:=true;
var_cond :='';
iterator := 0;
var_sql:='';
var_status_condition:='';
    
for r_condition in select transaction_filter.*,v.data_type from operational_data_terminal.transaction_filter left join master.variable v on v.id=factor_variable_id where transaction_id=var_transaction_id and transaction_filter.variable_id=r_variable.variable_id 
    and transaction_filter.status<>'selected'
order by id asc
LOOP
        
    var_query_val:=' and d.value';
                
    if r_condition.data_type='text' or r_condition.data_type='string' or r_condition.data_type='character varying' then
        if r_condition.operator='has' then
            select 'ilike' ,'$$%'||r_condition.value||'%$$' into r_condition.operator,r_condition.value ;
            
        elsif r_condition.operator='does not have' then
            select 'not ilike','$$%'||r_condition.value||'%$$' into r_condition.operator, r_condition.value ;
        elsif r_condition.operator='=' then
            select '=','$$'||r_condition.value||'$$' into r_condition.operator, r_condition.value ;
        end if;
    elsif r_condition.data_type='integer' or r_condition.data_type='float' or r_condition.data_type='double precision' then
        var_query_val:=var_query_val||'::'||r_condition.data_type;
    elsif r_condition.data_type='date'  then
        select TO_DATE(r_condition.value, 'YYYY-MM-DD') into r_condition.value ;
        r_condition.value:=''''||r_condition.value||'''';
        var_query_val:=var_query_val||'::'||r_condition.data_type;
        
    end if;
        
    if(iterator=0) then
        var_cond:= var_cond || ' d.variable_id='||r_condition.factor_variable_id|| var_query_val||' '||r_condition.operator||' '||r_condition.value||'';
        
    else
        var_cond:= var_cond ||' '|| r_condition.conjunction|| ' d.variable_id='||r_condition.factor_variable_id|| var_query_val||r_condition.operator||' '||r_condition.value||' ';
        
    end if;
    iterator := iterator + 1;
        
raise notice '         variable_id: %',r_condition.variable_id;
raise notice '         factor_variable_id: %',r_condition.factor_variable_id;
raise notice '         var_query_val: %',var_query_val;
raise notice '         operator: %',r_condition.operator;
raise notice '         value: %',r_condition.value;
raise notice '         condition: %',var_cond;

END LOOP;

    IF(r_variable.type='metadata') then
        var_sql:= '
        select 
            terminal.key 
        from 
            operational_data_terminal.terminal,
            (
                select  
                    '||var_transaction_id ||' as transaction_id,
                    p.key
                from 
                    operational.plot_metadata d 
                        left join operational.plot p on p.id=d.plot_id 
                where 
                    ('||var_cond||') 
                    and d.is_void=false 
                    and d.study_id=(
                        select 
                            s.id 
                        from 
                            operational_data_terminal.transaction2 t2 
                                left join operational.study s on t2.study_name=s.name 
                        where 
                            t2.id='||var_transaction_id ||'
                        ) 
            )t
        where 
            t.transaction_id=terminal.transaction_id 
            and t.key=terminal.key 
            and terminal.variable_id='||r_variable.variable_id ||'
        ';	
        
    
    ELSIF(r_variable.type='observation') then
        var_sql:= '
            select 
                terminal.key 
            from 
                operational_data_terminal.terminal,
                (select  
                    '||var_transaction_id ||' as transaction_id,
                    p.key
                from 
                    operational.plot_data d 
                        left join operational.plot p on p.id=d.plot_id 
                where 
                    ('||var_cond||') 
                    and d.is_void=false 
                    and d.study_id=(
                            select 
                                s.id 
                            from 
                                operational_data_terminal.transaction2 t2 
                                    left join operational.study s on t2.study_name=s.name 
                            where 
                                t2.id='||var_transaction_id ||'
                    ) 

                )t 
            where 
                t.transaction_id=terminal.transaction_id 
                and t.key=terminal.key 
                and terminal.variable_id='||r_variable.variable_id ||'
        ';	
    end if;
    
    
    execute'
    select 
        array_to_string(
            array(
                select 
                    terminal.key 
                from 
                    operational_data_terminal.terminal,
                    (select 
                        transaction_id,
                        true as is_suppress,
                        d.key
                    from 
                        operational_data_terminal.terminal d 
                    where 
                        d.transaction_id='||var_transaction_id ||' 
                        and ('||var_cond||') 
                        and d.is_data_value_valid=true 
                        and d.is_data_type_valid=true 
                        and d.is_void=false
                    )t  
                where  
                    terminal.is_data_value_valid=true 
                    and terminal.is_data_type_valid=true
                    and t.transaction_id=terminal.transaction_id 
                    and t.key=terminal.key 
                    and terminal.variable_id='||r_variable.variable_id ||'

        union
        '||  var_sql || '),'','') as key' into r_result ;
    
    if r_result.key='' then 
	    var_status:='';
    else
        var_status:=' and t.key in ('||r_result.key||') ';
        var_status_condition:='key in ('||r_result.key||') ';
    end if;

END LOOP;

raise notice 'var_has_filter: %',var_has_filter;

if var_has_filter=true then 
    select 
        t.data_unit into variable_data_unit 
    from 
        operational_data_terminal.terminal t 
            left join master.variable v on v.id=t.variable_id 
    where 
        v.abbrev=var_abbrev limit 1;

    for r_rec in 
        select * 
        from 
            api.get_related_formula(var_transaction_id) f(id integer,data_levelx text, variable_id integer, is_computed boolean)
        
    LOOP
        if(r_rec.data_levelx = 'plot' and variable_data_unit ilike 'plot%') then 
            raise notice 'here%','select 
                    count(1)::integer as count,
                    $$'||r_rec.data_levelx||'$$::text as data_level_x,
                    '||r_rec.variable_id||'::int as variable_id, 
                    '''||var_status_condition||'''::text as condition 
                from 
                    operational_data_terminal.terminal t 
                where 
                    t.is_data_value_valid=true 
                    and t.is_data_type_valid=true 
                    and transaction_id='||var_transaction_id||' 
                    and variable_id='||r_rec.variable_id ||'
            '||var_status;
            return query execute '
                select 
                    count(1)::integer as count,
                    $$'||r_rec.data_levelx||'$$::text as data_level_x,
                    '||r_rec.variable_id||'::int as variable_id, 
                    '''||var_status_condition||'''::text as condition 
                from 
                    operational_data_terminal.terminal t 
                where 
                    t.is_data_value_valid=true 
                    and t.is_data_type_valid=true 
                    and transaction_id='||var_transaction_id||' 
                    and variable_id='||r_rec.variable_id ||'
            '||var_status;
        elsif(r_rec.data_levelx = 'entry' and variable_data_unit ilike 'plot%') then 
            
            select type into var_unit from master.variable where id=r_rec.variable_id;

            var_unit='entry';
            var_entry_sql:= '
            select 
                count(1)::integer,
                ''entry''::text as data_levelx,
                '||r_rec.variable_id::int||' as variable_id, 
                '''||var_status_condition||'''::text as condition
            from 
                operational_data_terminal.terminal t 
            where 
                transaction_id='||var_transaction_id ||'
                and identifier_key in (select 
                e.entno||''-''||e.product_gid 
            from 
                operational.entry e 
                    left join operational.plot p on p.entry_id=e.id, 
                operational_data_terminal.terminal t 
                    left join (
                        select 
                            s.id as study_id,
                            tx.id as transaction_id 
                        from 
                            operational_data_terminal.transaction2 tx 
                                left join operational.study s on s.name=tx.study_name 
                        where 
                        tx.id='||var_transaction_id||'
                    )x on x.transaction_id=t.transaction_id 
            where 
                p.plotno||''-''||p.rep=t.identifier_key 
                and x.study_id=p.study_id 
                and t.transaction_id='||var_transaction_id||' 
                and p.is_void=false 
                and p.entry_id=e.id 
                and e.is_active=true
                and e.is_void=false  and p.'||var_status_condition ||'
                ) 
                and variable_id='||r_rec.variable_id ;
            return query  execute var_entry_sql;
        else
        end if;
    END LOOP;
end if;
return query execute 'select 0 as count, ''''::text as data_levelx, 0::int,''''::text';   

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
