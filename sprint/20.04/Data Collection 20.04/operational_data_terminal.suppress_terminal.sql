-- Function: operational_data_terminal.suppress_terminal(integer, text, boolean, text)

-- DROP FUNCTION operational_data_terminal.suppress_terminal(integer, text, boolean, text);

CREATE OR REPLACE FUNCTION operational_data_terminal.suppress_terminal(
    var_transaction_id integer,
    var_abbrev text,
    var_is_suppress boolean,
    var_data_level text)
  RETURNS character varying AS
$BODY$

DECLARE
	r_variable RECORD;
	r_condition RECORD;
	r_terminal_rec RECORD;
	r_result RECORD;
	iterator integer;
	var_cond text;
	var_sql text;
	var_query_val text;
	var_transaction_status text[];
	var_study_id integer;
	var_keys text;
	var_cond_arr text[];
	var_remarks_arr text[];
	var_op_id text;
	var_condition text;
	var_suppress_cond boolean;
	var_remarks text;
	suppress_rec text;
BEGIN
	var_cond :='';
	iterator:=0;
	if var_is_suppress then 
		var_suppress_cond:=false;
	else 
		var_suppress_cond:=true; 
		
	end if;

	select s.id into var_study_id from operational_data_terminal.transaction2 t left join operational.study s on s.name=t.study_name where t.id=var_transaction_id;
	
	select * into r_variable from master.variable where abbrev=var_abbrev;
		
	for r_condition in 
		select 
			transaction_filter.*,v.data_type, v.type 
		from operational_data_terminal.transaction_filter 
			left join master.variable v on v.id=factor_variable_id 
		where transaction_id=var_transaction_id and transaction_filter.variable_id=r_variable.id and transaction_filter.status in ('complete','new') order by id asc
	LOOP
		
		var_query_val:=' and d.value';
					
		if r_condition.data_type='text' or r_condition.data_type='string' or r_condition.data_type='character varying' then
		
			if r_condition.operator='has' then
			
				select 'ilike' ,'$$%'||r_condition.value||'%$$' into r_condition.operator,r_condition.value ;
				
			elsif r_condition.operator='does not have' then
			
				select 'not ilike','$$%'||r_condition.value||'%$$' into r_condition.operator, r_condition.value ;
				
			else
			
				select '=','$$'||r_condition.value||'$$' into r_condition.operator, r_condition.value ;
				
			end if;
			
		elsif r_condition.data_type='integer' or r_condition.data_type='float' or r_condition.data_type='double precision' then

			var_query_val:=var_query_val||'::'||r_condition.data_type;
			
		elsif r_condition.data_type='date'  then

			select TO_DATE(r_condition.value, 'YYYY-MM-DD') into r_condition.value ;
			
			r_condition.value:=''''||r_condition.value||'''';
			
			var_query_val:=var_query_val||'::'||r_condition.data_type;
			
		end if;
			
		
		var_cond:=  ' d.variable_id='||r_condition.factor_variable_id|| var_query_val||' '||r_condition.operator||' '||r_condition.value||'';
			
	--raise notice 'suppress var ID: %',r_condition.variable_id;
	--raise notice '         factor var ID: %',r_condition.factor_variable_id;
		if iterator=0 then 
			r_condition.conjunction:='';
		end if;
		
		iterator := iterator + 1;

		var_remarks_arr:=array_append(var_remarks_arr,r_condition.remarks);

		IF r_condition.variable_id=r_condition.factor_variable_id then 
			execute ' 
				select 
					''d.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')''
				from 
					operational_data_terminal.terminal d
				where
					transaction_id='|| var_transaction_id ||' and  
					d.is_data_value_valid=true and
					d.is_data_type_valid=true and
					d.is_void=false and
					('||var_cond||')

				' into var_keys;
			var_cond_arr:=array_append(var_cond_arr,r_condition.conjunction||' '||var_keys);
		else

			if r_condition.data_level in ('plot', 'entry') then 
				
				if (var_data_level='plot' and r_condition.data_level='plot') or (var_data_level='entry' and r_condition.data_level='entry') then -- extract plot/entry keys
					var_op_id= ' p.id=d.'||r_condition.data_level||'_id ';
				
				if var_data_level='plot' and r_condition.data_level='entry' then -- extract plot keys
					var_op_id= ' p.entry_id=d.'||r_condition.data_level||'_id ';
					
				elsif var_data_level='entry' and r_condition.data_level='plot' then -- extract entry keys
					var_op_id= ' p.id=d.'||var_data_level||'_id ';
				end if;

					
					IF(r_condition.type='metadata') then
						execute ' 
						select 
							COALESCE(terminal.key, operational.key) as key
						FROM
						(
							select 
								''d.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')'' as key
							from 
								operational_data_terminal.terminal d
							where
								transaction_id='|| var_transaction_id ||' and
								d.is_data_value_valid=true and
								d.is_data_type_valid=true and
								d.is_void=false and
								('||var_cond||')
						)terminal FULL OUTER JOIN(
						
							select 
								''d.key in(''|| coalesce(string_agg(p.key::text,'',''), ''00'') || '')'' as key
							from 
								operational.'||r_condition.data_level||'_metadata d 
									left join operational.'||var_data_level||' p on '||var_op_id||' 
							where 
								('||var_cond||') and 
								d.is_void=false and 
								
								d.study_id='||var_study_id||'
						) operational USING (key)
						' into var_keys;	
						
					
					ELSIF(r_condition.type='observation') then
						
						execute ' 
						select 
							COALESCE(terminal.key, operational.key) as key
						FROM
						(
							select 
								''d.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')'' as key
							from 
								operational_data_terminal.terminal d
							where
								transaction_id='|| var_transaction_id ||' and 
								d.is_data_value_valid=true and
								d.is_data_type_valid=true and
								d.is_void=false and
								('||var_cond||')
						)terminal FULL OUTER JOIN(select 
							''d.key in(''|| coalesce(string_agg(p.key::text,'',''), ''00'') || '')'' as key
						from 
							operational.'||r_condition.data_level||'_data d 
								left join operational.'||var_data_level||' p on '||var_op_id||' 
						where 
							('||var_cond||') and 
							d.is_void=false and 
							
							d.study_id='||var_study_id||'
						)operational USING (key)
						' into var_keys;	

					end if;

				END IF;
				var_cond_arr:=array_append(var_cond_arr,r_condition.conjunction||' '||var_keys);

			ELSE
				if var_data_level in ('plot', 'entry')  then
					execute ' 
						select 
							COALESCE(terminal.key, operational.key) as key
						FROM
						(
							select 
								''d.key in(''|| coalesce(string_agg(d.key::text,'',''), ''00'') || '')'' as key
							from 
								operational_data_terminal.terminal d
							where
								transaction_id='|| var_transaction_id ||' and 
								d.is_data_value_valid=true and
								d.is_data_type_valid=true and
								d.is_void=false and
								('||var_cond||')
						
						)terminal FULL OUTER JOIN(
							select 
								''d.key in(''|| coalesce(string_agg(p.key::text,'',''), ''00'') || '')'' as key
							from 
								operational.'||var_data_level||' p
								operational.study_metadata d 
							where 
								d.study_id=p.study_id and
								
								('||var_cond||') and d.is_void=false and d.study_id='||var_study_id||'

						' into var_keys;

					var_cond_arr:= array_append(var_cond_arr,r_condition.conjunction||' '||var_keys);
				ELSE
					execute ' 
						select 
							COALESCE(terminal.variable_id, operational.variable_id) 
						FROM
						(
							select 
								variable_id
							from 
								operational_data_terminal.terminal d
							where
								transaction_id='|| var_transaction_id ||' and  
								is_suppress='||var_suppress_cond||' and
								d.is_data_value_valid=true and
								d.is_data_type_valid=true and
								d.is_void=false and
								('||var_cond||') 
						
						)terminal FULL OUTER JOIN(
							select 
								variable_id
							from 
								
								operational.study_metadata d 
							where 

								('||var_cond||') and d.is_void=false and d.study_id='||var_study_id||' 
								
							)operational USING (variable_id)

						' into var_keys;
					if var_keys is not null then 
						var_cond_arr:= array_append(var_cond_arr,r_condition.conjunction||' variable_id='||r_condition.factor_variable_id);
					else
						var_cond_arr:= array_append(var_cond_arr,r_condition.conjunction||' variable_id=00');
					end if;

				END IF;
			END IF;
		END IF;

	END LOOP;

	var_condition:= '(' || array_to_string(var_cond_arr,' ') || ')';
	var_remarks:= array_to_string(var_remarks_arr,'; ');
	raise notice 'cond: %',var_condition;
	raise notice 'var_remarks: %',var_remarks;
	IF var_is_suppress is false THEN
		var_remarks:='';
	END IF;
raise notice '%','with rows as(
		update operational_data_terminal.terminal d 
			set is_suppress='|| var_is_suppress ||',
			suppress_remarks='''|| var_remarks ||'''
		where 
			d.transaction_id='||var_transaction_id ||' 
			and d.is_data_value_valid=true 
			and d.is_data_type_valid=true 
			and d.is_void=false 
			and d.variable_id='|| r_variable.id ||' 
			and ('||var_condition||')'||
			' returning d.key
			) select string_agg(key::text, '','') from rows';
	execute '
	with rows as(
		update operational_data_terminal.terminal d 
			set is_suppress='|| var_is_suppress ||',
			suppress_remarks='''|| var_remarks ||'''
		where 
			d.transaction_id='||var_transaction_id ||' 
			and d.is_data_value_valid=true 
			and d.is_data_type_valid=true 
			and d.is_void=false 
			and d.variable_id='|| r_variable.id ||' 
			and ('||var_condition||')'||
			' returning d.key
			) select string_agg(key::text, '','') from rows' into suppress_rec;

	IF var_is_suppress is false THEN
		execute 'DELETE from operational_data_terminal.transaction_filter t 
			WHERE
			transaction_id='||var_transaction_id||' and variable_id='||r_variable.id||' and 
			status in (''selected'') and t.identifier_key in ('||suppress_rec||')';
		
	END IF;
				
	 return 'success';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
