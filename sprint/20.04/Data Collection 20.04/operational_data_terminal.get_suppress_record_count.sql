-- Function: operational_data_terminal.get_suppress_record_count(integer, integer, boolean, text, text, integer)

-- DROP FUNCTION operational_data_terminal.get_suppress_record_count(integer, integer, boolean, text, text, integer);

CREATE OR REPLACE FUNCTION operational_data_terminal.get_suppress_record_count(
    var_study_id integer,
    var_transaction_id integer,
    var_is_suppress boolean,
    var_key text,
    var_data_level text,
    var_variable_id integer)
  RETURNS SETOF record AS
$BODY$

DECLARE
	formula_rec RECORD;
	param_rec RECORD;
	valid_keys text;
	var_condition text;
	var_suppress_cond boolean;
var_sql text;
BEGIN

CREATE TEMP TABLE temp_var2(
	 variable_id integer, count_updated integer
) ON COMMIT DROP;
CREATE UNIQUE INDEX temp_var2_idx on temp_var2 (variable_id) ;

--return the keys of valid records for suppress action
if var_is_suppress then var_suppress_cond:=false; else var_suppress_cond:=true; end if;

if(var_key like '%key%') then
	execute 'select ''key in (''||string_agg(key::text,'','')||'')'' from operational_data_terminal.terminal where transaction_id='||var_transaction_id||
	' and '||var_key||' and variable_id='||var_variable_id||' and is_suppress='||var_suppress_cond into valid_keys;
else 
	select var_key into valid_keys;
end if;

if valid_keys is null 
	then return query select * from temp_var2; 

else

	for formula_rec in select f.id,data_levelx as data_level,f.variable_id,v.abbrev from  operational_data_terminal.get_related_formula_by_variable(var_transaction_id,
		var_variable_id,var_data_level) f(id integer,data_levelx text, variable_id integer) left join master.variable v on v.id=f.variable_id order by f.id desc
		
	LOOP
	--raise notice 'ID: % - % - %', formula_rec.variable_id,formula_rec.abbrev,formula_rec.data_level;
		var_condition:= valid_keys;
		
        IF formula_rec.data_level='plot' then -- if resulting variable is plot and the parameter is entry
            
            if(var_data_level='entry') then -- the parameter is entry and the passed parameter data_level= 'entry' then we add filter for entry.key
                execute 'insert into temp_var2 (count_updated,variable_id) select count(1) , '||formula_rec.variable_id||' as variable_id 
                from 
                    operational_data_terminal.terminal t 
                        left join operational.plot p on p.key=t.key,
                    operational.entry e
                where 
                    transaction_id='|| var_transaction_id|| ' and 
                    variable_id= '||formula_rec.variable_id||' and
                    e.id=p.entry_id
                    and t.is_data_value_valid=true 
                    and t.is_data_type_valid=true 
                    and e.'||var_condition
        ;
                
            else --the parameter is entry and the passed parameter data_level= 'plot' it mean we filter the key by plot.key
            
                execute 'insert into temp_var2 (count_updated,variable_id) select count(1) , '||formula_rec.variable_id||' as variable_id 
                from 
                    operational_data_terminal.terminal t 
                        left join operational.plot p on p.key=t.key
                    
                where 
                    transaction_id='|| var_transaction_id|| ' and 
                    variable_id= '||formula_rec.variable_id||' and
                    t.is_data_value_valid=true and
                    t.is_data_type_valid=true and
                    p.'||var_condition;
            end if;
            
        ELSIF formula_rec.data_level='entry' then -- if resulting variable is entry and the parameter is plot

            if(var_data_level='entry') then -- the parameter is entry and the passed parameter data_level= 'entry' then we add filter for entry.key
                execute 'insert into temp_var2 (count_updated,variable_id) select count(1) , '||formula_rec.variable_id||' as variable_id 
                from 
                    operational_data_terminal.terminal t 
                        left join operational.entry e on e.key=t.key
                where 
                    transaction_id='|| var_transaction_id|| ' and 
                    variable_id= '||formula_rec.variable_id||' and
                    t.is_data_value_valid=true and
                    t.is_data_type_valid=true and
                    e.'||var_condition
        ;
                
            else --the parameter is entry and the passed parameter data_level= 'plot' it mean we filter the key by plot.key
                execute 'insert into temp_var2 (count_updated,variable_id) select count(1) , '||formula_rec.variable_id||' as variable_id 
                from 
                    operational_data_terminal.terminal t 
                        left join operational.entry e on e.key=t.key,
                        operational.plot p
                    
                where 
                    transaction_id='|| var_transaction_id|| ' and 
                    variable_id= '||formula_rec.variable_id||' and						
                    e.id=p.entry_id and 
                    t.is_data_value_valid=true and
                    t.is_data_type_valid=true and
                    e.'||var_condition;
            end if;

        ELSIF formula_rec.data_level='study' and (var_data_level='plot' or var_data_level='entry') then  
        -- if resulting variable is study and the parameter is plot or entry
            
            execute 'insert into temp_var2 (count_updated,variable_id) select count(1) , '||formula_rec.variable_id||' as variable_id 
                from 
                    operational_data_terminal.terminal t 
                        
                where 
                    transaction_id='|| var_transaction_id|| ' and 
                    t.is_suppress='||var_suppress_cond||' and
                    t.is_data_value_valid=true and
                    t.is_data_type_valid=true and
                    '
        ;
        
        END IF;

    END LOOP;

    return query select * from temp_var2 where temp_var2.count_updated <>0;
	--	return 'success';
end if;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
