﻿-- Function: operational_data_terminal.compute_formula(integer, integer, character varying, boolean, text)

-- DROP FUNCTION operational_data_terminal.compute_formula(integer, integer, character varying, boolean, text);

CREATE OR REPLACE FUNCTION operational_data_terminal.compute_formula2(
    var_study_id integer,
    var_transaction_id integer,
    var_abbrev character varying,
    var_is_suppress boolean,
    var_data_level text
    
    )
  RETURNS character varying AS
$BODY$
declare
    
    function_call text; -- holder of the stored procedure of the formula
    formula_rec RECORD; -- holder of the parameters of the related computed variable to be suppressed
    var_condition text; -- holder of the keys that are derived by the affected suppression by rules
    var_variable_id integer; -- holder of the variable record of variable_abbrev to be suppressed
    suppress_rec text; -- holder of the related computed variable records
    param_rec RECORD; -- holder of the parameters of the related computed variable
    
    schema_name text = 'operational'; -- schema of the operational data
    operational_unit text; -- the operational data table with schema {operational.plot_data,operational.plot_metadata,}
    column_name text; -- current data level, used for identifying the identifier of the operational data table {plot_id, study_id, entry_id}
    
    data_level_sql text; -- the sql used  to get the records in the operational data based of the data level of the formula parameters
    data_level text; -- the current data level in the data_level_array(stores all data level where to find the formula parameters)
    data_level_array text[]; -- array of all data levels to look for the variables {plot, study, entry}
    data_level_sql_array text[];    

    variable_type_suffix text; -- holder of the imploded variable_type_array
    variable_type_array text[]; -- type of formula parameter {observation,metadata}
    variable_id_array integer[]; -- formula parameter variable ID
    variable_col_array text[]; -- formula parameter for select column
    variable_abbrev_array text[]; -- formula parameter variable abbrev
    variable_abbrev_type_array text[];

    param_value_array text[];
    variable_id_text text;
    variable_col_text text;
    variable_abbrev_text text;
    param_value_text text;
    data_level_sql_text text;
    result_operational_unit text;   -- operational data table of the computed variable {plot_metadata, study_metadata, entry_data, ...}
    operational_unit_data text; -- operational data tables where to look for the parameter values of the formula {plot_metadata, study_metadata, entry_data, ...}
    row_id text; -- data unit id (plot_id,entry_id)
    result_unit_id text; -- data unit id of the computed variable (plot_id,entry_id)
    formula_sql text; -- holder of the retrieval of sql of the formula computation
    param_sql text;
    computed_variable_data_unit text; -- data unit of the computed variable {plot_data,plot_metadata,entry_data, entry_metadata, study_metadata}
begin

raise notice '%',var_is_suppress;
raise notice 'get suppress formula : %','
select 
    count, 
    data_levelx as data_level,
    variable_id,
    v.abbrev,
    condition 
from  
    operational_data_terminal.get_suppress_formula('||var_transaction_id||','''||var_abbrev||''','||var_is_suppress||')f 
        left join master.variable v on v.id=f.variable_id 
where 
    count>0';
-- get all plot keys or entry keys that are suppressed in the data terminal
for formula_rec in 
    select 
        count, 
        data_levelx as data_level,
        variable_id,
        v.abbrev,
        condition,
        type
    from  
        operational_data_terminal.get_suppress_formula(var_transaction_id,var_abbrev,var_is_suppress)f 
            left join master.variable v on v.id=f.variable_id 
    where 
        count>0
LOOP

    var_condition:=formula_rec.condition;
    data_level_array = array[]::text[];
    variable_type_array = array[]::text[];
    variable_id_array = array[]::text[];
    variable_col_array = array[]::text[];
    variable_abbrev_array = array[]::text[];
    variable_abbrev_type_array = array[]::text[];

    data_level_sql_array = array[]::text[];
    param_value_array = array[]::text[];
    
    -- loop through all parameters in the formula
    for param_rec in 
        select 
            v.abbrev,
            fp.param_variable_id,
            fp.data_level::text,
            v.data_type,
            v.type::text 
        from 
            master.formula_parameter fp 
                left join master.variable v on v.id=fp.param_variable_id 
        where 
            fp.result_variable_id=formula_rec.variable_id 
            and fp.is_void=false
    LOOP

        variable_col_array=array_append( variable_col_array, lower(param_rec.abbrev)||' '|| param_rec.data_type );
        variable_abbrev_array=array_append( variable_abbrev_array, lower(param_rec.abbrev));
        variable_type_array= array_append(variable_type_array,param_rec.type);
        variable_id_array= array_append(variable_id_array,param_rec.param_variable_id);
        
        if (position(';' in param_rec.data_level) > 0) then
            data_level_array = array_cat(data_level_array,string_to_array(param_rec.data_level, ';'));
        else
            data_level_array= array_append(data_level_array,param_rec.data_level); end if;
             
    END LOOP;		

    select array_to_string(array(select distinct unnest(variable_type_array)),',')a into variable_type_suffix; 
--raise notice 'variable_type_suffix: %',variable_type_suffix;
    -- implode formula parameter arrays
    SELECT array_to_string(variable_id_array,',')a  into variable_id_text; 
    SELECT array_to_string(variable_col_array,',')a  into variable_col_text;
    SELECT array_to_string(variable_abbrev_array,',')a  into variable_abbrev_text;
    
    -- hierarchy where to check the formula parameter data 1.) plot 2.) entry 3.) study
    select array(select data_level.a from (select distinct unnest(data_level_array) a )data_level  order by case when data_level.a='plot' then 1 when data_level.a='entry' then 2 else 3 end) into data_level_array;

    foreach data_level in array(data_level_array)
    LOOP
--raise notice 'data_level: %',data_level;
        if (variable_type_suffix = 'observation') then
            variable_type_suffix = '_data';
        elsif (variable_type_suffix = 'metadata') then
            variable_type_suffix = '_metadata';
        end if;

        operational_unit = quote_ident(schema_name) || '.' || quote_ident(data_level);
        column_name = data_level || '_id';
        
        result_operational_unit='';
        row_id = '';
        result_unit_id ='op.id ';

--raise notice 'formula_rec.data_level: %',formula_rec.data_level;
        
        IF formula_rec.data_level='plot' and data_level='entry' then
            result_operational_unit=' ,operational.plot opx';
            row_id = 'and opx.entry_id=op.id';
            result_unit_id ='opx.id';
        ELSIF formula_rec.data_level='entry' and data_level='plot' then
            result_operational_unit=' ,operational.entry opx';
            row_id = 'and opx.id=op.entry_id';
            result_unit_id ='opx.id';

        ELSIF formula_rec.data_level='study' and (data_level='plot' or data_level='entry')then
            
            result_unit_id ='op.study_id as id';

        ELSIF data_level='study' and (formula_rec.data_level='plot' or formula_rec.data_level='entry' ) then
                result_operational_unit=' ,operational.'||formula_rec.data_level||' opx';
                row_id = 'and opx.study_id='||var_study_id;
                result_unit_id ='opx.id';
        
        END IF;
        
        if data_level = 'study' then
            data_level_sql:= '
                select 
                    op.'||column_name||' as id,
                    op.variable_id,
                    op.value
                from 
                    operational.study_metadata op
                where 
                    op.study_id='||var_study_id||' 
                    and op.variable_id in (' || variable_id_text || ') 
                    and op.is_void=false 
                    and op.is_suppress = false';
            
            
        elsif (variable_type_suffix = 'metadata,observation' or  variable_type_suffix = 'observation,metadata') THEN
            
            data_level_sql:= '
                select  
                    '||result_unit_id||',
                    op.variable_id,
                    op.value
                from 
                    '||operational_unit||'_metadata op
                    '||result_operational_unit||'
                
                where 
                    op.study_id='||var_study_id||' 
                    and op.variable_id in (' || variable_id_text || ') 
                    and op.is_void=false 
                    and op.is_suppress = false
                
                UNION
                select 
                    '||result_unit_id||',
                    op.variable_id,
                    op.value
                from 
                    '||operational_unit||'_data op
                    '||result_operational_unit||'
                
                where 
                op.study_id='||var_study_id||' 
                and op.variable_id in (' || variable_id_text || ') 
                and op.is_void=false 
                and op.is_suppress = false';
        else
        
            operational_unit_data = quote_ident(schema_name) || '.' || quote_ident(data_level || variable_type_suffix);         
            data_level_sql:= '
                select 
                    op.'||column_name||' as id,
                    op.variable_id,
                    op.value
                from 
                    '||operational_unit_data||' op
                where 
                    op.study_id='||var_study_id||' 
                    and op.variable_id in (' || variable_id_text || ') 
                    and op.is_void=false 
                    and op.is_suppress = false
                ';

        end if;

        IF data_level='study' then
        
            data_level_sql_array=array_append(data_level_sql_array , '
                select
                    id,
                    variable_id,
                    (CASE
                        when terminal.is_suppress=true then null
                        when terminal.is_suppress=false and terminal.value is null then null
                        when terminal.value is not null then terminal.value
                        else operational.value
                    END)as value
                from 
                    (select
                        '||result_unit_id||',
                        t.variable_id,
                        t.value,
                        t.is_suppress
                    from
                        operational_data_terminal.terminal t 
                        '||result_operational_unit||'
                    where
                        t.transaction_id = ' || var_transaction_id|| '
                        and t.is_data_type_valid = true
                        and t.is_data_value_valid = true
                        and t.variable_id in (' || variable_id_text || ')
                        and t.data_unit = $$study_metadata$$
                        '||row_id||'
                    )terminal FULL OUTER JOIN(
                        '||data_level_sql||'
                    
                )operational USING(id,variable_id)
                ');
        else
            
            data_level_sql_text='
                select
                    id,
                    variable_id,
                    (CASE
                        when terminal.is_suppress=true then null
                        when terminal.is_suppress=false and terminal.value is null then null
                        when terminal.value is not null then terminal.value
                        else operational.value
                    END)as value
                from 
                    (select
                        '||result_unit_id||',
                        t.variable_id,
                        t.value,
                        t.is_suppress    
                    from
                        operational_data_terminal.terminal t left join
                        ' || operational_unit || ' op on op.key=t.key
                        '||result_operational_unit||'
                    where
                        t.transaction_id = ' || var_transaction_id|| '
                        and t.is_data_type_valid = true
                        and t.is_data_value_valid = true
                        and t.variable_id in (' || variable_id_text || ')                     
                        '||row_id||'

                    )terminal FULL OUTER JOIN(
                        '||data_level_sql||'
                    
                )operational USING(id,variable_id)
                ';
            data_level_sql_array=array_append(data_level_sql_array , data_level_sql_text);
        end if;

        param_value_array=array_append(param_value_array,data_level||'.value');

    END LOOP;

    SELECT array_to_string(param_value_array,',')a  into param_value_text;


    if array_length(data_level_sql_array, 1)>1 then 

        SELECT array_to_string(data_level_sql_array, ') # FULL OUTER JOIN(')a into data_level_sql_text;

        data_level_sql_text= '('||data_level_sql_text||') ';
        select string_to_array(data_level_sql_text, '#') into data_level_sql_array;
        
        FOR i IN 1..array_length(data_level_array, 1) LOOP
            select (data_level_sql_array)[i] into param_sql;

            if i=1 then
                data_level_sql_array=array_replace(data_level_sql_array, param_sql, param_sql||''||(data_level_array)[i]||'');
                
            else 
                data_level_sql_array=array_replace(data_level_sql_array,param_sql, param_sql||''||(data_level_array)[i]||' USING(id,variable_id) ');
                
            end if;
        end loop;
        
        SELECT array_to_string(data_level_sql_array, ' ') into data_level_sql_text;
        
        if(formula_rec.data_level ='entry') then
            data_level_sql_text='
            crosstab(''
                select
                    id ,
                    variable_id,
                    array_agg(COALESCE('|| param_value_text||')) as value
                from 
                    '|| data_level_sql_text ||'
                group by id,variable_id
                order by id

                '',''select unnest(array[' ||variable_id_text ||' ])'') as data ( id integer,'||variable_col_text ||')';

        else
            data_level_sql_text='
            crosstab(
                ''select
                    id ,
                    variable_id,
                    COALESCE('|| param_value_text||') as value
                from '|| data_level_sql_text ||'
                order by id

                '',''select unnest(array[' ||variable_id_text ||' ])'') as data ( id integer,'||variable_col_text ||')';
        end if;
    ELSE
        
        SELECT array_to_string(data_level_sql_array, ',') into data_level_sql_text;
        
        if  formula_rec.data_level ='entry' then 
            variable_abbrev_text='array_agg('||variable_abbrev_text||')'; 
        end if;

        data_level_sql_text= '
            crosstab('''||data_level_sql_text||'
            order by id
            '',''select unnest(array[' ||variable_id_text ||' ])'') as data ( id integer,'||variable_col_text ||')';

    end if;

    function_call := 'master.formula_' || lower(formula_rec.abbrev) || '(' || variable_abbrev_text ||')' ;

    IF formula_rec.type='observation' then 
        computed_variable_data_unit := formula_rec.data_level||'_data';
    ELSE
        computed_variable_data_unit := formula_rec.data_level||'_metadata';
    END IF;

    if formula_rec.data_level ='plot' then
        formula_sql:='
        select
            p.key ,
            ' ||function_call|| ' as value
        from 
            operational.plot p 
                left join '||data_level_sql_text||' on p.id=data.id and p.is_void=false
        where 
            p.'||var_condition || ' 
            and p.study_id='||var_study_id;
        
        execute 'UPDATE operational_data_terminal.terminal t
            SET is_committed_operational = TRUE
            FROM operational.plot e 
                left join operational.'||computed_variable_data_unit||' sm on e.id=sm.plot_id
            WHERE 
                t.data_unit = '''||computed_variable_data_unit||'''
                and sm.variable_id = t.variable_id
                and sm.is_void = false
                and t.key=e.key
                and t.'||var_condition || '
                and e.study_id = '||var_study_id ||'
                and t.transaction_id='|| var_transaction_id|| ' 
                and t.variable_id= '||formula_rec.variable_id;

    elsif  formula_rec.data_level ='entry' then
        formula_sql:='
        select
            p.key ,
            ' ||function_call|| ' as value
        from 
            operational.entry p 
                left join '||data_level_sql_text||' on p.id=data.id and p.is_void=false
        where 
            p.'||var_condition || '
            and p.study_id='||var_study_id;

        execute 'UPDATE operational_data_terminal.terminal t
            SET is_committed_operational = TRUE
            FROM operational.entry e 
                left join operational.'||computed_variable_data_unit||' sm on e.id=sm.entry_id
            WHERE 
                t.data_unit = '''||computed_variable_data_unit||'''
                and sm.variable_id = t.variable_id
                and sm.is_void = false
                and t.key=e.key
                and t.'||var_condition || '
                and e.study_id = '||var_study_id ||'
                and t.transaction_id='|| var_transaction_id|| ' 
                and t.variable_id= '||formula_rec.variable_id;
    else 

        execute 'UPDATE operational_data_terminal.terminal t
            SET is_committed_operational = TRUE
                FROM operational.study_metadata sm,
                operational.study s
            WHERE t.data_unit = ''study_metadata''
                and sm.variable_id = t.variable_id
                and sm.study_id = s.id
                and s.id = '||var_study_id ||'
                and sm.is_void=false
                and t.'||var_condition || '
                and t.transaction_id='|| var_transaction_id|| ' 
                and t.variable_id= '||formula_rec.variable_id;
    END IF;

    execute '
        with rows as(
            update operational_data_terminal.terminal t 
                set 
                is_void=(case when o.value is null or trim(o.value::text)=''''  then true else false end),
                value=(
                    case 
                        when o.value is null and t.value is not null then null
                        when o.value is not null and t.value is  null then o.value::text 
                        when o.value::text<>t.value then o.value::text 
                        else t.value 
                    end)
            from 
                ('||formula_sql||')o
                
            where 
                o.key=t.key 
                and transaction_id='|| var_transaction_id|| '
                and variable_id= '||formula_rec.variable_id|| '
            returning t.key
        ) select * from rows' into suppress_rec;

/*raise notice 'SQL: %','
with rows as(
    update operational_data_terminal.terminal t 
        set 
        is_void=(case when o.value is null or trim(o.value::text)=''''  then true else false end),
        value=(
            case 
                when o.value is null and t.value is not null then null
                when o.value is not null and t.value is  null then o.value::text 
                when o.value::text<>t.value then o.value::text 
                else t.value 
            end)
    from 
        ('||formula_sql||')o
        
    where 
        o.key=t.key 
        and transaction_id='|| var_transaction_id|| '
        and variable_id= '||formula_rec.variable_id|| '
    returning t.key
) select * from rows';
*/
    IF length(suppress_rec)>0  THEN
        IF var_is_suppress=true THEN
            INSERT INTO operational_data_terminal.transaction_filter(
                 variable_id, transaction_id, factor_variable_id, factor_variable_abbrev, 
                factor_variable_label, factor_data_type, conjunction, operator, 
                value, status, creator_id, creation_timestamp, remarks, 
                identifier_key, data_level)
            select 
                formula_rec.variable_id, 
                transaction_id, 
                factor_variable_id, 
                factor_variable_abbrev, 
                factor_variable_label, 
                factor_data_type, 
                conjunction, 
                operator, 
                value, 
                'complete', 
                creator_id, 
                now(), 
                remarks, 
                identifier_key, 
                formula_rec.data_level
            FROM 
                operational_data_terminal.transaction_filter 
            where
                transaction_id=var_transaction_id
                and variable_id=var_variable_id and status in ('complete','new');
        ELSE 
            DELETE from operational_data_terminal.transaction_filter WHERE
                transaction_id=var_transaction_id and variable_id=formula_rec.variable_id and 
                status in ('complete','new', 'all');

            execute 'DELETE from operational_data_terminal.transaction_filter t 
            WHERE
            transaction_id='||var_transaction_id||' and variable_id='||formula_rec.variable_id||' and 
            status in (''selected'') and t.identifier_key in ('||suppress_rec||')';
                
        END IF;
    END IF;
END LOOP;

select id into var_variable_id from master.variable where abbrev=var_abbrev;

-- set status of condition into 'complete'
if var_is_suppress THEN 
    UPDATE operational_data_terminal.transaction_filter 
    set 
        status='complete' 
    where 
        transaction_id=var_transaction_id
        and variable_id=var_variable_id
        and status in ('new');
ELSE 
    DELETE from operational_data_terminal.transaction_filter WHERE
    transaction_id=var_transaction_id and variable_id=var_variable_id and 
    status in ('complete','new', 'all');
    
END IF;

return 'success';
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;