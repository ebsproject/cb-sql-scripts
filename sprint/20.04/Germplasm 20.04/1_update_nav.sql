--create new application for germplasm
insert into platform.application (abbrev, label, action_label, icon, creator_id)
values ('GERMPLASM_CATALOG', 'Germplasm', 'Browse Germplasm', 'assignment', 1);

--map new application with action (url)
insert into platform.application_action (application_id, module, controller,creator_id)
select
	app.id,
	'germplasm',
	'default',
	1
from
	platform.application app
where
	app.abbrev = 'GERMPLASM_CATALOG';

--update navigation
UPDATE
	platform.space
SET
	menu_data = '{
    "left_menu_items": [
        {
            "name": "experiment-creation",
            "label": "Experiments",
            "appAbbrev": "EXPERIMENT_CREATION"
        },
        {
            "name": "operational-studies",
            "label": "Operational studies",
            "appAbbrev": "OPERATIONAL_STUDIES"
        },
        {
            "name": "tasks",
            "label": "Tasks",
            "appAbbrev": "TASKS"
        },
        {
            "name": "data-collection-qc",
            "items": [
                {
                    "name": "data-collection-qc-quality-control",
                    "label": "Quality control",
                    "appAbbrev": "QUALITY_CONTROL"
                }
            ],
            "label": "Data collection & QC"
        },
        {
            "name": "seeds",
            "items": [
                {
                    "name": "seeds-find-seeds",
                    "label": "Find seeds",
                    "appAbbrev": "FIND_SEEDS"
                },
                {
                    "name": "seeds-harvest-manager",
                    "label": "Harvest manager",
                    "appAbbrev": "HARVEST_MANAGER"
                }
            ],
            "label": "Seeds"
        }
    ],
    "main_menu_items": [
        {
            "icon": "folder_special",
            "name": "data-management",
            "items": [
                {
                    "name": "data-management_seasons",
                    "label": "Seasons",
                    "appAbbrev": "MANAGE_SEASONS"
                },
                {
                    "name": "data-management_germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                }
            ],
            "label": "Data management"
        },
        {
            "icon": "settings",
            "name": "administration",
            "items": [
                {
                    "name": "administration_users",
                    "label": "Users",
                    "appAbbrev": "USERS"
                }
            ],
            "label": "Administration"
        }
    ]
}'
WHERE abbrev = 'ADMIN';