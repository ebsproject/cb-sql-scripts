update platform.config
set
	config_value = '{
    "name": "Germplasm Query Fields",
    "fields": {
        "basic": [
            {
                "label": "Germplasm State",
				"fieldName": "germplasmState",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "input_type": "multiple",
                "description": "The state of the product",
                "input_field": "selection",
                "primary_key": "",
                "order_number": "1",
                "advanced_search": "false",
                "variable_abbrev": "GERMPLASM_STATE",
				"search_sort_order":"ASC",
				"search_sort_column":"value",
				"api_resource_method":"GET",
				"api_resource_endpoint":"variables?abbrev=GERMPLASM_STATE",
				"api_resource_output":"0->variableDbId",
				"secondary_resource_endpoint": "variables/{id}/scales",
				"secondary_resource_endpoint_method": "GET",
				"secondary_resource_replacements":{
					"{id}":"api_resource_output"
				},
				"secondary_resource_output":"0->scales->0->scaleValues->value"
            }
        ],
        "additional": [
            {
                "label": "Designation",
				"fieldName": "designation",
                "disabled": "false",
                "required": "false",
                "list_type": "designation|seed",
                "allow_filter_list": "true",
                "list_filter_field": "germplasm.designation",
                "input_type": "multiple",
                "description": "Designation of the product",
                "input_field": "input field",
                "primary_key": "",
                "order_number": "2",
                "advanced_search": "true",
                "variable_abbrev": "DESIGNATION",
				"search_sort_order":"ASC",
				"search_sort_column":"designation",
				"api_resource_method":"POST",
				"api_resource_endpoint":"germplasm-search",
				"api_resource_output":"designation",
				"secondary_resource_endpoint": "",
				"secondary_resource_endpoint_method": "",
				"secondary_resource_replacements":"",
				"secondary_resource_output":""
            },
            {
                "label": "Parentage",
				"fieldName": "parentage",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "input_type": "multiple",
                "description": "The parentage of the product",
                "input_field": "input field",
                "primary_key": "",
                "order_number": "3",
                "advanced_search": "true",
                "variable_abbrev": "PARENTAGE",
				"search_sort_column":"parentage",
				"api_resource_method":"POST",
				"api_resource_endpoint":"germplasm-search",
				"api_resource_output":"parentage",
				"secondary_resource_endpoint": "",
				"secondary_resource_endpoint_method": "",
				"secondary_resource_replacements":"",
				"secondary_resource_output":""
            },
            {
                "label": "Generation",
				"fieldName": "generation",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "input_type": "multiple",
                "description": "The generation of the product",
                "input_field": "selection",
                "primary_key": "",
                "order_number": "4",
                "advanced_search": "true",
                "variable_abbrev": "GENERATION",
				"search_sort_order":"ASC",
				"search_sort_column":"value",
				"api_resource_method":"GET",
				"api_resource_endpoint":"variables?abbrev=GENERATION",
				"api_resource_output":"0->variableDbId",
				"secondary_resource_endpoint": "variables/{id}/scales",
				"secondary_resource_endpoint_method": "GET",
				"secondary_resource_replacements":{
					"{id}":"api_resource_output"
				},
				"secondary_resource_output":"0->scales->0->scaleValues->value"
            },
            {
                "label": "Germplasm Name Type",
				"fieldName": "germplasmNameType",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "input_type": "multiple",
                "description": "Name type of the germplasm",
                "input_field": "selection",
                "primary_key": "",
                "order_number": "5",
                "advanced_search": "true",
                "variable_abbrev": "GERMPLASM_NAME_TYPE",
				"search_sort_order":"ASC",
				"search_sort_column":"value",
				"api_resource_method":"GET",
				"api_resource_endpoint":"variables?abbrev=GERMPLASM_NAME_TYPE",
				"api_resource_output":"0->variableDbId",
				"secondary_resource_endpoint": "variables/{id}/scales",
				"secondary_resource_endpoint_method": "GET",
				"secondary_resource_replacements":{
					"{id}":"api_resource_output"
				},
				"secondary_resource_output":"0->scales->0->scaleValues->value"
            },
            {
                "label": "Creator",
				"fieldName": "creator",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "input_type": "multiple",
                "description": "Name of the creator of the product",
                "input_field": "selection",
                "primary_key": "id",
                "order_number": "6",
                "advanced_search": "true",
                "variable_abbrev": "CREATOR",
				"search_sort_order":"ASC",
				"search_sort_column":"creator",
				"api_resource_method":"POST",
				"api_resource_endpoint":"germplasm-search",
				"api_resource_output":"creator",
				"api_resource_body":{
					"distinctOn":"creator",
					"fields":"creator.person_name as creator"
				},
				"secondary_resource_endpoint": "",
				"secondary_resource_endpoint_method": "",
				"secondary_resource_replacements":"",
				"secondary_resource_output":""
            },
            {
                "label": "Other Germplasm Names",
				"fieldName": "otherNames",
                "disabled": "false",
                "required": "false",
                "list_type": "designation|seed",
                "allow_filter_list": "true",
                "list_filter_field": "germplasm.designation",
                "input_type": "multiple",
                "description": "Other names of the germplasm",
                "input_field": "input field",
                "primary_key": "",
                "order_number": "7",
                "advanced_search": "true",
                "variable_abbrev": "OTHER_NAME",
				"search_sort_order":"ASC",
				"search_sort_column":"otherNames",
				"api_resource_method":"POST",
				"api_resource_endpoint":"germplasm-search",
				"api_resource_output":"otherNames",
				"secondary_resource_endpoint": "",
				"secondary_resource_endpoint_method": "",
				"secondary_resource_replacements":"",
				"secondary_resource_output":""
            }
        ]
    },
    "main_table": "germplasm.germplasm",
    "search_sort_order": "ASC",
    "search_sort_column": "id",
    "api_resource_method": "POST",
    "api_resource_endpoint": "germplasm-search",
    "main_table_primary_key": "germplasmDbId"
}'
where
	abbrev = 'GERMPLASM_QUERY_FIELDS'