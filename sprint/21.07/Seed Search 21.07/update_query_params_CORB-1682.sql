-- CORB-1682 SS - Searching for packages and seeds harvested from a nursery should return results in a reasonable amount of time
--------------------------------------------------------------------------------------
UPDATE platform.config
    SET config_value=$${
        "name": "Find Seeds Query Parameters",
        "main_table": "germplasm.package",
        "main_table_primary_key": "id",
        "api_resource_method": "POST",
        "api_resource_endpoint": "seed-packages-search",
        "search_sort_column": "id",
        "search_sort_order": "ASC",
        "input_field_type": [
            { 
            "id":"name", 
            "label":"GERMPLASM NAME", 
            "description":"Designation and Germplasm Names information",
            "validation":"string"
            },
            {
            "id":"seed", 
            "label":"SEED NAME", 
            "description":"Seed Name information",
            "validation":"string"
            },
            {
            "id":"label", 
            "label":"PACKAGE LABEL", 
            "description":"Package Label information",
            "validation":"string"
            }
        ],
        "values": [
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "programDbId",
                "basic_parameter": "true",
                "output_id_value": "id",
                "variable_abbrev": "PROGRAM",
                "reference_column": "programDbId",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "program_name",
                "api_resource_endpoint": "seed-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "program"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "experimentYear",
                "basic_parameter": "false",
                "output_id_value": "experiment_year",
                "variable_abbrev": "YEAR",
                "reference_column": "experimentYear",
                "search_sort_order": "DESC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "experiment_year",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "experiment"
            },
            {
                "disabled": "false",
                "required": "true",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "seasonDbId",
                "basic_parameter": "true",
                "output_id_value": "id",
                "variable_abbrev": "SEASON",
                "reference_column": "seasonDbId",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "season_name",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "season"
            },
            {
                "disabled": "false",
                "required": "true",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "experimentType",
                "basic_parameter": "true",
                "output_id_value": "experiment_type",
                "variable_abbrev": "EXPERIMENT_TYPE",
                "reference_column": "experimentDbId",
                "search_sort_order": "DESC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "experiment_type",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "experiment"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "stageDbId",
                "basic_parameter": "false",
                "output_id_value": "id",
                "variable_abbrev": "EVALUATION_STAGE",
                "reference_column": "stageDbId",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "stage_name",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "stage"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "experimentDbId",
                "basic_parameter": "false",
                "output_id_value": "id",
                "variable_abbrev": "EXPERIMENT_NAME",
                "reference_column": "experimentDbId",
                "search_sort_order": "DESC",
                "search_sort_column": "id",
                "api_resource_method": "POST",
                "output_display_value": "experiment_name",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "experiment"
            },
            {
                "disabled": "false",
                "required": "true",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "occurrenceDbId",
                "basic_parameter": "true",
                "output_id_value": "id",
                "variable_abbrev": "OCCURRENCE_NAME",
                "reference_column": "occurrenceDbId",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "occurrence_name",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "occurrence"
            },
            {
                "disabled": "false",
                "required": "true",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "facility_class: equals structure | facility_type: equals building,equals seed warehouse,equals farm house",
                "api_body_param": "facilityDbId",
                "basic_parameter": "true",
                "output_id_value": "id",
                "variable_abbrev": "FACILITY",
                "reference_column": "facility",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "facility_code",
                "api_resource_endpoint": "seed-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "facility"
            },
            {
                "disabled": "false",
                "required": "true",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "facility_class: not equals structure | facility_type: not equals building,not equals seed warehouse,not equals farm house",
                "api_body_param": "subFacilityDbId",
                "basic_parameter": "true",
                "output_id_value": "id",
                "variable_abbrev": "SUB_FACILITY",
                "reference_column": "subFacility",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "facility_name",
                "api_resource_endpoint": "seed-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "subFacility"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "locationDbId",
                "basic_parameter": "false",
                "variable_abbrev": "LOCATION",
                "reference_column": "locationDbId",
                "search_sort_order": "DESC",
                "search_sort_column": "id",
                "api_resource_method": "POST",
                "output_id_value": "id",
                "output_display_value": "location_code",
                "secondary_display_value": "location_name",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "location"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "locationDbId",
                "basic_parameter": "false",
                "variable_abbrev": "LOCATION_NAME",
                "reference_column": "locationDbId",
                "search_sort_order": "DESC",
                "search_sort_column": "id",
                "api_resource_method": "POST",
                "output_id_value": "id",
                "output_display_value": "location_name",
                "secondary_display_value": "location_code",
                "api_resource_endpoint": "experiment-packages-search",
                "secondary_resource_endpoint": "packages-search",
                "secondary_resource_endpoint_method": "POST",
                "target_table" : "location"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "input field",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "plotNumber",
                "basic_parameter": "false",
                "output_id_value": "id",
                "variable_abbrev": "PLOTNO",
                "reference_column": "sourcePlotDbId",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "plot_number",
                "api_resource_endpoint": "",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "location_occurrence_group"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "input field",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "plotCode",
                "basic_parameter": "false",
                "output_id_value": "id",
                "variable_abbrev": "PLOT_CODE",
                "reference_column": "",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "plot_code",
                "api_resource_endpoint": "",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "plot"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "input field",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "entryNumber",
                "basic_parameter": "false",
                "output_id_value": "id",
                "variable_abbrev": "ENTNO",
                "reference_column": "sourceEntryDbId",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "entry_number",
                "api_resource_endpoint": "",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "entry"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "input field",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "entryCode",
                "basic_parameter": "false",
                "output_id_value": "id",
                "variable_abbrev": "ENTCODE",
                "reference_column": "sourceEntryDbId",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "entry_code",
                "api_resource_endpoint": "",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "entry"
            },
            {
                "disabled": "true",
                "required": "false",
                "input_type": "single",
                "input_field": "input field",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "harvestDate",
                "basic_parameter": "false",
                "output_id_value": "",
                "variable_abbrev": "HVDATE_CONT",
                "reference_column": "harvestDate",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "",
                "output_display_value": "",
                "api_resource_endpoint": "",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "seed"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "single",
                "input_field": "selection",
                "default_value": "et:equals (=), gt:greater than (>), lt:less than (<), gte:greater than or equal to (>=), lte:less than or equal to (<=)",
                "allowed_values": "",
                "api_body_param": "quantity",
                "basic_parameter": "false",
                "output_id_value": "package_volume",
                "variable_abbrev": "VOLUME",
                "reference_column": "",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "package_volume",
                "api_resource_endpoint": "seed-packages-search",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "package"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "single",
                "input_field": "selection",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "unit",
                "basic_parameter": "false",
                "output_id_value": "package_unit",
                "variable_abbrev": "UNIT",
                "reference_column": "",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "package_unit",
                "api_resource_endpoint": "seed-packages-search",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "package"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "input field",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "replication",
                "basic_parameter": "false",
                "output_id_value": "id",
                "variable_abbrev": "REP",
                "reference_column": "",
                "search_sort_order": "ASC",
                "search_sort_column": "display_value",
                "api_resource_method": "POST",
                "output_display_value": "rep",
                "api_resource_endpoint": "",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : "plot"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "multiple",
                "input_field": "input field",
                "default_value": "",
                "allowed_values": "",
                "api_body_param": "names",
                "basic_parameter": "",
                "output_id_value": "id",
                "variable_abbrev": "NAME",
                "reference_column": "",
                "search_sort_order": "ASC",
                "search_sort_column": "",
                "api_resource_method": "",
                "output_display_value": "",
                "api_resource_endpoint": "",
                "secondary_resource_endpoint": "",
                "secondary_resource_endpoint_method": "",
                "target_table" : ""
            }
        ]
    }
    $$
WHERE abbrev = 'FIND_SEEDS_PARAMS';